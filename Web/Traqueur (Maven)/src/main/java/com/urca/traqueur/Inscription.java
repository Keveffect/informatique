/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.urca.traqueur;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;

/**
 *
 * @author kevch
 */
@WebServlet(urlPatterns = {"/Inscription"})
public class Inscription extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String username = request.getParameter("NomUtilisateur");
		String password = request.getParameter("MotDePasseUtilisateur");
		
		
		try {
	         DocumentBuilderFactory dbFactory =
	         DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.newDocument();
	         
	         // root element
	         Element rootElement = doc.createElement("Utilisateurs");
	         doc.appendChild(rootElement);

	         // User element
	         Element Utilisateur = doc.createElement("Utilisateur");
	         rootElement.appendChild(Utilisateur);

	         // User element
	         Element nom = doc.createElement("Nom");
	         nom.appendChild(doc.createTextNode(username));
	         Utilisateur.appendChild(nom);
	         
	         // User element
	         Element passwd = doc.createElement("Password");
	         passwd.appendChild(doc.createTextNode(password));
	         Utilisateur.appendChild(passwd);

	         // write the content into xml file
	         TransformerFactory transformerFactory = TransformerFactory.newInstance();
	         Transformer transformer = transformerFactory.newTransformer();
	         DOMSource source = new DOMSource(doc);
	         StreamResult result = new StreamResult(new File("C:\\Users\\kevch\\Desktop\\RT0805\\Projet 805\\Traqueur\\src\\main\\java\\com\\urca\\traqueur\\xml"));
	         transformer.transform(source, result);
	         
	         // Output to console for testing
	         
	         StreamResult consoleResult = new StreamResult(System.out);
	         transformer.transform(source, consoleResult);
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
        }

}
