in             ;a
store mem[0]
in 			   ;b
store mem[1]
cmp mem[0]		
jc 7			;echange les valeur si a>b
jmp 13			
load mem[0]
store mem[2]
load mem[1]
store mem[0]
load mem[2]
store mem[1]
in
store mem[2]	;valeur dont on cherche les multiples
mod mem[0]
cmp 0
jz 22			;si %=0 on affiche le multiple
load mem[0]
add 1
store mem[0]
jmp 15
out
load mem[0]
add 1			;incrementation
store mem[0]
jmp 15
end
