in ;a
store mem[0]
in ;b
store mem[1]
in ;op
store mem[2]
cmp 1        ;1 addition
jz 9
jmp 12
load mem[0]
add mem[1]
store mem[3]   
jmp 44        ;fin
load mem[2]
cmp 2         ;2 soustraction
jz 17
jmp 20
load mem[0]
sub mem[1]
store mem[3]    
jmp 44        ;fin
load mem[2]
cmp 3         ;3 multiplipcation
jz 25
jmp 28
load mem[0]
mul mem[1]
store mem[3]    
jmp 44         ;fin
load mem[2]
cmp 4          ;4 division
jz 33
jmp 36
load mem[0]
div mem[1] 
store mem[3]     
jmp 44         ;fin
load mem[2]
cmp 5           ;5 modulo
jz 41
jmp 44         ;fin
load mem[0]
mod mem[1]
store mem[3] 
load mem[2]
cmp 5
jc 52
cmp 0
jz 52
load mem[3]    ;on affiche le resultat normal
out
jmp 54         ;fin
load 0         ;si op >5 ou =0 on affiche 0
out
end