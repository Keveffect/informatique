inverse(pile,face).
inverse(face,pile).

iteration(P1,P2,P3,NP1,NP2,P3):- inverse(P1,NP1), inverse(P2, NP2).
iteration(P1,P2,P3,P1,NP2,NP3):- inverse(P3,NP3), inverse(P2, NP2).
iteration(P1,P2,P3,NP1,P2,NP3):- inverse(P1,NP1), inverse(P3, NP3).

jeu(P1,P2,P3,R,R,R):- iteration(P1,P2,P3,NP1,NP2,NP3), iteration(NP1,NP2,NP3,NNP1,NNP2,NNP3),iteration(NNP1,NNP2,NNP3,R,R,R),affcihePiece(P1,P2,P3),affcihePiece(P1,P2,P3),affcihePiece(NP1,NP2,NP3),affcihePiece(NNP1,NNP2,NNP3),affcihePiece(R,R,R).
affcihePiece(P1,P2,P3):- write(P1), write(' '), write(P2),write(' '),write(P3),nl.
