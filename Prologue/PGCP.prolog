pgcd(X,Y,R):- number(X), number(Y), X>1, Y>1, var(R), vpgcd(X,Y,R).
pgcd(X,Y,R):- number(X), number(Y), X>1, Y>1, number(R), vpgcd(X,Y,R).

vpgcd(X,X,X).

vpgcd(X,Y,R):- X<Y, pgcd(Y,X,R).

vpgcd(X,Y,Y):-X>Y,X mod Y =:= 0.

vpgcd(X,Y,R):-X>Y,Reste is X mod Y,Reste\==0,pgcd(Y,Reste,R).
