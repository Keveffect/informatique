affiche([], 0).
affiche([]|L).

compte2([], 0).
compte2([_|L],R):- compte2(L,T), R is T+1.

compte([],C,C).
compte([_|L],C,R):- C1 is C+1, compte(L, C1, R).
compte(L, R):- compte(L, 0, R).

somme([],0).
somme([X|L], R):- somme(L,S), R is S+X.

somme2([], 0).
somme2([X|L],R):- number(X),somme2(L,S), R is S+X.
somme2([X|L],R):- not(number(X)), somme2(L,R).
                  %\+number(X)

%appartient(_,[]):- fail.
appartient(X,[X|_]).
appartient(X,[Y|L]):- X \== Y, appartient(X,L).

