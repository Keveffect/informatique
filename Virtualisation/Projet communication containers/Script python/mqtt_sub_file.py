#!/usr/bin/python3

import paho.mqtt.client as mqtt
import json
import pika

# The callback for when the client receives a CONNACK response from the ser
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        client.subscribe("/home/mqttFile")
    else:
        print("Connection failed")

# The callback for when a PUBLISH message is received from the server
def on_message(clientMain, userdata, msg):
    channel.basic_publish(exchange='', routing_key='fileTampon', body=msg.payload)
    print(str(msg.payload))


credentials = pika.PlainCredentials('test', 'test')
parameters = pika.ConnectionParameters('100.100.100.5', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='fileTampon')

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("127.0.0.1")

# Blocking call that processes network traffic, dispatches callbacks and ha>
client.loop_forever()