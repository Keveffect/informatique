#!/usr/bin/env python
import pika, sys, os, redis, json

redis = redis.Redis(host='localhost', port='6379')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))    
channel = connection.channel()

channel.queue_declare(queue='fileTampon')

def fonction():
    i = 0
    def callback(ch, method, properties, body):
        nonlocal i
        msgjson = json.loads(body)
        redis.set("key"+str(i), body)
        i += 1
        print(" |_ Message recus et stoqué dans la base de données:\n %r" % body)

    channel.basic_consume(queue='fileTampon', on_message_callback=callback, auto_ack=True)
    print("/!\ En attente de message")
    channel.start_consuming()

fonction()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)