#!/usr/bin/python3

import paho.mqtt.client as mqtt
import time
import sys

mqtt_broker_address="100.100.100.6"
client = mqtt.Client("central")
client.connect(mqtt_broker_address)

mqtt_broker_address2="127.0.0.1"
client2 = mqtt.Client("central")
client2.connect(mqtt_broker_address2)

client.publish("/home/mqttFile", sys.argv[1])
print("Send "+ str(sys.argv[1]))

client.publish("/home/mqttFlask", "coucou")
print("Send "+ "coucou")