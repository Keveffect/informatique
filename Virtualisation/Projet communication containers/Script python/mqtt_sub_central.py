#!/usr/bin/python3

import paho.mqtt.client as mqtt
import json
import subprocess

# The callback for when the client receives a CONNACK response from the server
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        client.subscribe("/home/mqttCentral")
    else:
        print("Connection failed")

def affichage(msg):
    msgjson = json.loads(msg.payload)

    if msgjson['stationType'] == 5:
        typeVehicule = "Ordinnaire"
    elif msgjson['stationType'] == 10:
        typeVehicule = "Urgence"
    elif msgjson['stationType'] == 15:
        typeVehicule = "Operateur routier"

    if msgjson['cause code'] == 3:
        print("-- Travaux detecte --\nId vehicule : " + str(msgjson['stationId']) + "Type de vehicule : " + typeVehicule + "\nPosition : " + msgjson['position'] + "\n")
    elif msgjson['cause code'] == 4:
        print("-- Accident detecte --\nId vehicule : " + str(msgjson['stationId']) + "Type de vehicule : " + typeVehicule + "\nPosition : " + msgjson['position'] + "\n")
    elif msgjson['cause code'] == 5:
        print("-- Embouteillage detecte --\nId vehicule : " + str(msgjson['stationId']) + "Type de vehicule : " + typeVehicule + "\nPosition : " + msgjson['position'] + "\n")
    elif msgjson['cause code'] == 6:
        print("-- route glissante detecte --\nId vehicule : " + str(msgjson['stationId']) + "Type de vehicule : " + typeVehicule + "\nPosition : " + msgjson['position'] + "\n")
    elif msgjson['cause code'] == 7:
        print("-- brouillard detecte --\nId vehicule : " + str(msgjson['stationId']) + "Type de vehicule : " + typeVehicule + "\nPosition : " + msgjson['position'] + "\n")

# The callback for when a PUBLISH message is received from the server
def on_message(client, userdata, msg):
    affichage(msg)
    subprocess.call(['./mqtt_pub.py', msg.payload])

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("127.0.0.1")

# Blocking call that processes network traffic, dispatches callbacks and handles re
client.loop_forever()