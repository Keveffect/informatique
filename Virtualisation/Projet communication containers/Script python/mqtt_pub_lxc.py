#!/usr/bin/python3

import paho.mqtt.client as mqtt
import threading, time, json, random


def truck(i):
    print('--- Worker START')

    file = open('scenario'+ str(i) + '.json', "r")
    for line in file:
        msgjson = json.loads(line)
        if "vitesse" in msgjson :
            if msgjson['vitesse'] >= 90 :
                time.sleep(1)
            else:
                time.sleep(10)

        val = random.randint(0, 100)
        if val <95 :
            client.publish("/home/mqttlxc", line)
            print(str(line))
        else:
            print("--- Message perdu ---")
    file.close()

    print('--- Worker END')
    return

mqtt_broker_address="192.168.252.2"
client = mqtt.Client("voiture")
client.connect(mqtt_broker_address)

threads = []
for i in range(4):
    t = threading.Thread(target=truck, args=(i+1,))
    threads.append(t)
    t.start()
    print('Thread') 