#!/usr/bin/python3

import paho.mqtt.client as mqtt
import time
import sys

mqtt_broker_address="100.100.100.4"
client = mqtt.Client("Passerelle")
client.connect(mqtt_broker_address)

client.publish("/home/mqttCentral", sys.argv[1])
print("Send "+ str(sys.argv[1]))
time.sleep(1)