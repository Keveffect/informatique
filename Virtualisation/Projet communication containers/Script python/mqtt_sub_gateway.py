#!/usr/bin/python3

import paho.mqtt.client as mqtt
import json
import subprocess

global keyAccident
global valAccident
global keyVitesse
global valVitesse

# The callback for when the client receives a CONNACK response from
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        client.subscribe("/home/mqttlxc")
    else:
        print("Connection failed")

def gestionCauseCode(msgjson, msg):
    causeCode = msgjson['cause code']
    if causeCode == 3 or causeCode == 5 or causeCode == 6 or causeCode == 7:
        subprocess.call(['home/script/mqtt_pub.py', msg.payload])
    elif causeCode == 4:
        if msgjson['position'] in valAccident and keyAccident[valAccident.index(msgjson['position'])] != msgjson['stationId']  :
            subprocess.call(['home/script/mqtt_pub.py', msg.payload])
            keyAccident.remove(keyAccident[valAccident.index(msgjson['position'])])
            valAccident.remove(msgjson['position'])
        else :
            valAccident.append(msgjson['position'])
            keyAccident.append(msgjson['stationId'])
    else:
        print("Message inconnu non transmi au Centralisateur")

def controleVitesse(msgjson, msg):
    if msgjson['vitesse'] < 90 :
        if msgjson['position'] in valVitesse and keyVitesse[valVitesse.index(msgjson['position'])] != msgjson['stationId']  :
            if valVitesse.count(msgjson['position']) >= 2 :
                messagedenm = {
                    "stationId": msgjson['stationId'],
                    "stationType" : msgjson['stationType'],
                    "cause code" : 5,
                    "position" : msgjson['position']
                }
                jsondenm = json.dumps(messagedenm, indent = 4)
                subprocess.call(['home/script/mqtt_pub.py', jsondenm])
                keyVitesse.remove(keyVitesse[valVitesse.index(msgjson['position'])])
                valVitesse.remove(msgjson['position'])
                keyVitesse.remove(keyVitesse[valVitesse.index(msgjson['position'])])
                valVitesse.remove(msgjson['position'])
            else:
                valVitesse.append(msgjson['position'])
                keyVitesse.append(msgjson['stationId'])
        else :
        # elif keyVitesse[valVitesse.index(msgjson['position'])] != msgjson['stationId'] :
            valVitesse.append(msgjson['position'])
            keyVitesse.append(msgjson['stationId'])
    #elif msgjson['stationId'] in keyVitesse :
        #valVitesse.remove(keyVitesse[valVitesse.index(msgjson['position'])])
        #keyVitesse.remove(msgjson['stationId'])

# The callback for when a PUBLISH message is received from the server
def on_message(client, userdata, msg):
    #print(msg.topic+" "+str(msg.payload))
    msgjson = json.loads(msg.payload)
    if "cause code" in msgjson :    # message DENM
        gestionCauseCode(msgjson, msg)
    else:                           # message CAM
        controleVitesse(msgjson, msg)

keyAccident = []
valAccident = []
keyVitesse = []
valVitesse = []
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("127.0.0.1", 1883, 60)

# Blocking call that processes network traffic, dispatches callback
client.loop_forever()