   
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        @foreach($classe as $c)
           {{$c->nom}}
    </x-slot>
         @endforeach
        <div class="max-w-6xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="block mb-8">
                <a href="{{ route('classes.index') }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour</a>
                @if(Auth::user()->role=='prof')
                    <button data-toggle="modal" data-target="#infos" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-2 rounded">?</button>
                    <div class="modal" id="infos">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Informations</h4>
                                    <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                    </button>            
                                </div>
                                <div class="modal-body">
                                    Ici vous accedez à la liste des etudiants de cette classe. 
                                    Vous pouvez en ajouter des etudiants en les cherchants ou en entrant leur mail etudiant.
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="container">
                <div class="row">
                    <div class="col">
                            <form method="post" action="{{route('users.classe.store',['id'=>$c->id])}}" enctype="multipart/form-data">
                                    @csrf
                                        <label><strong>Liste etudiants:</strong></label><br/>
                                        @if($user_all->isEmpty())
                                            Il n'y a pas d'etudiant
                                        @else
                                            <select class="selectpicker" multiple data-live-search="true" name="etudiant[]">
                                            @foreach ($user_all as $u)
                                                {{$val=$u->id}}
                                                @if ($u->role == 'eleve')
                                                    <option value="{{$val}}">{{$u->email}}</option>
                                                @endif
                                            @endforeach
                                        
                                        </select>
                                        <!-- Send button -->
                                        <button class="btn btn-primary" type="submit">Ajouter</button> 
                                    @endif         
                        </form>
                    </div>
                </div>
            </div> 
            @if (Session::has('message'))
                <div class="alert alert-info color red" style="color: #d11a1a;">vous essayez d'ajouter un etudiant qui n'existe pas !</div>           
            @endif
        <br>
            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200 w-full">
                                <thead>
                                    <tr>
                                        <th scope="col" width="50" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            NOM
                                        </th>
                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Mail
                                        </th>
                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Gestion
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                    @if($users->isEmpty())
                                        <tr>
                                            <td></td>
                                            <td>
                                                Il n'y a encore auccun étudiant dans cette classe, 
                                                vous devez en ajouter pour qu'ils repondent aux exercices
                                            </td>
                                        </tr>
                                    @else
                                        @foreach ($users as $user)
                                            <tr>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                    {{ $user->name }}
                                                </td>

                                                <td class=" text-sm text-gray-900">
                                                    {{ $user->email }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                                    <form class="inline-block" action="{{ route('users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('Vous etes sur ?');">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="submit" class="text-red-600 hover:text-red-900 mb-2 mr-2" value="Supprimer">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                {{ $users->links() }}
            </footer>
        </div>
    <style>
        select, .is-info {
            margin: 0.3em;
        }
    </style>
</x-app-layout>