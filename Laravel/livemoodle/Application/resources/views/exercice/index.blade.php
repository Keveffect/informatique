<x-app-layout>
    <x-slot name="header">
         @foreach ($matiere as $m)
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Matiere : {{$m->nom}}<br>
            
        </h2>
    </x-slot>

    <div>
        <div class="max-w-6xl mx-auto py-10 sm:px-6 lg:px-8">
            <h2 class="block mb-8 font-semibold text-xl text-gray-800 leading-tight">
           Exercices
            
        </h2>
            <div class="block mb-8">
                @if(Auth::user()->role=='prof')
                    <a href="{{ route('matieres.exercices.create',$m) }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Ajouter exercice</a>
                @endif
                <a href="{{ route('matieres.index') }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour</a>
                @if(Auth::user()->role=='prof')
                    <button data-toggle="modal" data-target="#infos" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-2 rounded">?</button>
                    <div class="modal" id="infos">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Informations</h4>
                                    <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                    </button>            
                                </div>
                                <div class="modal-body">
                                    Cliquez sur le nom de l'exercice pour acceder à la creation des questions.
                                    En cliquant sur consulter la liste vous accedez à la liste des classes participants à cette matiere.
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            
            @endforeach
            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200 w-full">
                                <thead>
                                <tr>
                                    <th scope="col" width="50" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        NOM
                                    </th>
                                    @if(Auth::user()->role=='prof')
                                    <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Visibilité
                                    </th>
                                    <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Gestion
                                    </th>
                                    <th scope="col" width="200" class="px-6 py-3 bg-gray-50">

                                    </th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                    @if(Auth::user()->role=='eleve') 
                                        @if($exercices->isEmpty())
                                            <tr>
                                                <td></td>
                                                <td>
                                                    Il n'y a pas d'exercice dans cette matiere ou alors vous n'y avez pas accés
                                                </td>
                                            </tr>
                                        @else
                                            @foreach ($exercices as $exercice)
                                                @if($exercice->etat == true)
                                                    <tr>
                                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                            <a href="{{ route('exercices.users.questions.index',[ $exercice, $user = Auth::user()]) }}" class="text-blue-600 hover:text-blue-900 mb-2 mr-2">{{$exercice->nom}}</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif

                                    @if(Auth::user()->role=='prof') 
                                        @if($exercices->isEmpty())
                                            <tr>
                                                <td></td>
                                                <td>
                                                    Il n'y a encore auccun exercice, vous devez les creer pour les rendre accessible aux etudiants
                                                </td>
                                            </tr>
                                        @else
                                            @foreach ($exercices as $exercice)
                                            <input type="hidden" id="exercice_id" name="exercice_id" value="{{$exercice->id}}">
                                                <tr>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                        <a href="{{ route('exercices.users.questions.index',[ $exercice, $user = Auth::user()]) }}" class="text-blue-600 hover:text-blue-900 mb-2 mr-2">{{$exercice->nom}}</a>
                                                    </td>

                                                    @if($exercice->etat==true)
                                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                                        
                                                    <input type="button" class="btn btn-primary" id="{{$exercice->id}}" value="Desactiver">
                                                
                                                        </td>
                                                    @else
                                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                            
                                                    <input type="button" class="btn btn-primary" id="{{$exercice->id}}" value="Activer">
                                                
                                                        </td>
                                                    @endif

                                                    </td>
                                                        @foreach($matiere as $m)
                                                            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                                            <a href="{{ route('listeclasse', [$m->id, $exercice->id]) }}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">Liste des reponses</a>

                                                            <a href="{{route ('exercices.edit', $exercice)}}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">Modifier</a>
                                                        
                                                            <form class="inline-block" action="{{route('matieres.exercices.destroy',[$m,$exercice])}}" method="POST" onsubmit="return confirm('Vous etes sur ?');">
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="submit" class="text-red-600 hover:text-red-900 mb-2 mr-2" value="Supprimer">
                                                            </form>
                                                        @endforeach
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endif    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
            {{ $exercices->links() }}
        </footer>
        
        <script>
            function changeBouton(exercice){
                requete = $.ajax({
                    "type" : "GET",
                    "url" : "{{ url('/exercices/exercice') }}",
                    "data" : {id:exercice}
                    });
                    requete.done(function(response, textStatus, jqXHR){
                    if(response['etat']==0){
                        $("#"+response['id']).val("Activer");
                    }else if (response['etat']==1){
                        $("#"+response['id']).val("Desactiver");
                    }
                });
            }

            function etat(exercice){
                $('#'+exercice).click( function() {
                    requete = $.ajax({
                        "type" : "GET",
                        "url" : "{{ url('/exercices/exercice') }}",
                        "data" : {id:exercice}
                    });
                    requete.done(function(response, textStatus, jqXHR){
                        if(response['etat']==0){
                            etat = 1;
                            $("#"+response['id']).val("Desactiver");
                        }else if (response['etat']==1){
                            etat = 0; 
                            $("#"+response['id']).val("Activer");
                        }
                        
                        $.ajax({
                            url : "/updateEtat",
                            type : 'put',
                            data : {
                                _token : '{{ csrf_token() }}',
                                etat : etat, id:response['id']
                            }
                        });
                    });
                });
            }


            $(document).ready(function (){
                $('input[name=exercice_id]').each(function(){
                    var mydata = $(this).val();
                    changeBouton(mydata);
                    etat(mydata);
                });
            });
        </script>
        </div>
    </div>
</x-app-layout>

