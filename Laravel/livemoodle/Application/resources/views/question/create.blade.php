<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
           Creation question
        </h2>
    </x-slot>

    <div>
        <div class="max-w-4xl mx-auto py-10 sm:px-6 lg:px-8">
            <a href="{{ route('exercices.users.questions.index', [$exercice, $user = Auth::user()]) }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour</a>
            <div class="mt-5 md:mt-0 md:col-span-2">
                <form method="post" action="{{ route('exercices.users.questions.store',[$exercice,$user = Auth::user()]) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="shadow overflow-hidden sm:rounded-md">
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="enonce" class="block font-medium text-sm text-gray-700">Enoncé</label>
                            <input type="text" name="enonce" id="enonce" class="form-input rounded-md shadow-sm mt-1 block w-full"/>
                            @error('enonce')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                    
                        <input id="exercice_id" type="hidden" name="exercice_id" value="{{ $exercice->id }}" />

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <button type="button" onclick="addImage()" class="btn btn-outline-info">Ajouter une image</button>
                            <div id="divImage" class="rounded-md shadow-sm mt-1 block w-full" style="display: none;">
                                <div class="px-4 py-1 bg-gray sm:p-6 ">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <input type="file" id="image_question" name="image_question" accept="image/*"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" onclick="addTable()" class="btn btn-outline-info">Ajouter un tableau</button>
                            <div id="divTable" class="rounded-md shadow-sm mt-1 block w-full" style="display: none;">
                                <div class="px-4 py-1 bg-gray sm:p-6 ">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="nb_ligne" class="block font-medium text-sm text-gray-700">Nombre de lignes</label>
                                            <input type="number" name="nb_ligne" id="nb_ligne" class="form-control rounded-md shadow-sm mt-1" value="0" max="10" min="0"/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="nb_colonne" class="block font-medium text-sm text-gray-700">Nombre de colonnes</label>
                                            <input type="number" name="nb_colonne" id="nb_colonne" class="form-control rounded-md shadow-sm mt-1" value="0" max="10" min="0"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                 
                        <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                Creer
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>

        function addTable() {
            var x = document.getElementById("divTable");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function addImage() {
            var x = document.getElementById("divImage");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
</x-app-layout>
