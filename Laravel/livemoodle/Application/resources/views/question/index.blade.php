<x-app-layout>
    <x-slot name="header">
        <?php $exercice = $exercice[0]; ?>
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Exercice :  {{$exercice->nom}}<br>
            <input type="hidden" id="exercice_id" value="{{$exercice->id}}">
            <input type="hidden" id="exercice_etat" value="{{$exercice->etat}}">
            <input type="hidden" id="matiere_slug" value="{{$matiere->slug}}">
     	</h2>
    </x-slot>

    <div class="max-w-6xl mx-auto py-10 sm:px-6 lg:px-8">
        <h2 class="block mb-8 font-semibold text-xl text-gray-800 leading-tight">
            @if(Auth::user()->role == 'prof' & Auth::user()->id != $user->id)
                Reponse De {{ $user->name }}
            @else
                Questions
            @endif
            </br>
            @if($exercice->etat == true)
                Temps restant : <span id="minutes"></span> minutes
            @endif
            @if(Auth::user()->role == 'prof')
            	<input type="text" id="ajouter" value="">
            	<input id="{{$exercice->id}}" type="button" class="btn btn-primary" value="+" onclick="plus(<?php echo $exercice->id ?>);">
                <a href="{{ route('stop', [$exercice->id, $matiere->slug]) }}" type="button" class="btn btn-primary" style="background-color: red; border-color:red ;" value="STOP">STOP</a>
            @endif
        </h2>

        <div class="block mb-8">
            @if(Auth::user()->role == 'prof' & Auth::user()->id != $user->id)
                <a href="{{ route('exercices.classes.show', [$exercice->id, $user->classe_id]) }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour à la liste d'étudiants</a>
                <a href="{{ route('matieres.exercices.index', $matiere->slug ) }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour à la liste des exercices</a>
            @elseif(Auth::user()->role == 'prof' & Auth::user()->id == $user->id)
                <a href="{{ route('exercices.users.questions.create',[$exercice,Auth::user()]) }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Ajouter question</a>
                <a href="{{route ('matieres.exercices.index', $matiere->slug) }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour</a>
                <button data-toggle="modal" data-target="#infos" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-2 rounded">?</button>
                <div class="modal" id="infos">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Informations</h4>
                                <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                                </button>            
                            </div>
                            <div class="modal-body">
                                Sur cette page vous pouvez creer des questions pour votre exercice.
                                Inserez un enoncé pour la question puis choisissez s'il sagit d'une reponse avec un tableau et dans ce cas indiquez sa dimension.
                                Vous pouvez inserer une image ou un gif dans votre enoncé.
                                Pendant l'exercice vous pouvez ajouter du temps en minutes ou choisir de terminer l'exercice.
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <a href="{{route ('matieres.exercices.index', $matiere->slug) }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour</a>
            @endif
        </div>

        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">              
                <?php $k = 1?>
                @if($questions->isEmpty())
                    <tr>
                        <td></td>
                        <td>
                            Il n'y a pas de question dans cet exercice
                        </td>
                    </tr>
                @else
                    @foreach ($questions as $q)
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-black-200 w-full">
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        <thead>
                                            <tr>
                                                <th scope="col" width="50" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                questions {{$k}} :
                                                </th>
                                                <?php $k = $k + 1;?>
                                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500  tracking-wider">
                                                    @if(Auth::user()->role=='prof')
                                                        <a href="{{route ('questions.edit', $q)}}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">Modifier</a>
                                                        <form class="inline-block" action="{{route('exercices.users.questions.destroy',[$exercice,$user_prof = Auth::user(),$q->id])}}" method="POST" onsubmit="return confirm('Vous etes sur ?');">
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <input type="submit" class="text-red-600 hover:text-red-900 mb-2 mr-2" value="Supprimer">
                                                        </form>
                                                    @endif
                                                </th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Enonce : 
                                            </th>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                {{ $q->enonce }}
                                            </td>
                                        </tr>
                                        
                                        @if(Auth::user()->role=='eleve')
                                            @if (isset($tableau_enonce))
                                                @foreach ($tableau_enonce as $tab)
                                                    @if ($tab->question_id == $q->id)
                                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                            Tableau Enoncé : 
                                                        </th>
                                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                            <?php $arr_decode = json_decode($tab->tab, true); ?>
                                                            <input type="hidden" value="{{$tab->id}}" name="tableau_id">
                                                            <table class="table" style="border:1px solid black">
                                                                <tbody>
                                                                    @for ($i = 0; $i < $tab->nb_lignes; $i++)
                                                                        <tr>
                                                                            @for ($j = 0; $j < $tab->nb_colonnes; $j++)
                                                                                @if ($j == 0 || $i == 0)
                                                                                    <th style="border:1px solid black" contenteditable="false" scope="col"  id="{{$tab->id}}_tab{{$i}}{{$j}}"></th>
                                                                                @else
                                                                                    <td style="border:1px solid black" contenteditable="false" id="{{$tab->id}}_tab{{$i}}{{$j}}" ></td>
                                                                                @endif
                                                                                
                                                                            @endfor
                                                                        </tr>
                                                                    @endfor
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    @endif
                                                @endforeach
                                            @endif
                                        @else <!-- role prof -->
                                            @if($table_pivot_tableau_user->isEmpty())   
                                                @foreach ($tableaux as $tab)
                                                    @if ($tab->question_id == $q->id)
                                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                            Tableau Enoncé : 
                                                        </th>
                                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                            <?php $arr_decode = json_decode($tab->tab, true); ?>
                                                            <input type="hidden" value="{{$tab->id}}" name="tableau_id">
                                                            <table class="table" style="border:1px solid black">
                                                                <tbody>
                                                                    @for ($i = 0; $i < $tab->nb_lignes; $i++)
                                                                        <tr>
                                                                            @for ($j = 0; $j < $tab->nb_colonnes; $j++)
                                                                                @if ($j == 0 || $i == 0)
                                                                                    <th style="border:1px solid black" contenteditable="false" scope="col"  id="{{$tab->id}}_tab{{$i}}{{$j}}"></th>
                                                                                @else
                                                                                    <td style="border:1px solid black" contenteditable="false" id="{{$tab->id}}_tab{{$i}}{{$j}}" ></td>
                                                                                @endif
                                                                                
                                                                            @endfor
                                                                        </tr>
                                                                    @endfor
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    @endif
                                                @endforeach
                                                @if(Auth::user()->role == 'prof' & Auth::user()->id != $user->id)
                                                    <tr>
                                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                            Tableau Reponse Etudiant : 
                                                        </th>
                                                        <td>
                                                            Personne n'a commencé l'exercice
                                                        </td>
                                                    </tr>
                                                @endif
                                            @else
                                                @if (isset($tableau_enonce))
                                                <tr> 
                                                    @foreach ($tableau_enonce as $tab)
                                                        @if ($tab->question_id == $q->id)
                                                            <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                Tableau Enoncé : 
                                                            </th>
                                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                                <?php $arr_decode = json_decode($tab->tab, true); ?>
                                                                <input type="hidden" value="{{$tab->id}}" name="tableau_id">
                                                                <table class="table" style="border:1px solid black">
                                                                    <tbody>
                                                                        @for ($i = 0; $i < $tab->nb_lignes; $i++)
                                                                            <tr>
                                                                                @for ($j = 0; $j < $tab->nb_colonnes; $j++)
                                                                                    @if ($j == 0 || $i == 0)
                                                                                        <th style="border:1px solid black" contenteditable="false" scope="col"  id="{{$tab->id}}_tab{{$i}}{{$j}}"></th>
                                                                                    @else
                                                                                        <td style="border:1px solid black" contenteditable="false" id="{{$tab->id}}_tab{{$i}}{{$j}}" ></td>
                                                                                    @endif
                                                                                @endfor
                                                                            </tr>
                                                                        @endfor
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            @if(Auth::user()->role == 'prof' & Auth::user()->id != $user->id)
                                                                <!-- Tableau reponse de l'etudiant -->
                                                                <?php $bouleen_exercice_commence = false ?>
                                                                @foreach ($tableaux as $tab_reponse_etudiant)
                                                                    @foreach ($table_pivot_tableau_user as $pivot)
                                                                        @if ($tab_reponse_etudiant->question_id == $q->id && $pivot->user_id == $user->id && $pivot->tableau_id == $tab_reponse_etudiant->id)
                                                                            <?php $bouleen_exercice_commence = true ?>
                                                                            <tr>
                                                                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                                    Tableau Reponse Etudiant : 
                                                                                </th>
                                                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                                                    <?php $arr_decode = json_decode($tab_reponse_etudiant->tab, true); ?>
                                                                                    <input type="hidden" value="{{$tab_reponse_etudiant->id}}" name="tableau_id">
                                                                                    <table class="table table-bordered" id="main" style="border:1px solid black">
                                                                                        <tbody>
                                                                                            @for ($i = 0; $i < $tab_reponse_etudiant->nb_lignes; $i++)
                                                                                                <tr>
                                                                                                    @for ($j = 0; $j < $tab_reponse_etudiant->nb_colonnes; $j++)
                                                                                                        @if ($j == 0 || $i == 0)
                                                                                                            <th style="border:1px solid black" scope="col"  id="{{$tab_reponse_etudiant->id}}_tab{{$i}}{{$j}}"></th>
                                                                                                        @else
                                                                                                            <td style="border:1px solid black" id="{{$tab_reponse_etudiant->id}}_tab{{$i}}{{$j}}" ></td>
                                                                                                        @endif
                                                                                                    @endfor
                                                                                                </tr>
                                                                                            @endfor
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                                @if($bouleen_exercice_commence == false)
                                                                    <tr>
                                                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            Tableau Reponse Etudiant : 
                                                                        </th>
                                                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                                            L'étudiant n'a pas encore commencé son travail
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endif

                                        <!-- Tableau Reponse -->
                                        @if(Auth::user()->role=='eleve')
                                            <tr>
                                                @if (isset($tableaux))
                                                    <?php $i = 0 ?>
                                                    @foreach ($tableaux as $tab)
                                                        @foreach ($table_pivot_tableau_user as $pivot)
                                                            @if ($tab->question_id == $q->id && $pivot->user_id == $user->id && $pivot->tableau_id == $tab->id)
                                                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                    Tableau Reponse : 
                                                                </th>
                                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                                    <?php $arr_decode = json_decode($tab->tab, true); ?>
                                                                    <input type="hidden" value="{{$tab->id}}" name="tableau_id">
                                                                    <table class="table table-bordered" id="main" style="border:1px solid black">
                                                                        <tbody>
                                                                            @for ($i = 0; $i < $tab->nb_lignes; $i++)
                                                                                <tr>
                                                                                    @for ($j = 0; $j < $tab->nb_colonnes; $j++)
                                                                                        @if ($j == 0 || $i == 0)
                                                                                            <th style="border:1px solid black" contenteditable="true" scope="col"  id="{{$tab->id}}_tab{{$i}}{{$j}}_update"></th>
                                                                                        @else
                                                                                            <td style="border:1px solid black" contenteditable="true" id="{{$tab->id}}_tab{{$i}}{{$j}}_update" ></td>
                                                                                        @endif
                                                                                    @endfor
                                                                                </tr>
                                                                            @endfor
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                                                                        <div class="collapse" id="div1">
                                                                            <p class="card-text"> Mis a jour </p>
                                                                        </div>
                                                                        <button data-toggle="collapse" href="#div1" onclick="updateTab(<?php echo $tab->id ?>); disparait();" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150" value="{{$tab->id}}">
                                                                            Modifier
                                                                        </button>
                                                                    </div>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            </tr>
                                        @endif

                                        @if(isset($q->image_question))
                                            <tr>
                                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Image 
                                                </th>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                    <img src="{{asset('/storage/'.$q->image_question)}}"/>
                                                </td> 
                                            </tr>
                                        @endif
                                        @if ((Auth::user()->role=='prof' && Auth::user()->id != $user->id ) || (Auth::user()->role=='eleve' && Auth::user()->id == $user->id ))
                                            <tr>  
                                                <th scope="col" width="50" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Reponse
                                                </th>
                                                <td scope="col" width="50" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                </td>

                                            </tr>
                                            <tr>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                </td> 
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                    @if(Auth::user()->role=='eleve')
                                                        @foreach($reponses_user as $r)
                                                            <!-- on recupere la reponse correspondant exactement a cette utilisateur -->
                                                            @if($r[0]->question_id == $q->id )
                                                                <input type="hidden" value="{{$r[0]->id}}" name="reponse_id">   
                                                                <textarea id="{{$r[0]->id}}" class="txtarea form-input rounded-md shadow-sm mt-1 block w-full"></textarea>
                                                                <form action="{{ route('upload.post.image') }}" method="POST" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">  
                                                                        <div class="col-md-6 px-6 py-3">
                                                                            <input type="hidden" value="{{$r[0]->id}}" name="reponse_id"/>
                                                                            <input type="hidden" value="{{Auth::user()->id}}" name="user_id"/>
                                                                            <input type="hidden" value="{{$r[0]->question_id}}" name="question_id"/>
                                                                            @if(!empty($img))
                                                                                <input type="hidden" value="{{$img->path}}" name="path"/>
                                                                            @else
                                                                                <input type="hidden" value="" name="path"/>
                                                                            @endif
                                                                                <input type="file" name="fichier" class="form-control" accept="image/*" />
                                                                        </div>
                                                                        <div class="col-md-6 px-6 py-1">
                                                                            <button type="submit" class="btn btn-success">Envoyer l'image</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                @if(!empty($image))
                                                                    @foreach($image as $img)
                                                                        @if($img->user_id == $user->id && $img->question_id == $r[0]->question_id)
                                                                            <tr>
                                                                                <th scope="col" width="50" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                                    Image enregistrée :
                                                                                </th>
                                                                                <td>
                                                                                    <div class="container" >
                                                                                        <div class="row">
                                                                                            <div class="col">
                                                                                
                                                                                            </div>
                                                                                            <div class="col">
                                                                                                <a href="javascript:pop_up('{{asset('/storage/'.$img->path)}}')">
                                                                                                    <img src="{{asset('/storage/'.$img->path)}}" class="rounded"/>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="col">
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach 
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    
                                                    @if(Auth::user()->role=='prof')
                                                        @foreach($reponses as $r)
                                                            @foreach($table_pivot as $pivot)
                                                            @if(($r->question_id == $q->id) && ($r->id==$pivot->reponse_id && $user->id==$pivot->user_id))
                                                                <input type="hidden" value="{{$r->id}}" name="reponse_id">
                                                                <div name="{{$r->id}}_prof" id="{{$r->id}}_prof"></div>
                                                                    @foreach($image as $img)
                                                                        @if($img->user_id == $user->id && $img->question_id == $r->question_id)
                                                                            <tr>
                                                                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                                    Image Reponse : 
                                                                                </th>
                                                                                <td>
                                                                                    <div class="container" >
                                                                                        <div class="row">
                                                                                            <div class="col"></div>
                                                                                            <div class="col">
                                                                                                <a href="javascript:pop_up('{{asset('/storage/'.$img->path)}}')">
                                                                                                    <img src="{{asset('/storage/'.$img->path)}}" class="rounded"/>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="col"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endif
                                                </td>        
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        {{-- <footer class="card-footer">{{ $questions->links() }}</footer> --}}
    </div>
    <style>
        select, .is-info {
            margin: 0.3em;
        }
    </style>
    <script>
        var ma_Reponse = " "; 
        
        function disparait(){
            document.getElementById("div1").style.visibility = "visible";
            setTimeout(function() {
                document.getElementById("div1").style.visibility = "hidden";
            }, 4000);
        }

        function pop_up(url){
            window.open(url,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=800,height=500,directories=no,location=no') 
        }

        function etat(exercice){
            requete = $.ajax({
                "type" : "GET",
                "url" : "{{ url('/exercices/exercice') }}",
                "data" : {id:exercice}
            });
            requete.done(function(response, textStatus, jqXHR){
                if(response['etat']==0){
                    etat = 1;  
                }else if (response['etat']==1){
                    etat = 0; 
                }
                $.ajax({
                    url : "/updateEtat",
                    type : 'put',
                    data : {
                        _token : '{{ csrf_token() }}',
                        etat : etat, id:response['id']
                    }
                });
            });
        }

        function plus(exercice){
        	 requete = $.ajax({
                "type" : "GET",
                "url" : "{{ url('/exercices/exercice') }}",
                "data" : {id:exercice}
            });
        	requete.done(function(reponse, textStatus, jqXHR){
        		var tempsAdd = $("#ajouter").val();
        		$("#ajouter").text(" ");
        		    $.ajax({
                    url : "/updateTemps",
                    type : 'put',
                    data : {
                        _token : '{{ csrf_token() }}',
                        tempsAdd : tempsAdd, id:reponse['id']
                    },
                    success : function(){$("#ajouter").text(" ");}
                });
        	});
        }

        function getReponse(mydata){
            $.ajax({
                "url" : "{{ url('/reponses/reponse') }}",
                "type" : "GET",
                "dataType" :"json",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                "data" : {
                    id : mydata,
                },
                success : function(reponse){
                    if(reponse){
                        affichage(reponse);
                    }
                }
            });
        }
    
        function affichage(reponse){
                $('#'+reponse['id']+"_prof").text(reponse['reponse']);
                $('#'+reponse['id']).text(reponse['reponse']);
        
            setTimeout(getReponse(reponse['id'],1000));
        }

        function update(mydata){
            requete = $.ajax({
                "type" : "GET",
                "url" : "{{ url('/reponses/reponse') }}",
                "data" : {id:mydata}
            });
            requete.done(function (response, textStatus, jqXHR) {
                $('#'+response['id']).on('input', function() {
                        var reponse = $('#'+response['id']).val();
                        $.ajax({
                        url : "/updateReponse",
                        type:'put',
                        data : {
                            _token:'{{ csrf_token() }}',
                            reponse: reponse, id:response['id']
                        }
                    });            
                });
            });
        };

        function chrono(exercice){
            var slug =  $('#matiere_slug').val()
            setInterval( function(){
                $.ajax({
                    "type" : "GET",
                    "url" : "{{ url('/exercices/exercice') }}",
                    "data" : {id:exercice},
                    success : function(reponse, textStatus, jqXHR){
                        if((reponse['dateActuelle'] != reponse['dateFin']) && reponse['duree'] >=0){
                            $.ajax({
                                url : "/updateExercice",
                                type : 'put',
                                data : {
                                    _token : '{{ csrf_token() }}',
                                    dateDebut : reponse['dateDebut'], dateFin : reponse['dateFin'], id:reponse['id']
                                },
                                success : function(reponse, textStatus, jqXHR){
                                    if(reponse){
                                        $("#minutes").html(reponse['duree']);
                                    }
                                }
                            });
                        }else{
                            window.alert("Exercice terminé");
                            etat(exercice);
                            window.location.replace("/matiere/"+slug+"/exercices");
                        }
                    }
                });
            }, 1000);
        }

        function getTableau(mydata){
            
            requete = $.ajax({
                "url" : "{{ url('/reponses/tableau') }}",
                "type" : "GET",
                "dataType" :"json",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                "data" : {
                    id : mydata,
                }
            });
            requete.done(function(reponse, textStatus, jqXHR){
                    affichageTableau(reponse);
            });
        }
        function affichageRefresh(mydata){
            requete = $.ajax({
                "url" : "{{ url('/reponses/tableau') }}",
                "type" : "GET",
                "dataType" :"json",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                "data" : {
                    id : mydata,
                }
            });
            requete.done(function(reponse, textStatus, jqXHR){
                    affichageTableauEtudiant(reponse);
            });
        }

        function updateTab(mydata){ 

            requete = $.ajax({
                "url" : "{{ url('/reponses/tableau') }}",
                "type" : "GET",
                "dataType" :"json",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                "data" : {id : mydata}
            });
            requete.done(function (response, textStatus, jqXHR) {
                var myArray = JSON.parse(response['tab']);
                for(var i = 0; i <response['nb_lignes']; i++){
                    for (var j = 0; j <response['nb_colonnes']; j++) {
                        myArray[i][j] =  document.getElementById(response['id']+"_tab"+i+j+"_update").textContent;
                    }
                }
                var update = JSON.stringify(myArray);
                $.ajax({
                    url : "/updateTab",
                    type:'put',
                    data : {
                        _token:'{{ csrf_token() }}',
                        tab: update, id:response['id']
                    }
                }); 
            });
        }

        function affichageTableauEtudiant(reponse){
            var myArray = JSON.parse(reponse['tab']);
            for(i=0;i<reponse['nb_lignes'];i++){
                for(j=0;j<reponse['nb_colonnes'];j++){
                    $('#'+reponse['id']+"_tab"+i+j+"_update").text( myArray[i][j]);
                }
            }
        }

        function affichageTableau(reponse){
            var myArray = JSON.parse(reponse['tab']);
            for(i=0;i<reponse['nb_lignes'];i++){
                for(j=0;j<reponse['nb_colonnes'];j++){
                    $('#'+reponse['id']+"_tab"+i+j).text( myArray[i][j]);
                }
            }
            setTimeout(getTableau(reponse['id'],1000));
        }
            
        $(document).ready(function (){
            var exercice =  $('#exercice_id').val();
            var etatex = $('#exercice_etat').val();
            if(etatex == 1){
            	chrono(exercice);
            }
            $('input[name=reponse_id]').each(function(){
                var mydata = $(this).val();
                getReponse(mydata);
                update(mydata);
            });
            $('input[name=tableau_id]').each(function(){
                var tab = $(this).val();
                getTableau(tab);
                affichageRefresh(tab);
            });
        });   
    
    </script>
</x-app-layout>