<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Modifier question
        </h2>
    </x-slot>

    <div>
        <div class=" mx-auto py-10 sm:px-6 lg:px-8">
            <div class="block mb-8">
               <a href="{{ route('exercices.users.questions.index', [$exercice, $user = Auth::user()]) }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour</a>
            </div>

            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <form method="post" action="{{route('questions.update',$question)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                                <table class="min-w-full divide-y divide-gray-200 w-full">
                                    <tr class="border-b">
                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            <label for="nom" class="block font-medium text-sm text-gray-700">Enonce</label>
                                        </th>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                            <input type="text" name="enonce" id="enonce" class="form-input rounded-md shadow-sm mt-1 block w-full" value="{{ old('enonce', $question->enonce) }}"/>
                                            @error('enonce')
                                                <p class="text-sm text-red-600">{{ $message }}</p>
                                            @enderror
                                        </td>
                                    </tr>
                                    @if (isset($tableau))
                                    <tr class="border-b">
                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                    Tableau : 
                                        </th>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">

                                            <?php 
                                                $arr_decode = json_decode($tableau->tab, true);
                                            ?>
                                            <table id="tableau" class="table" style="border:1px solid black">
                                                <tbody>
                                                    @for ($i = 0; $i < $tableau->nb_lignes; $i++)
                                                        <tr>
                                                            @for ($j = 0; $j < $tableau->nb_colonnes; $j++)
                                                                @if ($j == 0 || $i == 0)
                                                                    <th style="border:1px solid black" id="case{{$i.$j}}" contenteditable="true" scope="col">{{ $arr_decode[$i][$j] }}</th>
                                                                @else
                                                                    <td style="border:1px solid black" id="case{{$i.$j}}" contenteditable="true">{{ $arr_decode[$i][$j] }}</td>
                                                                @endif
                                                                
                                                            @endfor
                                                        </tr>
                                                    @endfor
                                                </tbody>
                                            </table>
                                            <input type="hidden" id="tab" name="tab" value="" />
                                        </td>
                                    </tr>
                                    @endif
                                    <tr class="border-b">
                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Image : 
                                        </th>
                                        @if(isset($question->image_question))
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                <img src="{{asset('/storage/'.$question->image_question)}}" id="img"/>
                                                <input type="file" id="image_new" name="image_new" accept="image/*" /><br>
                                                <input type="checkbox" id="supp" name="supp" value="1" class="inline-flex items-center px-1 py-1 border border-transparent rounded-md">
                                                <label for="supp">Cocher pour supprimer</label>
                                            </td>
                                        @else
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900 bg-white divide-y divide-gray-200">
                                                <input type="file" id="image_new" name="image_new" accept="image/*" />
                                            </td>
                                        @endif
                                    </tr>
                                </table>
                                <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button onclick="editRow();" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                        Modifier
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <script type="text/javascript"> 

        function editRow()
        {
            var my_var = <?php if(isset($arr_decode)){echo json_encode($arr_decode);} ?>;
            for(var i = 0; i < my_var.length; i++)
            {
                idCase="case";
                idCasei = idCase + i;
                for (let j = 0; j < my_var[i].length; j++) {
                    idCasej = idCasei + j;
                    my_var[i][j] =  document.getElementById(idCasej).textContent;
                    console.log(my_var[i][j]);
                }
            }
            document.getElementById("tab").value = JSON.stringify(my_var);
        }
        
    </script>
</x-app-layout>