<x-app-layout>
<!DOCTYPE html>
<html>
  <head>
  <x-slot name="header">
        <h3 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Matieres') }}
        </h3>
    </x-slot>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Matieres</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.0/css/bulma.min.css">
    @yield('javascript')
    @yield('css')
  </head>
  <body>
    <main class="section">
        <div class="container">
              @yield('content')
        </div>
    </main>
  </body>
</html>
</x-app-layout>