<div class="relative">
    <input
        type="text"
        class="form-input"
        placeholder="Ajouter un etudiant..."
        wire:model="query"
        wire:keydown.escape=" restart"
        wire:keydown.tab=" restart"
        wire:keydown.enter="selectuser"
    />

    @if(!empty($query))
        <div class="fixed top-0 right-0 bottom-0 left-0" wire:click=" restart"></div>

        <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg" style="list-style: none;font-weight: bold;color: black;font-size: 16px;">
            @if(!empty($users))
                @foreach($users as $i => $user)
                    <a
                        href="{{ route('users.edit', $user['id'],$this->classe->id) }}"
                        class="list-item {{ $highlightIndex === $i ? 'highlight' : '' }} "
                    >{{ $user['email'] }}</a>
                @endforeach
            @else
                <div class="list-item">Pas de résultat !</div>
            @endif
        </div>
    @endif
</div>