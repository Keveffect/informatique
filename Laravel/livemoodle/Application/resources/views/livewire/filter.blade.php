<div class="relative">
<div class="input-group mb-3" style="top: 6px;">
  <input
        type="text"
        class="form-input "
        placeholder="Rechercher une matiere..."
        wire:model="query"
        wire:keydown.escape=" restart"
        wire:keydown.tab=" restart"
        wire:keydown.enter="selectmatiere"
        style="border-color: #dfdfdf;"
    />
</div>
    
    @if(Auth::user()->role=='prof')
        @if(!empty($query))
            <div class="fixed top-0 right-0 bottom-0 left-0" wire:click=" restart"></div>

            <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg" style="list-style: none;font-weight: bold;color: black;font-size: 16px; " >
                @if(!empty($matieres))
                    @foreach($matieres as $i => $matiere)
                        <a
                            href="{{ route('exercices.matiere', $matiere['slug']) }}"
                            class="list-item {{ $highlightIndex === $i ? 'highlight' : '' }} "
                        >{{ $matiere['nom'] }}</a>
                    @endforeach
                @else
                    <div class="list-item">Pas de résultat !</div>
                @endif
            </div>
        @endif
    @endif

    @if(Auth::user()->role=='eleve')
        @if(!empty($query))
            <div class="fixed top-0 right-0 bottom-0 left-0" wire:click=" restart"></div>

            <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg" style="list-style: none;font-weight: bold;color: black;font-size: 16px;">
                @if(!empty($matieres_eleve))
                    @foreach($matieres_eleve as $i => $matiere)
                        <a
                            href="{{ route('exercices.matiere', $matiere['slug']) }}"
                            class="list-item {{ $highlightIndex === $i ? 'highlight' : '' }} "
                        >{{ $matiere['nom'] }}</a>
                    @endforeach
                @else
                    <div class="list-item">Pas de résultat !</div>
                @endif
            </div>
        @endif
    @endif
</div>