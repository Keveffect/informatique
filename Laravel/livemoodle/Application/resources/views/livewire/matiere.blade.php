<tr>
    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
    <a href="{{ route('exercices.matiere', $matiere->slug) }}" class="text-blue-600 hover:text-blue-900 mb-2 mr-2">{{$matiere->nom}}</a>
    </td>
    
    <td class="px-6 text-sm text-gray-900">
        {{ $matiere->description }}
    </td>

    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
            @if(Auth::user()->role=='prof')
            <a href="{{ route('gestionclasse', $matiere->id) }}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">gestion classes</a>
            <a href="{{ route('matieres.edit', $matiere->id) }}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">Modifier</a>
            <form class="inline-block" action="{{ route('matieres.destroy', $matiere->id) }}" method="POST" onsubmit="return confirm('Vous etes sur ?');">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="text-red-600 hover:text-red-900 mb-2 mr-2" value="Supprimer">
            </form>
            @endif
        </td>
        @if(Auth::user()->role=='eleve')
        <td>
          <div class ="flex justify-between"> 
          <button class="h-6 w-6 focus:outline-none" wire:click="addLike">
            <svg xmlns="http://www.w3.org/2000/svg" fill="{{ $matiere->isLiked() ? 'red' : 'gray' }}" viewBox="0 0 24 24" stroke="white">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
            </svg>
        </button>
        </div>
        </td>
        @endif
    </tr>
</div>