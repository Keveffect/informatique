<div class="p-6 sm:px-20 bg-white border-b border-gray-200">
    <div>
        <x-jet-application-logo class="block h-12 w-auto" />
    </div>

    <div class="mt-8 text-2xl" style="font-family: monospace;font-size: xx-large;">Bienvenue sur LiveMoodle</div>
<br>
<div class="row">
    <div class="col">
        <img src="https://image.freepik.com/vecteurs-libre/personnes-ciblees-etudiant-ecole-ligne_74855-5834.jpg" alt="" srcset="">
    </div>
    <div class="col">
        <div style="height:30%;"></div>
        <div>
            <p style="text-align: justify;font-family: cursive;color: #1397ffb8;">
            Moodle est l'environnement d'apprentissage le plus répandu au monde. C'est pourquoi nous l'avons amélioré afin de créer LiveMoodle !
            Cette application permettra à votre professeur de suivre en temps réel vos réponses aux exercices et aussi d'améliorer vos conditions de travail
            à travers l'ajout de fonctionnalités spectaculaires ! 
            </p>
        </div>
    </div>
  </div>

  <div class="row">
    <div class="col">
        <div style="height:30%"></div>
        <div>
            <p style="text-align: justify;font-family: cursive;color: #1397ffb8;">A présent vous pouvez remplir des tableaux, ajouter des images, faire des copiers collés plus facilement qu'auparavant ! Vous êtes à présent dans l'obligation de réaliser vos exercices pour votre progression puisque le professeur voit ce que vous faites. N'hésitez pas à intéragir avec votre professeur si celui-ci vous envoie un message suite à vos réponses correctes ou eronnées.
            </p>
        </div>
    </div>
    <div class="col">
        <img src="https://static.vecteezy.com/ti/vecteur-libre/p1/1260867-education-en-ligne-avec-un-garcon-et-une-fille-etudiant-sur-ordinateur-vectoriel.jpg" alt="">
    </div>
  </div>
<br>
<style>
svg:hover{
    fill: blue;
}
</style>
@if(count(auth()->user()->likes) > 0)
<div class="card-header" style="font-family: fantasy;font-size: larger;">Vos Cours Favoris</div>
@endif

<div class="bg-gray-200 bg-opacity-25 grid grid-cols-1 md:grid-cols-2">
@foreach(auth()->user()->likes as $like) 
<div class="row row-cols-1 row-cols-md-3 g-4">

  <div class="col">
        <div class="card h-100">
        <div class ="flex justify-between"> 
            <button class="h-10 w-10 focus:outline-none" onclick="window.location.href='{{route('exercices.matiere', $like->slug) }}'">
            <svg xmlns="https://www.svgrepo.com/show/14723/book.svg" class="h-10 w-10" fill="gray" viewBox="0 0 24 24" stroke="white"style="margin-left: 12px;margin-top: 5px;">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253" />
            </svg>
            </div>
            <div class="card-body">
                <h5 class="card-title" style="font-weight:bold">{{$like->nom}}</h5>
                <p class="card-text">{{$like->description}}</p>
            </div>
        
        </div>
    </div>
</div>
@endforeach
</div>

