@if ($paginator->hasPages())
    <nav class="pagination is-centered" role="navigation" aria-label="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="pagination-previous" style="color:grey;" disabled>@lang('pagination.previous')</a>
        @else
            <a class="pagination-previous" href="{{ $paginator->previousPageUrl() }}">@lang('pagination.previous')</a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <span class="pagination-ellipsis">&hellip;</span>
            @endif
            {{-- Array Of Links --}}
            @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="pagination-link is-current" style="color:#FF0000;" aria-label="Goto page {{ $page }}">&nbsp;{{ $page }}&nbsp;</a>
                        @else
                            <a href="{{ $url }}" class="pagination-link" aria-label="Goto page {{ $page }}">&nbsp;{{ $page }}&nbsp;</a>
                        @endif
                    @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="pagination-next" href="{{ $paginator->nextPageUrl() }}">@lang('pagination.next')</a>
        @else
            <a class="pagination-next" style="color:grey;" disabled>@lang('pagination.next')</a>
        @endif
        
    
    </nav>
@endif