<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
        <x-jet-application-mark class="block h-9 w-auto" />
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Oublie de mot de passe ? Pas de problème. Donnez nous juste votre adresse mail et nous allons vous envoyer un lien de réinitialisation de mot de passe') }}
        </div>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="block">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Retour a la connexion') }}
                </a>

            <div class="flex items-center justify-end mt-4">
                <x-jet-button>
                    {{ __('Valider') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
