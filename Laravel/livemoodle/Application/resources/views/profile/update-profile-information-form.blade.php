<x-jet-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{ __('Informations du profil') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Vous pouvez modifier votre photo de profile') }}
    </x-slot>

    <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('Photo') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20"
                          x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Parcourir') }}
                </x-jet-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Retirer la photo') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
        @endif

        <!-- Name -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="name" style="bold;font-weight: bold;font-size: unset;" value="{{ __('Name') }} : {{ Auth::user()->name }}" />
        </div>

        <!-- Email -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email" style="bold;font-weight: bold;font-size: unset;" value="{{ __('Email') }} : {{ Auth::user()->email }}" />
        </div>

        <!-- Statut -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="statut" style="bold;font-weight: bold;font-size: unset;" value="{{ __('Statut') }} : {{ Auth::user()->role }}" />
        </div>

        @if(Auth::user()->role=='eleve' && Auth::user()->classe!=NULL)
            <!-- Classe -->
            <div class="col-span-6 sm:col-span-4">
                <x-jet-label for="classe" style="bold;font-weight: bold;font-size: unset;" value="{{ __('Classe') }} : {{ Auth::user()->classe->nom }}" />
            </div>
        @elseif(Auth::user()->role=='eleve' && Auth::user()->classe==NULL)
        <div class="col-span-6 sm:col-span-4">
                <x-jet-label for="classe" style="bold;font-weight: bold;font-size: unset;" value="{{ __('Classe') }} : Non renseigné" />
            </div>
        @endif
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Enregistrer') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
