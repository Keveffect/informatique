<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Matières
        </h2>
    </x-slot>

    <div>
        <div class="max-w-6xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="block mb-8">
                @if(Auth::user()->role=='prof')
                <a href="{{ route('matieres.create') }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Ajouter matière</a>
                @endif
                <a href="{{ route('dashboard') }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Retour</a>
                @if(Auth::user()->role=='prof')
                    <button data-toggle="modal" data-target="#infos" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-2 rounded">?</button>
                    <div class="modal" id="infos">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Informations</h4>
                                    <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                    </button>            
                                </div>
                                <div class="modal-body">
                                    Sur cette page vous pouvez creer/modifier/supprimer des matieres. 
                                    En cliquant sur le nom de la matiere vous accedez a la creation des exercices pour cette matiere.
                                    En cliquant sur gestion classe vous pourrez ajouter les classes qui pourront consulter cette matiere
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200 w-full">
                                <thead>
                                <tr>
                                    <th scope="col" width="50" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        NOM
                                    </th>
                                    <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Description
                                    </th>
                                    @if(Auth::user()->role=='prof')
                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Gestion
                                        </th>
                                    @endif
                                    @if(Auth::user()->role=='eleve')
                                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            
                                        </th>
                                        <th scope="col" class=" py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Favori
                                        </th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @if(Auth::user()->role=='prof')
                                    @if($matieres->isEmpty())
                                        <tr>
                                            <td></td>
                                            <td>
                                                Il n'y a encore auccune matiere, vous devez les creer pour y ajouter des exercices
                                            </td>
                                        </tr>
                                    @else
                                        @foreach ($matieres as $matiere)
                                            <livewire:matiere :matiere="$matiere"/>
                                        @endforeach
                                    @endif
                                @elseif(Auth::user()->role=='eleve')
                                    @if($matieres_eleve->isEmpty())
                                        <tr>
                                            <td></td>
                                            <td>
                                                Vous n'avez accés a auccune matiere, attendez l'action de vos professeurs
                                            </td>
                                        </tr>
                                    @else
                                        @foreach ($matieres_eleve as $matiere)
                                            <livewire:matiere :matiere="$matiere"/>
                                        @endforeach
                                    @endif
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @if(Auth::user()->role=='prof')
            <footer class="card-footer">
                {{ $matieres->links() }}
            </footer>
        @else
            <footer class="card-footer">
                {{ $matieres_eleve->links() }}
            </footer>
        @endif
        </div>
    </div>
</x-app-layout>