<?php

use Illuminate\Support\Facades\Route;
use App\http\Controllers\PhotoController;
use App\http\Controllers\MatiereController;
use App\http\Controllers\ExerciceController;
use App\http\Controllers\QuestionController;
use App\http\Controllers\ClasseController;
use App\http\Controllers\UserController;
use App\http\Controllers\ImageController;

Route::middleware('guest')->group(function () {
    Route::get('/', function () {
        return view('auth/login');
    });
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

/*Reserve aux utilisateurs authentifiés*/
Route::middleware('auth')->group(
    function () {
        Route::get('image-upload',[ImageController::class, 'image_upload'])->name('image.upload');
        Route::post('image-upload',[ImageController::class, 'upload_post_image'])->name('upload.post.image');

        Route::resource('users', UserController::class);
        Route::Post('classe/{id}/store', [UserController::class, 'store'])->name('users.classe.store');
        Route::get('classe/{slug}/users', [UserController::class, 'index'])->name('users.classe');

        Route::resource('matieres', MatiereController::class);
        Route::get('matieres/{matiere}/', [MatiereController::class, 'gestionclasse'])->name('gestionclasse');

        Route::resource('matieres.exercices', ExerciceController::class)->middleware('AccesMiddleware');
        Route::resource('exercices', ExerciceController::class, ['only' => ['edit', 'update']]);
        Route::get('matiere/{slug}/exercices', [ExerciceController::class, 'index'])->name('exercices.matiere')->middleware('AccesMiddleware');
        Route::put('/updateEtat', [ExerciceController::class, 'updateEtat']);
        Route::put('/updateExercice', [ExerciceController::class, 'updateChrono']);
        Route::get('exercices/exercice', [ExerciceController::class, 'exercice']);
        Route::resource('exercices.classes', ExerciceController::class, ['only' => ['show']]);

        Route::resource('exercices.users.questions', QuestionController::class)->middleware('QuestionMiddleware');
        Route::get('/stop/{id}/{slug}/', [QuestionController::class, 'stop'])->name('stop');
        Route::resource('questions', QuestionController::class, ['only' => ['edit', 'update']]);
        Route::get('reponses/reponse', [QuestionController::class, 'reponse']);
        Route::get('reponses/tableau', [QuestionController::class, 'tableau']);
        Route::put('/updateReponse', [QuestionController::class, 'updateReponse']);
        Route::put('/updateTab', [QuestionController::class, 'updateTab']);
        Route::put('/updateTemps', [QuestionController::class, 'updateTemps']);
        
        Route::resource('classes', ClasseController::class);
        Route::get('classe/{classe}/{exercice}/', [ClasseController::class, 'listeclasse'])->name('listeclasse');
    }
);
