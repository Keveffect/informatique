<?php

namespace Database\Factories;

use App\Models\Matiere;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class MatiereFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Matiere::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nom = $this->faker->word();
        return [
            'nom' => $nom,
            'description' => $this->faker->paragraph(),
            'slug' => Str::slug($nom)
        ];
    }
}
