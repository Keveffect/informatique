<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert([
            [
                'nom' => 'S607',
                'slug' => 's607',
            ],
            [
                'nom' => 'S606',
                'slug' => 's606',
            ],
        ]);

        DB::table('matieres')->insert([
            [
                'nom' => 'Info0501',
                'slug' => 'info0501',
                'description' => 'Algorithmique avancée',
            ],
            [
                'nom' => 'Info0502',
                'slug' => 'info0502',
                'description' => 'Logique et programmation logique',
            ],
            [
                'nom' => 'Info0503',
                'slug' => 'info0503',
                'description' => 'Modelisation client-serveur et programmation web avancée',
            ],
            [
                'nom' => 'Info0504',
                'slug' => 'info0504',
                'description' => 'Base de données : concepts avancés',
            ],
            [
                'nom' => 'An0501',
                'slug' => 'an0501',
                'description' => 'Anglais',
            ],
            [
                'nom' => 'Ppro0502',
                'slug' => 'Ppro0502',
                'description' => 'Conferences proffesionnelles; gestion de projet informatique',
            ],
            [
                'nom' => 'Info0601',
                'slug' => 'info0601',
                'description' => 'Systeme d\'exploitation concepts avancés',
            ],
            [
                'nom' => 'Info0602',
                'slug' => 'info0602',
                'description' => 'Langages et compilation',
            ],
            [
                'nom' => 'Info0603',
                'slug' => 'info0603',
                'description' => 'Compression et cryptographie',
            ],
            [
                'nom' => 'Info0604',
                'slug' => 'info0604',
                'description' => 'Programmation multi-threadée',
            ],
            [
                'nom' => 'Info0605',
                'slug' => 'info0605',
                'description' => 'Ouverture - Introduction à la securité informatique',
            ],
            [
                'nom' => 'Info0606',
                'slug' => 'info0606',
                'description' => 'Ouverture - Introduction a l\'imagerie',
            ],
            [
                'nom' => 'Info0607',
                'slug' => 'info0607',
                'description' => 'Ouverture - Introduction à la l\'intelligence artificielle',
            ],
            [
                'nom' => 'An0601',
                'slug' => 'an0601',
                'description' => 'Anglais',
            ],
            [
                'nom' => 'Ppro0605',
                'slug' => 'ppro0605',
                'description' => 'Stage en entreprise ou projet Informatique',
            ],
            [
                'nom' => 'MA0601',
                'slug' => 'ma0601',
                'description' => 'Math 1',
            ],
            [
                'nom' => 'MA0602',
                'slug' => 'ma0602',
                'description' => 'Math 2',
            ],
            [
                'nom' => 'SVT0602',
                'slug' => 'svt0602',
                'description' => 'SVT 2',
            ],
            [
                'nom' => 'SVT0601',
                'slug' => 'svt0601',
                'description' => 'svt 1',
            ],
        ]);

        DB::table('users')->insert([
            [
                'name' => 'Cyril Rabat',
                'email' => 'cyril.rabat@univ-reims.fr',
                'role' => 'prof',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Pierre Delisle',
                'email' => 'pierre.delisle@univ-reims.fr',
                'role' => 'prof',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Quentin Burette',
                'email' => 'quentin.burette@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Kevin Charlier',
                'email' => 'kevin.charlier@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Johnny Vong',
                'email' => 'johnny.vong@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Anthony Cailleaux',
                'email' => 'anthony.cailleaux@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Basel Alghoul',
                'email' => 'basel.alghoul@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Alexi Wentzinger',
                'email' => 'alexi.wentzinger@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Math 1',
                'email' => 'math1@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Math 2',
                'email' => 'math2@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Physique 1',
                'email' => 'physique1@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Physique 2',
                'email' => 'physique2@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Staps 1',
                'email' => 'staps1@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
            [
                'name' => 'Staps 2',
                'email' => 'staps2@etudiant.univ-reims.fr',
                'role' => 'eleve',
                'password' => Hash::make('root'),
                'email_verified_at' => now(),
            ],
        ]);
    }
}
