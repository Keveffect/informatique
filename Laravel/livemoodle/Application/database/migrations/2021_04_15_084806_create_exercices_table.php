<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExercicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('exercices', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->boolean('etat');
            $table->integer('duree');
            $table->dateTime('dateDebut', $precision = 0)->nullable();
            $table->dateTime('dateFin', $precision = 0)->nullable();
            $table->dateTime('dateActuelle', $precision = 0)->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('matiere_id');
            $table->foreign('matiere_id')->references('id')->on('matieres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercices');
    }
}
