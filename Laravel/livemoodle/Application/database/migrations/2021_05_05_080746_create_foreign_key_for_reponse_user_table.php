<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeyForReponseUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reponse_user', function (Blueprint $table) {
            $table->foreign('reponse_id')->references('id')->on('reponses')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reponse_user', function (Blueprint $table) {
            $table->dropForeign('reponse_user_reponse_id_foreign');
            $table->dropForeign('reponse_user_user_id_foreign');
        });
    }
}
