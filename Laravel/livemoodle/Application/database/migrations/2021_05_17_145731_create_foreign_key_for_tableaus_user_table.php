<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeyForTableausUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tableau_user', function (Blueprint $table) {
            $table->foreign('tableau_id')->references('id')->on('tableaus')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tableau_user', function (Blueprint $table) {
            $table->dropForeign('tableau_user_tableau_id_foreign');
            $table->dropForeign('tableau_user_user_id_foreign');
        });
    }
}
