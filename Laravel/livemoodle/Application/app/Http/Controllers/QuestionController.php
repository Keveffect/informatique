<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use App\Models\Matiere;
use App\Models\Exercice;
use App\Models\Question;
use App\Models\Reponse;
use App\Models\Image;
use App\Models\User;
use App\Models\Tableau;
use App\Http\Requests\Question as QuestionRequest;

function do_alert($msg)
{
    echo '<script type="text/javascript">alert("' . $msg . '"); </script>';
}

class QuestionController extends Controller
{
    use SoftDeletes;

    public function reponse(Request $request)
    {
        return Reponse::find($request->id);
    }

    public function tableau(Request $request)
    {
        return Tableau::find($request->id);
    }

    public function index(Exercice $exercice, User $user, Image $image)
    {
        $user_auth = Auth::user();
        if ($user != $user_auth && $user_auth->role == "eleve") {
            return redirect()->route('exercices.users.questions.index', [$exercice->id, Auth::id()]);
        } else {
            $exercice2 = $exercice;
            $tableaux = DB::table('tableaus')->where('exercice_id', $exercice->id)->get();
            $query = $exercice->id ? Exercice::whereId($exercice->id)->firstOrFail()->questions() : Question::query();
            $query2 = $exercice->id ? Exercice::whereId($exercice->id)->firstOrFail()->reponses() : Reponse::query();
            $reponses = $query2->paginate();
            $questions = $query->paginate();
            $exercice =  Exercice::whereId($exercice->id)->get();

            $reponses = Reponse::all();
            $reponses_pivot = DB::table('reponse_user')->where('user_id', Auth()->user()->id)->get();
            
            // creation des reponses en cas de table pivot vide
            if ($reponses_pivot->isEmpty() && Auth()->user()->role == "eleve") {
                foreach ($questions as $q) {
                    $bool1 = true;
                    //creation des champs reponses
                    $reponse = new Reponse;
                    $reponse->exercice_id = $exercice2->id;
                    $q->reponse()->save($reponse);
                    $user->reponses()->attach($reponse);
                    //creation des tableaux
                    foreach($tableaux as $t){
                        if ($t->question_id == $q->id && $bool1==true) {
                            $bool1=false;
                            $tableau = new Tableau;
                            $tableau->question_id = $t->question_id;
                            $tableau->exercice_id = $t->exercice_id;
                            $tableau->nb_colonnes = $t->nb_colonnes;
                            $tableau->nb_lignes = $t->nb_lignes;
                            $tableau->tab = json_encode(array_fill(0, ($t->nb_lignes), array_fill(0, ($t->nb_colonnes), '')));
                            $q->tableau()->save($tableau);
                            $user->tableau()->attach($tableau);
                        }
                    }
                }
            } elseif (Auth()->user()->role == "eleve") {
                foreach ($questions as $q) {
                    $bool = true;
                    foreach ($reponses as $r) {
                        if ($r->exercice_id == $exercice2->id && $r->question_id == $q->id) {
                            $bool = false;
                        }
                    }
                    if ($bool) {
                        $reponse = new Reponse;
                        $reponse->exercice_id = $exercice2->id;
                        $q->reponse()->save($reponse);
                        $user->reponses()->attach($reponse);
                        //creation des tableaux
                        foreach($tableaux as $t){
                            if ($t->question_id == $q->id) {
                                $tableau = new Tableau;
                                $tableau->question_id = $t->question_id;
                                $tableau->exercice_id = $t->exercice_id;
                                $tableau->nb_colonnes = $t->nb_colonnes;
                                $tableau->nb_lignes = $t->nb_lignes;
                                $tableau->tab = json_encode(array_fill(0, ($t->nb_lignes), array_fill(0, ($t->nb_colonnes), '')));
                                $q->tableau()->save($tableau);
                                $user->tableau()->attach($tableau);
                            }
                        }
                    }
                }
            }

            //on rechage la table pivot avec les nouvelles données
            $reponses_pivot = DB::table('reponse_user')->where('user_id', Auth()->user()->id)->get();

            if ($reponses_pivot->isEmpty()) {
                $reponses_user = null;
            } else {
                $j = 0;
                foreach ($reponses_pivot as $rp) {
                    $reponses_user[$j++] = DB::table('reponses')->where('id', $rp->reponse_id)->get();
                }
            }

            $matiere =  Exercice::find($exercice2->id);
            $matiere = Matiere::find($matiere->matiere_id);
            $table_pivot = DB::table('reponse_user')->get();
            
            //recuperation des tableaux
            if(Auth()->user()->role == "eleve"){
                $table_pivot_tableau_user = DB::table('tableau_user')->get();
                $i = 0;
                $tableau_enonce = null;
                foreach($tableaux as $tab){
                    $bool = true;
                    foreach($table_pivot_tableau_user as $pivot){
                        if ($tab->exercice_id == $exercice2->id && $tab->id == $pivot->tableau_id && Auth()->user()->id == $pivot->user_id) {
                            $bool = false;
                        }
                    }
                    // on chage les tableau de type enoncé (qui n'ont pas de dependance a un etudiant)
                    if($bool)
                        $tableau_enonce[$i++] = $tab;
                }

                // on recupere les tableaux avec les nouvelles données
                $tableaux = DB::table('tableaus')->where('exercice_id', $exercice2->id)->get();
                $table_pivot_tableau_user = DB::table('tableau_user')->where('user_id',Auth()->user()->id)->get();
                $i = 0;
                $tableau = null;
                foreach($table_pivot_tableau_user as $pivot){
                    foreach($tableaux as $tab){
                        if ($tab->exercice_id == $exercice2->id && $tab->id == $pivot->tableau_id && Auth()->user()->id == $pivot->user_id) {
                            $tableau[$i++] = $tab;
                        }
                    }
                }
                $tableaux = $tableau; //changement de variable pour rien casser
                
            }else{ //prof
                $table_pivot_tableau_user = DB::table('tableau_user')->get();
                $i = 0;
                $tableau_enonce = null;
                foreach($tableaux as $tab){
                    $bool = true;
                    foreach($table_pivot_tableau_user as $pivot){
                        if ($tab->exercice_id == $exercice2->id && $tab->id == $pivot->tableau_id && Auth()->user()->id != $pivot->user_id) {
                            $bool = false;
                        }
                    }
                    if($bool)
                        $tableau_enonce[$i++] = $tab;
                }
            }

            $image = Image::all();
    
            return view('question\index', compact('questions', 'exercice', 'matiere', 'reponses', 'user', 'reponses_user', 'table_pivot', 'tableaux','table_pivot_tableau_user', 'tableau_enonce', 'image'));
        }
    }

    public function updateReponse(Request $request)
    {
        $reponse = $request->reponse;
        $id = $request->id;
        $reponse2 = Reponse::find($id);
        $reponse2->reponse = $reponse;
        $reponse2->save();

        return response()->json();
    }

    public function updateTab(Request $request)
    {
        $reponse = $request->tab;
        $id = $request->id;
        $reponse2 = Tableau::find($id);
        $reponse2->tab = $reponse;
        $reponse2->save();

        $request->session()->flash('info',"Tableau mis a jour avec succés");
        return response()->json();
    }

    public function create($id)
    {
        $exercice = Exercice::find($id);
        return view('question\create', compact('exercice'));
    }


    public function store(QuestionRequest $request)
    {
        $question = new Question;
        $question->enonce = $request->enonce;
        $question->exercice_id = $request->exercice_id;
        if ($request->hasFile('image_question')) {
            if (($_FILES['image_question']['type'] == "image/gif")
                || ($_FILES["image_question"]["type"] == "image/jpeg")
                || ($_FILES["image_question"]["type"] == "image/jpg")
                || ($_FILES["image_question"]["type"] == "image/png")
            ) {
                $question->image_question = $request->file('image_question')->store('images', 'public');
            } else {
                do_alert('Le fichier n\'est pas une image.');
                $exercice = Exercice::find($request->exercice_id);
                return view('question\create', compact('exercice'));
            }
        }
        $question->save();
        $exercice = Exercice::find($question->exercice_id);
        /* $reponse = new Reponse;
        $reponse->exercice_id = $exercice->id;
        $question->reponse()->save($reponse);*/
        $user = Auth::user();
        if ($request->nb_ligne != 0 && $request->nb_colonne != 0) {
            $tableau = new Tableau;
            $tableau->exercice_id = $request->exercice_id;
            $tableau->nb_colonnes = $request->nb_colonne;
            $tableau->nb_lignes = $request->nb_ligne;
            $tableau->tab = json_encode(array_fill(0, ($request->nb_ligne), array_fill(0, ($request->nb_colonne), '')));
            $question->tableau()->save($tableau);
        }

        return redirect()->route('exercices.users.questions.index', [$exercice->id, $user])->with('info', 'Question créée');
    }



    public function show($id)
    {
        //
    }



    public function edit(Exercice $exercice, Question $question)
    {
        $user_id = Auth::user()->id;
        $exercice = Exercice::find($question->exercice_id);
        $tableau = DB::table('tableaus')->where('question_id', $question->id)->get();
        if (isset($tableau[0])) {
            $tableau = $tableau[0];
        } else {
            $tableau = null;
        }
        return view('question\edit', compact('exercice', 'question', 'user_id', 'tableau'));
    }



    public function update(QuestionRequest $request, Question $question)
    {
        if ($request->hasFile('image_new')) {
            if (($_FILES['image_new']['type'] == "image/gif")
                || ($_FILES["image_new"]["type"] == "image/jpeg")
                || ($_FILES["image_new"]["type"] == "image/jpg")
                || ($_FILES["image_new"]["type"] == "image/png")
            ) {
                if ($question->image_question != null) {
                    unlink(public_path('storage/' . $question->image_question));
                }
                $question->image_question = $request->file('image_new')->store('images', 'public');
            } else {
                do_alert('Le fichier n\'est pas une image.');
                $exercice = Exercice::find($question->exercice_id);
                $tableau = DB::table('tableaus')->where('question_id', $question->id)->get();
                if (isset($tableau[0])) {
                    $tableau = $tableau[0];
                } else {
                    $tableau = null;
                }
                return view('question\edit', compact('exercice', 'question', 'tableau'));
            }
        } else {
            if ($request->supp == 1) {
                unlink(public_path('storage/' . $question->image_question));
                $question->image_question = null;
            }
        }

        $tableau = DB::table('tableaus')->where('question_id', $question->id)->get();
        if (isset($tableau[0])) {
            $tableau = $tableau[0];
            if ($request->tab != null) {
                $tableau = Tableau::find($tableau->id);
                $tableau->tab = $request->tab;
                $tableau->update();
            }
        }
        $question->update($request->all());

        $exercice = Exercice::find($question->exercice_id);
        return redirect()->route('exercices.users.questions.index', [$exercice->id, Auth::user()])->with('info', 'la question a bien été modifié');
    }

    public function stop($id,$slug)
    {
        $exercice = Exercice::find($id);
        $exercice->etat = 0;
        $exercice->dateFin = null;
        $exercice->dateDebut = null;
        $exercice->dateActuelle = null;
        $exercice->update();

        return redirect()->route('matieres.exercices.index',$slug)->with('info', 'L\'exercice a bien été arrêté');
    }

    public function updateTemps(Request $request){
        if($request->tempsAdd != ''){
            $temps = intval($request->tempsAdd);
            if($temps>=1 && $temps <=60){
                $exerciceid = $request->id;
                $exercice = Exercice::find($exerciceid);
                
                $dateFin = new Carbon($exercice->dateFin);
                $dateDebut = new Carbon($exercice->dateDebut);

                $dateFin->addMinutes($temps);
                $duree = $dateFin->diffInMinutes($dateDebut);

                if($duree <62){
                    $exercice->dateFin = $dateFin;
                    $exercice->update();
                    return response()->json();
                }
            }
        }
    }

    public function destroy(Exercice $exercice, User $user, Question $question)
    {
        $images = Image::all();
        foreach ($images as $img) {
            if ($img->question_id == $question->id) {
                unlink(public_path('storage/' . $img->path));
            }
        }
        if ($question->image_question != null) {
            unlink(public_path('storage/' . $question->image_question));
        }
        $question->delete();
        return back()->with('info', 'La question a bien été supprimé dans la base de données.');
    }
}
