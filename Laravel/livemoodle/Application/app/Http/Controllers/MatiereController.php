<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Matiere;
use App\Models\Classe;
use App\Models\Exercice;
use App\Models\Question;
use App\Http\Requests\Matiere as MatiereRequest;
use App\Http\Requests\UpdateMatiereRequest as updateMatiereRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MatiereController extends Controller
{
    public function __construct()
    {
        $this->middleware(['roleChecker:prof,null'])->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i = 0;
        $matieres = Matiere::oldest('nom')->paginate(5);
        $id = Auth::user()->classe_id;
        $matiers_id_array = array();
        $matieres_classes = DB::table('classe_matiere')->where('classe_id', $id)->get();
        foreach ($matieres_classes as $matiere_classe) {
            $matiers_id_array[$i] = $matiere_classe->matiere_id;
            $i++;
        }
        $matieres_eleve = Matiere::whereIn('id', $matiers_id_array)->paginate(5);
        return view('matiere\index', compact('matieres', 'matieres_eleve'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('matiere\create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatiereRequest $matiererequest)
    {
        $matiere = new Matiere;
        $matiere->nom = $matiererequest->nom;
        $matiere->slug = Str::slug($matiere->nom);
        $matiere->description = $matiererequest->description;
        $matiere->save();

        return redirect()->route('matieres.index')->with('info', 'Matiére créée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Matiere $matiere)
    {
        return view('matiere\show', compact('matiere'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Matiere $matiere)
    {
        return view('matiere\edit', compact('matiere'));
    }

    public function gestionclasse($id)
    {
        $matiere = Matiere::find($id);
        $classe_all = Classe::all();

        return view('matiere\gestionclasse', compact('matiere', 'classe_all'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateMatiereRequest $matiererequest, Matiere $matiere)
    {
        $matiere->update($matiererequest->all());

        return redirect()->route('matieres.index')->with('info', 'La matiere a bien été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matiere $matiere)
    {
        $query = $matiere->slug ? Matiere::whereSlug($matiere->slug)->firstOrFail()->exercices() : Exercice::query();
        $exercice = $query->paginate(1000);
        foreach ($exercice as $e) {
            $query = $e->id ? Exercice::whereId($e->id)->firstOrFail()->questions() : Question::query();
            // $query2 = $e->id ? Exercice::whereId($e->id)->firstOrFail()->reponses() : Reponse::query();
            // $reponses = $query2->paginate(1000);
            $questions = $query->paginate(1000);
            foreach ($questions as $q) {
                if ($q->image_question != null) {
                    unlink(public_path('storage/' . $q->image_question));
                }
            }
        }

        if ($matiere->isLiked()) {
            $matiere->unlike();
        }
        $matiere->delete();
        return back()->with('info', 'La matiere a bien été supprimé dans la base de données.');
    }
}
