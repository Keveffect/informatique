<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use App\Models\User;
use App\Models\Exercice;
use App\Models\Question;

class ImageController extends Controller
{
  public function image_upload()
  {
      return view('image_upload');
  }
  
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function upload_post_image(Request $request)
  {
    $request->validate([
      'fichier' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
    ]);

    $image_old = Image::where('reponse_id', $request->reponse_id)->get();
    if($image_old->isNotEmpty()) {
      unlink(public_path('storage/' . $image_old[0]->path));
      $img = Image::find($image_old[0]->id);

      if ($request->file('fichier')) {
        $imagePath = $request->file('fichier');
        $path = $request->file('fichier')->store('uploads', 'public');
      }

      $img->path = $path;
      $img->reponse_id = $request->reponse_id;
      $img->user_id = $request->user_id;
      $img->question_id = $request->question_id;
      $img->update();

    }else{
    
      $img = new Image;

      if ($request->file('fichier')) {
        $imagePath = $request->file('fichier');
        $path = $request->file('fichier')->store('uploads', 'public');
      }

      $img->path = $path;
      $img->reponse_id = $request->reponse_id;
      $img->user_id = $request->user_id;
      $img->question_id = $request->question_id;
      $img->save();
    }

    return back()->with('success','Image upload.');
  
  }
}