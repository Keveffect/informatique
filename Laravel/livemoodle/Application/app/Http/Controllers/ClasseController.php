<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Classe;
use App\Models\Exercice;
use App\Models\User;
use App\Models\Matiere;
use App\Http\Requests\Classe as ClasseRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UpdateClasseRequest as updateClasseRequest;

class ClasseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['roleChecker:prof,null']);
    }
    public function index()
    {
        $classes = Classe::oldest('nom')->paginate(5);
        return view('classe\index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classe\create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClasseRequest $classerequest)
    {
        $classe = new Classe;
        $classe->nom = $classerequest->nom;
        $classe->slug = Str::slug($classe->nom);
        $classe->save();

        return redirect()->route('classes.index')->with('info', 'classe créée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($classe)
    {
        $users = User::all();
        return view('classe\show', compact('classe', 'users'));
    }

    public function listeclasse($idmatiere, $idexercice)
    {
        $exercice = Exercice::findOrFail($idexercice);
        $matieres_classes = DB::table('classe_matiere')->where('matiere_id', $idmatiere)->get();
        if (!$matieres_classes->isEmpty()) {
            $i = 0;
            foreach ($matieres_classes as $matiere_classe) {
                $classe_id_array[$i] = $matiere_classe->classe_id;
                $i++;
            }
            $classes = Classe::whereIn('id', $classe_id_array)->get();
        } else {
            $classes = null;
        }
        $matiere = Matiere::find($exercice->matiere_id);
        return view('classe\listeclasse', compact('classes', 'exercice', 'matiere'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classe = Classe::findOrFail($id);
        return view('classe\edit', compact('classe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateClasseRequest $classerequest, $id)
    {
        $classe = Classe::findOrFail($id);
        $classe->update($classerequest->all());
        return redirect()->route('classes.index')->with('info', 'La classe a bien été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('classe_id', $id)->update(['classe_id' => null]);
        $classe = Classe::findOrFail($id);
        $classe->delete();
        return back()->with('info', 'La classe a bien été supprimé dans la base de données.');
    }
}
