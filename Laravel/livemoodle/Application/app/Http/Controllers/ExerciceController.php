<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Matiere;
use App\Models\Exercice;
use App\Models\Classe;
use App\Models\User;
use App\Models\Question;
use App\Http\Requests\Exercice as ExerciceRequest;
use Carbon\Carbon;


class ExerciceController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        $this->middleware(['roleChecker:prof,null'])->except(['index', 'updateChrono','exercice']);
    }

    public function exercice(Request $request)
    {
        return Exercice::find($request->id);
    }

    public function matiere(Request $request)
    {
        return Matiere::find($request->id);
    }

    public function updateChrono(Request $request)
    {
        $dateDebut = Carbon::parse(Carbon::now());
        $dateFin = Carbon::parse($request->dateFin);
        $dateActuelle = Carbon::parse(Carbon::now());
        
        $duree = $dateFin->diffInMinutes($dateDebut);
        if($dateFin->lessThan($dateActuelle)){
            $duree = -1;
        }
        gmdate('H:i:s', $duree);

        $exercice = Exercice::find($request->id);
        $exercice->duree = $duree;
        $exercice->dateActuelle = $dateActuelle; 
        $exercice->save();

        return $exercice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {
        $query = $slug ? Matiere::whereSlug($slug)->firstOrFail()->exercices() : Exercice::query();
        $exercices = $query->paginate(5);
        $matiere =  Matiere::whereSlug($slug)->get();
        return view('exercice\index', compact('exercices', 'matiere', 'slug'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Matiere $matiere)
    {
        return view('exercice\create', compact('matiere'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExerciceRequest $request)
    {
        $exercice = new Exercice;
        $exercice->nom = $request->nom;
        $exercice->etat = false;
        $exercice->duree = $request->duree;     
        $exercice->matiere_id = $request->matiere_id;
        $exercice->save();
        $matiere = Matiere::find($exercice->matiere_id);

        return redirect()->route('matieres.exercices.index', $matiere->slug)->with('info', 'Exercice créé');
    }
    public function changeEtat(Request $request)
    {
        $exo = Exercice::find($request->exo_id);

        if ($exo->etat = true) {
            $exo->etat = false;
            $exo->save();
        } elseif ($exo->etat = false) {
            $exo->etat = true;
            $exo->save();
        }

        return response()->json(['success' => 'Status change successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idexercice, $idclasse)
    {
        $exercice = Exercice::findOrFail($idexercice);
        $classe = Classe::findOrFail($idclasse);
        $matiere = Matiere::findOrFail($exercice->matiere_id);

        $users = DB::table('users')
            ->where('classe_id', $classe->id)
            ->get();

        return view('exercice\show', compact('exercice', 'users', 'matiere'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Matiere $matiere, Exercice $exercice)
    {
        $matiere = Matiere::findOrFail($exercice->matiere_id);
        return view('exercice\edit', compact('exercice', 'matiere'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateEtat(Request $request)
    {
        $exercice = Exercice::find($request->id);
        if($exercice->etat == 0){
            $exercice->etat = $request->etat;
            $exercice->dateDebut = Carbon::now();   
            $exercice->dateFin = Carbon::now()->addMinutes($exercice->duree);  
            $exercice->save();
        }else{
            $exercice->etat = $request->etat;
            $exercice->dateDebut = null;   
            $exercice->dateFin = null;
            $exercice->dateActuelle = null;
            $exercice->save();
        }
        
        return response()->json();
    }
    public function update(ExerciceRequest $exercicerequest, Exercice $exercice)
    {
        $exercice->update($exercicerequest->all());
        $matiere = Matiere::find($exercice->matiere_id);
        return redirect()->route('matieres.exercices.index', $matiere->slug)->with('info', 'l\'exercice a bien été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matiere $matiere, Exercice $exercice)
    {
        $query = $exercice->id ? Exercice::whereId($exercice->id)->firstOrFail()->questions() : Question::query();
        $questions = $query->paginate(1000);
        foreach ($questions as $q) {
            if ($q->image_question != null) {
                unlink(public_path('storage/' . $q->image_question));
            }
        }
        $exercice->delete();
        return back()->with('info', 'L\'exercice a bien été supprimé dans la base de données.');
    }
}
