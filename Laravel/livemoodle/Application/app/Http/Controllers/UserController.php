<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Classe;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware(['roleChecker:prof,null']);
    }

    public function index($slug = null)
    {
        $query = $slug ? Classe::whereSlug($slug)->firstOrFail()->user() : Classe::query();
        $users = $query->paginate(10); 
        $user_all=DB::table('users')->where('classe_id',null)
                                    ->get();
        $classe =  Classe::whereSlug($slug)->get();
        return view('user\index', compact('users','classe','user_all','slug'));
    }

    public function store($id_classe,Request $request){
    $input=$request->all();
    if(isset($input['etudiant'])!=false){
        $etudiants=$input['etudiant'];
        foreach($etudiants as $etudiant_id){
            $etudiant=User::find($etudiant_id);
            $etudiant->classe_id=$id_classe;
            $etudiant->update();
        }
    }else{
        Session::flash('message', "vous essayé d'ajouter un etudiant qui n'exisite pas !");
    }
    return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $id2)
    {
       return back()->with('info', 'Eleve ajoute a la classe.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->classe_id = null;
        $user->save();
        return back()->with('info', 'Eleve supprime de la classe.');
    }
}
