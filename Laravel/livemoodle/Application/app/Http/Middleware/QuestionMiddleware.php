<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Matiere;
use App\Models\Exercice;

class QuestionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
          
        $tableau = explode('/',$request->fullUrl());
        $idexercice = $tableau[4]; // on recupere l'id de l'exercice
        $i=0;
        $bool = false;
        $bool2=false;

        if(Auth::user()->role=='eleve'){
            $classe_id=Auth::user()->classe_id;
            $matiers_id_array=array();

            $matieres_classes=DB::table('classe_matiere')->where('classe_id',$classe_id)->get();

            foreach($matieres_classes as $matiere_classe){
                $matiers_id_array[$i]=$matiere_classe->matiere_id;
                $i++;
            }

            $matieres = Matiere::whereIn('id',$matiers_id_array)->get();
            
            $exercices = Exercice::find($idexercice);
          
            if($exercices->etat == 1){
                $bool2 = true;
            }

            foreach($matieres as $matiere){
                if ($matiere->id == $exercices->matiere_id) {
                   $bool = true;
                }
            }

            if($bool && $bool2)
                return $next($request);
            else
               return response()->view('droit');
           
        }else{
            return $next($request);
        }
        
    }
        
}
