<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Matiere;

class AccesMiddleware
{
    /*
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle(Request $request, Closure $next)
    {
       //MIDDLEWARE POUR LES MATIERES
         
        $tableau = explode('/',$request->fullUrl());
        $slug = $tableau[4]; // on recupere le slug de l'url
        $i=0;
        $bool = false;

        if(Auth::user()->role=='eleve'){
            $classe_id=Auth::user()->classe_id;
            $matiers_id_array=array();

            $matieres_classes=DB::table('classe_matiere')->where('classe_id',$classe_id)->get();

            foreach($matieres_classes as $matiere_classe){
                $matiers_id_array[$i]=$matiere_classe->matiere_id;
                $i++;
            }
            $matieres = Matiere::whereIn('id',$matiers_id_array)->get();
            
            foreach($matieres as $matiere){
                if ($matiere->slug == $slug) {
                   $bool = true;
                }
            }

            if($bool)
                return $next($request);
            else
               return response()->view('droit');
           
        }else{
            return $next($request);
        }
        
    }
      
}
