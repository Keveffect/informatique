<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class RoleChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
public function handle($request, Closure $next, $prof_Role,$EleveRole)
{
    if(Auth::check()){
        $role=Auth::user()->role;
        if($role==$prof_Role){
            return $next($request);
        }else if($role==$EleveRole){
            return $next($request);
        }
    }
    return response()->view('droit');
}
}
