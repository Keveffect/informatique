<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Matiere;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Filter extends Component
{
    public $query;
    public $matieres;
    public $matieres_eleve;
    public $highlightIndex;

    public function mount()
    {
        $this->restart();
    }

    public function restart()
    {
        $this->query = '';
        $this->matieres = [];
        $this->matieres_eleve = [];
        $this->highlightIndex = 0;
    }

    public function selectmatiere()
    {
        $matiere = $this->matieres[$this->highlightIndex] ?? null;
        if ($matiere) {
            $this->redirect(route('exercices.matiere', $matiere['slug']));
        }
    }

    public function updatedQuery()
    {
        // pour prof
        $this->matieres = matiere::where('nom', 'like', '%' . $this->query . '%')
            ->get()
            ->toArray();

        $i = 0;
        $id=Auth::user()->classe_id;
        $matiers_id_array=array();
        $matieres_classes=DB::table('classe_matiere')->where('classe_id',$id)->get();
        foreach($matieres_classes as $matiere_classe){
            $matiers_id_array[$i]=$matiere_classe->matiere_id;
            $i++;
        }
        
        // pour eleve
        $this->matieres_eleve = matiere::where('nom', 'like', '%' . $this->query . '%')
            ->whereIn('id',$matiers_id_array)
            ->get()
            ->toArray();
    }

    public function render()
    {
        return view('livewire.filter');
    }
}