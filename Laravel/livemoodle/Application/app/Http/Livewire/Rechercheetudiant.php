<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use App\Models\Classe;

class Rechercheetudiant extends Component
{
    public $query;
    public $users;
    public $highlightIndex;

    public $classeId;

    public function getClasseProperty()
    {
        return Classe::find($this->classeId);
    }

    public function mount()
    {
        $this->restart();
    }

    public function restart()
    {
        $this->query = '';
        $this->users = [];
        $this->highlightIndex = 0;
    }

    public function selectuser($id)
    {
        $user = $this->users[$this->highlightIndex] ?? null;$this->users = [];
    }

    public function updatedQuery()
    {
        $this->users = User::where('email', 'like', '%' . $this->query . '%')
            ->get()
            ->toArray();
    }

    public function render()
    {
        return view('livewire.rechercheetudiant');
    }
}
