<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Matiere extends Component
{
    public $matiere;

    public function addLike(){
        auth()->user()->likes()->toggle($this->matiere->id);
    }
    
    public function render()
    {
        return view('livewire.matiere');
    }
}
