<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use App\Models\Classe;
use Livewire\Component;

class Classematiere extends Component
{
    public $classe;
    public $matiere;


    public function addclasse(){
        Classe::find($this->classe->id)->faitpartie()->toggle($this->matiere->id);
    }

    public function render()
    {
        return view('livewire.classematiere');
    }
}