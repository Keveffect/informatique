<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users', 'regex:/^(.+@etudiant.univ-reims.fr)|(.+@univ-reims.fr)$/i'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['required', 'accepted'] : '',
        ])->validate();

        $string = strtolower($input['email']);
        $role = "";
        if (str_contains($string, '@etudiant.univ-reims.fr')) {
            $role = "eleve";
        } else if(str_contains($string, '@univ-reims.fr')) {
            $role = "prof";
        }
        else{
            $role = "inconnu";
        }

        return User::create([
            'name' => $input['name'],
            'role' => $role,
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
    }
}
