<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    use HasFactory;

    protected $fillable = ['reponse'];

     /*BelongsTo (question)*/ 
     public function question(){
        return $this->belongsTo(Question::class);
    }

    public function user(){
        return $this->belongsToMany(User::class);
    }
}
