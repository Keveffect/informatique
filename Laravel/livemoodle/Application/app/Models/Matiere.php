<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matiere extends Model
{
    use HasFactory;

    /*Fillable*/
    protected $fillable = ['nom','description'];

    protected $table = "matieres";

    /*hasMany (exercices)*/
    public function exercices(){
        return $this->hasMany(Exercice::class);
    }

    public function likes(){
        return $this->belongsToMany('App\Models\User');
    }

    public function isLiked(){
        if(auth()->check())
            return auth()->user()->likes->contains('id', $this->id);
    }

    public function unlike(){
        auth()->user()->likes()->toggle($this->id);
    }

    public function faitpartie(){
        return $this->belongsToMany('App\Models\Classe');
    }
}
