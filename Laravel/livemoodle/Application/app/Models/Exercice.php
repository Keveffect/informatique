<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exercice extends Model
{
    use HasFactory;

    /*Fillable*/
    protected $fillable = ['nom','etat','duree','dateDebut','dateFin','dateActuelle'];

    /*BelongsTo (matiere)*/
    public function matiere()
    {
        return $this->belongsTo(Matiere::class);
    }

    /*hasMany (questions)*/
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /*hasMany (reponses)*/
    public function reponses()
    {
        return $this->hasMany(Reponse::class);
    }
}
