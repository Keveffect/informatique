<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = ['enonce'];

    /*BelongsTo (exercice)*/
    public function exercice()
    {
        return $this->belongsTo(Exercice::class);
    }

    /*hasOne (reponse)*/
    public function reponse()
    {
        return $this->hasOne(Reponse::class);
    }

    /*hasOne (tableau)*/
    public function tableau()
    {
        return $this->hasOne(Tableau::class);
    }
}
