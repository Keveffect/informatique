<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom'
    ];

    /*BelongsTo (user)*/ 
    public function user(){
        return $this->hasMany(User::class);
    }

    public function faitpartie(){
        return $this->belongsToMany('App\Models\Matiere');
    }

    public function isAjouter($id_matiere){

            return Classe::find($this->id)->faitpartie->contains('id', $id_matiere);
    }
}
