@extends('layout')

@section('corps')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <ul>
                    <li class="title">
                        <h3>Client</h3>
                    </li>
                    <li>
                        <p>
                            Notre association met en relation des particuliers et des proprietaires de bateaux.
                        </p>
                    </li>
                    <li class="title">
                        <h3>Proprietaire de bateaux</h3>
                    </li>
                    <li>
                        <p>
                            Inscrivez vous pour proposer des sorties a des particuliers. Le baateaux devra respecter des contraintes
                            et subire des entretiens reguliers.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
