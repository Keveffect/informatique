@extends('layout')

@section('corps')
<!-- j'ai tenté en php mais c'est pas fou
    <?php
    function AfficherMasquer($id, $id2)
        {
            if (isset($id) && isset($id2)) {
                $id= 'block';
                $id2= 'block';
            }
            else {
                $id= 'none';
                $id2= 'none';
            }
        }
    ?>
-->
<!-----------------------------------------boutons Inscription / Connexion--------------------------------------------->
    <div id="wrapper">
        <div id="page" class="container">
            <div class="button">
                <a href="/Connexion" type="button" class="btn btn-default">Connexion</a>
            </div>
            <div class="button">
                <a href="/Inscription" type="button" class="btn btn-default">Inscription</a>
            </div>
        </div>
    </div>
<!-----------------------------------------------Premier formulaire---------------------------------------------------->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <div id="wrapper">
        <div id="page" class="container">
            <p>Suite a votre inscription, vous devrez renseigner les caracteristiques de votre ou vos bateaux ci dessous.</p>
                <form method="post" action="proprietaire.blade.php">
                    <p class="titre"><strong>Caractéristiques de votre Bateau :</strong></p>
                    <fieldset>
                        <fieldset>
                            <div class="row align-items-start custom-line">
                                <div class="col" ><p id="categorie"><label>Categorie de bateau : </label> </div>
                                <div class="col"> <input type="radio" name="categorie" value="Basique" />Basique </div>
                                <div class="col"> <input type="radio" name="categorie" value="Côtier" />Côtier</div>
                                <div class="col"><input type="radio" name="categorie" value="Semi-Hauturier" />Semi-Hauturier </div>
                                <div class="col"><input type="radio" name="categorie" value="Hauturier" />Hauturier </div>
                            </div>

                            <label>Immatriculation : </label>
                            <input type="number" min="0" name="Immatriculation" size="30" class="text-black bg-dark-50" /><br />
                            <label>Nom du Bateau : </label>
                            <input type="text" name="nomBateau" size="5" /><br />
                            <label>Dimension (en mètre) : </label>
                            <input type="number" min="0" max="460" name="Dimension" size="30" /><br />
                            <label>Date de construction : </label>
                            <input type="date" min="1930-01-01" max="2019-12-15" name="anneeConstru" size="30" /><br />
                            <label>Nombre de place : </label>
                            <input type="number" min="0" max="5479" name="nbPlace" size="30" /><br />
                            <label>Date d'achat: </label>
                            <input type="date" min="1930-01-01" max="2019-12-15" name="dateAchat" size="30" /><br />
                        </fieldset>
                    <p id="buttons">
                        <input type="submit" id='boutonFormulaire' value='Envoyer' >
                        <input type="reset" id='boutonFormulaire' value='Recommencer' >
                    </p>
                    </fieldset>
                </form>
            </div>
<!-----------------------------------------------Deuxieme formulaire--------------------------------------------------->
            <div id="page" class="container">
                <form method="post" action="proprietaire.blade.php">
                    <p class="titre"><strong>Liste de Votre équipement :</strong></p>
                            <div>
                                <p id="equipement">
<!------------------------------------------------Liste Mecanique------------------------------------------------------>
                                <div class="text-center" id="mecanique">
                                    <br/>
                                    <p class="title"><strong>Equipement Mécanique</strong></p>

                                    <div class="row align-items-start custom-line">
                                        <div class="col" >
                                            <input type="checkbox" name="mecanique[]" value="caisseOutils"  onclick="AfficherMasquer('nbCaisse', 'EtatCaisse')" />Caisse à Outils
                                        </div>
                                        <div id="nbCaisse" class="col" style="display:none;"> <input type="number" min="1" size="30" placeholder="1"> </div>
                                        <div id="EtatCaisse" class="col" style="display:none;">
                                            <label>Etat : </label>
                                            <select name="Etat">
                                                <option value="bon">Bon</option>
                                                <option value="moyen">Moyen</option>
                                                <option value="mauvais">Mauvais</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row align-items-start custom-line">
                                        <div class="col">
                                            <input type="checkbox" name="mecanique[]" value="gants" onclick="AfficherMasquer('nbGants', 'EtatGants')"/>Paire de Gants
                                        </div>
                                        <div id="nbGants" style="display:none;" class="col"> <input type="number" min="1"  size="30" placeholder="1"/> </div>
                                        <div id="EtatGants" style="display:none;" class="col">
                                            <label>Etat : </label>
                                            <select name="Etat">
                                                <option value="bon">Bon</option>
                                                <option value="moyen">Moyen</option>
                                                <option value="mauvais">Mauvais</option>
                                            </select>
                                        </div>
                                    </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="mecanique[]" value="piles" onclick="AfficherMasquer('nbPiles', 'EtatPiles')" />Piles electique
                                    </div>
                                    <div id="nbPiles" style="display:none;" class="col"> <input type="number" min="1" size="30" placeholder="1"/> </div>
                                    <div id="EtatPiles" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="mecanique[]" value="coupeCircuit" onclick="AfficherMasquer('nbCoupeCircuit', 'EtatCoupeCircuit')"/>Coupe Circuit
                                    </div>
                                    <div id="nbCoupeCircuit" style="display:none;" class="col"> <input type="number" min="1" name="nbCoupeCircuit" size="30" placeholder="1"/> </div>
                                    <div id="EtatCoupeCircuit" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="mecanique[]" value="gaffe" onclick="AfficherMasquer('nbGaffe', 'etatGaffe')"/>Gaffe
                                    </div>
                                    <div id="nbGaffe" style="display:none;" class="col"> <input type="number" min="1" name="nbGaffe" size="30" placeholder="1"/> </div>
                                    <div id="etatGaffe" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="mecanique[]" value="cisaille" onclick="AfficherMasquer('nbCisaille', 'etatCisaille')"/>Cisaille
                                    </div>
                                    <div id="nbCisaille" style="display:none;" class="col"> <input type="number" min="1" name="nbCisaille" size="30" placeholder="1"/> </div>
                                    <div id="etatCisaille" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
<!------------------------------------------------Liste Survie--------------------------------------------------------->
                            <div class="text-center" id="survie">
                                <br/>
                                    <p class="title"><strong>Equipement De Survie</strong></p>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="bouee" onclick="AfficherMasquer('nbBouee', 'etatBouee')"/>Bouée
                                    </div>
                                    <div id="nbBouee" style="display:none;" class="col"> <input type="number" min="1" name="nbBouée" size="30" placeholder="1"/> </div>
                                    <div id="etatBouee" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="corde" onclick="AfficherMasquer('nbCorde', 'etatCorde')"/>Corde
                                    </div>
                                    <div id="nbCorde" style="display:none;" class="col"> <input type="number" min="1" name="nbCorde" size="30" placeholder="1"/> </div>
                                    <div id="etatCorde" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="carte" onclick="AfficherMasquer('nbCarte', 'etatCarte')"/>Carte Marine
                                    </div>
                                    <div id="nbCarte" style="display:none;" class="col"> <input type="number" min="1" name="nbCarte" size="30" placeholder="1"/> </div>
                                    <div id="etatCarte" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="lampe" onclick="AfficherMasquer('nbLampe', 'etatLampe')"/>Lampes Frontales
                                    </div>
                                    <div id="nbLampe" style="display:none;" class="col"> <input type="number" min="1" name="nbLampe" size="30" placeholder="1"/> </div>
                                    <div id="etatLampe" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="radeau" onclick="AfficherMasquer('nbSurvie', 'etatSurvie')"/>Radeau de Survie
                                    </div>
                                    <div id="nbSurvie" style="display:none;" class="col"> <input type="number" min="1" name="nbSurvie" size="30" placeholder="1"/> </div>
                                    <div id="etatSurvie" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="rame" onclick="AfficherMasquer('nbRames', 'etatRames')"/>Rames
                                    </div>
                                    <div id="nbRames" style="display:none;" class="col"> <input type="number" min="1" name="nbRames" size="30" placeholder="1"/> </div>
                                    <div id="etatRames" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="exctincteur" onclick="AfficherMasquer('nbExtincteur', 'etatExtincteur')"/>Extincteur
                                    </div>
                                    <div id="nbExtincteur" style="display:none;" class="col"> <input type="number" min="1" name="nbExtincteur" size="30" placeholder="1"/> </div>
                                    <div id="etatExtincteur" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="fusee" onclick="AfficherMasquer('nbFusee', 'etatFusee')"/>Fusée
                                    </div>
                                    <div id="nbFusee" style="display:none;" class="col"> <input type="number" min="1" name="nbFusee" size="30" placeholder="1"/> </div>
                                    <div id="etatFusee" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="gilets" onclick="AfficherMasquer('nbGilets', 'etatGilets')"/>Gilet de Sauvetage
                                    </div>
                                    <div id="nbGilets" style="display:none;" class="col"> <input type="number" min="1" name="nbGilets" size="30" placeholder="1"/> </div>
                                    <div id="etatGilets" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="radio" onclick="AfficherMasquer('nbRadio', 'etatRadio')"/>Radio VHF
                                    </div>
                                    <div id="nbRadio" style="display:none;" class="col"> <input type="number" min="1" name="nbradio" size="30" placeholder="1"/> </div>
                                    <div id="etatRadio" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="couteau" onclick="AfficherMasquer('nbCouteau', 'etatCouteau')"/>Couteau Suisse
                                    </div>
                                    <div id="nbCouteau" style="display:none;" class="col"> <input type="number" min="1" name="nbCouteau" size="30" placeholder="1"/> </div>
                                    <div id="etatCouteau" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="montre" onclick="AfficherMasquer('nbMontre', 'etatMontre')"/>Montre
                                    </div>
                                    <div id="nbMontre" style="display:none;" class="col"> <input type="number" min="1" name="nbMontre" size="30" placeholder="1"/> </div>
                                    <div id="etatMontre" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="boussole" onclick="AfficherMasquer('nbBoussole', 'etatBoussole')"/>Boussole
                                    </div>
                                    <div id="nbBoussole" style="display:none;" class="col"> <input type="number" min="1" name="nbBoussole" size="30" placeholder="1"/> </div>
                                    <div id="etatBoussole" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="couverture" onclick="AfficherMasquer('nbCouverture', 'etatCouverture')"/>Couverture de Survie
                                    </div>
                                    <div id="nbCouverture" style="display:none;" class="col"> <input type="number" min="1" name="nbCouverture" size="30" placeholder="1"/> </div>
                                    <div id="etatCouverture" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="balise" onclick="AfficherMasquer('nbBalise', 'etatBalise')"/>Balise de Detresse
                                    </div>
                                    <div id="nbBalise" style="display:none;" class="col"> <input type="number" min="1" name="nbBalise" size="30" placeholder="1"/> </div>
                                    <div id="etatBalise" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="gps" onclick="AfficherMasquer('nbGPS', 'etatGPS')"/>GPS
                                    </div>
                                    <div id="nbGPS" style="display:none;" class="col"> <input type="number" min="1" name="nGPS" size="30" placeholder="1"/> </div>
                                    <div id="etatGPS" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="survie[]" value="jumelles" onclick="AfficherMasquer('nbJumelles', 'etatJumelles')"/>Jumelles
                                    </div>
                                    <div id="nbJumelles" style="display:none;" class="col"> <input type="number" min="1" name="nbJumelles" size="30" placeholder="1"/> </div>
                                    <div id="etatJumelles" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
<!------------------------------------------------Liste Tourisme------------------------------------------------------->
                            <div class="text-center" id="tourisme">
                                <br/>
                                <p class="title"><strong>Equipement De Tourisme</strong></p>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="tourisme[]" value="combinaison" onclick="AfficherMasquer('nbCombinaison', 'etatCombinaison')"/>Combinaison de Plongée
                                    </div>
                                    <div id="nbCombinaison" style="display:none;" class="col"> <input type="number" min="1" name="nbCombinaison" size="30" placeholder="1"/> </div>
                                    <div id="etatCombinaison" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="tourisme[]" value="echelle" onclick="AfficherMasquer('nbEchelle', 'etatEchelle')"/>Echelle
                                    </div>
                                    <div id="nbEchelle" style="display:none;" class="col"> <input type="number" min="1" name="nbEchelle" size="30" placeholder="1"/> </div>
                                    <div id="etatEchelle" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="tourisme[]" value="canne" onclick="AfficherMasquer('nbCanne', 'etatCanne')"/>Canne à Pêche
                                    </div>
                                    <div id="nbCanne" style="display:none;" class="col"> <input type="number" min="1" name="nbCanne" size="30" placeholder="1"/> </div>
                                    <div id="etatCanne" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row align-items-start custom-line">
                                    <div class="col">
                                        <input type="checkbox" name="tourisme[]" value="glaciere" onclick="AfficherMasquer('nbGlaciere', 'etatGlaciere')"/>Glacière
                                    </div>
                                    <div id="nbGlaciere" style="display:none;" class="col"> <input type="number" min="1" name="nbGlaciere" size="30" placeholder="1"/> </div>
                                    <div id="etatGlaciere" style="display:none;" class="col">
                                        <label>Etat : </label>
                                        <select name="Etat">
                                            <option value="bon">Bon</option>
                                            <option value="moyen">Moyen</option>
                                            <option value="mauvais">Mauvais</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </p>
                        <p class="text-center" id="buttons">
                            <input type="submit" id='boutonFormulaire' value='Envoyer' >
                           <!-- on cache le bouton pour le moment parce qu'il pose probleme
                                <input type="reset" id='boutonFormulaire' value='Recommencer' onclick="Masquer()">
                           -->
                        </p>
                </form>
            </div>
        </div>
    </div>
@endsection
