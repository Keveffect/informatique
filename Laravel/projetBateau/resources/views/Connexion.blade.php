@extends('layout')

@section('corps')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div id="connection">
                    <form action="verification.php" method="POST">
                        <p id="identité"><label>Identité : </label>
                        <p>
                            <input type="radio" name="identité" />Client
                            <input type="radio" name="identité" />Propriétaire<br />
                        </p>
                        <label><b>Identifiant</b></label>
                        <input type="text" placeholder="Entrer l'identifiant" name="identifiant" required>

                        <label><b>Mot de passe</b></label>
                        <input type="password" placeholder="Entrer le mot de passe" name="Mot de passe" required>

                        <input type="submit" id='boutonConnection' value='CONNECTION' >
                            <?php
                            if(isset($_GET['erreur'])){
                                $err = $_GET['erreur'];
                                if($err==1 || $err==2)
                                    echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                            }
                            ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
