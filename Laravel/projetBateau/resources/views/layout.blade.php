<!DOCTYPE>
<html lang="fr">
<head>
    <meta charset=utf-8" />
    <title>Omonbatôô</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <link href="default.css" rel="stylesheet" />
    <link href="Connexion.css" rel="stylesheet"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('site.webmanifest')}}">

    <!--afficher un  element lors d'un clic sur un bouton ou autre-->
    <script type="text/javascript">

        function AfficherMasquer(id, id2)
        {
            if (document.getElementById(id).style.display === 'none' && document.getElementById(id2).style.display === 'none') {
                document.getElementById(id).style.display = 'block';
                document.getElementById(id2).style.display = 'block';
            }
            else {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id2).style.display = 'none';
            }
        }

        function Masquer()
        {
            document.getElementById().style.display = 'block';
            document.getElementById().style.display = 'block';
        }
    </script>
</head>

<body>
    <div id="header-wrapper">
        <div id="header" class="container">
            <div id="Logo">
                <h1> <a href="/">Omonbatoo</a> </h1>
            </div>
            <div id="menu">
                <ul>
                    <li class="{{Request::path() == "/" ? 'current_page_item' : ''}}"><a href="/" accesskey="1" title="">Accueil</a></li>
                    <li class="{{Request::path() == "client" ? 'current_page_item' : ''}}"><a href="/client" accesskey="2" title="">Client</a></li>
                    <li class="{{Request::path() == "proprietaire" ? 'current_page_item' : ''}}"><a href="/proprietaire" accesskey="3" title="">Proprietaire</a></li>
                    <li class="{{Request::path() == "apropos" ? 'current_page_item' : ''}}"><a href="/apropos" accesskey="4" title="">A Propos</a></li>
                    <li><a href="mailto:kevin.charlier@etudiant.univ-reims.fr?cc=samuel.bertin@etudiant.univ-reims.fr"> Contact </a></li>
                </ul>
            </div>
        </div>
    @yield('header')
    </div>
<!----------------------------------------------------------------------------------------------------------------->
    @yield('corps')
<!----------------------------------------------------------------------------------------------------------------->
    <div id="copyright" class="container">
        <p><b>Kevin Charlier | Samuel Bertin</b> 2019</p>
    </div>
</body>
</html>
