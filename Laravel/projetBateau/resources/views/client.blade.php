@extends('layout')

@section('corps')
    <div id="wrapper">
        <div id="page" class="container">
            <div class="button">
                <a href="/Connexion" type="button" class="btn btn-default">Connexion</a>
            </div>
            <div class="button">
                <a href="/Inscription" type="button" class="btn btn-default">Inscription</a>
            </div>
        </div>
    </div>
@endsection
