<?php

Route::get('/', function () {
    return view('accueil');
});

Route::get('/client', function () {
    return view('client');
});

Route::get('/proprietaire', function () {
    return view('proprietaire');
});

Route::get('/apropos', function () {
    return view('apropos');
});

Route::get('/Connexion', function () {
    return view('Connexion');
});

Route::get('/Inscription', function () {
    return view('Inscription');
});
