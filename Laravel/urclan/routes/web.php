<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
   // return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/',function(){
	$infoLan = DB::table('info_lans')->select('id','description','name','twitter','facebook','discord')->get();
		
		if (Auth::check()) {
			if($infoLan != '[]')
			{
				return view('welcome');
			}else{
				return redirect()->route('home')->with('erreur', 'Remplissez les champs dans gestion Lan !');
			}
		}else{
			if($infoLan != '[]')
			{
				return view('welcome');
			}else{
				return redirect()->route('login')->with('status', 'Connectez-vous en admin et remplissez les champs dans gestion Lan !');
			}
		}
});


Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function() {
	Route::resource('users','UsersController');
	Route::resource('infolans','InfoLanController');
	Route::post('infolans','InfoLanController@store');
});

Route::namespace('Orga')->prefix('orga')->name('orga.')->middleware('can:manage-users')->group(function() {
	Route::resource('materiels','MaterielController');
});

Route::namespace('Orga')->prefix('orga')->name('orga.')->middleware('can:seetaches')->group(function() {
	Route::resource('taches','TacheController');
	Route::post('taches','TacheController@store');
});


Route::namespace('Orga')->prefix('orga')->name('orga.')->middleware('can:seetournois')->group(function() {
	Route::resource('tournois','TournoiController');
});

Route::namespace('Orga')->prefix('orga')->name('orga.')->group(function() {
	Route::resource('games','GameController');
});

Route::namespace('Orga')->prefix('orga')->name('orga.')->middleware('can:seeclassements')->group(function() {
	Route::post('classements','ClassementController@store');
	Route::resource('classements','ClassementController');
	Route::resource('scores','ScoreController');
});

Route::namespace('Player')->prefix('player')->name('player.')->group(function() {
	Route::resource('equipes','EquipesController');
});

Route::namespace('Player')->prefix('player')->name('player.')->group(function() {
	Route::resource('equipes','EquipesController');
});

Route::get('profile','UserController@profile');
Route::post('profile','UserController@update_avatar');
Route::post('/modification-mot-de-passe','UserController@modificationMotDePasse');
