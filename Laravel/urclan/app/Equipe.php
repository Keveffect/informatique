<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function tournois()
    {
        return $this->belongsToMany('App\Tournoi');
    }

    public function score()
    {
      return $this->hasOne('App\Score');
    }
}
