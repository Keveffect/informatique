<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classement extends Model
{
	protected $table = 'classements';

	protected $guarded = [];

	public function tournoi()
	{
		return $this->belongsTo('App\Tournoi');
	}


}
