<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

class UserController extends Controller
{
    public function profile(){
    	return view('profile',array('user' => Auth::user()));
    }

    public function update_avatar(Request $request){

    	if($request->hasFile('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' . $filename ) );
    	
    		$user = Auth::user();
    		$user->avatar = $filename;
    		$user->save();

    	}
    	return back()->with('success','Votre photo de profile a bien été changé ! ');
    }
      public function modificationMotDePasse()
        {

           request()->validate([

            'password' => ['required','confirmed','min:8'],
            'password_confirmation' => ['required'],

       ]);


       auth()->user()->update([
        'password' => bcrypt(request('password')),

       ]);

      return redirect('/profile')->with('add','Votre mot de passe a bien été changé ! ');
        }
 
}