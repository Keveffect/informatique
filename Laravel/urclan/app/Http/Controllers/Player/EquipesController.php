<?php

namespace App\Http\Controllers\Player;

use App\Equipe;
use App\User;
use App\Score;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class EquipesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipes = Equipe::all();
        $user = auth()->user();

        return view('player.equipes.index',compact('equipes','user'))->with('i',(request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('player.equipes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required | unique:equipes',
        ]);

        $user = auth()->user();
        $role = Role::select('id')->where('name','joueur')->first();
        $role2 = Role::select('id')->where('name','chefEquipe')->first();

        if(Gate::denies('manage-users')){

            $user->roles()->sync($role);
            $user->roles()->attach($role2);
        }

        if(Gate::allows('manage-users')){
           
            $user->roles()->attach($role);
        }

        $equipe = Equipe::create($request->all());
        $score = Score::create();

        $equipe->score()->save($score);
        $equipe->users()->attach($user);

         return redirect()->route('player.equipes.index',compact('equipe'))->with('success','Team added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Equipe  $equipe
     * @return \Illuminate\Http\Response
     */
    public function show(Equipe $equipe)
    {
        return view('player.equipes.show',compact('equipe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Equipe  $equipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipe $equipe)
    {
        return view('player.equipes.edit',compact('equipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Equipe  $equipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equipe $equipe)
    {
        $user = auth()->user();
        $role = Role::select('id')->where('name','joueur')->first();
        $leave = Role::select('id')->where('name','utilisateur')->first();
        $leave2 = Role::select('id')->where('name','chefEquipe')->first();

        if(Gate::allows('leave-teams')){

            $user->roles()->detach($role);
            $user->roles()->detach($leave2);
            $equipe->users()->detach($user);
            $user->roles()->attach($leave);

        }else if(Gate::allows('manage-users')){
            $user->roles()->attach($role);
            $equipe->users()->attach($user);
        }else{

            $user->roles()->sync($role);
            $equipe->users()->attach($user);
        }

        return redirect()->route('player.equipes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Equipe  $equipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Equipe $equipe)
    {
        $equipe->delete();
        $user = auth()->user();
        $role = Role::select('id')->where('name','joueur')->first();
        $leave2 = Role::select('id')->where('name','chefEquipe')->first();
        $utilisateur = Role::select('id')->where('name','utilisateur')->first();
        $equipe->users()->detach($user);
        $user->roles()->detach($role);
        $user->roles()->detach($leave2);
        $user->roles()->attach($utilisateur);

  
        return redirect()->route('player.equipes.index')->with('success','Team deleted successfully');
    }
}
