<?php

namespace App\Http\Controllers\Orga;

use App\Http\Controllers\Controller;
use App\Tournoi;
use App\Equipe;
use App\Game;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TournoiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournois = Tournoi::latest()->paginate(5);
        $user = auth()->user();
        $equipes = Equipe::all();
              
        return view('orga.tournois.index',compact('tournois','user','equipes'))->with('i',(request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $games = Game::all();
        
        return view('orga.tournois.create',compact('games'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:tournois',
            'game' => 'required',
            'description' => 'required',
            'rules' => 'required',
        ]);

        Tournoi::create($request->all());
   
        return redirect()->route('orga.tournois.index')->with('success','Tournament added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tournoi  $tournoi
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tournoi = Tournoi::findOrFail($id);

        return view('orga.tournois.show',compact('tournoi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tournoi  $tournoi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tournoi = Tournoi::findOrFail($id);

        return view('orga.tournois.edit',compact('tournoi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tournoi  $tournoi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $tournoi = Tournoi::findOrFail($id);

        if(Gate::allows('manage-users')){
           
            $request->validate([
            'name' => 'required',
            'game' => 'required',
            'description' => 'required',
            'rules' => 'required',
            ]);

            $tournoi->update($request->all());
        }

        if(Gate::allows('delete-team')){
            
            $user = auth()->user();
            $equipe = $user->equipes()->pluck('equipe_id');
            $tournoi->equipes()->attach($equipe);
        }
        

        return redirect()->route('orga.tournois.index')->with('add','tournament updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tournoi  $tournoi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tournoi = Tournoi::findOrFail($id);
        
        $tournoi->delete();

         return back()->with('success','Tournament deleted successfully');
    }
}
