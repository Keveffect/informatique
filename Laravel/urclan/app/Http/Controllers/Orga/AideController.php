<?php

namespace App\Http\Controllers\Orga;

use App\Aide;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Aide  $aide
     * @return \Illuminate\Http\Response
     */
    public function show(Aide $aide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Aide  $aide
     * @return \Illuminate\Http\Response
     */
    public function edit(Aide $aide)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Aide  $aide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aide $aide)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Aide  $aide
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aide $aide)
    {
        //
    }
}
