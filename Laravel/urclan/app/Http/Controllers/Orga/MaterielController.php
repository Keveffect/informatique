<?php

namespace App\Http\Controllers\Orga;

use App\Http\Controllers\Controller;
use App\Materiel;
use Illuminate\Http\Request;

class MaterielController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materiels = Materiel::latest()->paginate(10);

        return view('orga.materiels.index',compact('materiels'))->with('i',(request()->input('page',1)-1)*10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty(request('name2')) || empty(request('quantite2'))){

        if(empty(request('name2')) && empty(request('quantite2')))
        {
            request()->validate([
            'name' => 'required|max:25',
            'quantite' => 'required',
        ]);

        }else if(empty(request('quantite2')))
        {
            request()->validate([
            'quantite' => 'required',
        ]);

        }else if(empty(request('name2'))){
            request()->validate([
            'name' => 'required|max:25',
        ]);

        }
            $name = request('name');
            $quantite = request('quantite');

        }else{

            $name = request('name2');
            $quantite = request('quantite2');
        }

        
        $description = request('description');

        $materiel = new Materiel();
        $materiel->name = $name;
        $materiel->quantite = $quantite;
        $materiel->description = $description;

        $materiel->save();

        return back()->with('add','Achat add !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function show(Materiel $materiel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function edit(Materiel $materiel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Materiel $materiel)
    {
        $quantite = request('quantite');
        if($quantite == 0){
            $materiel->etat=1;
        }

        $materiel->quantite = $quantite;
        $materiel->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Materiel  $materiel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Materiel $materiel)
    {
        $materiel->delete();

        return back()->with('success','Materiel supprimé !');
    }
}
