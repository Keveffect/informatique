<?php

namespace App\Http\Controllers\Orga;

use App\Classement;
use App\Tournoi;
use App\Score;
use App\Equipe;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class ClassementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $classements = Classement::latest()->paginate(5);
        $tournois = Tournoi::all();

        return view('orga.classements.index', compact('tournois', 'classements'))->with('i',(request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|unique:classements',
            'tournoi_id' => 'required',
        ]);

        $name = request('name');
        $tournoi_id = request('tournoi_id');

        $classement = new Classement();
        $classement->name = $name;
        $classement->tournoi_id = $tournoi_id;
        $classement->save();

        return back()->with('add','Classement add !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Classement  $classement
     * @return \Illuminate\Http\Response
     */
    public function show(Classement $classement)
    {
        return view('orga.classements.show', compact('classement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Classement  $classement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classement  $classement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classement $classement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Classement  $classement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classement $classement)
    {
        $tournoi = $classement->tournoi;
        $equipe = $tournoi->equipes;
        foreach ($equipe as $key) {
            $key->score->update(['victoire' => '0','defaite' => '0']);
        }
        
        $classement->delete();
  
        return redirect()->route('orga.classements.index')->with('success','Classement supprimé !');
    }
}
