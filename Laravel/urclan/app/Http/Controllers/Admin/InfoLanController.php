<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\InfoLan;
use Illuminate\Http\Request;

class InfoLanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infoLan = InfoLan::all();

        return view('admin.infolans.index',compact('infoLan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|unique:classements',
            'description' => 'required',
        ]);

        $name = request('name');
        $description = request('description');
        $twitter = request('twitter');
        $facebook = request('facebook');
        $discord = request('discord');

        $infoLan = new InfoLan();
        $infoLan->name = $name;
        $infoLan->description = $description;
        $infoLan->twitter = $twitter;
        $infoLan->facebook = $facebook;
        $infoLan->discord = $discord;
        $infoLan->save();

        return back()->with('ok','Maintenant que tu as nommé la LAN et que tu as remplis la description, tu peux commencer à créer le monde ! Bonne chance ! PS: Tu peux toujour changer le  TITRE !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InfoLan  $infoLan
     * @return \Illuminate\Http\Response
     */
    public function show(InfoLan $infoLan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InfoLan  $infoLan
     * @return \Illuminate\Http\Response
     */
    public function edit(InfoLan $infoLan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InfoLan  $infoLan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = request('name');
        $description = request('description');
        $twitter = request('twitter');
        $facebook = request('facebook');
        $discord = request('discord');

        $infoLan = InfoLan::find($id);
        $infoLan->update(['name'=>$name,'description'=>$description,'twitter'=>$twitter,'facebook'=>$facebook,'discord'=>$discord]);

        return back()->with('okbut','Edit success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InfoLan  $infoLan
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfoLan $infoLan)
    {
        //
    }
}
