<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournoi extends Model
{
    protected $table = 'tournois';

    protected $fillable = [
   		'name','game','description','rules'
   	];

   	public function equipes()
    {
        return $this->belongsToMany('App\Equipe');
    }

    public function classement()
    {
      return $this->hasOne(Classement::class);
    }
}
