<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoLan extends Model
{
    protected $table = 'info_lans';

	protected $guarded = [];
}
