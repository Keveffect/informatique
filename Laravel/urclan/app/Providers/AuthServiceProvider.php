<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('manage-users', function ($user) {
            return $user->hasAnyRole(['admin','organisateur']);
        });


        Gate::define('delete-team', function ($user) {
            return $user->hasAnyRole(['chefEquipe']);
        });

        Gate::define('tache', function ($user) {
            return $user->hasAnyRole(['admin','organisateur']);
        });

        Gate::define('seetaches', function ($user) {
            return $user->hasAnyRole(['admin','organisateur','staff']);
        });

        Gate::define('tournois', function ($user) {
            return $user->hasAnyRole(['admin','organisateur','staff','chefEquipe']);
        });

        Gate::define('seetournois', function ($user) {
            return $user->hasAnyRole(['admin','organisateur','staff','chefEquipe','joueur']);
        });

        Gate::define('seeclassements', function ($user) {
            return $user->hasAnyRole(['admin','organisateur','staff','chefEquipe','joueur','utilisateur']);
        });

        Gate::define('edit-users', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('join-teams', function ($user) {
            return $user->isUtilisateur();
        });

        Gate::define('leave-teams', function ($user) {
            return $user->isJoueur();
        });

        Gate::define('delete-users', function ($user) {
            return $user->isAdmin();
        });
    }
}
