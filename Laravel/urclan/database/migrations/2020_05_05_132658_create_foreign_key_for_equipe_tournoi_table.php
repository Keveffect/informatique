<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeyForEquipeTournoiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipe_tournoi', function (Blueprint $table) {
            $table->foreign('equipe_id')->references('id')->on('equipes')->onDelete('cascade');
            $table->foreign('tournoi_id')->references('id')->on('tournois')->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipe_tournoi', function (Blueprint $table) {
            $table->dropForeign('equipe_tournoi_equipe_id_foreign');
            $table->dropFForeign('equipe_tournoi_tournoi_id_foreign');
        });
    }
}
