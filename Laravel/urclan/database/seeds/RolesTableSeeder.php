<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        
        Role::create(['name'=>'admin']);
        Role::create(['name'=>'organisateur']);
        Role::create(['name'=>'staff']);
        Role::create(['name'=>'chefEquipe']);
        Role::create(['name'=>'joueur']);
        Role::create(['name'=>'utilisateur']);
    }
}
