<?php

use App\Equipe;
use Illuminate\Database\Seeder;

class EquipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Equipe::create(['name'=>'Kevin\'s team']);
        Equipe::create(['name'=>'Anthony\'s team']);
        Equipe::create(['name'=>'Lorenzo\'s team']);
        Equipe::create(['name'=>'Quentin\'s team']);
        Equipe::create(['name'=>'Benjamin\'s team']);
        Equipe::create(['name'=>'Alexandre\'s team']);
    }
}
