<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('role_user')->delete();
        
        $admin = User::create([
        	'name' => 'admin',
        	'email' => 'admin@admin.com',
        	'password' => Hash::make('password')
        ]);
        $utilisateur1 = User::create([
        	'name' => 'ut1',
        	'email' => 'ut1@ut.com',
        	'password' => Hash::make('password')
        ]);
        $utilisateur2 = User::create([
        	'name' => 'ut2',
        	'email' => 'ut2@ut.com',
        	'password' => Hash::make('password')
        ]);

        $adminRole = Role::where('name','admin')->first();
        $ut1Role = Role::where('name','utilisateur')->first();
        $ut2Role = Role::where('name','utilisateur')->first();

        $admin->roles()->attach($adminRole);
        $utilisateur1->roles()->attach($ut1Role);
        $utilisateur2->roles()->attach($ut2Role);
    }
}
