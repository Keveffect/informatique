<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

  @php
    $lan = DB::table('info_lans')->select('name','twitter','facebook','discord','description')->get();
    @endphp
    @foreach($lan as $l)
    <title>{{$l->name}}</title>
</head>

<style>
body{
  text-decoration: none;
  color: white;
  margin:0;
  padding:0;
}
a{
  color: white;
}
a:hover{
  color: orange;
}
h1{
  margin: 0;
  padding: 5% 25% 5% 44%;
  font-size: 300%;
}
section{
  margin:0;
}
p{
  margin: 0;
  padding: 0;
}
footer{
  background-color: lightgreen;
  background-size: cover;
}
header{
  background-image: url(img/processeur.jpg);
  background-size: cover;
  min-height: 100vh; 
}
#div-slide{
  padding: 1% 19%;
  height: 500px;
  display: flex;
}
#div-radio{
  padding: 1% 25% 5% 45%;
}
#input-radio{
  height: 10px;
}
.span-slide{
  font-size: larger;
}
.footer-copyright{
  background-color: grey;
}
.btn-floating:hover{
  color: grey;
}
.header-h1{
  padding: 25% 25% 25% 45%;
  
}
.logo-bloc{
  background-color: lightyellow;
  background-size: cover;
  width: auto;

}
.section-description{

  background-image: url(img/lol6.jpg);
  background-size: cover;
  background-attachment: fixed;
  width: auto;
  min-height: 100vh;

}


.div-description{
  padding: 5% 25% 5% 10%;
}

.div-actualité{
  padding: 5% 25% 5% 10%;
}
.btn btn-outline-danger{
  padding: 5% 25% 5% 10%;
}

#last{
  margin: 0 auto;
  width: 100px;
}
#verylast{
  margin: 0 auto;
  width: 100px;
}


/*-----------------------------------------------------------------------------------------------------------------------------------------*/

/* Cela ajuste la propriété de hauteur d'une boite*/
.container{
  padding-left: 55px;
}
/*classe pour changer la couleur de la navbar*/
/*caractéristiques de la navbar */
/* color rgb est la couleur des lettres */
/* text transform correspond aux majuscules */
/* border bottom correspond a la couleur et caractre de la ligne en dessus des mots*/
.navbar-light .navbar-nav .nav-link:link,
.navbar-light .navbar-nav .nav-link:visited {
  color:rgb(255, 255, 255);
  text-transform: uppercase;
  border-bottom :2px solid transparent;
  transition: border-bottom 0.5s;

}
/* correpond a la couleur quand tu mets la souris dessus et la bordure en dessous*/
.navbar-light .navbar-nav .nav-link:hover,
.navbar-light .navbar-nav .nav-link:active{
  border-bottom: 2px solid #fd7e14;
  color: orange;
}
/* j'ai ajusté la hauteur des icons réseaux sociaux*/
.text-md-right{
  height: 30px;
}
/*fonction qui permet de rendre l'image sous forme circulaire*/
.image-ronde{
  width : 40px; height : 40px;
  border: none;
  -moz-border-radius : 75px;
  -webkit-border-radius : 75px;
  border-radius : 75px;
}
/*--------------------------------------------------------------------------------------------------*/
/* Carousel */
a:not([href]) {
  /*Couleur de l'intérieur du bouton*/
    text-decoration: none;
    color: white;
  }

.btn-warning {
  /* détails du bouton noir*/
  background-color: black;
  font-size: 16px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;
  border-radius: 5px;
  text-align: center;
}
.btn:hover {
  /* change la couleur des boutons lorsqu'on pose la souris dessus*/
  background-color: orange;
}
/* .b{
  color:orange;
}*/
.mb-2{
  /*Taille des cartes*/
  height:398px;
  width: 350px;

  }
.card-img-top{
  /* ajustement de la hauteur de l'image*/
  height:50%;
}
.card-title{
  /* caractéristiques des noms de jeu*/
  text-align: center;
  color:black;
}
.card-text-test{
    text-align:center;
  font-family: Tahoma, Geneva, sans-serif;
  color:orange;
}
.card-text{
  text-align:center;
  font-family: Tahoma, Geneva, sans-serif;
  color:black;
  }
</style>


<body data-spy="scroll" data-target="#navbarSupportedContent" data-offset="100">

  <header>
 <!-- Alex taf dessus -->
<!-- Navbar -->
  <nav class="navbar navbar-expand-lg fixed-top navbar-light yo">
    <div class="container">
       <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
        </button>
     <div class="collapse navbar-collapse">
     @if (Route::has('login'))
  <!-- cette configuration permet de ranger le plus a droite possible -->
         <ul class="navbar-nav mr-auto">
         
            
            <li class="nav-item ">
              <a class="nav-link js-scrollTo" href="#Description">Description</a>
            </li>
            @auth
            <li class="nav-item ">
              <a class="nav-link js-scrollTo" href="{{ url('/home') }}">Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scrollTo" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Déconnection') }}
               </a>
               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
               </form>
            </li>
            

            <li class="nav-item dropdown ">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
             Menu <span class="caret"></span>
             </a>

             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
             <a class="dropdown-item" onclick="event.preventDefault();"</a>


            
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    <a href="{{url('/profile')}}"  class="dropdown-item"> Profile</a>
                                    @php
                                        $user = auth()->user();
                                        $equipe = DB::table('users')->where('users.id','=',$user->id)->join('equipe_user','equipe_user.user_id','=','users.id')->select('equipe_user.equipe_id')->join('equipes','equipes.id','=','equipe_user.equipe_id')->select('equipes.id')->get();
                                        $classement = DB::table('classements')->select('id','tournoi_id')->get();
                                        $game = DB::table('games')->select('id')->get();
                                        $tournoi = DB::table('tournois')->get();
                                    @endphp
                                    @forelse($equipe as $equipe)
                                    <a href="{{ route('player.equipes.show',$equipe->id) }}"  class="dropdown-item"> Mon équipe</a>
                                    @empty
                                    @endforelse
                                    @can('manage-users')
                                    <a class="dropdown-item" href="{{ route('admin.infolans.index') }}">Gestion Lan</a>
                                    <a class="dropdown-item" href="{{ route('orga.materiels.index') }}">Matériels à acheter</a>
                                    @if($game == '[]')
                                    <a class="dropdown-item" href="{{ route('orga.games.index') }}">Liste des jeux</a>
                                    @endif
                                    @if($tournoi !='[]')
                                    @if($classement == '[]')
                                    <a class="dropdown-item" href="{{ route('orga.classements.index') }}">Liste des classements</a>
                                    @endif
                                    @endif
                                    @endcan
                                    @if($game != '[]' || $tournoi !='[]')
                                    @can('seetournois')
                                    <a class="dropdown-item" href="{{ route('orga.tournois.index') }}">Liste des tournois</a>
                                    @endcan
                                    @endif
                                    @can('seeclassements')
                                    <a class="dropdown-item" href="{{ route('player.equipes.index') }}">Liste des equipes</a>
                                    @forelse($classement as $classemen)
                                    <a class="dropdown-item" href="{{ route('orga.classements.index') }}">Liste des classements</a>
                                    @empty
                                    @endforelse
                                    @endcan
                                    @can('seetaches')
                                    <a class="dropdown-item" href="{{ route('orga.taches.index') }}">Liste des taches</a>
                                    @endcan
                                    @if($game != '[]')
                                    <a class="dropdown-item" href="{{ route('orga.games.index') }}">Liste des jeux</a>
                                    @endif
                                </div>
                            </li>
      
            @else
            
            <li class="nav-item">
              <a class="nav-link js-scrollTo" href="{{ route('login') }}">Connexion</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item">
              <a class="nav-link js-scrollTo" href="{{ route('register') }}">Inscription</a>
            </li>
            
            @endif
            @endauth
            
        </ul>
        @endif
    </div>
     </div>
<!-- Logos pour les réseaux sociaux et site de l'université -->
    <div class="col-md-5 col-lg-4 ml-lg-0">
          <!-- Social buttons -->
    <div class="text-center text-md-right">
    <ul class="list-unstyled list-inline">
            <li class="list-inline-item">
                <a class="btn-floating btn-sm rgba-white-slight mx-1" href="{{$l->facebook}}" target="_blank">
                   <i class="fab fa-facebook-f"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-sm rgba-white-slight mx-1" href="{{$l->twitter}}" target="_blank">
                  <i class="fab fa-twitter"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn-floating btn-sm rgba-white-slight mx-1" href="{{$l->discord}}" target="_blank">
                  <i class="fab fa-discord"></i>
                </a> 
            </li>
        </ul>
    </div>
  </div>
</nav>
   <div class="logo-bloc"></div>
    <h1 class="header-h1" id="Bienvenue">{{$l->name}}</h1>
          <a href="{{ route('orga.games.index') }}" class="btn btn-lg btn-secondary">Voir les jeux</a>
  </header>

  <main>

    <section id="Description" class="section-description">
      <article>
        <h1>Description</h1>
          <div>
            <div id="div-slide" >
              <div  id="last" class="col-sm-8">
                <span class="span-slide">
                  {{$l->description}}
                </span>
              </div>
              
            </div>
        </div>

      </article>
    </section>

  </main>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="js/scripts.js"></script>
    <!-- Fonction utilisée pour rétrécir la barre de navigation en enlevant les rembourrages et en ajoutant un fond noir-->
  <script>
      $(window).scroll(function() {
          if ($(document).scrollTop() > 50) {
              $('.nav').addClass('affix');
              console.log("OK");
          } else {
              $('.nav').removeClass('affix');
          }
      });
  </script>
<!-- Fonction qui permet un défilement lisse -->  
  <script>
   $(document).ready(function() {
      $('.js-scrollTo').on('click', function() { // Au clic sur un élément
       var page = $(this).attr('href'); // Page cible
        var speed = 750; // Durée de l'animation (en ms)
        $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
       return false;
    });
  });
</script>
  <script src="https://kit.fontawesome.com/200562c315.js" crossorigin="anonymous"></script>
  <script src="js/main.js"></script>
</body>
@endforeach
</html>