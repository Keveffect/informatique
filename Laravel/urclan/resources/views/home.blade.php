@extends('layouts.app')

@section('css')
    
@endsection
    <style type="text/css">
        #divcenter {
            margin: 0 auto;
            width: 100px;
        }
        body{
           background: url("/img/esportv2.png");
            text-decoration: none;
            width: auto;
            background-size: cover;
            align-items: center;
            background-attachment: fixed;
        }
    </style>
@section('content')

@if (session('erreur'))
    <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Warning!</h4>
        {{ session('erreur') }}
    </div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:150px; height:150px; float: left; border-radius:50%; margin-right:50px;">
                  <div style="margin:55px">Vous êtes bien connecté </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

@php
    $infoLan = DB::table('info_lans')->select('id')->get();
@endphp
@forelse($infoLan as $info)
@empty
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Bienvenu(e) sur le site de création et gestion de Lan</h5>
      </div>
      <div class="modal-body">
        <p><p>Bonjour,<br>Pour commencer, je t'invite à aller dans l'onglet "Gestion Lan" et renseigner les différents choix.<br>Ensuite, pour l'organistion je te laisse aller dans l'onglet achat à effectuer, pour faire ta liste  de course.<br>Pour finir, tu peux commencer à ajouter tes jeux et créer tes différents tournois.<br>Bonne Lan !<br>PS : La box s'enleve des que vous avez renseigné les différentes informations donc cliquez sur Gestion de Lan ou elle restera !</p></p>
      </div>
      <div class="modal-footer">
        <a href="{{ route('admin.infolans.index') }}"><button type="button" class="btn btn-primary">Gestion Lan</button></a>
      </div>
    </div>
  </div>
@endforelse
  <!-- Footer -->
<footer class="page-footer font-small blue">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <p class="text-muted">AnthonyCAILLEAUX | KevinCHARLIER | QuentinBURETTE | AlexandreCAUTY | BenjaminCORNEC | LorenzoREIS-GRACIO</p>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
@endsection
