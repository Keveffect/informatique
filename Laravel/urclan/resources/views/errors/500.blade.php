@extends('errors::minimal')

@section('title', __('Erreur 500'))
@section('code', 'Erreur 500')
@section('message', __('Le serveur n'a pas compris  votre demande'))
