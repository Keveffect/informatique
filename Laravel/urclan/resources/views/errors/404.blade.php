@extends('errors::minimal')

@section('title', __('Erreur 404'))
@section('code', 'Erreur 404')
@section('message', __('Aucune page web de ce nom trouvé. Peut être que la page a changé de nom ou alors elle n\'existe pas'))
