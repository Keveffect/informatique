@extends('errors::minimal')

@section('title', __('Erreur 401'))
@section('code', '401')
@section('message', __('L'accès a cette page vous est délibérément interdit par l'administrateur'))
