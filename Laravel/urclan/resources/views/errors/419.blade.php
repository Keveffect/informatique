@extends('errors::minimal')

@section('title', __('Erreur 419'))
@section('code', 'Erreur 419')
@section('message', __('le delai d\'authentification a expiré'))