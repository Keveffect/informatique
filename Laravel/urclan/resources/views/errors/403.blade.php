@extends('errors::minimal')

@section('title', __('Erreur 403'))
@section('code', 'Erreur 403')
@section('message', __('Votre page est bien éxistante mais le serveur refuse l'accès au client'))
