@extends('errors::minimal')

@section('title', __('Erreur 503'))
@section('code', 'Erreur 503')
@section('message', __($exception->getMessage() ?: 'Le serveur est temporairement a l'arrêt pour maintenance'))
