@extends('layouts.app')

@section('css')
	<style type="text/css">
		#foo{
			display: none;
		}
	</style>
@endsection

@section('content')
@can('manage-users')
@if (session('success'))
    <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Warning!</h4>
        {{ session('success') }}
    </div>
@endif
@if (session('add'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Success!</h4>
        {{ session('add') }}
    </div>
@endif
@endcan
<div class="col-sm-8">
<p class="h1">Liste des taches</p>
@foreach($taches as $tache)
	<form method="POST" action="{{ route('orga.taches.destroy',$tache->id) }}">
		<div class="alert alert-dismissible alert-secondary">
			<a style="text-decoration:none" href="">{{$tache->name}}
					@can('seetaches')
						@csrf
                		@method('DELETE')
                		@php
                			$userFait = DB::table('taches')->where('taches.id',$tache->id)->join('users','taches.user_id','=','users.id')->select('users.id','taches.description','taches.name')->get();
                		@endphp
                		@foreach($userFait as $fait)
                		@if($user->id != $fait->id)
                		<button type="submit" class="close" id="foo">
  							<span aria-hidden="true">&#x221A;</span>
						</button>
                		@else
                		<button type="submit" class="close">
  							<span aria-hidden="true">&#x221A;</span>
						</button>			
                		@endif
                		@endforeach
                		@can('tache')
						<button type="submit" class="close">
  							<span aria-hidden="true">&times;</span>
						</button>
						@endcan
					@endcan
			</a>
		</div>
		@if($user->id == $fait->id)
		<div class="col-xs-12 col-sm-12 col-md-12">
           	<div class="form-group">
                <strong>Description de {{$fait->name}}:</strong>
                	{{ $fait->description }}
            </div>
        </div>
        @endif
	</form>	
	@endforeach
</div>
<div class="col-sm-8">{{ $taches->links() }}</div>
<hr>
@can('tache')
<div class="col-sm-8 ">
	<div class="card">
		 <div class="card-header">Attribuer une tache</div>
				<div class="card-body">
						<form action="#" method="POST">
						@csrf
							<div class="form-group">
								<input type="text" type="HIDDEN" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Entrez un nom" autofocus required>
								@error('name')
									<div class="invalid-feedback">
										{{	$errors->first('name')	}}
									</div>
								@enderror
							</div>
							<div class="form-group">
    							<textarea class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Description de la tache" name="description"></textarea>
    								@error('description')
										<div class="invalid-feedback">
											{{	$errors->first('description')	}}
										</div>
									@enderror
  							</div>
  							<div class="form-group">
								<select class="custom-select" name="user_id">
									@php
										$users = DB::table('roles')->where('roles.name','staff')->join('role_user','role_user.role_id','=','roles.id')->select('role_user.user_id')->join('users','users.id','=','role_user.user_id')->select('users.name','users.id')->get();
									@endphp
									<option disabled selected>Choisissez un menbre d'équipe</option>
									@foreach($users as $user)
										<option value="{{$user->id}}">{{$user->name}}</option>
									@endforeach
								</select>
							</div>
							<button type="submit" class="btn btn-primary">Attribuer</button>
						</form>
				</div>
		</div>
</div>
@endcan
@endsection
