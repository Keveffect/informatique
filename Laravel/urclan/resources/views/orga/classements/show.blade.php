@extends('layouts.app')

@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <div class="pull-left">
                <p class="h1">Classement {{ $classement->name }}</p>
                <hr>
                @php
                    $tournoi = $classement->tournoi;
                    $equipe = $tournoi->equipes;
                    $i=0;
                @endphp
                    
                   
                @foreach($equipe as $a)
                    @php $tournoi->equipes = DB::table('equipe_tournoi')->where('tournoi_id','=',$tournoi->id)->join('equipes','equipe_tournoi.equipe_id','=','equipes.id')->join('scores','scores.equipe_id','=','equipes.id')->orderBy('victoire','DESC')->select('scores.id','scores.victoire','scores.defaite','equipes.name')->get(); @endphp
                @endforeach
                
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Equipes</th>
                                <th scope="col">Score</th>
                                @can('manage-users')
                                <th scope="col">Edit</th>
                                @endcan
                            </tr>
                        </thead>
                       
                        @foreach($tournoi->equipes as $tournoiEquipe)
                        <tbody>
                            <tr>

                                <th scope="row">{{ $i=$i+1 }}</th>
                                <td>
                                    {{$tournoiEquipe->name}}
                                </td>
                                <td>
                                    {{$tournoiEquipe->victoire}}-{{$tournoiEquipe->defaite}}
                                </td>
                                @can('manage-users')
                                <td>
                                <form action="{{route('orga.scores.update',$tournoiEquipe->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="col-md-3 mb-3">
                                    
                                    <input type="number" class="form-control @error('victoire') is-invalid @enderror" name="victoire" value="{{$tournoiEquipe->victoire}}" min="0">
                                    @error('victoire')
                                        <div class="invalid-feedback">
                                            {{  $errors->first('victoire')  }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-3 mb-3">
                                    <input type="number" class="form-control @error('defaite') is-invalid @enderror" name="defaite" value="{{$tournoiEquipe->defaite}}" min="0">
                                    @error('defaite')
                                        <div class="invalid-feedback">
                                            {{  $errors->first('defaite')  }}
                                        </div>
                                    @enderror
                                    <div>
                                        <button type="submit" class="btn btn-primary">Editer</button>
                                    </div>
                                </div>
                            </form>
                                </td>
                                 @endcan
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('orga.classements.index') }}">Back</a>
            </div>
        </div>
    </div>

@endsection