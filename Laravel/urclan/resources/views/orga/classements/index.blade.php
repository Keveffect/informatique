@extends('layouts.app')

@section('content')
@can('manage-users')
@if (session('success'))
    <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Warning!</h4>
        {{ session('success') }}
    </div>
@endif
@if (session('add'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Success!</h4>
        {{ session('add') }}
    </div>
@endif
@endcan
<div class="col-sm-8">
<p class="h1">Liste classements</p>
@foreach($classements as $classement)
<div class="alert alert-dismissible alert-primary">
	<form method="POST" action="{{ route('orga.classements.destroy',$classement->id) }}">
			<a style="text-decoration:none" href="{{ route('orga.classements.show',$classement->id) }}">
				{{	$classement->name	}}
					@can('manage-users')	
						@csrf
                		@method('DELETE')
						<button type="submit" class="close">
  							<span aria-hidden="true">&times;</span>
						</button>
					@endcan
			</a>
		
	</form>	
	</div>
	@endforeach
</div>
<div class="col-sm-8">{{ $classements->links() }}</div>
<hr>
@can('manage-users')
<div class="col-sm-8 ">
	<div class="card">
		 <div class="card-header">Ajouter un classement</div>
				<div class="card-body">
						<form action="#" method="POST">
						@csrf
							<div class="form-group">
								<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Entrez un nom" autofocus required>
								@error('name')
									<div class="invalid-feedback">
										{{	$errors->first('name')	}}
									</div>
								@enderror
							</div>
							<div class="form-group">
								<select class="custom-select" name="tournoi_id">
									<option disabled selected>Choisissez un tournoi </option>
								@php
									$tes = App\Tournoi::leftjoin('classements','tournois.id','=','classements.tournoi_id')->select('tournois.name','tournoi_id','tournois.id')->get();
								@endphp
								@foreach($tes as $t)
									@if(!isset($t->tournoi_id))
										<option value="{{$t->id}}">{{$t->name}}</option>
									@endif
								@endforeach
								</select>
							</div>
							<button type="submit" class="btn btn-primary">Ajouter un classement</button>
						</form>
				</div>
		</div>
</div>
@endcan
@endsection
