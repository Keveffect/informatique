@extends('layouts.app')

@section('css')
    <style type="text/css">
        .box { 
            display: flex;
            padding-right: 10px;
        }
        


.bg,
.button {
  position: absolute;
  width: 20px;
  height: 20px;
  border-radius: 100%;
}



.bg {
  animation: pulse 1.2s ease infinite;
  background: lightgrey;
}

.button {
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  z-index: 99;
  border: none;
  background: lightgrey;
  background-size: 18px;
  cursor: pointer;
  outline: none;
}

.button a {
  position: absolute;
  color: #FFF;
  font-size: 17px;
}

@keyframes pulse {
  0% {
    transform: scale(1, 1);
  }
  50% {
    opacity: 0.3;
  }
  100% {
    transform: scale(1.5);
    opacity: 0;
  }
}
p{
    margin: 0;
    margin-left: 40px;

}
#test{
    display: flex;
    
}
.div-span{
    margin: 2% 50% 0 2%;
    background: lightgrey;
    opacity: 0.7;
    border-radius: 15px;
    width: 600px;
}
.p-span{
    margin: 1% 10%;
}
.p-span-f{

    padding-top: 2%; 
}
.p-span-d{
    padding-bottom: 2%;
    margin: 1% 10%;
}
    </style>
@endsection

@section('content')
@if (session('success'))
    <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Warning!</h4>
        {{ session('success') }}
    </div>
@endif
@if (session('add'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Success!</h4>
        {{ session('add') }}
    </div>
@endif
<div class="box">
<div class="container">
        <div class="">
            <div class="card">
                <div id="test" class="card-header"><p>Matériels necessaire</p>
                
                <div id="test" class="bg"></div>
                <div id="test" class="button"  onclick="toggle_text();"> ? </button>
                </div>
                </div>
                

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Quantite</th>
                                    <th scope="col">Etat</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($materiels as $materiel)
                                    <tr>
                                        <th scope="row"></th>
                                        <td>{{ $materiel->name }}</td>
                                        <td>{{ $materiel->quantite }}</td>
                                        <td>
                                            @if($materiel->etat == 1)
                                            <span class="badge badge-pill badge-success">Success</span>
                                            @else
                                            <span class="badge badge-pill badge-warning">Warning</span>
                                            @endif
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('orga.materiels.destroy',$materiel->id) }}">
                                            @csrf
                                            @method('DELETE')
                                                <button type="submit" class="close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </form>
                                        </td>
                                        <td>@if($materiel->etat != 1)
                                            <form action="{{route('orga.materiels.update',$materiel->id)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                             <div class="col-md-8">
                                                <input type="number" class="form-control" name="quantite" value="{{$materiel->quantite}}" max="{{$materiel->quantite}}" min="0">
                                            </div>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<div class="col-sm-5">
    <div class="card">
         <div class="card-header">Matériels necessaire</div>
                <div class="card-body">
                        <form action="#" method="POST">
                        @csrf
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="chkType">
                                <label class="custom-control-label" for="chkType">Périphérique / PC  / Autre</label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name2" placeholder="Entrez un nom" id="pType">
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" name="quantite2" placeholder="Entrez une quantité" id="pTypeq" min="0">
                            </div>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="chkType2">
                                <label class="custom-control-label" for="chkType2">Réseau</label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Entrez un nom" id="pTypeR">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{  $errors->first('name')  }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control @error('quantite') is-invalid @enderror" name="quantite" placeholder="Entrez une quantité" id="pTypeRq" min="0">
                                @error('quantite')
                                    <div class="invalid-feedback">
                                        {{  $errors->first('quantite')  }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" style="height:180px" name="description" id="pTypeRd" placeholder="Description"></textarea>
                            </div>
                            
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                        <a href="{{ route('home') }}"><button type="button" class="btn btn-secondary" data-dismiss="modal">Page home</button></a>
                    </form>
                </div>
        </div>
        {{$materiels->links()}}
    </div>
</div>
<div class="div-span">

<span id="span_txt" style="display:none;">
<p class="p-span-f">Pour ajouter du matériels choississez soit Périphérique/PC/Autre soit Réseau : </p>  
<p class="p-span">- Ensuite ajoutez lui un nom et le nombre de d'éléments voulu </p>
<p class="p-span">- Vous pouvez ensuite diminuer le nombre d'éléments au fur et à mesure des achats </p>
<p class="p-span">- Puis cliquer sur entrée pour valider la selection</p>
<p class="p-span-d">- Cliquez enfin sur la croix pour supprimer l'élément de la liste</p>
</span>


</div>

    <script>
        $(function () {
            $("#pType").hide();
            $("#pTypeq").hide();
            $("#pTypeR").hide();
            $("#pTypeRq").hide();
            $("#pTypeRd").hide();
        $("#chkType").click(function () {
            if ($(this).is(":checked")) {
                $("#pType").show();
                $("#pTypeq").show();
                $("#pTypeR").hide();
                $("#pTypeRq").hide()
                $("#pTypeRd").hide();;
            } else {
                $("#pType").hide();
                $("#pTypeq").hide();
            }
        });
        $("#chkType2").click(function () {
            if ($(this).is(":checked")) {
                $("#pTypeR").show();
                $("#pTypeRq").show();
                $("#pTypeRd").show();
                $("#pType").hide();
                $("#pTypeq").hide();
            } else {
                $("#pTypeR").hide();
                $("#pTypeRq").hide();
                $("#pTypeRd").hide();
            }
        });
    });

        function toggle_text() {
    var span = document.getElementById("span_txt");
    if(span.style.display == "none") {
        span.style.display = "inline";
    } else {
        span.style.display = "none";
    }
    }
    </script>
@endsection