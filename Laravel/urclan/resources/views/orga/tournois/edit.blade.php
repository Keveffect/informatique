@extends('layouts.app')

@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <div class="pull-left">
                @can('manage-users')
                <h2>Edit tournament</h2>
                @endcan
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('orga.tournois.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check input field code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-xs-12 col-sm-12 col-md-12">
    <form action="{{ route('orga.tournois.update',$tournoi) }}" method="POST">
        @csrf
        @method('PUT')
        
        @can('manage-users')        
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom:</strong>
                    <input type="text" name="name" value="{{ $tournoi->name }}" class="form-control">
                </div>
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Game:</strong>
                <input type="text" name="game" value="{{ $tournoi->game }}" class="form-control"  readOnly="readOnly">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    <textarea class="form-control" style="height:180px" name="description">{{ $tournoi->description }}</textarea>
                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Regles:</strong>
                    <textarea class="form-control" style="height:180px" name="rules">{{ $tournoi->rules }}</textarea>
                </div>
        </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        @endcan
        @can('delete-team')
            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom:</strong>
                    <input type="text" name="name" value="{{ $tournoi->name }}" class="form-control" readonly="readonly">
                </div>
            </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        @endcan
        </form>
    </div>

@endsection