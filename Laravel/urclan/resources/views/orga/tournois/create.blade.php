@extends('layouts.app')


@section('content')
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        <div class="pull-left">
            <h1>Create New Tournament</h1>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Warning!</strong> Please check your input code<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('orga.tournois.store') }}" method="POST">
    @csrf
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Nom">
            </div>
        </div>
     <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Game:</strong>
                <select class="form-control" name="game">
                @foreach ($games as $game)
                <option  value="{{ $game->name }}">{{ $game->name }}</option>
                @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                <textarea class="form-control" style="height:100px" name="description" placeholder="Description"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Regles:</strong>
                <textarea class="form-control" style="height:100px" name="rules" placeholder="Regles"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Valider</button>
                <a href="{{ route('orga.tournois.index') }}"><button type="button" class="btn btn-secondary" data-dismiss="modal">Retour</button></a>
        </div>
    </div>
   
</form>

@endsection

