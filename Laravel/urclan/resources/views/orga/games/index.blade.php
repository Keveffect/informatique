@extends('layouts.app')
@section('css')
<style>
body{
    text-decoration: none;
    min-height: 100vh;
}
#input-text{
    background: #F2EFEF;
}
#div-gen{
    padding: 5% 15%;
}
#div-check-all-games{
    text-align: center;
    margin-top: 5%;
}
#btn-submit{
    margin: 16% 5%;
    padding: 3% 8%; 
}
#div-card{
    background: #F2EFEF ;
    font-size:150%;
    height: 250px;
    margin: 4.9%;
}
#card-h5{
    color: black;
    text-align: center;
    margin: 5% 2%;
}
#card-p{
    color: black;
    text-align: center;
    margin: 10% 2% 0 2%;
}
#a-show{
    margin: 16% 5%;
    padding: 3% 6%;
}
h1{
    font-size: 300%;
}
#div-btn-add{
    padding-top: 1%;
}

#center-div{
    margin:0 auto;
    width: 100px;
}

</style>
@endsection
@section('content')



    <div id="div-check-all-games" class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <div class="pull-left">
                <h1>Games</h1>
            </div>
            @can('manage-users')
            <div id="div-btn-add" class="pull-right">
                <a class="btn btn-primary" href="{{ route('orga.games.create') }}"> Ajouter un game</a>
            </div>
            @endcan
        </div>
    </div>
    @if (session('success'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Success!</h4>
        {{ session('success') }}
    </div>
    @endif
    @if (session('add'))
    <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Success!</h4>
        {{ session('add') }}
    </div>
    @endif

    <div id="div-flex" class="d-flex flex-wrap">

    @foreach ($games as $game)

        <div id="div-card" class="card" style="width: 18rem;">
        
            <div class="card-body">
                
                <h5 id="card-h5" class="card-title">{{ $game->name }}</h5>

                <p id="card-p" class="card-text">version : {{ $game->version }}</p>

               
                <form action="{{ route('orga.games.destroy',$game->id) }}" method="POST">
                    <a id="a-show" class="btn btn-success" href="{{ route('orga.games.show',$game->id) }}">
                    <i class="far fa-eye"></i>
                    </a>
                     @can('manage-users')
                    <a id="a-show" class="btn btn-warning" href="{{ route('orga.games.edit',$game->id) }}">
                    <i class="far fa-edit"></i>
                    </a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger" id="a-show">
                    <i class="far fa-trash-alt"></i>
                    </button>
                </form>
                @endcan
            </div>
        </div>
    @endforeach
    </div>

  <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', () => {
        (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
            $notification = $delete.parentNode;

            $delete.addEventListener('click', () => {
                $notification.parentNode.removeChild($notification);
            });
        });
    });
  </script>

  <div id="center-div">{{ $games->links() }}</div>
            
      
@endsection

