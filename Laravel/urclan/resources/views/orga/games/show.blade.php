@extends('layouts.app')
@section('css')
    <style>
        body{
           background: url("/img/esportv2.png");
            text-decoration: none;
            width: auto;
            background-size: cover;
            align-items: center;
            background-attachment: fixed;
        }

        .row{
            padding-top: 200px;;
        }
        .btn:hover{
            color: white;
            background-color: deepskyblue;
        }
        .pull-right{
            margin-top: 80px;
        }

        h2{
            font-weight: bold;
            font-family: Bookman, URW Bookman L, serif;
        }

        p{
            font-family: Bookman, URW Bookman L, serif;
            color: white;
        }

    </style>
@endsection
@section('content')
    <!-- *********** Presentation ************ -->
    <div class="container" id="pres">
        <div class="row">
            <div class="col-12 mb-4"><h2>Qu'est ce que {{ $game->name }}  ?</h2></div>
            <div class="col-12 col-md-6">
                <br><br>
                <div class="media">
                    <span><i class="fas fa-gamepad"></i></span>
                    <div class="media-body ml-3">
                        <h5><strong><u> Présentation </u></strong></h5>
                        <p class="text-justify">{{ $game->description }}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <br><br>
                <div class="media">
                    <span><i class="fas fa-network-wired"></i></span>
                    <div class="media-body ml-3">
                        <h5><strong><u> Réseau </u></strong></h5>
                        <p class="text-justify"><strong> Version : </strong>{{ $game->version }}<br><strong> Port Réseau : </strong>{{ $game->portReseau }}</p>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <br><br><br>
                <a class="btn btn-primary" href="{{ route('orga.games.index') }}">Retour</a>
                <a href="{{ route('home') }}"><button type="button" class="btn btn-secondary" data-dismiss="modal">Page home</button></a>
            </div>
        </div>
    </div>
@endsection