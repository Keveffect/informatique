@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Erreur !</strong> Verifiez vos informations<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        <div class="pull-left">
            <h1>Create New Game</h1>
        </div>
    </div>
</div>
       <div id="div-gen" class="col-xs-12 col-sm-12 col-md-12">
    <form action="{{ route('orga.games.store') }}" method="POST">
        @csrf
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <p class="text-pa"> Nom :</p>
                    <input type="text" name="name" id="input-text" class="form-control" placeholder="Nom">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <p class="text-pa">Version :</p>
                    <input type="number" name="version" id="input-text" class="form-control" placeholder="Port reseau">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <p class="text-pa">Port réseau :</p>
                    <input type="number" name="portReseau" id="input-text"  class="form-control" placeholder="Nombres d'inscriptions">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <p class="text-pa">Description :</p>
                    <textarea class="form-control" id="input-text" style="height:180px" name="description" placeholder="Description"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <a href="{{ route('orga.games.index') }}"><button type="button" class="btn btn-secondary" data-dismiss="modal">Retour</button></a>

                    <button id="btn-submit" type="submit" class="btn btn-primary">Valider</button>
                    
            </div>

        </div>
    </form>
        </div> 
@endsection