@extends('layouts.app')

@section('css')
<style>
body{
    margin:0;
    padding:0;
    background: lightgrey;
    min-height: 100vh;
}
#input-text{
    background: #F2EFEF;
}
h2{
    font-family:Times New Roman;
    font-size:50px;
}
h1{
  margin: 0;
  padding: 5% 25% 5% 45%;
  font-size: 300%;
}
#div-gen{
    background: lightgrey;
    padding: 5% 15%;
}
.btn-warning {
  /* détails du bouton noir*/
  color:white;
  background-color: black;
  font-size: 16px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;
  border-radius: 5px;
  text-align: center;
}
.btn:hover {
  /* change la couleur des boutons lorsqu'on pose la souris dessus*/
  background-color: orange;
  color:white;
}
.text-pa{
    color:white;
}
#div-edit{
    background: lightgrey;
    text-align: center;
}
#h2_edit{
    color: black;
}


</style>
@endsection
@section('content')
    <div id="div-edit" class="col-xs-12 col-sm-12 col-md-12">
        <h2 id="h2-edit">Editez game</h2>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
        <strong>Erreur !</strong> Verifiez vos informations<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <div id="div-gen" class="col-xs-12 col-sm-12 col-md-12">
    <form action="{{ route('orga.games.update',$game->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <p class="text-pa"> Nom :</p>
                    <input type="text" id="input-text" name="name" value="{{ $game->name }}" class="form-control">
                </div>
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <p class="text-pa">Version :</p>
                <input type="number" id="input-text" name="version" value="{{ $game->version }}" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <p class="text-pa">Port réseau :</p>
                <input type="number" id="input-text" name="portReseau" value="{{ $game->portReseau }}" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <p class="text-pa">Description :</p>
                    <textarea id="input-text" class="form-control" style="height:180px" name="description">{{ $game->description }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <a class="btn btn-warning" href="{{ route('orga.games.index') }}"> Retour</a>
              <button id="btn-submit" type="submit" class="btn btn-warning">Valider</button>
            </div>
        </div>
    </form>
        </div> 
@endsection
