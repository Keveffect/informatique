@extends('layouts.app')
@section('css')
    <style type="text/css">
        #marg{
            margin: 2% 0 2% 21.5%;
        }
    </style>
@endsection
@section('content')
@if (session('success'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Succes!</h4>
        {{ session('success') }}
    </div>
@endif
@if (session('add'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Succes!</h4>
        {{ session('add') }}
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float: left; border-radius:50%; margin-right:50px;">
            <h2>Mon profil : {{ $user->name}}</h2></br>
            <h6>Votre adresse mail : {{ $user->email}}</h6>
            <form enctype="multipart/form-data" action="/profile" method="POST">
                <label >Update Profile Image</label></br>
                <input type="file" name="avatar"></br>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="pull-right btn btn-sm btn-primary" id="marg">
            </form>
            </br>
        </div>
    </div>
</div>
</br><hr>
<center>
        <form action="/modification-mot-de-passe" method="POST">
              {{ csrf_field() }}
            <div class="form-group col-md-6 col-sm-12">
                <label>Nouveau mot de passe :</label>
                <input type="password" name="password" class="form-control">
                     <small id="emailHelp" class="form-text text-muted">Vous changez votre mot de passe</small>
            </div>
            @if($errors->has('password'))
                    <p class="help is-danger">{{ $errors->first('password') }}</p>
            @endif
            <div class="form-group col-md-6 col-sm-12">
                <label>Mot de passe de confirmation</label>
                <input type="password" name="password_confirmation" class="form-control">
            </div>
            @if($errors->has('password_confirmation'))
                <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
            @endif
              <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </center>
@endsection
