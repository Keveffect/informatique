@extends('layouts.app')

@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <div class="pull-left">
                <h1>Check all Teams</h1>
            </div>
            <div class="pull-right">
                @can('join-teams')
                <a class="btn btn-success" href="{{ route('player.equipes.create') }}"> Add new team</a>
                @endcan
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
       <div class="notification is-success alert alert-success">
            <button class="delete"></button>
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th></th>
            <th>Name</th>
            <th width="250px">Action</th>
        </tr>
        @foreach ($equipes as $equipe)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $equipe->name }}</td>
            <td>
                <form action="{{ route('player.equipes.destroy',$equipe->id) }}" method="POST">
                    
                    <a class="btn btn-info" href="{{ route('player.equipes.show',$equipe->id) }}">Show</a>

                    @can('join-teams')
                        <a class="btn btn-primary" href="{{ route('player.equipes.edit',$equipe->id) }}">Join</a>
                    @endcan 

                    
                    @foreach($user->equipes as $equipeUser)
                    @if($equipeUser->id === $equipe->id )
                    <a class="btn btn-warning" href="{{ route('player.equipes.edit',$equipe->id) }}">Leave</a>
                    @endif
                    @endforeach

                    @csrf
                    @method('DELETE')
                    
                    @can('delete-team')
                    @foreach($user->equipes as $equipeUser)
                    @if($equipeUser->id === $equipe->id )
                    <button type="submit" class="btn btn-danger">Delete</button>
                    @endif
                    @endforeach
                    @endcan

                </form>
            </td>
        </tr>
        @endforeach
    </table>
  <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', () => {
        (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
            $notification = $delete.parentNode;

            $delete.addEventListener('click', () => {
                $notification.parentNode.removeChild($notification);
            });
        });
    });
  </script>
      
@endsection
