@extends('layouts.app')

@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <div class="pull-left">
                @can('join-teams')
                <h2>Join team</h2>
                @endcan
                @can('leave-teams')
                <h2>Leave team</h2>
                @endcan
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('player.equipes.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check input field code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <div class="col-xs-12 col-sm-12 col-md-12">
    <form action="{{ route('player.equipes.update',$equipe->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom:</strong>
                    <input type="text" name="name" value="{{ $equipe->name }}" class="form-control" readonly="readonly">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
        </div> 
@endsection