@extends('layouts.app')
@section('css')
    <style>
        body{
           background: url("/img/esportv2.png");
            text-decoration: none;
            width: auto;
            background-size: cover;
            align-items: center;
            background-attachment: fixed;
        }

        .row{
            padding-top: 200px;;
        }
        .btn:hover{
            color: white;
            background-color: deepskyblue;
        }
        .pull-right{
            margin-top: 80px;
        }

        h2{
            font-weight: bold;
            font-family: Bookman, URW Bookman L, serif;
        }

        p{
            font-family: Bookman, URW Bookman L, serif;
            color: white;
        }

    </style>
@endsection
@section('content')
    <div class="container" id="pres">
        <div class="row">
            <div class="col-12 mb-4"><h2>Mon équipe  : {{ $equipe->name  }} </h2></div>
            <div class="col-12 col-md-6">
                <br><br>
                <div class="media">
                    <span><i class="fas fa-gamepad"></i></span>
                    <div class="media-body ml-3">
                        <h5><strong><u> Joueurs </u></strong></h5>
                       <div class="col-xs-12 col-sm-12 col-md-12">
                         @foreach($equipe->users as $equipeUser)
                            <div class="form-group">
                                <img src="/uploads/avatars/{{ $equipeUser->avatar }}" style="width:32px; height:32px; top:10px; left:10px; border-radius:50%; ">
                                {{ $equipeUser->name  }}
                            </div>
                            @endforeach
                        </div> 
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <br><br><br>
                <a class="btn btn-primary" href="{{ route('player.equipes.index') }}">Voir les equipes</a>
                <a href="{{ route('home') }}"><button type="button" class="btn btn-secondary" data-dismiss="modal">Page home</button></a>
            </div>
        </div>
    </div>
@endsection