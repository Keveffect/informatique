@extends('layouts.app')

@section('content')
@if (session('ok'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Success!</h4>
        {{ session('ok') }}
    </div>
@endif 
@if (session('okbut'))
    <div class="alert alert-dismissible alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Edit Success!</h4>
        {{ session('okbut') }}
    </div>
@endif
@forelse($infoLan as $info)
<div class="row justify-content-center">
<div class="col-sm-8">
    <div class="card">
         <div class="card-header">Editer Info Lan</div>
            <div class="card-body">
                <form action="{{route('admin.infolans.update',$info->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Nom de la Lan :</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Entrez un nom" value="{{ $info->name }}" autofocus required>
                        @error('name')
                            <div class="invalid-feedback">
                                {{  $errors->first('name')  }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Description de la Lan :</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Description de la tache" name="description">{{ $info->description }}</textarea>
                        @error('description')
                            <div class="invalid-feedback">
                                {{  $errors->first('description')   }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="twitter">Twitter :</label>
                        <input type="text" class="form-control" name="twitter" placeholder="Entrez le twitter" value="{{ $info->twitter }}">
                    </div>
                    <div class="form-group">
                        <label for="facebook">facebook :</label>
                        <input type="text" class="form-control" name="facebook" placeholder="Entrez le facebook" value="{{ $info->facebook }}">
                    </div>
                    <div class="form-group">
                        <label for="twitter">Discord :</label>
                        <input type="text" class="form-control" name="discord" placeholder="Entrez le discord" value="{{ $info->discord }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Attribuer</button>
                    <a href="{{ route('home') }}"><button type="button" class="btn btn-secondary" data-dismiss="modal">Page home</button></a>
                </form>
        </div>
    </div>
</div>
</div>
@empty
<div class="row justify-content-center">
<div class="col-sm-8">
    <div class="card">
         <div class="card-header">Info Lan</div>
            <div class="card-body">
                <form action="#" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nom de la Lan :</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Entrez un nom" autofocus required>
                        @error('name')
                            <div class="invalid-feedback">
                                {{  $errors->first('name')  }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Description de la Lan :</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Description de la tache" name="description"></textarea>
                        @error('description')
                            <div class="invalid-feedback">
                                {{  $errors->first('description')   }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="twitter">Twitter :</label>
                        <input type="text" class="form-control" name="twitter" placeholder="Entrez le twitter">
                    </div>
                    <div class="form-group">
                        <label for="facebook">facebook :</label>
                        <input type="text" class="form-control" name="facebook" placeholder="Entrez le facebook">
                    </div>
                    <div class="form-group">
                        <label for="twitter">Discord :</label>
                        <input type="text" class="form-control" name="discord" placeholder="Entrez le discord">
                    </div>
                    <button type="submit" class="btn btn-primary">Attribuer</button>
                </form>
        </div>
    </div>
</div>
</div>
@endforelse
@endsection