<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @php
        $titre = DB::table('info_lans')->select('name')->get();
    @endphp
    @forelse($titre as $t)
        <title>{{$t->name}}</title>
    @empty
        <title>ADMIN</title>
    @endforelse

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/200562c315.js" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/darkly/bootstrap.min.css">
    <style type="text/css">
        body{
           background: url("/img/esportv2.png");
            text-decoration: none;
            width: auto;
            background-size: cover;
            align-items: center;
            background-attachment: fixed;
        }
    </style>
    @yield('css')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Accueil
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                @auth
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                                <a class="nav-link " href="{{ route('home') }}" role="button" d aria-haspopup="true" aria-expanded="false" v-pre>
                                    Home
                                </a>
                            </li>
                            @endauth
                                @can('manage-users')
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Utilisateurs<span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    <a class="nav-link" href="{{ route('admin.users.index')  }}">Liste utilisateurs</a>
                                    <a class="nav-link" href="{{ route('orga.taches.index')   }}">Liste des taches</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Tournois <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    <a class="dropdown-item" href="{{ route('orga.tournois.index') }}">Liste des tournois</a>
                                    <a class="dropdown-item" href="{{ route('player.equipes.index') }}">Liste des equipes</a>
                                    <a class="dropdown-item" href="{{ route('orga.classements.index') }}">Liste des classements</a>
                                </div>
                            </li>
                               @endcan
                </ul>
            </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative; padding-left: 50px;"v-pre>
                                    <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:10px; border-radius:50%; ">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    <a href="{{url('/profile')}}"  class="dropdown-item"> Profile</a>
                                    @php
                                        $user = auth()->user();
                                        $equipe = DB::table('users')->where('users.id','=',$user->id)->join('equipe_user','equipe_user.user_id','=','users.id')->select('equipe_user.equipe_id')->join('equipes','equipes.id','=','equipe_user.equipe_id')->select('equipes.id')->get();
                                        $classement = DB::table('classements')->select('id','tournoi_id')->get();
                                        $game = DB::table('games')->select('id')->get();
                                        $tournoi = DB::table('tournois')->get();
                                    @endphp
                                    @forelse($equipe as $equipe)
                                    <a href="{{ route('player.equipes.show',$equipe->id) }}"  class="dropdown-item"> Mon équipe</a>
                                    @empty
                                    @endforelse
                                    @can('manage-users')
                                    <a class="dropdown-item" href="{{ route('admin.infolans.index') }}">Gestion Lan</a>
                                    <a class="dropdown-item" href="{{ route('orga.materiels.index') }}">Matériels à acheter</a>
                                    @if($game == '[]')
                                    <a class="dropdown-item" href="{{ route('orga.games.index') }}">Liste des jeux</a>
                                    @endif
                                    @if($tournoi !='[]')
                                    @if($classement == '[]')
                                    <a class="dropdown-item" href="{{ route('orga.classements.index') }}">Liste des classements</a>
                                    @endif
                                    @endif
                                    @endcan
                                    @if($game != '[]' || $tournoi !='[]')
                                    @can('seetournois')
                                    <a class="dropdown-item" href="{{ route('orga.tournois.index') }}">Liste des tournois</a>
                                    @endcan
                                    @endif
                                    @can('seeclassements')
                                    <a class="dropdown-item" href="{{ route('player.equipes.index') }}">Liste des equipes</a>
                                    @forelse($classement as $classemen)
                                    <a class="dropdown-item" href="{{ route('orga.classements.index') }}">Liste des classements</a>
                                    @empty
                                    @endforelse
                                    @endcan
                                    @can('seetaches')
                                    <a class="dropdown-item" href="{{ route('orga.taches.index') }}">Liste des taches</a>
                                    @endcan
                                    @if($game != '[]')
                                    <a class="dropdown-item" href="{{ route('orga.games.index') }}">Liste des jeux</a>
                                    @endif
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
