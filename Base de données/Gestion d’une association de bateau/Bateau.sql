DROP TABLE IF EXISTS Marque_bateau ;
CREATE TABLE Marque_bateau (Id_marque_bateau BIGINT  AUTO_INCREMENT NOT NULL,
MB_date_creation_modele DATE,
MB_nom_marque VARCHAR(50),
PRIMARY KEY (Id_marque_bateau) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Proprietaires ;
CREATE TABLE Proprietaires (Id_Proprietaire BIGINT  AUTO_INCREMENT NOT NULL,
P_nom VARCHAR(50),
P_prenom VARCHAR(50),
P_dateDeNaissance DATE,
P_adresse VARCHAR(50),
PRIMARY KEY (Id_Proprietaire) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Bateau ;
CREATE TABLE Bateau (Immatriculation INT  AUTO_INCREMENT NOT NULL,
B_nom VARCHAR(50),
B_largeur INT,
B_annee_constru INT,
B_nb_place INT,
B_date_achat DATE,
B_longueur INT,
Id_marque_bateau BIGINT,
Id_cat_bateau BIGINT,
PRIMARY KEY (Immatriculation) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Fournisseur ;
CREATE TABLE Fournisseur (Id_fournisseur BIGINT  AUTO_INCREMENT NOT NULL,
F_nom VARCHAR(50),
F_prenom VARCHAR(50),
F_adresse VARCHAR(50),
PRIMARY KEY (Id_fournisseur) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Moteur ;
CREATE TABLE Moteur (Id_Moteur BIGINT  AUTO_INCREMENT NOT NULL,
M_etat TINYINT,
M_nb_chevaux INT,
M_date_achat DATE,
Immatriculation INT,
Id_marque_moteur BIGINT,
PRIMARY KEY (Id_Moteur) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Entretien ;
CREATE TABLE Entretien (Id_entretient BIGINT  AUTO_INCREMENT NOT NULL,
ET_echeance INT,
ET_periodicite INT,
ET_date DATE,
ET_horametre INT,
Immatriculation INT NOT NULL,
id_type_entretien BIGINT NOT NULL,
PRIMARY KEY (Id_entretient) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Marque_moteur ;
CREATE TABLE Marque_moteur (Id_marque_moteur BIGINT  AUTO_INCREMENT NOT NULL,
MM_nom_marque VARCHAR(50),
MM_date_creation DATE,
PRIMARY KEY (Id_marque_moteur) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Cat_bateau ;
CREATE TABLE Cat_bateau (Id_cat_bateau BIGINT  AUTO_INCREMENT NOT NULL,
CB_nom_cat_bateau VARCHAR(50),
Id_cat_navigation BIGINT NOT NULL,
PRIMARY KEY (Id_cat_bateau) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Equipement ;
CREATE TABLE Equipement (Id_equipement BIGINT  AUTO_INCREMENT NOT NULL,
E_etat TINYINT,
E_nom VARCHAR(50),
E_reglementation VARCHAR(50),
E_date_achat DATE,
E_duree_de_vie_annee INT,
Immatriculation INT NOT NULL,
PRIMARY KEY (Id_equipement) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Cat_navigation ;
CREATE TABLE Cat_navigation (Id_cat_navigation BIGINT  AUTO_INCREMENT NOT NULL,
CN_nom_cat_nav VARCHAR(50),
PRIMARY KEY (Id_cat_navigation) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Piece ;
CREATE TABLE Piece (Id_piece BIGINT  AUTO_INCREMENT NOT NULL,
PI_nom VARCHAR(50),
PI_date_achat DATE,
Immatriculation INT NOT NULL,
Id_Moteur BIGINT NOT NULL,
PRIMARY KEY (Id_piece) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Type_entretien ;
CREATE TABLE Type_entretien (id_type_entretien BIGINT  AUTO_INCREMENT NOT NULL,
TE_reparation TINYINT,
TE_changement_piece TINYINT,
TE_verification TINYINT,
PRIMARY KEY (id_type_entretien) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Detient ;
CREATE TABLE Detient (Id_Proprietaire BIGINT  AUTO_INCREMENT NOT NULL,
Immatriculation INT NOT NULL,
PRIMARY KEY (Id_Proprietaire,
 Immatriculation) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Fait ;
CREATE TABLE Fait (Id_Proprietaire BIGINT  AUTO_INCREMENT NOT NULL,
Id_entretient BIGINT NOT NULL,
PRIMARY KEY (Id_Proprietaire,
 Id_entretient) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Accorde ;
CREATE TABLE Accorde (Id_entretient BIGINT  AUTO_INCREMENT NOT NULL,
Id_fournisseur BIGINT NOT NULL,
PRIMARY KEY (Id_entretient,
 Id_fournisseur) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Fournit ;
CREATE TABLE Fournit (Id_equipement BIGINT  AUTO_INCREMENT NOT NULL,
Id_fournisseur BIGINT NOT NULL,
F_quantitee INT,
PRIMARY KEY (Id_equipement,
 Id_fournisseur) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS A_des ;
CREATE TABLE A_des (Id_equipement BIGINT  AUTO_INCREMENT NOT NULL,
Id_Proprietaire BIGINT NOT NULL,
PRIMARY KEY (Id_equipement,
 Id_Proprietaire) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Echange ;
CREATE TABLE Echange (Id_entretient BIGINT  AUTO_INCREMENT NOT NULL,
Id_piece BIGINT NOT NULL,
PRIMARY KEY (Id_entretient,
 Id_piece) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Remplace ;
CREATE TABLE Remplace (Id_equipement BIGINT  AUTO_INCREMENT NOT NULL,
Id_entretient BIGINT NOT NULL,
PRIMARY KEY (Id_equipement,
 Id_entretient) ) ENGINE=InnoDB;

ALTER TABLE Bateau ADD CONSTRAINT FK_Bateau_Id_marque_bateau FOREIGN KEY (Id_marque_bateau) REFERENCES Marque_bateau (Id_marque_bateau);

ALTER TABLE Bateau ADD CONSTRAINT FK_Bateau_Id_cat_bateau FOREIGN KEY (Id_cat_bateau) REFERENCES Cat_bateau (Id_cat_bateau);
ALTER TABLE Moteur ADD CONSTRAINT FK_Moteur_Immatriculation FOREIGN KEY (Immatriculation) REFERENCES Bateau (Immatriculation);
ALTER TABLE Moteur ADD CONSTRAINT FK_Moteur_Id_marque_moteur FOREIGN KEY (Id_marque_moteur) REFERENCES Marque_moteur (Id_marque_moteur);
ALTER TABLE Entretien ADD CONSTRAINT FK_Entretien_Immatriculation FOREIGN KEY (Immatriculation) REFERENCES Bateau (Immatriculation);
ALTER TABLE Entretien ADD CONSTRAINT FK_Entretien_id_type_entretien FOREIGN KEY (id_type_entretien) REFERENCES Type_entretien (id_type_entretien);
ALTER TABLE Cat_bateau ADD CONSTRAINT FK_Cat_bateau_Id_cat_navigation FOREIGN KEY (Id_cat_navigation) REFERENCES Cat_navigation (Id_cat_navigation);
ALTER TABLE Equipement ADD CONSTRAINT FK_Equipement_Immatriculation FOREIGN KEY (Immatriculation) REFERENCES Bateau (Immatriculation);
ALTER TABLE Piece ADD CONSTRAINT FK_Piece_Immatriculation FOREIGN KEY (Immatriculation) REFERENCES Bateau (Immatriculation);
ALTER TABLE Piece ADD CONSTRAINT FK_Piece_Id_Moteur FOREIGN KEY (Id_Moteur) REFERENCES Moteur (Id_Moteur);
ALTER TABLE Detient ADD CONSTRAINT FK_Detient_Id_Proprietaire FOREIGN KEY (Id_Proprietaire) REFERENCES Proprietaires (Id_Proprietaire);
ALTER TABLE Detient ADD CONSTRAINT FK_Detient_Immatriculation FOREIGN KEY (Immatriculation) REFERENCES Bateau (Immatriculation);
ALTER TABLE Fait ADD CONSTRAINT FK_Fait_Id_Proprietaire FOREIGN KEY (Id_Proprietaire) REFERENCES Proprietaires (Id_Proprietaire);
ALTER TABLE Fait ADD CONSTRAINT FK_Fait_Id_entretient FOREIGN KEY (Id_entretient) REFERENCES Entretien (Id_entretient);
ALTER TABLE Accorde ADD CONSTRAINT FK_Accorde_Id_entretient FOREIGN KEY (Id_entretient) REFERENCES Entretien (Id_entretient);
ALTER TABLE Accorde ADD CONSTRAINT FK_Accorde_Id_fournisseur FOREIGN KEY (Id_fournisseur) REFERENCES Fournisseur (Id_fournisseur);
ALTER TABLE Fournit ADD CONSTRAINT FK_Fournit_Id_equipement FOREIGN KEY (Id_equipement) REFERENCES Equipement (Id_equipement);
ALTER TABLE Fournit ADD CONSTRAINT FK_Fournit_Id_fournisseur FOREIGN KEY (Id_fournisseur) REFERENCES Fournisseur (Id_fournisseur);
ALTER TABLE A_des ADD CONSTRAINT FK_A_des_Id_equipement FOREIGN KEY (Id_equipement) REFERENCES Equipement (Id_equipement);
ALTER TABLE A_des ADD CONSTRAINT FK_A_des_Id_Proprietaire FOREIGN KEY (Id_Proprietaire) REFERENCES Proprietaires (Id_Proprietaire);
ALTER TABLE Echange ADD CONSTRAINT FK_Echange_Id_entretient FOREIGN KEY (Id_entretient) REFERENCES Entretien (Id_entretient);
ALTER TABLE Echange ADD CONSTRAINT FK_Echange_Id_piece FOREIGN KEY (Id_piece) REFERENCES Piece (Id_piece);
ALTER TABLE Remplace ADD CONSTRAINT FK_Remplace_Id_equipement FOREIGN KEY (Id_equipement) REFERENCES Equipement (Id_equipement);
ALTER TABLE Remplace ADD CONSTRAINT FK_Remplace_Id_entretient FOREIGN KEY (Id_entretient) REFERENCES Entretien (Id_entretient);
