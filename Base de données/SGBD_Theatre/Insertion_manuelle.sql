--------------------------------------------
--          INSERTIONS ENTITES            --
--------------------------------------------
begin
  INSERT INTO Administrateur(nom_Admin) VALUES ('Angela');
  INSERT INTO Administrateur(nom_Admin) VALUES ('Patrick');
end;
/

begin
  INSERT INTO Salle_Spectacle(nom_salle_spect, ville_salle_spect, departement_salle_spect, pays_salle_spect, nb_place_spect) VALUES ('Arena Pierre-Mauroy','Villeneuve-d Ascq','département du Nord','France', 2000);
  INSERT INTO Salle_Spectacle(nom_salle_spect, ville_salle_spect, departement_salle_spect, pays_salle_spect, nb_place_spect) VALUES ('Auditorium Maurice-Ravel','Lyon', 'Rhône','France', 1000);
  INSERT INTO Salle_Spectacle(nom_salle_spect, ville_salle_spect, departement_salle_spect, pays_salle_spect, nb_place_spect) VALUES ('La Cité','Rennes', 'Ille-et-Vilaine', 'France', 1500);
end;
/

begin
  INSERT INTO Organisme(nom_Org) VALUES ('les gens sympas du coin');
  INSERT INTO Organisme(nom_Org) VALUES ('je vous presente un present');
  INSERT INTO Organisme(nom_Org) VALUES ('caritatif');
end;
/

begin
  INSERT INTO Subvention(date_Sub, montant_Sub) VALUES (TO_DATE('2010-04-02','YYYY-MM-DD'),300);
  INSERT INTO Subvention(date_Sub, montant_Sub) VALUES (TO_DATE('2020-12-29','YYYY-MM-DD'),400);
  INSERT INTO Subvention(date_Sub, montant_Sub) VALUES (TO_DATE('2008-01-06','YYYY-MM-DD'),500);
end;
/

begin
  INSERT INTO Compagnie(nom_C, adresse_C, id_administrateur) VALUES ('Le Grandiloquent moustache poésie club','4 rue Félibien - 75006 Paris 6e', 1);
  INSERT INTO Compagnie(nom_C, adresse_C, id_administrateur) VALUES ('Les Chicaneries','Orgères (35230)', 2);
  INSERT INTO Compagnie(nom_C, adresse_C, id_administrateur) VALUES ('Théâtre de l Echappée Belle','40 Rue Origet - 37000 TOURS', 2);
end;
/
	
begin
  INSERT INTO Recette(montant_R, id_compagnie) VALUES (10000, 1);
  INSERT INTO Recette(montant_R, id_compagnie) VALUES (8000, 2);
  INSERT INTO Recette(montant_R, id_compagnie) VALUES (15000, 3);
end;
/

begin
  INSERT INTO Depense(achat_spectacle_D, id_compagnie) VALUES (3500, 1);
  INSERT INTO Depense(achat_spectacle_D, id_compagnie) VALUES (3000, 2);
  INSERT INTO Depense(achat_spectacle_D, id_compagnie) VALUES (3500, 2);
  INSERT INTO Depense(achat_spectacle_D, id_compagnie) VALUES (3500, 2);
  INSERT INTO Depense(achat_spectacle_D, id_compagnie) VALUES (3500, 3);
  INSERT INTO Depense(achat_spectacle_D, id_compagnie) VALUES (4000, 3);
end;
/

begin
  INSERT INTO Billeterie(nb_billet_vendu, nb_carte_vendu, id_salle_spectacle) VALUES (1000, 100, 1);
  INSERT INTO Billeterie(nb_billet_vendu, nb_carte_vendu, id_salle_spectacle) VALUES (700, 70, 2);
  INSERT INTO Billeterie(nb_billet_vendu, nb_carte_vendu, id_salle_spectacle) VALUES (500, 55, 2);  
  INSERT INTO Billeterie(nb_billet_vendu, nb_carte_vendu, id_salle_spectacle) VALUES (1200, 120, 3);
end;
/

begin
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (1, 17, 2019, 1);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (1, 17, 2020, 1);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (1, 30, 2019, 2);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (2, 40, 2019, 2);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (2, 40, 2019, 2);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (2, 40, 2019, 3);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (3, 90, 2015, 4);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (1, 20, 2018, 4);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (2, 37, 2019, 4);  
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (3, 100, 2018, 4);
  INSERT INTO Carte_Abonnement(formule_abo, prix_abo, anne_Abo, id_billeterie) VALUES (3, 100, 2012, 4);
end;
/

begin
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('L Avare',10,TO_DATE('2011-05-19','YYYY-MM-DD'), 1);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('L Avare',10,TO_DATE('2011-05-19','YYYY-MM-DD'), 1);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('L Avare',10,TO_DATE('2011-05-19','YYYY-MM-DD'), 1);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('L Avare',10,TO_DATE('2011-05-19','YYYY-MM-DD'), 1);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('L Avare',10,TO_DATE('2016-06-18','YYYY-MM-DD'), 3);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Roméo et Juliette',8,TO_DATE('2020-03-02','YYYY-MM-DD'), 2);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Roméo et Juliette',8,TO_DATE('2020-03-02','YYYY-MM-DD'), 2);  
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Béranger',12,TO_DATE('2013-11-12','YYYY-MM-DD'), 3);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Béranger',12,TO_DATE('2013-11-12','YYYY-MM-DD'), 3);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Béranger',12,TO_DATE('2009-10-18','YYYY-MM-DD'), 4);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Béranger',12,TO_DATE('2009-10-18','YYYY-MM-DD'), 4);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('La jeune fille et la nuit',7,TO_DATE('2019-09-12','YYYY-MM-DD'), 1);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('La jeune fille et la nuit',7,TO_DATE('2019-09-12','YYYY-MM-DD'), 1);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Sérotonine',6,TO_DATE('2009-10-18','YYYY-MM-DD'), 4);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Sérotonine',6,TO_DATE('2009-10-18','YYYY-MM-DD'), 4);
  INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES ('Changer l eau des fleurs',9,TO_DATE('2009-10-18','YYYY-MM-DD'), 2);
end;
/

begin
  INSERT INTO Spectateur(type_spectateur) VALUES (1);
  INSERT INTO Spectateur(type_spectateur) VALUES (1);
  INSERT INTO Spectateur(type_spectateur) VALUES (1);
  INSERT INTO Spectateur(type_spectateur) VALUES (2);
  INSERT INTO Spectateur(type_spectateur) VALUES (2);
  INSERT INTO Spectateur(type_spectateur) VALUES (2);
  INSERT INTO Spectateur(type_spectateur) VALUES (2);
  INSERT INTO Spectateur(type_spectateur) VALUES (2);
  INSERT INTO Spectateur(type_spectateur) VALUES (3);
  INSERT INTO Spectateur(type_spectateur) VALUES (3);
  INSERT INTO Spectateur(type_spectateur) VALUES (3);
  INSERT INTO Spectateur(type_spectateur) VALUES (3);
  INSERT INTO Spectateur(type_spectateur) VALUES (3);
  INSERT INTO Spectateur(type_spectateur) VALUES (3);
  INSERT INTO Spectateur(type_spectateur) VALUES (3);
end;
/

begin
  INSERT INTO Lecture(nom_L, auteur_L, lecteur_L) VALUES ('La jeune fille et la nuit', 'Guillaume Musso', 'Robert');
  INSERT INTO Lecture(nom_L, auteur_L, lecteur_L) VALUES ('Sérotonine', 'Michel Houellebecq' , 'Emilly');
  INSERT INTO Lecture(nom_L, auteur_L, lecteur_L) VALUES ('Changer l eau des fleurs', 'Valérie Perrin', 'Christian');
end;
/

begin
  INSERT INTO Piece_de_Theatre(titre_P, auteur_P, metteur_en_scene_P) VALUES ('L Avare', 'Molière', 'Louis de Funès');
  INSERT INTO Piece_de_Theatre(titre_P, auteur_P, metteur_en_scene_P) VALUES ('Béranger', ' Sacha Guitry', 'Jacques Béranger');
  INSERT INTO Piece_de_Theatre(titre_P, auteur_P, metteur_en_scene_P) VALUES ('Roméo et Juliette', 'William Shakespeare', 'Adrien Dupuis-Hepner');
  INSERT INTO Piece_de_Theatre(titre_P, auteur_P, metteur_en_scene_P) VALUES ('Les particules élémentaires', 'Michel Houellebecq', 'Julien Gosselin');
end;
/

begin
  INSERT INTO Spectacle(date_Spectacle, prix_Spectacle) VALUES (TO_DATE('2013-09-21','YYYY-MM-DD'),5000.0);
  INSERT INTO Spectacle(date_Spectacle, prix_Spectacle) VALUES (TO_DATE('2017-04-16','YYYY-MM-DD'),3500.0);
  INSERT INTO Spectacle(date_Spectacle, prix_Spectacle) VALUES (TO_DATE('2015-08-25','YYYY-MM-DD'),6000.0);
  INSERT INTO Spectacle(date_Spectacle, prix_Spectacle) VALUES (TO_DATE('2016-12-26','YYYY-MM-DD'),3000.0);
  INSERT INTO Spectacle(date_Spectacle, prix_Spectacle) VALUES (TO_DATE('2017-08-22','YYYY-MM-DD'),6200.0);
  INSERT INTO Spectacle(date_Spectacle, prix_Spectacle) VALUES (TO_DATE('2015-03-24','YYYY-MM-DD'),6000.0);
end;
/

----------------------------------------------
--          INSERTIONS RELATIONS            --
----------------------------------------------
begin
  INSERT INTO Connait(id_administrateur, id_organisme) VALUES (1, 1);
  INSERT INTO Connait(id_administrateur, id_organisme) VALUES (2, 2);
  INSERT INTO Connait(id_administrateur, id_organisme) VALUES (2, 3);
end;
/

begin
  INSERT INTO Accord(id_organisme, id_subvention) VALUES (1, 1);
  INSERT INTO Accord(id_organisme, id_subvention) VALUES (2, 2);
  INSERT INTO Accord(id_organisme, id_subvention) VALUES (3, 3);
end;
/

begin
  INSERT INTO Reccupere(id_subvention, id_recette) VALUES (1, 1);
  INSERT INTO Reccupere(id_subvention, id_recette) VALUES (2, 2);
  INSERT INTO Reccupere(id_subvention, id_recette) VALUES (3, 3);
end;
/

begin
  INSERT INTO Gain(id_recette, id_billeterie) VALUES (1, 1);
  INSERT INTO Gain(id_recette, id_billeterie) VALUES (2, 2);
  INSERT INTO Gain(id_recette, id_billeterie) VALUES (2, 3);  
  INSERT INTO Gain(id_recette, id_billeterie) VALUES (3, 4);
end;
/

begin
  INSERT INTO accueille(id_compagnie, id_salle_spectacle) VALUES (1, 1);
  INSERT INTO accueille(id_compagnie, id_salle_spectacle) VALUES (2, 2);
  INSERT INTO accueille(id_compagnie, id_salle_spectacle) VALUES (3, 3);
end;
/

begin
  INSERT INTO Creer(id_spectacle, id_compagnie) VALUES (1, 1);
  INSERT INTO Creer(id_spectacle, id_compagnie) VALUES (2, 2);
  INSERT INTO Creer(id_spectacle, id_compagnie) VALUES (1, 3);
  INSERT INTO Creer(id_spectacle, id_compagnie) VALUES (3, 3);
  INSERT INTO Creer(id_spectacle, id_compagnie) VALUES (4, 3);
  INSERT INTO Creer(id_spectacle, id_compagnie) VALUES (5, 1);
  INSERT INTO Creer(id_spectacle, id_compagnie) VALUES (6, 2);
end;
/

begin
  INSERT INTO Joue(id_salle_spectacle, id_spectacle) VALUES (1, 1);
  INSERT INTO Joue(id_salle_spectacle, id_spectacle) VALUES (1, 2);
  INSERT INTO Joue(id_salle_spectacle, id_spectacle) VALUES (2, 3);
  INSERT INTO Joue(id_salle_spectacle, id_spectacle) VALUES (2, 4);
  INSERT INTO Joue(id_salle_spectacle, id_spectacle) VALUES (3, 5);
  INSERT INTO Joue(id_salle_spectacle, id_spectacle) VALUES (3, 6);
end;
/

begin
  INSERT INTO Auditionne(id_spectacle, id_lecture) VALUES (4, 1);
  INSERT INTO Auditionne(id_spectacle, id_lecture) VALUES (5, 2);
  INSERT INTO Auditionne(id_spectacle, id_lecture) VALUES (6, 3);
end;
/

begin
  INSERT INTO Presente(id_spectacle, id_piece_de_theatre) VALUES (1, 1);
  INSERT INTO Presente(id_spectacle, id_piece_de_theatre) VALUES (2, 2);
  INSERT INTO Presente(id_spectacle, id_piece_de_theatre) VALUES (3, 3);
  INSERT INTO Presente(id_spectacle, id_piece_de_theatre) VALUES (6, 4);
end;
/

begin
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (1, 1);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (1, 2);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (1, 12);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (1, 14);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (1, 8);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (2, 4);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (2, 5);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (2, 10);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (2, 11);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (2, 6);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (2, 9);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (3, 3);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (3, 13);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (3, 7);
  INSERT INTO Contient(id_salle_spectacle, id_spectateur) VALUES (3, 15);
end;
/

begin
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (1, 1);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (1, 2);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (1, 12);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (1, 14);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (1, 8);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (2, 4);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (2, 5);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (2, 10);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (2, 11);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (3, 6);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (3, 9);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (4, 3);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (4, 13);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (4, 7);
  INSERT INTO Commande(id_billeterie, id_spectateur) VALUES (4, 15);
end;
/

begin
  INSERT INTO Appartient(id_compagnie, id_salle_Spectacle) VALUES (1, 1);
  INSERT INTO Appartient(id_compagnie, id_salle_Spectacle) VALUES (2, 2);
  INSERT INTO Appartient(id_compagnie, id_salle_Spectacle) VALUES (3, 3);
end;
/