-- affiche toutes les entités et relations

select * from Administrateur;
select * from Organisme;
select * from Subvention;
select * from Recette;
select * from Compagnie;
select * from Depense;
select * from Carte_Abonnement;
select * from Billeterie;
select * from Billet;
select * from Spectateur;
select * from Salle_Spectacle;
select * from Lecture;
select * from Piece_de_Theatre;
select * from Spectacle;

select * from Connait;
select * from Accord;
select * from Reccupere;
select * from Gain;
select * from accueille;
select * from Creer;
select * from Joue;
select * from Auditionne;
select * from Presente;
select * from Contient;
select * from Commande;
select * from Appartient;

-----------------------------------------------------

-- Suppression d'une salle de spectacle entraine la suppresion de la billeterie
-- et par consequent des cartes d'abonnements et des billets de la billeterie en question

select * from Billeterie;
select * from Billet;
select * from Carte_Abonnement;
select * from Salle_Spectacle;
select * from appartient;
select * from accueille;
select * from contient;
select * from joue;

delete salle_spectacle where ville_salle_spect = 'Lyon';

select * from Salle_Spectacle;
select * from Billeterie;
select * from Billet;
select * from Carte_Abonnement;
select * from appartient;
select * from accueille;
select * from contient;
select * from joue;

--------------------------------------------------

-- afficher la date et le prix des spectacles qui ont un prix superieur a la moyenne des prix

select date_Spectacle, prix_Spectacle from Spectacle 
    where rownum=1 AND prix_Spectacle > (select AVG(prix_Spectacle) from Spectacle) order by prix_Spectacle;

-----------------------------------------------------

-- division des depenses par la table compagnie

SELECT *
FROM depense de1
WHERE NOT EXISTS (
    SELECT *
    FROM compagnie co
    WHERE NOT EXISTS(
        SELECT *
        FROM depense de2 
        WHERE (de1.achat_spectacle_d = de2.achat_spectacle_d)
        AND (de2.id_compagnie = co.id_compagnie)
    )
);

------------------------------------------------------

-- afficher le titre des spectacles et le nom des lectures qui ont lieu 
-- entre 2014 et 2018 et dont l'auteur est commun

SELECT titre_P, nom_L 
FROM Piece_de_Theatre, Lecture
WHERE id_piece_de_theatre IN(
    SELECT id_piece_de_theatre 
    FROM Presente
    WHERE id_spectacle IN(
       SELECT id_spectacle 
        FROM Spectacle
        WHERE date_Spectacle BETWEEN '13-JAN-14' AND '27-OCT-18' 
    )
)
AND id_lecture IN(  
    SELECT id_lecture 
    FROM Auditionne
    WHERE id_spectacle IN(
       SELECT id_spectacle 
        FROM Spectacle
        WHERE date_Spectacle BETWEEN '13-JAN-14' AND '27-OCT-18' 
    )
)
AND auteur_p = auteur_l;

-------------------------------------------------------

-- afficher le nombre de spectateur adolescent qui ont pris l'abonnement 
-- numéro 2 (soit 6 entrées) en 2019

SELECT count(id_spectateur)
FROM Spectateur WHERE id_spectateur IN (
    SELECT id_spectateur
    FROM Commande WHERE id_billeterie IN (
        SELECT id_billeterie
        FROM Billeterie WHERE id_billeterie IN (
            SELECT id_billeterie
            FROM Carte_Abonnement WHERE formule_abo = 2
            AND anne_Abo = 2019
        )    
    )
)
AND type_spectateur = 2;

------------------------------------------------------

-- afficher le nombre de carte et de billets des compagnies gérés par l'admin d'un nom choisit

SELECT id_billeterie, nb_billet_vendu, nb_carte_vendu
FROM Billeterie WHERE id_billeterie IN (
    SELECT id_billeterie 
    FROM Gain WHERE id_recette IN (
        SELECT id_recette
        FROM Recette WHERE id_compagnie IN (
            SELECT id_compagnie
            FROM Compagnie WHERE id_administrateur IN (
                SELECT id_administrateur
                FROM Administrateur WHERE nom_Admin = 'Patrick'
            )
        )
    )
);

-----------------------------------------------------

--affiche la liste des Pieces de Theatre

DECLARE
titre Piece_de_Theatre.titre_P%TYPE;
auteur Piece_de_Theatre.auteur_P%TYPE;
metteur Piece_de_Theatre.metteur_en_scene_P%TYPE;

Cursor monCursor IS
SELECT titre_P, auteur_P, metteur_en_scene_P
FROM Piece_de_Theatre;

BEGIN
    OPEN monCursor;
    LOOP
        FETCH monCursor INTO titre, auteur, metteur;
        EXIT WHEN monCursor%NOTFOUND;
        dbms_output.put_line('Titre : ' || titre);
        dbms_output.put_line('Auteur : ' || auteur);
        dbms_output.put_line('Metteur en Scene : ' || metteur);
        dbms_output.put_line(chr(10));
    END LOOP;
    CLOSE monCursor;
END;

----------------------------------------------------

-- verifie si la date de subvention est inferieur à la date d'aujourd'hui ou si le montant est superieur a 0

create or replace trigger TRIG_SUBVENTION
before insert or update of montant_Sub on SUBVENTION
for each row
when (new.montant_Sub < 0 OR new.date_Sub > SYSDATE)
begin
    raise_application_error(-20331, 'Montant ou date incorect, l insertion ne se fera pas !'|| chr(10));
end;
/

select * from subvention;
INSERT INTO Subvention(date_Sub, montant_Sub) VALUES (TO_DATE('2030-04-02','YYYY-MM-DD'),300);
select * from subvention;

--------------------------------------------------

-- procedure qui compare deux chaine et creer un billet si elles sont egales

CREATE OR REPLACE PROCEDURE COMPARE_CHAINE (PARAM1 IN VARCHAR2,
                                            PARAM2 IN VARCHAR2) AS
BEGIN
   IF UPPER(PARAM1) = UPPER(PARAM2) THEN
        INSERT INTO Billet(titre_spect, tarif_B, date_representation_B, id_billeterie) VALUES (PARAM1,10,SYSDATE, 1);
   ELSE
        dbms_output.put_line('Chaines differentes');
   END IF;
END;
/

-----------------------------

-- compare 2 nombres et dit lequel est si la la capacité max est atteinte, combien de place il reste, ou si elle est a son max.
CREATE OR REPLACE PROCEDURE COMPARE_CAPACITE (PARAM1 in INT,
                                             PARAM2 in INT) AS
BEGIN
    IF PARAM1 > PARAM2 THEN
        dbms_output.put_line('La salle est rempli');
    ELSE IF PARAM1 < PARAM2 THEN
            dbms_output.put_line('La salle peux encore acceuillir ' || (PARAM2-PARAM1) ||' personnes');
        ELSE
            dbms_output.put_line('La salle est a sa capacité maximal');
    END IF;
   END IF;
END;
/
-------------------------------------------
-------------------------------------------

EXECUTE COMPARE_CHAINE('Billet1','Billet2');
EXECUTE COMPARE_CHAINE('Billet1','Billet1');

EXECUTE COMPARE_CAPACITE(5,12);
EXECUTE COMPARE_CAPACITE(12,5);