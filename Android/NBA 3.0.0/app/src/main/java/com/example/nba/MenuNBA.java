package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MenuNBA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_nba);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public void openConsulterEffectif2019(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,ConsulterEffectif2019.class);
        startActivity(activity_menu);
    }

    public void openCreerEffectif(View view) {
        Toast.makeText(this, "Cette option n'est pas encore utilisable", Toast.LENGTH_LONG ).show();
    }

    public void openConsulterEffectifCrees(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,ConsulterEffectifCree.class);
        startActivity(activity_menu);
    }

    public void openSoundbox(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,Soundbox.class);
        startActivity(activity_menu);
    }

    //affiche le menu
    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
       getMenuInflater().inflate(R.layout.activity_menu, menu);
      return true;
    }

    // liste des options de la MenuBar
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(MenuNBA.this, "Vous etes deja dans MenuNBA", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Quitter:
                Toast.makeText(MenuNBA.this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(MenuNBA.this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(MenuNBA.this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    //ouvre le site web officiel de la NBA
    public void onClick(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.beinsports.com/france/nba/?gr=www"));
        startActivity(link);
    }
}

