package com.example.nba;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;


public class MainActivity extends AppCompatActivity {

    //declaration d'un objet de type DataBaseHelper
    DataBaseHelper  databaseHelper;

    EditText textUsername;
    EditText textPassword;
    Button boutonValider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHelper=new DataBaseHelper(this);
        textUsername = findViewById(R.id.edit_id);
        textPassword = findViewById(R.id.edit_MotDePasse);
        boutonValider = findViewById(R.id.Bouton_Valider);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public void creerCompte(View view) {
        Intent creer = new Intent(MainActivity.this, CreerCompte.class);
        startActivity(creer);
    }

    //methode appelé lors du clique sur le bouton connexion
    //elle verifie si l'identifiant et le mot de passe correspondent
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void openMenuNBA(View view) {
        String pseudoID = textUsername.getText().toString().trim();
        String mot_de_passe = textPassword.getText().toString().trim();

        if(mot_de_passe != null && !mot_de_passe.isEmpty()&& pseudoID != null && !pseudoID.isEmpty()){
            boolean val = databaseHelper.checkUser(pseudoID,mot_de_passe);

            if(val){
                String pseudo =textUsername.getText().toString().trim();
                notificationDialog(pseudo);
                Toast.makeText(MainActivity.this, "Bienvenue "+textUsername.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                Intent moveToMenuNBA = new Intent(MainActivity.this, MenuNBA.class);
                startActivity(moveToMenuNBA);
            }
            else{
                Toast.makeText(MainActivity.this, "identifiant ou mot de passe incorrect", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(MainActivity.this, "identifiant ou mot de passe incorrect", Toast.LENGTH_SHORT).show();
        }
    }


    /****menu****/
    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    // liste des options de la MenuBar
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(MainActivity.this, "Cette option n'est pas utilisabke ici", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Quitter:
                Toast.makeText(MainActivity.this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(MainActivity.this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(MainActivity.this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    // fonction qui crree une notification
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void notificationDialog(String pseudo) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "tutorialspoint_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription("Sample Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 100, 100, 100});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.nba)
                .setTicker("Tutorialspoint")
                //.setPriority(Notification.PRIORITY_MAX)
                .setContentTitle("Bienvenue")
                .setContentText("Bonjour " + pseudo + " vous vous etes connecté(e) avec succes")
                .setContentInfo("Information");
        notificationManager.notify(1, notificationBuilder.build());
    }
}
