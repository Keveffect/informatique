package com.example.nba.équipes;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.nba.Info;
import com.example.nba.MenuNBA;
import com.example.nba.R;

public class e_timberwolves extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_timberwolves);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(this, "MenuNBA", Toast.LENGTH_SHORT).show();

                Intent test = new Intent(e_timberwolves.this, MenuNBA.class);
                startActivity(test);
                return true;
            case R.id.Quitter:
                Toast.makeText(this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    public void openWiggins(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/andrew_wiggins"));
        startActivity(link);
    }

    public void openLayman(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/jake_layman"));
        startActivity(link);
    }

    public void openTeague(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/jeff_teague"));
        startActivity(link);
    }

    public void openTowns(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/karl-anthony_towns"));
        startActivity(link);
    }

    public void openCovington(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/robert_covington"));
        startActivity(link);
    }
}
