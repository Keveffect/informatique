package com.example.nba;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "User.db";
    public static final String TABLE_NAME = "user_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "IDENTIFIANT";
    public static final String COL_3 = "MOTDEPASSE";
    public static final String COL_4 = "CONFMOTDEPASSE";

    //Constructeur
    public DataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    // Création de la BD
    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table " + TABLE_NAME +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,IDENTIFIANT TEXT,MOTDEPASSE TEXT,CONFMOTDEPASSE TEXT)");
    }

    // Moddification de la BD
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    // Creation d'un utilisateur dans la BD
    public long addUser(String pseudo,String password,String confPassword) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,pseudo);
        contentValues.put(COL_3,password);
        contentValues.put(COL_4,confPassword);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        db.close();
        return result;
    }

    // Vérification d'un utilisateur dans la BD
    public boolean checkUser(String username, String password){
        String[] columns = {COL_1};
        SQLiteDatabase db = getReadableDatabase();
        String selection = COL_2 + "=?" + " and " + COL_3 + "=?";
        String[] selectionArgs = {username, password};
        Cursor cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null ,null,null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count > 0;
    }
}