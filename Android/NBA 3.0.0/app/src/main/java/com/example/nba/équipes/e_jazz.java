package com.example.nba.équipes;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.nba.Info;
import com.example.nba.MenuNBA;
import com.example.nba.R;

public class e_jazz extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_jazz);
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(this, "MenuNBA", Toast.LENGTH_SHORT).show();

                Intent test = new Intent(e_jazz.this, MenuNBA.class);
                startActivity(test);
                return true;
            case R.id.Quitter:
                Toast.makeText(this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    public void openMitchell(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/donovan_mitchell"));
        startActivity(link);
    }

    public void openIngles(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/joe_ingles"));
        startActivity(link);
    }

    public void openConley(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/mike_coley"));
        startActivity(link);
    }

    public void openONeale(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/royce_o'neale"));
        startActivity(link);
    }

    public void openGobert(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/rudy_gobert"));
        startActivity(link);
    }
}
