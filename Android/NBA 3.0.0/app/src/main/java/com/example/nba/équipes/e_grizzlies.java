package com.example.nba.équipes;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.nba.Info;
import com.example.nba.MenuNBA;
import com.example.nba.R;

public class e_grizzlies extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_grizzlies);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(e_grizzlies.this, "MenuNBA", Toast.LENGTH_SHORT).show();

                Intent test = new Intent(e_grizzlies.this, MenuNBA.class);
                startActivity(test);
                return true;
            case R.id.Quitter:
                Toast.makeText(e_grizzlies.this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    public void openClarke(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/brandon_clarke"));
        startActivity(link);
    }

    public void openBrooks(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/dillon_brooks"));
        startActivity(link);
    }

    public void openMorant(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/ja_morant"));
        startActivity(link);
    }

    public void openCrowder(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/jae_crowder"));
        startActivity(link);
    }

    public void openJackson(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/jaren_jackson_jr"));
        startActivity(link);
    }
}
