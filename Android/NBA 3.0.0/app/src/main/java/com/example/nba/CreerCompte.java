package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreerCompte extends AppCompatActivity {

    DataBaseHelper db;
    Button boutonCreer;
    EditText textUsername,textPassword,textConfPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_compte);

        db = new DataBaseHelper(this);
        textUsername = findViewById(R.id.edit_identifiant);
        textPassword = findViewById(R.id.edit_MotDePasse);
        textConfPassword = findViewById(R.id.edit_ConfirmerMotDePasse);
        boutonCreer = findViewById(R.id.BoutonCreer);
        TextView textJAiUnCompte = findViewById(R.id.JAiUnCompte);
        textJAiUnCompte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(CreerCompte.this, MainActivity.class);
                CreerCompte.this.startActivity(registerIntent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    //methode appelée lors du clic sur le bouton creer compte
    //elle verifie si les entrées sont corrects puis ajoute les données a la database
    public void onClick(View view) {
        String pseudoID = textUsername.getText().toString().trim();
        String mot_de_passe = textPassword.getText().toString().trim();
        String confPassword = textConfPassword.getText().toString().trim();

        if (mot_de_passe != null || !mot_de_passe.isEmpty() || pseudoID != null || !pseudoID.isEmpty() || confPassword != null && !confPassword.isEmpty()) {
            if (textPassword.getText().toString().trim().equals(textConfPassword.getText().toString().trim())) {
                long val = db.addUser(pseudoID, mot_de_passe, confPassword);
                if (val > 0) {
                    Toast.makeText(CreerCompte.this, "Compte créé avec succés", Toast.LENGTH_SHORT).show();
                    Intent moveToLogin = new Intent(CreerCompte.this, MenuNBA.class);
                    startActivity(moveToLogin);
                } else
                    Toast.makeText(CreerCompte.this, "Erreur", Toast.LENGTH_SHORT).show();
                    emptyInputEditText();
            }
            else
                Toast.makeText(CreerCompte.this, "mot de passe et confirmation mot de passe différent", Toast.LENGTH_SHORT).show();
                emptyInputEditText();
        }
    }

    //vide le contenu des entréee texte
    private void emptyInputEditText(){
        textUsername.setText(null);
        textPassword.setText(null);
        textConfPassword.setText(null);
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }
    // liste des options de la MenuBar
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(CreerCompte.this, "Cette option n'est pas utilisable ici", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Quitter:
                Toast.makeText(CreerCompte.this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }
}
