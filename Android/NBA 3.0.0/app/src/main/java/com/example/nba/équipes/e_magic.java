package com.example.nba.équipes;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.nba.Info;
import com.example.nba.MenuNBA;
import com.example.nba.R;

public class e_magic extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_magic);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(this, "MenuNBA", Toast.LENGTH_SHORT).show();

                Intent test = new Intent(e_magic.this, MenuNBA.class);
                startActivity(test);
                return true;
            case R.id.Quitter:
                Toast.makeText(this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    public void openGordon(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/aaron_gordon"));
        startActivity(link);
    }

    public void openFournier(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/evan_fournier"));
        startActivity(link);
    }

    public void openBacon(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/dwayne_bacon"));
        startActivity(link);
    }

    public void openIsaac(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/jonathan_isaac"));
        startActivity(link);
    }

    public void openVulcevic(View view) {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.global.nba.com/players/#!/nikola_vulcevic"));
        startActivity(link);
    }
}
