package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.nba.équipes.e_mavericks;
import com.example.nba.équipes.e_nets;
import com.example.nba.équipes.e_nuggets;
import com.example.nba.équipes.e_pacers;
import com.example.nba.équipes.e_pelicans;
import com.example.nba.équipes.e_pistons;
import com.example.nba.équipes.e_raptors;
import com.example.nba.équipes.e_rockets;
import com.example.nba.équipes.e_spurs;
import com.example.nba.équipes.e_suns;
import com.example.nba.équipes.e_thunder;
import com.example.nba.équipes.e_timberwolves;
import com.example.nba.équipes.e_trailblazers;
import com.example.nba.équipes.e_warriors;
import com.example.nba.équipes.e_wizards;

public class ConsulterEffectif2019_2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_effectif2019_2);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public void openConsulterEffectif2019(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this,ConsulterEffectif2019.class);
        startActivity(test);
    }

    public void open_mavericks(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_mavericks.class);
        startActivity(test);
    }

    public void open_nets(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_nets.class);
        startActivity(test);
    }

    public void open_nuggets(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_nuggets.class);
        startActivity(test);
    }

    public void openPacers(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_pacers.class);
        startActivity(test);
    }

    public void openPelican(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_pelicans.class);
        startActivity(test);
    }

    public void openPistons(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_pistons.class);
        startActivity(test);
    }

    public void openRaptors(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_raptors.class);
        startActivity(test);
    }

    public void openRockets(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_rockets.class);
        startActivity(test);
    }

    public void openSpurs(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_spurs.class);
        startActivity(test);
    }

    public void openSuns(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_suns.class);
        startActivity(test);
    }

    public void openOKC(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_thunder.class);
        startActivity(test);
    }

    public void openTimberwolves(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_timberwolves.class);
        startActivity(test);
    }

    public void opneBlazers(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_trailblazers.class);
        startActivity(test);
    }

    public void openWarriors(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_warriors.class);
        startActivity(test);
    }

    public void openWizards(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this, e_wizards.class);
        startActivity(test);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(ConsulterEffectif2019_2.this, "MenuNBA", Toast.LENGTH_SHORT).show();

                Intent test = new Intent(ConsulterEffectif2019_2.this, MenuNBA.class);
                startActivity(test);
                return true;
            case R.id.Quitter:
                Toast.makeText(ConsulterEffectif2019_2.this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }
}
