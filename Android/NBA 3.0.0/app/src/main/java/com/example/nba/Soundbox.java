package com.example.nba;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Soundbox extends AppCompatActivity {

    //declaration de tous les sons
    private MediaPlayer mediaPlayer,mediaPlayer2,mediaPlayer3,mediaPlayer4,mediaPlayer5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soundbox);

        //instanciation de tous les sons
        int songId = this.getRawResIdByName("curry");
        int songId2 = this.getRawResIdByName("harden");
        int songId3 = this.getRawResIdByName("defense");
        int songId4 = this.getRawResIdByName("caruso");
        int songId5 = this.getRawResIdByName("intro");
        this.mediaPlayer=   MediaPlayer.create(this, songId);
        this.mediaPlayer2=   MediaPlayer.create(this, songId2);
        this.mediaPlayer3=   MediaPlayer.create(this, songId3);
        this.mediaPlayer4=   MediaPlayer.create(this, songId4);
        this.mediaPlayer5=   MediaPlayer.create(this, songId5);

    }

    //methode appelé lors du clic sur les logo de volume pour declancher un son
    public void son1(View view) {this.mediaPlayer.start();}
    public void son2(View view) {this.mediaPlayer2.start();}
    public void son3(View view) {this.mediaPlayer3.start();}
    public void son4(View view) {this.mediaPlayer4.start();}
    public void son5(View view) {this.mediaPlayer5.start();}

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseMedia();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopMedia();
    }

    //arrete le son quand on ferme l'application
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            stopMedia();
            mediaPlayer.release();
        }
        if (mediaPlayer2 != null) {
            stopMedia();
            mediaPlayer2.release();
        }
        if (mediaPlayer3 != null) {
            stopMedia();
            mediaPlayer3.release();
        }
        if (mediaPlayer4 != null) {
            stopMedia();
            mediaPlayer4.release();
        }
        if (mediaPlayer5 != null) {
            stopMedia();
            mediaPlayer5.release();
        }
    }

    //arrete le son quand on met l'application en arriere plan
    private void stopMedia() {
        if (mediaPlayer == null) return;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        if (mediaPlayer2 == null) return;
        if (mediaPlayer2.isPlaying()) {
            mediaPlayer2.stop();
        }
        if (mediaPlayer3 == null) return;
        if (mediaPlayer3.isPlaying()) {
            mediaPlayer3.stop();
        }
        if (mediaPlayer4 == null) return;
        if (mediaPlayer4.isPlaying()) {
            mediaPlayer4.stop();
        }
        if (mediaPlayer5 == null) return;
        if (mediaPlayer5.isPlaying()) {
            mediaPlayer5.stop();
        }
    }

    //arrete le son quand on met l'application en arriere plan
    private void pauseMedia() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
        if (mediaPlayer2.isPlaying()) {
            mediaPlayer2.pause();
        }
        if (mediaPlayer3.isPlaying()) {
            mediaPlayer3.pause();
        }
        if (mediaPlayer4.isPlaying()) {
            mediaPlayer4.pause();
        }
        if (mediaPlayer5.isPlaying()) {
            mediaPlayer5.pause();
        }
    }

    private void resumeMedia() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        }
        if (!mediaPlayer2.isPlaying()) {
            mediaPlayer2.start();
        }
        if (!mediaPlayer3.isPlaying()) {
            mediaPlayer3.start();
        }
        if (!mediaPlayer4.isPlaying()) {
            mediaPlayer4.start();
        }
        if (!mediaPlayer5.isPlaying()) {
            mediaPlayer5.start();
        }
    }

    /******sons******/
    public int getRawResIdByName(String resName)  {
        String pkgName = this.getPackageName();
        // Return 0 if not found.
        int resID = this.getResources().getIdentifier(resName, "raw", pkgName);
        return resID;
    }

    /****menu****/
    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(Soundbox.this, "MenuNBA", Toast.LENGTH_SHORT).show();
                Intent test = new Intent(Soundbox.this, MenuNBA.class);
                startActivity(test);
                return true;
            case R.id.Quitter:
                Toast.makeText(Soundbox.this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(Soundbox.this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(Soundbox.this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }
}
