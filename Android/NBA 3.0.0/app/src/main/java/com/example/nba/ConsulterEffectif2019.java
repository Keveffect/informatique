package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.nba.équipes.e_76ers;
import com.example.nba.équipes.e_bucks;
import com.example.nba.équipes.e_bulls;
import com.example.nba.équipes.e_cavaliers;
import com.example.nba.équipes.e_celtics;
import com.example.nba.équipes.e_clippers;
import com.example.nba.équipes.e_grizzlies;
import com.example.nba.équipes.e_hawks;
import com.example.nba.équipes.e_heat;
import com.example.nba.équipes.e_hornets;
import com.example.nba.équipes.e_jazz;
import com.example.nba.équipes.e_kings;
import com.example.nba.équipes.e_knicks;
import com.example.nba.équipes.e_lakers;
import com.example.nba.équipes.e_magic;

public class ConsulterEffectif2019 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_effectif2019);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public void openConsulterEffectif2019_2(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,ConsulterEffectif2019_2.class);
        startActivity(test);
    }

    public void open_76ers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_76ers.class);
        startActivity(test);
    }

    public void open_bucks(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_bucks.class);
        startActivity(test);
    }

    public void open_bulls(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_bulls.class);
        startActivity(test);
    }

    public void open_cavaliers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_cavaliers.class);
        startActivity(test);
    }

    public void open_celtics(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_celtics.class);
        startActivity(test);
    }

    public void open_clippers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_clippers.class);
        startActivity(test);
    }

    public void open_grizzlies(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_grizzlies.class);
        startActivity(test);
    }

    public void open_hawks(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_hawks.class);
        startActivity(test);
    }

    public void open_heat(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_heat.class);
        startActivity(test);
    }

    public void open_hornets(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_hornets.class);
        startActivity(test);
    }

    public void open_jazz(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_jazz.class);
        startActivity(test);
    }

    public void open_kings(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_kings.class);
        startActivity(test);
    }

    public void open_knicks(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_knicks.class);
        startActivity(test);
    }

    public void open_lakers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_lakers.class);
        startActivity(test);
    }

    public void open_magic(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this, e_magic.class);
        startActivity(test);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(ConsulterEffectif2019.this, "MenuNBA", Toast.LENGTH_SHORT).show();

                Intent test = new Intent(ConsulterEffectif2019.this, MenuNBA.class);
                startActivity(test);
                return true;
            case R.id.Quitter:
                Toast.makeText(ConsulterEffectif2019.this, "Quitter", Toast.LENGTH_SHORT).show();
                finishAffinity();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }
}
