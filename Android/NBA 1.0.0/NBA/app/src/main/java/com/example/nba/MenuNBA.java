package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MenuNBA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }
    public void openConsulterEffectif2019(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,ConsulterEffectif2019.class);
        startActivity(activity_menu);
    }

}

