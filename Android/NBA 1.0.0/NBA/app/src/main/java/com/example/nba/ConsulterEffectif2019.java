package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ConsulterEffectif2019 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_effectif2019);
    }

    public void openConsulterEffectif2019_2(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,ConsulterEffectif2019_2.class);
        startActivity(test);
    }
}
