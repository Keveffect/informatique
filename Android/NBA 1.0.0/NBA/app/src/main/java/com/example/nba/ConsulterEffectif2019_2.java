package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ConsulterEffectif2019_2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_effectif2019_2);
    }

    public void openConsulterEffectif2019(View view) {
        Intent test = new Intent(ConsulterEffectif2019_2.this,ConsulterEffectif2019.class);
        startActivity(test);
    }
}
