package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MenuNBA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_nba);
    }

    public void openConsulterEffectif2019(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,ConsulterEffectif2019.class);
        startActivity(activity_menu);
    }

    public void openSoundbox(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,Soundbox.class);
        startActivity(activity_menu);
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
       getMenuInflater().inflate(R.layout.activity_menu, menu);
      return true;
    }
}

