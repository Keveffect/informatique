package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreerCompte extends AppCompatActivity {

    DataBaseHelper db;
    Button boutonCreer;
    EditText textUsername,textPassword,textConfPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_compte);

        db = new DataBaseHelper(this);
        textUsername = findViewById(R.id.edit_identifiant);
        textPassword = findViewById(R.id.edit_MotDePasse);
        textConfPassword = findViewById(R.id.edit_ConfirmerMotDePasse);
        boutonCreer = findViewById(R.id.BoutonCreer);
        TextView textJAiUnCompte = findViewById(R.id.JAiUnCompte);
            textJAiUnCompte.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent registerIntent = new Intent(CreerCompte.this, MainActivity.class);
                    CreerCompte.this.startActivity(registerIntent);
                }
            });

 /*       boutonCreer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = textUsername.getText().toString().trim();
                String pwd = textPassword.getText().toString().trim();
                String conf_pwd = textConfPassword.getText().toString().trim();

                if(pwd.equals(conf_pwd)){
                    long val = db.insertData(user,pwd);
                   if(val>0){
                        Toast.makeText(CreerCompte.this,"Compte créé",Toast.LENGTH_SHORT).show();

                        Intent moveToLogin = new Intent(CreerCompte.this,MainActivity.class);
                        startActivity(moveToLogin);
                    }
                   else {
                        Toast.makeText(CreerCompte.this,"Mot de passe incorrect",Toast.LENGTH_SHORT).show();
                   }
                }
                else{
                    Toast.makeText(CreerCompte.this,"Mot de passe incorrect",Toast.LENGTH_SHORT).show();
                }
            }
        });
*/    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }
}
