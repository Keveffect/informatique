package com.example.nba;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.nba.DataBaseHelper;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(MenuItem item){
        switch (item.getItemId()){
            case R.id.info:
                Toast.makeText(this, "info", Toast.LENGTH_LONG ).show();
                return true;
            case R.id.menu:
                Toast.makeText(this, "menu", Toast.LENGTH_LONG ).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void creerCompte(View view) {
        Intent creer = new Intent(MainActivity.this, CreerCompte.class);
        startActivity(creer);
    }

    public void openMenuNBA(View view) {
        Intent creer = new Intent(MainActivity.this, MenuNBA.class);
        startActivity(creer);
    }

    /****menu****/
    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }
}
