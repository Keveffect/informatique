package com.example.nba;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Soundbox extends AppCompatActivity {


    private MediaPlayer mediaPlayer,mediaPlayer2,mediaPlayer3,mediaPlayer4,mediaPlayer5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soundbox);

        int songId = this.getRawResIdByName("curry");
        int songId2 = this.getRawResIdByName("harden");
        int songId3 = this.getRawResIdByName("defense");
        int songId4 = this.getRawResIdByName("caruso");
        int songId5 = this.getRawResIdByName("intro");
        this.mediaPlayer=   MediaPlayer.create(this, songId);
        this.mediaPlayer2=   MediaPlayer.create(this, songId2);
        this.mediaPlayer3=   MediaPlayer.create(this, songId3);
        this.mediaPlayer4=   MediaPlayer.create(this, songId4);
        this.mediaPlayer5=   MediaPlayer.create(this, songId5);
    }

    public void son1(View view) {this.mediaPlayer.start();}
    public void son2(View view) {this.mediaPlayer2.start();}
    public void son3(View view) {this.mediaPlayer3.start();}
    public void son4(View view) {this.mediaPlayer4.start();}
    public void son5(View view) {this.mediaPlayer5.start();}

    /******sons******/
    public int getRawResIdByName(String resName)  {
        String pkgName = this.getPackageName();
        // Return 0 if not found.
        int resID = this.getResources().getIdentifier(resName, "raw", pkgName);
        return resID;
    }

    /****menu****/
    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }
}
