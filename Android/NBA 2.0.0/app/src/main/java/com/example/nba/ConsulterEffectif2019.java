package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class ConsulterEffectif2019 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_effectif2019);
    }

    public void openConsulterEffectif2019_2(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,ConsulterEffectif2019_2.class);
        startActivity(test);
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    public void open_76ers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_76ers.class);
        startActivity(test);
    }

    public void open_bucks(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_bucks.class);
        startActivity(test);
    }

    public void open_bulls(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_bulls.class);
        startActivity(test);
    }

    public void open_cavaliers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_cavaliers.class);
        startActivity(test);
    }

    public void open_celtics(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_celtics.class);
        startActivity(test);
    }

    public void open_clippers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_clippers.class);
        startActivity(test);
    }

    public void open_grizzlies(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_grizzlies.class);
        startActivity(test);
    }

    public void open_hawks(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_hawks.class);
        startActivity(test);
    }

    public void open_heat(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_heat.class);
        startActivity(test);
    }

    public void open_hornets(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_hornets.class);
        startActivity(test);
    }

    public void open_jazz(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_jazz.class);
        startActivity(test);
    }

    public void open_kings(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_kings.class);
        startActivity(test);
    }

    public void open_knicks(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_knicks.class);
        startActivity(test);
    }

    public void open_lakers(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_lakers.class);
        startActivity(test);
    }

    public void open_magic(View view) {
        Intent test = new Intent(ConsulterEffectif2019.this,e_magic.class);
        startActivity(test);
    }
}
