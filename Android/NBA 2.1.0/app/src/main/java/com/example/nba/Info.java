package com.example.nba;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Info extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(Info.this, "Menu NBA", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, MenuNBA.class);
                startActivity(menu);
                return true;
            case R.id.quitter:
                Toast.makeText(Info.this, "Quitter", Toast.LENGTH_SHORT).show();
                onDestroy();
                return true;
            case R.id.info:
                Toast.makeText(this, "Vous etes deja dans A propos", Toast.LENGTH_SHORT).show();
                return true;
        }
        return false;
    }
}
