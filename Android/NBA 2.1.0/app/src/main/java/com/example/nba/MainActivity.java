package com.example.nba;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    DataBaseHelper  databaseHelper;
    EditText textUsername;
    EditText textPassword;
    Button boutonValider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHelper=new DataBaseHelper(this);
        textUsername = findViewById(R.id.edit_id);
        textPassword = findViewById(R.id.edit_MotDePasse);
        boutonValider = findViewById(R.id.Bouton_Valider);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public void creerCompte(View view) {
        Intent creer = new Intent(MainActivity.this, CreerCompte.class);
        startActivity(creer);
    }

    public void openMenuNBA(View view) {
        verifyFromSQLite();
    }

    private void verifyFromSQLite() {
        if (!databaseHelper.checkUser(textUsername.getText().toString().trim(), textPassword.getText().toString().trim())) {
            Intent menu = new Intent(MainActivity.this, MenuNBA.class);
            startActivity(menu);
            Toast.makeText(MainActivity.this,"Bonjour "+textUsername.getText().toString().trim(),Toast.LENGTH_SHORT).show();
        }
        else if(databaseHelper.checkUser(textUsername.getText().toString().trim())) {
            Toast.makeText(MainActivity.this,"identifiant inconnu",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(MainActivity.this, "identifiant ou Mot de passe incorrect", Toast.LENGTH_SHORT).show();
        }
    }

    /****menu****/
    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(MainActivity.this, "Cette option n'est pas utilisabke ici", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.quitter:
                Toast.makeText(MainActivity.this, "Quitter", Toast.LENGTH_SHORT).show();
                onDestroy();
                return true;
            case R.id.info:
                Toast.makeText(MainActivity.this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(MainActivity.this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }
}
