package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MenuNBA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_nba);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public void openConsulterEffectif2019(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,ConsulterEffectif2019.class);
        startActivity(activity_menu);
    }

    public void openCreerEffectif(View view) {
        Toast.makeText(this, "Cette option n'est pas encore utilisable", Toast.LENGTH_LONG ).show();
    }

    public void openConsulterEffectifCrees(View view) {
        Toast.makeText(this, "Cette option n'est pas encore utilisable", Toast.LENGTH_LONG ).show();
    }

    public void openSoundbox(View view) {
        Intent activity_menu = new Intent(MenuNBA.this,Soundbox.class);
        startActivity(activity_menu);
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
       getMenuInflater().inflate(R.layout.activity_menu, menu);
      return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(MenuNBA.this, "Vous etes deja dans MenuNBA", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.quitter:
                Toast.makeText(MenuNBA.this, "Quitter", Toast.LENGTH_SHORT).show();
                onDestroy();
                return true;
            case R.id.info:
                Toast.makeText(MenuNBA.this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(MenuNBA.this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }
}

