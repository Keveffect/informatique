package com.example.nba;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreerCompte extends AppCompatActivity {

    DataBaseHelper db;
    Button boutonCreer;
    EditText textUsername,textPassword,textConfPassword;
     User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_compte);

        db = new DataBaseHelper(this);
        textUsername = findViewById(R.id.edit_identifiant);
        textPassword = findViewById(R.id.edit_MotDePasse);
        textConfPassword = findViewById(R.id.edit_ConfirmerMotDePasse);
        boutonCreer = findViewById(R.id.BoutonCreer);
        TextView textJAiUnCompte = findViewById(R.id.JAiUnCompte);
        textJAiUnCompte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(CreerCompte.this, MainActivity.class);
                CreerCompte.this.startActivity(registerIntent);
            }
        });

        user = new User();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() { super.onResume(); }

    @Override
    protected void onPause() { super.onPause(); }

    @Override
    protected void onStop() { super.onStop(); }

    @Override
    public void onDestroy() { super.onDestroy(); }

    public void creerUnCompte(View view) {
        if(!(textPassword.getText().toString().trim().equals(textConfPassword.getText().toString().trim()))){
            Toast.makeText(CreerCompte.this,"Erreur dans la saisie des mots de passes",Toast.LENGTH_SHORT).show();
        }

        if (!db.checkUser(textUsername.getText().toString().trim())) {

            user.setName(textUsername.getText().toString().trim());
            user.setPassword(textPassword.getText().toString().trim());
            db.addUser(user);

            Toast.makeText(CreerCompte.this,"Compte Créé",Toast.LENGTH_SHORT).show();
            emptyInputEditText();

            Intent connect = new Intent(CreerCompte.this, MainActivity.class);
            startActivity(connect);
        }
    }

    private void emptyInputEditText(){
        textUsername.setText(null);
        textPassword.setText(null);
        textConfPassword.setText(null);
    }

    @SuppressLint("ResourceType")
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.layout.activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menunba:
                Toast.makeText(CreerCompte.this, "Cette option n'est pas utilisable ici", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.quitter:
                Toast.makeText(CreerCompte.this, "Quitter", Toast.LENGTH_SHORT).show();
                onDestroy();
                return true;
            case R.id.info:
                Toast.makeText(this, "A Propos", Toast.LENGTH_SHORT).show();
                Intent menu = new Intent(this, Info.class);
                startActivity(menu);
                return true;
        }
        return false;
    }
}
