import java.util.Scanner;

class Fibonacci {

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int n=0;
			
		System.out.print("Entrer le nombre de terme a afficher : ");
		n = clavier.nextInt();
		
		Fibonacci(n);
    }

    public static long factorielle(long n) {
        long sum=1;


        for (int i=1; i<=n; i++) {
            sum = sum * i;
        }
        return (sum);
    }

    public static long coeffBinomial(long n, long p) {
        long binom, np;

        np = factorielle(n-p);
        n = factorielle(n);
        p = factorielle(p);

        binom = n/(p*np);

        return (binom);
    }

    public static long coeffMaximum(int n) {
        long max=0, ligne;

        for (int i=0; i<=n; i++) {
            ligne = coeffBinomial(n, i);

            if (max < ligne)
                max=ligne;
        }

        return (max);
    }
	
	public static int ligneMax(long seuil) {
		long max=0;
		int i=1;
		
		while (max<seuil) {
			max = coeffMaximum(i);
			i++;
		}
		
		return(i);
	}
	
	public static long puissance2(long n) {
		long puis=1;
		
		for (int i=1; i==n; i++) {
			puis*=2;
		}
		return(puis);
	}
	
	public static long sommeCoefficient(long n) {
		long somme=0;
		
		for (int i=0; i<=n; i++) {
			for (int j=0; j<=i ; j++) {
				somme += coeffBinomial(i, j);
			}
		}
		return(somme);
	}
	
	public static int lapin(int n) {
		int lap1=1, lap2=0, lap3=0;
		
		for (int i=1; i<=n; i++) {
			lap1 = lap1 + lap3;
			lap3 = lap2;
			lap2 = lap1;
		}
		return(lap1);
	}
	
	public static void Fibonacci(int n) {
		int Fibo=0;
		
		for (int i=1; i<=n; i++) {
			Fibo = lapin(i);
			System.out.println(Fibo);
		}
	}
}
