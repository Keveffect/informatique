/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monaco_gp;

import java.awt.Graphics;
import java.awt.Color;

/**
 *
 * @author char0121
 */
public class NewJPanel2 extends javax.swing.JPanel {  // niveau 1
    int ArriveY;
    int carX;
    int carY;
    int blocY;
    int blocX;
    int blocX1;
    int blocY1;
    int blocX2;
    int blocY2;
    int blocX3;
    int blocY3;
    int blocX4;
    int blocY4;
    int blocX5;
    int blocY5;
    int blocX6;
    int blocY6;
    int blocX7;
    int blocY7;
    int blocX8;
    int blocY8;
    int blocX9;
    int blocY9;
    int blocX10;
    int blocY10;
    int blocX11;
    int blocY11;
    int blocX12;
    int blocY12;
    int blocX13;
    int blocY13;
    int blocX14;
    int blocY14;
    int blocX15;
    int blocY15;
    int blocX16;
    int blocY16;
    int blocX17;
    int blocY17;
    int blocX18;
    int blocY18;
    int blocX19;
    int blocY19;
    int blocX20;
    int blocY20;
    //int hauteur=getHeight();
    
    /**
     * Creates new form NewJPanel
     */
    public NewJPanel2() {
        initComponents();
        ArriveY = -2205;
        carX = 505;
        carY = 450;
        blocX = 375;
        blocY = 5;
        blocX1 = 175;  
        blocY1 = 15;   
        blocX2 = 275;
        blocY2 = -145;
        blocX3 = 375;
        blocY3 = -245;
        blocX4 = 575;
        blocY4 = -545;
        blocX5 = 175;
        blocY5 = -545;
        blocX6 = 280 ;
        blocY6 = -650;
        blocX7 = 305;
        blocY7 = -755;
        blocX8 = 575;
        blocY8 = -1000;
        blocX9 = 600;
        blocY9 = -1150;
        blocX10 = 150;
        blocY10 = -1150;
        blocX11 = 150;  
        blocY11 = -1250;   
        blocX12 = 500;
        blocY12 = -1350;
        blocX13 = 375;
        blocY13 = -1450;
        blocX14 = 575;
        blocY14 = -1550;
        blocX15 = 175;
        blocY15 = -1650;
        blocX16 = 300 ;
        blocY16 = -1750;
        blocX17 = 150;
        blocY17 = -1850;
        blocX18 = 575;
        blocY18 = -1950;
        blocX19 = 425;
        blocY19 = -2050;
        blocX20 = 150;
        blocY20 = -2150;
    }
    //*****************************elements graphiques********************************************************************************
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
    
        g.setColor(Color.red);
        g.fillRect(700,0, 5, 550); // limitte droite
        g.fillRect(150,0, 5, 550); // limitte gauche
        
        g.setColor(Color.black);
        g.fillRect(155,0,545, 550); // route
        
        g.setColor(Color.white);
        g.fillRect(getWidth()/2-15,5, 10, 30); // P1
        g.fillRect(getWidth()/2-15,60, 10, 30); // P2
        g.fillRect(getWidth()/2-15,115, 10, 30); // P3
        g.fillRect(getWidth()/2-15,170, 10, 30); // P4
        g.fillRect(getWidth()/2-15,225, 10, 30); // P5
        g.fillRect(getWidth()/2-15,280, 10, 30); // P6
        g.fillRect(getWidth()/2-15,335, 10, 30); // P7
        g.fillRect(getWidth()/2-15,390, 10, 30); // P8
        g.fillRect(getWidth()/2-15,445, 10, 30); // P9
        g.fillRect(getWidth()/2-15,500, 10, 30); // P10
        
        g.setColor(Color.black);
        g.fillRect(155,ArriveY,545, 25); // arrivée
        g.setColor(Color.white);
        g.fillRect(155,blocY-2195,545, 5);
        g.fillRect(155,blocY-2205,545, 5);
        
        g.setColor(Color.blue);
        g.fillRect(carX+25, carY-50, 20 ,110 ); //centre
        
        g.setColor(Color.DARK_GRAY);
        g.fillRect(carX, carY-40, 20, 30); //roue avant gauche
        g.fillRect(carX, carY+20, 20, 30); //roue arriere gauche
        g.fillRect(carX+50, carY-40, 20, 30);  // roue avant droite
        g.fillRect(carX+50, carY+20, 20, 30);  // roue arriere droite       voiture
        
        g.setColor(Color.green);
        g.fillRect(carX+30, carY-45, 10 ,100 ); //centre centre
        
        g.setColor(Color.green);
        g.fillRect(blocX-200,blocY+10, 100, 100); // bloque ennemi 1
        g.fillRect(blocX-100,blocY-150, 100, 100); // bloque ennemi 2
        g.fillRect(blocX,blocY-250, 100, 100); // bloque ennemi 3
        g.fillRect(blocX4,blocY4, 100, 100); // bloque ennemi 4
        g.fillRect(blocX5,blocY5, 100, 100); // bloque ennemi 5
        g.fillRect(blocX6,blocY6, 100, 100); // bloque ennemi 6
        g.fillRect(blocX7,blocY7, 100, 100); // bloque ennemi 7
        g.fillRect(blocX8,blocY8, 100, 100); // bloque ennemi 8
        g.fillRect(blocX9,blocY9, 100, 100); // bloque ennemi 9
        g.fillRect(blocX10,blocY10, 100, 100); // bloque ennemi 10
        g.fillRect(blocX11,blocY11, 100, 100); // bloque ennemi 11
        g.fillRect(blocX12,blocY12, 100, 100); // bloque ennemi 12
        g.fillRect(blocX13,blocY13, 100, 100); // bloque ennemi 13
        g.fillRect(blocX14,blocY14, 100, 100); // bloque ennemi 14
        g.fillRect(blocX15,blocY15, 100, 100); // bloque ennemi 15
        g.fillRect(blocX16,blocY16, 100, 100); // bloque ennemi 16
        g.fillRect(blocX17,blocY17, 100, 100); // bloque ennemi 17
        g.fillRect(blocX18,blocY18, 100, 100); // bloque ennemi 18
        g.fillRect(blocX19,blocY19, 100, 100); // bloque ennemi 19
        g.fillRect(blocX20,blocY20, 100, 100); // bloque ennemi 20
        
       //********************************ennemi 1**************************************************************
        g.setColor(Color.yellow);
        g.fillRect(blocX-200,blocY+25, 100, 5); // barre du haut
        g.fillRect(blocX-200,blocY+90, 100, 5); // barre du bas
        
        g.fillRect(blocX-185,blocY+50, 15, 5);   // T
        g.fillRect(blocX-180,blocY+50, 5, 20);
        
        g.fillRect(blocX-160,blocY+50, 5, 20);  // N
        g.fillRect(blocX-140,blocY+50, 5, 20);
        g.fillRect(blocX-155,blocY+50, 5, 5);
        g.fillRect(blocX-150,blocY+55, 5, 5);
        g.fillRect(blocX-145,blocY+60, 5, 5);

        g.fillRect(blocX-130,blocY+50, 15, 5);  // T
        g.fillRect(blocX-125,blocY+50, 5, 20);
        
        //**********************************ennemi 2**********************************************************
        g.fillRect(blocX-100,blocY-135, 100, 5); // barre du haut
        g.fillRect(blocX-100,blocY-70, 100, 5); // barre du bas
        
        g.fillRect(blocX-85,blocY-110, 15, 5);   // T
        g.fillRect(blocX-80,blocY-110, 5, 20);
        
        g.fillRect(blocX-60,blocY-110, 5, 20);  // N
        g.fillRect(blocX-40,blocY-110, 5, 20);
        g.fillRect(blocX-55,blocY-110, 5, 5);
        g.fillRect(blocX-50,blocY-105, 5, 5);
        g.fillRect(blocX-45,blocY-100, 5, 5);

        g.fillRect(blocX-30,blocY-110, 15, 5);  // T
        g.fillRect(blocX-25,blocY-110, 5, 20);
        
          //**********************************ennemi 3**********************************************************
        g.fillRect(blocX,blocY-235, 100, 5); // barre du haut
        g.fillRect(blocX,blocY-170, 100, 5); // barre du bas
        
        g.fillRect(blocX+15,blocY-215, 15, 5);   // T
        g.fillRect(blocX+20,blocY-215, 5, 20);
        
        g.fillRect(blocX+40,blocY-215, 5, 20);  // N
        g.fillRect(blocX+60,blocY-215, 5, 20);
        g.fillRect(blocX+45,blocY-215, 5, 5);
        g.fillRect(blocX+50,blocY-210, 5, 5);
        g.fillRect(blocX+55,blocY-205, 5, 5);

        g.fillRect(blocX+70,blocY-215, 15, 5);  // T
        g.fillRect(blocX+75,blocY-215, 5, 20);
        
        //********************************ennemi 4**************************************************************
        g.fillRect(blocX+200,blocY-535, 100, 5); // barre du haut
        g.fillRect(blocX+200,blocY-470, 100, 5); // barre du bas
        
        g.fillRect(blocX+275,blocY-510, 15, 5);   // T
        g.fillRect(blocX+280,blocY-510, 5, 20);
        
        g.fillRect(blocX+260,blocY-510, 5, 20);  // N
        g.fillRect(blocX+240,blocY-510, 5, 20);
        g.fillRect(blocX+245,blocY-510, 5, 5);
        g.fillRect(blocX+250,blocY-505, 5, 5);
        g.fillRect(blocX+255,blocY-500, 5, 5);

        g.fillRect(blocX+220,blocY-510, 15, 5);  // T
        g.fillRect(blocX+225,blocY-510, 5, 20);
        
        //********************************ennemi 5**************************************************************
        g.fillRect(blocX-200,blocY-535, 100, 5); // barre du haut
        g.fillRect(blocX-200,blocY-470, 100, 5); // barre du bas
        
        g.fillRect(blocX-185,blocY-510, 15, 5);   // T
        g.fillRect(blocX-180,blocY-510, 5, 20);
        
        g.fillRect(blocX-160,blocY-510, 5, 20);  // N
        g.fillRect(blocX-140,blocY-510, 5, 20);
        g.fillRect(blocX-155,blocY-510, 5, 5);
        g.fillRect(blocX-150,blocY-505, 5, 5);
        g.fillRect(blocX-145,blocY-500, 5, 5);

        g.fillRect(blocX-130,blocY-510, 15, 5);  // T
        g.fillRect(blocX-125,blocY-510, 5, 20);
        
         //********************************ennemi 6**************************************************************
        g.fillRect(blocX-95,blocY-640, 100, 5); // barre du haut
        g.fillRect(blocX-95,blocY-575, 100, 5); // barre du bas
        
        g.fillRect(blocX-80,blocY-615, 15, 5);   // T
        g.fillRect(blocX-75,blocY-615, 5, 20);
        
        g.fillRect(blocX-55,blocY-615, 5, 20);  // N
        g.fillRect(blocX-35,blocY-615, 5, 20);
        g.fillRect(blocX-50,blocY-615, 5, 5);
        g.fillRect(blocX-45,blocY-610, 5, 5);
        g.fillRect(blocX-40,blocY-605, 5, 5);

        g.fillRect(blocX-25,blocY-615, 15, 5);  // T
        g.fillRect(blocX-20,blocY-615, 5, 20);
         //********************************ennemi 7**************************************************************
        g.fillRect(blocX-70,blocY-745, 100, 5); // barre du haut
        g.fillRect(blocX-70,blocY-680, 100, 5); // barre du bas
        
        g.fillRect(blocX-55,blocY-720, 15, 5);   // T
        g.fillRect(blocX-50,blocY-720, 5, 20);
        
        g.fillRect(blocX-30,blocY-720, 5, 20);  // N
        g.fillRect(blocX-10,blocY-720, 5, 20);
        g.fillRect(blocX-25,blocY-720, 5, 5);
        g.fillRect(blocX-20,blocY-715, 5, 5);
        g.fillRect(blocX-15,blocY-710, 5, 5);

        g.fillRect(blocX,blocY-720, 15, 5);  // T
        g.fillRect(blocX+5,blocY-720, 5, 20);
         //********************************ennemi 8**************************************************************
        g.fillRect(blocX+200,blocY-990, 100, 5); // barre du haut
        g.fillRect(blocX+200,blocY-925, 100, 5); // barre du bas
        
        g.fillRect(blocX+215,blocY-965, 15, 5);   // T
        g.fillRect(blocX+220,blocY-965, 5, 20);
        
        g.fillRect(blocX+240,blocY-965, 5, 20);  // N
        g.fillRect(blocX+260,blocY-965, 5, 20);
        g.fillRect(blocX+245,blocY-965, 5, 5);
        g.fillRect(blocX+250,blocY-960, 5, 5);
        g.fillRect(blocX+255,blocY-955, 5, 5);

        g.fillRect(blocX+270,blocY-965, 15, 5);  // T
        g.fillRect(blocX+275,blocY-965, 5, 20);
         //********************************ennemi 9**************************************************************
        g.fillRect(blocX+225,blocY-1140, 100, 5); // barre du haut
        g.fillRect(blocX+225,blocY-1075, 100, 5); // barre du bas
        
        g.fillRect(blocX+240,blocY-1115, 15, 5);   // T
        g.fillRect(blocX+245,blocY-1115, 5, 20);
        
        g.fillRect(blocX+265,blocY-1115, 5, 20);  // N
        g.fillRect(blocX+285,blocY-1115, 5, 20);
        g.fillRect(blocX+270,blocY-1115, 5, 5);
        g.fillRect(blocX+275,blocY-1110, 5, 5);
        g.fillRect(blocX+280,blocY-1105, 5, 5);

        g.fillRect(blocX+295,blocY-1115, 15, 5);  // T
        g.fillRect(blocX+300,blocY-1115, 5, 20);
         //********************************ennemi 10**************************************************************
        g.fillRect(blocX-225,blocY-1140, 100, 5); // barre du haut
        g.fillRect(blocX-225,blocY-1075, 100, 5); // barre du bas
        
        g.fillRect(blocX-210,blocY-1115, 15, 5);   // T
        g.fillRect(blocX-205,blocY-1115, 5, 20);
        
        g.fillRect(blocX-185,blocY-1115, 5, 20);  // N
        g.fillRect(blocX-165,blocY-1115, 5, 20);
        g.fillRect(blocX-180,blocY-1115, 5, 5);
        g.fillRect(blocX-175,blocY-1110, 5, 5);
        g.fillRect(blocX-170,blocY-1105, 5, 5);

        g.fillRect(blocX-155,blocY-1115, 15, 5);  // T
        g.fillRect(blocX-150,blocY-1115, 5, 20);
         //********************************ennemi 11**************************************************************
        g.fillRect(blocX-225,blocY-1240, 100, 5); // barre du haut
        g.fillRect(blocX-225,blocY-1175, 100, 5); // barre du bas
        
        g.fillRect(blocX-215,blocY-1215, 15, 5);   // T
        g.fillRect(blocX-210,blocY-1215, 5, 20);
        
        g.fillRect(blocX-185,blocY-1215, 5, 20);  // N
        g.fillRect(blocX-165,blocY-1215, 5, 20);
        g.fillRect(blocX-180,blocY-1215, 5, 5);
        g.fillRect(blocX-175,blocY-1210, 5, 5);
        g.fillRect(blocX-170,blocY-1205, 5, 5);

        g.fillRect(blocX-155,blocY-1215, 15, 5);  // T
        g.fillRect(blocX-150,blocY-1215, 5, 20);
         //********************************ennemi 12**************************************************************
        g.fillRect(blocX+125,blocY-1340, 100, 5); // barre du haut
        g.fillRect(blocX+125,blocY-1275, 100, 5); // barre du bas
        
        g.fillRect(blocX+135,blocY-1315, 15, 5);   // T
        g.fillRect(blocX+140,blocY-1315, 5, 20);
        
        g.fillRect(blocX+165,blocY-1315, 5, 20);  // N
        g.fillRect(blocX+185,blocY-1315, 5, 20);
        g.fillRect(blocX+170,blocY-1315, 5, 5);
        g.fillRect(blocX+175,blocY-1310, 5, 5);
        g.fillRect(blocX+180,blocY-1305, 5, 5);

        g.fillRect(blocX+195,blocY-1315, 15, 5);  // T
        g.fillRect(blocX+200,blocY-1315, 5, 20);
         //********************************ennemi 13**************************************************************
        g.fillRect(blocX,blocY-1440, 100, 5); // barre du haut
        g.fillRect(blocX,blocY-1380, 100, 5); // barre du bas
        
        g.fillRect(blocX+15,blocY-1415, 15, 5);   // T
        g.fillRect(blocX+20,blocY-1415, 5, 20);
        
        g.fillRect(blocX+40,blocY-1415, 5, 20);  // N
        g.fillRect(blocX+60,blocY-1415, 5, 20);
        g.fillRect(blocX+45,blocY-1415, 5, 5);
        g.fillRect(blocX+50,blocY-1410, 5, 5);
        g.fillRect(blocX+55,blocY-1405, 5, 5);

        g.fillRect(blocX+70,blocY-1415, 15, 5);  // T
        g.fillRect(blocX+75,blocY-1415, 5, 20);
         //********************************ennemi 14**************************************************************
        g.fillRect(blocX+200,blocY-1540, 100, 5); // barre du haut
        g.fillRect(blocX+200,blocY-1475, 100, 5); // barre du bas
        
        g.fillRect(blocX+215,blocY-1520, 15, 5);   // T
        g.fillRect(blocX+220,blocY-1520, 5, 20);
        
        g.fillRect(blocX+240,blocY-1520, 5, 20);  // N
        g.fillRect(blocX+260,blocY-1520, 5, 20);
        g.fillRect(blocX+245,blocY-1520, 5, 5);
        g.fillRect(blocX+250,blocY-1515, 5, 5);
        g.fillRect(blocX+255,blocY-1510, 5, 5);

        g.fillRect(blocX+270,blocY-1520, 15, 5);  // T
        g.fillRect(blocX+275,blocY-1520, 5, 20);
         //********************************ennemi 15**************************************************************
        g.fillRect(blocX-200,blocY-1640, 100, 5); // barre du haut
        g.fillRect(blocX-200,blocY-1575, 100, 5); // barre du bas
        
        g.fillRect(blocX-185,blocY-1620, 15, 5);   // T
        g.fillRect(blocX-180,blocY-1620, 5, 20);
        
        g.fillRect(blocX-160,blocY-1620, 5, 20);  // N
        g.fillRect(blocX-140,blocY-1620, 5, 20);
        g.fillRect(blocX-155,blocY-1620, 5, 5);
        g.fillRect(blocX-150,blocY-1615, 5, 5);
        g.fillRect(blocX-145,blocY-1610, 5, 5);

        g.fillRect(blocX-130,blocY-1620, 15, 5);  // T
        g.fillRect(blocX-125,blocY-1620, 5, 20);
         //********************************ennemi 16**************************************************************
        g.fillRect(blocX-75,blocY-1740, 100, 5); // barre du haut
        g.fillRect(blocX-75,blocY-1675, 100, 5); // barre du bas
        
        g.fillRect(blocX-60,blocY-1720, 15, 5);   // T
        g.fillRect(blocX-55,blocY-1720, 5, 20);
        
        g.fillRect(blocX-35,blocY-1720, 5, 20);  // N
        g.fillRect(blocX-15,blocY-1720, 5, 20);
        g.fillRect(blocX-30,blocY-1720, 5, 5);
        g.fillRect(blocX-25,blocY-1715, 5, 5);
        g.fillRect(blocX-20,blocY-1710, 5, 5);

        g.fillRect(blocX-5,blocY-1720, 15, 5);  // T
        g.fillRect(blocX,blocY-1720, 5, 20);
         //********************************ennemi 17**************************************************************
        g.fillRect(blocX-225,blocY-1840, 100, 5); // barre du haut
        g.fillRect(blocX-225,blocY-1775, 100, 5); // barre du bas
        
        g.fillRect(blocX-210,blocY-1820, 15, 5);   // T
        g.fillRect(blocX-205,blocY-1820, 5, 20);
        
        g.fillRect(blocX-185,blocY-1820, 5, 20);  // N
        g.fillRect(blocX-165,blocY-1820, 5, 20);
        g.fillRect(blocX-180,blocY-1820, 5, 5);
        g.fillRect(blocX-175,blocY-1815, 5, 5);
        g.fillRect(blocX-170,blocY-1810, 5, 5);

        g.fillRect(blocX-155,blocY-1820, 15, 5);  // T
        g.fillRect(blocX-150,blocY-1820, 5, 20);
         //********************************ennemi 18**************************************************************
        g.fillRect(blocX+200,blocY-1940, 100, 5); // barre du haut
        g.fillRect(blocX+200,blocY-1875, 100, 5); // barre du bas
        
        g.fillRect(blocX+215,blocY-1920, 15, 5);   // T
        g.fillRect(blocX+220,blocY-1920, 5, 20);
        
        g.fillRect(blocX+240,blocY-1920, 5, 20);  // N
        g.fillRect(blocX+260,blocY-1920, 5, 20);
        g.fillRect(blocX+245,blocY-1920, 5, 5);
        g.fillRect(blocX+250,blocY-1915, 5, 5);
        g.fillRect(blocX+255,blocY-1910, 5, 5);

        g.fillRect(blocX+270,blocY-1920, 15, 5);  // T
        g.fillRect(blocX+275,blocY-1920, 5, 20);
         //********************************ennemi 19**************************************************************
        g.fillRect(blocX+50,blocY-2045, 100, 5); // barre du haut
        g.fillRect(blocX+50,blocY-1975, 100, 5); // barre du bas
        
        g.fillRect(blocX+65,blocY-2020, 15, 5);   // T
        g.fillRect(blocX+70,blocY-2020, 5, 20);
        
        g.fillRect(blocX+90,blocY-2020, 5, 20);  // N
        g.fillRect(blocX+110,blocY-2020, 5, 20);
        g.fillRect(blocX+95,blocY-2020, 5, 5);
        g.fillRect(blocX+100,blocY-2015, 5, 5);
        g.fillRect(blocX+105,blocY-2010, 5, 5);

        g.fillRect(blocX+120,blocY-2020, 15, 5);  // T
        g.fillRect(blocX+125,blocY-2020, 5, 20);
         //********************************ennemi 20**************************************************************
        g.fillRect(blocX-225,blocY-2145, 100, 5); // barre du haut
        g.fillRect(blocX-225,blocY-2075, 100, 5); // barre du bas
        
        g.fillRect(blocX-210,blocY-2120, 15, 5);   // T
        g.fillRect(blocX-205,blocY-2120, 5, 20);
        
        g.fillRect(blocX-185,blocY-2120, 5, 20);  // N
        g.fillRect(blocX-165,blocY-2120, 5, 20);
        g.fillRect(blocX-180,blocY-2120, 5, 5);
        g.fillRect(blocX-175,blocY-2115, 5, 5);
        g.fillRect(blocX-170,blocY-2110, 5, 5);

        g.fillRect(blocX-155,blocY-2120, 15, 5);  // T
        g.fillRect(blocX-150,blocY-2120, 5, 20);
        
        //***************************************DECO**********************************************************************
        g.setColor(Color.red);
        g.fillRect(blocX+410,blocY, 10, 10); // toit top
        g.fillRect(blocX+400,blocY+10, 30, 10); // toit mid
        g.fillRect(blocX+390,blocY+20, 50, 10); // toit bott             maison
        g.setColor(Color.orange);
        g.fillRect(blocX+400,blocY+30, 30, 30); // mur
        g.setColor(Color.DARK_GRAY);
        g.fillRect(blocX+410,blocY+40, 10, 20); // porte
        
        g.setColor(Color.green);
        g.fillRect(blocX+370,blocY+60, 10, 10); // feuille top
        g.fillRect(blocX+360,blocY+70, 30, 10); // feuille mid top
        g.fillRect(blocX+360,blocY+80, 30, 10); // feuille mid bott     arbre 1
        g.fillRect(blocX+370,blocY+90, 10, 10); // feuille bott
        
        g.setColor(Color.DARK_GRAY);
        g.fillRect(blocX+370,blocY+100, 10, 20); // tron
        
         g.setColor(Color.green);
        g.fillRect(blocX+450,blocY+60, 10, 10); // feuille top
        g.fillRect(blocX+440,blocY+70, 30, 10); // feuille mid top
        g.fillRect(blocX+440,blocY+80, 30, 10); // feuille mid bott     arbre 2
        g.fillRect(blocX+450,blocY+90, 10, 10); // feuille bott
        
        g.setColor(Color.DARK_GRAY);
        g.fillRect(blocX+450,blocY+100, 10, 20); // tron
       
        g.setColor(Color.DARK_GRAY);
        g.fillRect(blocX-368,blocY-137, 45, 90); // tour gauche
        g.fillRect(blocX-379,blocY-146, 63, 9); 
        g.fillRect(blocX-325,blocY-109, 63, 54); //mid
        g.fillRect(blocX-275,blocY-137, 45, 90); // tour droite     //chateau
        g.fillRect(blocX-284,blocY-146, 63, 9); 
        g.setColor(Color.black);
        g.fillRect(blocX-314,blocY-92, 30, 36); // porte
        g.fillRect(blocX-355,blocY-130, 20,20); // fenettre gauche
        g.fillRect(blocX-265,blocY-130, 20,20); // fenettre droite
         
        g.setColor(Color.YELLOW);
        g.fillOval(blocX+370, blocY-300, 100, 100);
        g.setColor(Color.black);
        g.fillArc(blocX+370, blocY-265, 80, 80, 10, 10);
        g.fillRect(blocX+400,blocY-275, 5,5);
        g.fillRect(blocX+430,blocY-275, 5,5);
        
        g.setColor(Color.YELLOW);
        g.fillRect(5,35, 140, 30); // Score
        
    }
    
    //***************************code brute************************************************************************

    public void setX(int x)
    {
        if(carX<175){
           this.carX=175;
        }
        else{
            this.carX+=x;
        }
        if(carX>630){
              this.carX=630;
        }
        else{
            this.carX+=x;
        }
    }
    
    public void setY(int y)
    {
      this.blocY+=y;
      this.blocY1+=y;
      this.blocY2+=y;
      this.blocY3+=y;
      this.blocY4+=y;
      this.blocY5+=y;
      this.blocY6+=y;
      this.blocY7+=y;
      this.blocY8+=y;
      this.blocY9+=y;
      this.blocY10+=y;
      this.blocY11+=y;
      this.blocY12+=y;
      this.blocY13+=y;
      this.blocY14+=y;
      this.blocY15+=y;
      this.blocY16+=y;
      this.blocY17+=y;
      this.blocY18+=y;
      this.blocY19+=y;
      this.blocY20+=y;
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>                        


    // Variables declaration - do not modify                     
    // End of variables declaration                   
}