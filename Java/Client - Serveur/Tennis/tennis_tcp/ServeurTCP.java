package tennis_tcp;

import JSON.*;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import org.json.JSONObject;
import java.lang.invoke.MethodHandles;
import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Classe correspondant à un serveur TCP.
 * Communique avec les clients pour les cnnecter 2 par 2.
 * Le numéro de port du serveur est spécifié dans la classe dans un
 * objet fichier configServeurTCP.json dans le dossier JSON.
 * @author Samuel Bertin / Kevin Charlier
 */
public class ServeurTCP {

    public static void main(String[] args) {
        JSONObject obj;
        int numClient = 0;
        int portEcoute;
        String adresse1 = null;
        int port1 = 0;

        // Création de de la configuration du serveur
        String fichier = "config"+ MethodHandles.lookup().lookupClass().getSimpleName() + ".json";
        if(!Config.fichierExiste(fichier)) {
            GestionJSON.creerFichierConfiguation(fichier);  // dans dossier JSON
            System.out.println("creer config "+fichier);
        }
        
        // Recuperation des donnée du fichier de configuration 
        obj = GestionJSON.lectureAdresse(fichier);      // dans dossier JSON
        portEcoute = obj.getInt("port");
        obj.remove("port");
        obj.remove("adresse");
        System.out.println("Port du serveur : "+portEcoute);

        // Création de la socket serveur
        ServerSocket socketServeur = null;
        try {    
            socketServeur = new ServerSocket(portEcoute);
        } catch(IOException e) {
            System.err.println("Création de la socket impossible : " + e);
            System.exit(0);
        }

        do
        {
            // Attente d'une connexion d'un client
            Socket socketClient = null;
            try {
                socketClient = socketServeur.accept();
                ++numClient;
            } catch(IOException e) {
                System.err.println("Erreur lors de l'attente d'une connexion : " + e);
                System.exit(0);
            }

            // Association d'un flux d'entrée et de sortie
            BufferedReader input = null;
            PrintWriter output = null;
            try {
                input = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
                output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream())), true);
            } catch(IOException e) {
                System.err.println("Association des flux impossible : " + e);
                System.exit(0);
            }

            // Lecture du message du client
            try {
                obj = new JSONObject(input.readLine());
            } catch(IOException e) {
                System.err.println("Erreur lors de la lecture : " + e);
                System.exit(0);
            }

            // Traitement des données du client
            if(numClient%2 == 1){
                // System.out.println("(1)Client"+numClient+"\tJSON : "+obj);
                adresse1 = obj.getString("adresse");
                port1 = obj.getInt("port");
            }
            obj.remove("adresse");
            obj.remove("port");
            obj.put("enregistre", "true");

            // ajoute les informations du client 2k+1 (tel que k € N) pour l'autre client
            if(numClient % 2 == 0){
                // System.out.println("Client"+numClient+"\tdonnée : "+obj);
                obj.put("adresse", adresse1);
                obj.put("port", port1);
            } 
            String reponse = obj.toString();

            // Réponse au client
            // System.out.println("JSON envoie : "+reponse+"\n");
            output.println(reponse);

            // Fermeture des flux et des sockets
            try {
                input.close();
                output.close();
                socketClient.close();
            } catch(IOException e) {
                System.err.println("Erreur lors de la fermeture des flux et des sockets : " + e);
                System.exit(0);
            }

            if(numClient%2 == 0) {
                System.out.println("Les 2 clients ont ete mis en relation\nArreter le Serveur : Oui\t\t(sinon appuyer sur n'importe quelle touche pour accepter d'autre clients)");
                Scanner rep = new Scanner(System.in);
                String choix = rep.nextLine(); 
                if(choix.equalsIgnoreCase("Y") || choix.equalsIgnoreCase("Yes") || choix.equalsIgnoreCase("O") || choix.equalsIgnoreCase("Oui")) 
                    break;
                System.out.println("Vous aurez a nouveau la possibilit\u00e9s de fermer le serveur lors de la creation des prochains clients.");
            }
        } while(true);

        // Fermeture des flux et des sockets
        try {
            socketServeur.close();
        } catch(IOException e) {
            System.err.println("Erreur lors de la fermeture des flux et des sockets : " + e);
            System.exit(0);
        }
    }

}