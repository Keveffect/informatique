package tennis_tcp;

import JSON.GestionJSON;
import tennis.*;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import org.json.JSONObject;
import org.json.JSONArray;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Classe correspondant à un client TCP.
 * Le client envoie la chaine 'Bonjour' et lit une réponse de la part du serveur.
 * Le client envoie ensuite la chaine 'Au revoir' et lit une réponse.
 * Le numéro de port du serveur est spécifié dans la classe ServeurTCP.
 * @author Cyril Rabat
 */
public class ClientTCP {

    public static void main(String[] args) {
        int numClient = 1;
        JSONObject obj;
        Scanner sc = new Scanner(System.in);
        String nomJoueur = null;
        String nomAdversaire = null;
        
        // Récuperation du port d'écoute du serveur 
        obj = GestionJSON.lectureAdresse("configServeurTCP.json");
        int portEcouteServeur = obj.getInt("port");
        String adresseServeur = obj.getString("adresse");
        obj.remove("port");
        obj.remove("adresse");

        // Création de la socket
        Socket socket = null;
        try {
            socket = new Socket(adresseServeur, portEcouteServeur);
        } catch(UnknownHostException e) {
            System.err.println("Erreur sur l'hôte : " + e);
            System.exit(0);
        } catch(IOException e) {
            System.err.println("Création de la socket impossible : " + e);
            System.exit(0);
        }
    
        // Association d'un flux d'entrée et de sortie
        BufferedReader input = null;
        PrintWriter output = null;
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        } catch(IOException e) {
            System.err.println("Association des flux impossible : " + e);
            System.exit(0);
        }

        int monPort = socket.getLocalPort();                 // recupere le port depuis le datagramSocket
        String monAdresse = null;
        try {
            monAdresse = InetAddress.getLocalHost().getHostAddress();  // garde que la partie IP de l'Inet adresse
        } catch(UnknownHostException e) {
            monAdresse = InetAddress.getLoopbackAddress().getHostAddress();
            System.err.println("Erreur lors de la création de l'adresse : " + e);
        }

        // Création de l'objet JSON pour l'échange
        obj.put("adresse", monAdresse);
        obj.put("port", monPort);                     
        String json = obj.toString(); 
        // System.out.println("JSON envoye : "+json);
    
        // Envoi de l'adresse et du port
        output.println(json);

        // Lecture du message du Serveur
        try {
            obj = new JSONObject(input.readLine());
        } catch(IOException e) {
            System.err.println("Erreur lors de la lecture : " + e);
            System.exit(0);
        }
        // System.out.println("JSON recu : " + obj);
       
        // Fermeture des flux et de la socket
        try {
            input.close();
            output.close();
            socket.close();
        } catch(IOException e) {
            System.err.println("Erreur lors de la fermeture des flux et de la socket : " + e);
            System.exit(0);
        }

        // traintement des données recu
        boolean enregistre = obj.getBoolean("enregistre");
        
        String adresseClient = null;
        int portClient = 0;
        if(!obj.isNull("adresse") && !obj.isNull("port")) {
            numClient = 2;                
            adresseClient = obj.getString("adresse");
            portClient = obj.getInt("port");
            // System.out.println("Client1 adresse : "+adresseClient+"\tport : "+portClient);
        } else {
            System.out.println("En attente d'un 2eme joueur ...");
        }
        obj.remove("enregistre");

        

        
        // CLIENT/CLIENT
        String adresseClient2 = null;
        int portClient2 = 0;

        // Création de la socket
        ServerSocket socketServeur = null;

        Socket socketClient1 = null;
        BufferedReader inputClient1 = null;
        PrintWriter outputClient1 = null;

        Socket socketClient2 = null;
        BufferedReader inputClient2 = null;
        PrintWriter outputClient2 = null;

        if(numClient == 1) {
            // Client 1
            // Création de la socket serveur
            try {    
                socketServeur = new ServerSocket(monPort);
            } catch(IOException e) {
                System.err.println("Création de la socket impossible : " + e);
                System.exit(0);
            } 

            // Attente d'une connexion d'un client2
            try {
                socketClient1 = socketServeur.accept();
            } catch(IOException e) {
                System.err.println("Erreur lors de l'attente d'une connexion : " + e);
                System.exit(0);
            }

            // Association d'un flux d'entrée et de sortie pour le client 1
            try {
                inputClient1 = new BufferedReader(new InputStreamReader(socketClient1.getInputStream()));
                outputClient1 = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socketClient1.getOutputStream())), true);
            } catch(IOException e) {
                System.err.println("Association des flux impossible : " + e);
                System.exit(0);
            }

        } else {
            // Client 2
            try {
                socketClient2 = new Socket(adresseClient, portClient);
            } catch(UnknownHostException e) {
                System.err.println("Erreur sur l'hôte : " + e);
                System.exit(0);
            } catch(IOException e) {
                System.err.println("Création de la socket impossible : " + e);
                System.exit(0);
            }

            // Association d'un flux d'entrée et de sortie pour le client 2
            try {
                inputClient2 = new BufferedReader(new InputStreamReader(socketClient2.getInputStream()));
                outputClient2 = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socketClient2.getOutputStream())), true);
            } catch(IOException e) {
                System.err.println("Association des flux impossible : " + e);
                System.exit(0);
            }
        }
        // System.out.println("adresse : "+monAdresse+"\tmon port : "+ monPort);
        

    
        if (numClient == 2) {
            /** Client 2
              * envoie de son adresse et son port au 2eme client 
              * grace a l'adresse et au port recu du serveur 
              */
            // choix du pseudonyme du client 2
            System.out.println("Choisissez votre pseudonyme : ");
            nomJoueur = sc.nextLine();

            obj.put("adresse", monAdresse);                  // defini plus tot 
            obj.put("port", monPort);                      // le port defini avant est le même
            obj.put("nom", nomJoueur);

            // System.out.println("JSON envoye : "+ obj);
            // envoie
            //pour eviter un déréfrencement
            if(outputClient2 != null)
                outputClient2.println(obj.toString());
        
        } else {
            // Client 1
            // lecture
            try {   
                //pour eviter un déréfrencement
                if(inputClient1 != null){
                    obj = new JSONObject(inputClient1.readLine());
                }
            } catch(IOException e) {
                System.err.println("Erreur lors de la lecture : " + e);
                System.exit(0);
            }
            // System.out.println("JSON recu : " + json);
            
            adresseClient2 = obj.getString("adresse");
            portClient2 = obj.getInt("port");

            nomAdversaire = obj.getString("nom");

            // System.out.println("(1) Client 2 : adresse => "+adresseClient2+"\tport +> "+portClient2);
        }

        int choix;
        PartieTennis partie = null;

        boolean serviceClient1 = false;    // pour le client1
        boolean serviceAdvClient2 = false; // pour le client2
        
         // choix du service
         if (numClient == 1) {
            // Client1
            obj.remove("adresse");
            obj.remove("port");
            
            // Choix du pseudonyme du client 1
            System.out.println("Choisissez votre pseudonyme : ");
            nomJoueur = sc.nextLine();

            // choix du service
            serviceClient1 = ( Math.random() >= 0.5 );


            obj.put("service", serviceClient1);
            obj.put("nom", nomJoueur);

            // envoie du Json contenant les données du service et le nom du client 1
            json = obj.toString();

            //pour eviter un déréfrencement
            if (outputClient1 != null)
                outputClient1.println(obj.toString());
            
            if (serviceClient1) {
                System.out.println("Vous allez affronter : "+nomAdversaire+"\n");

               // Création de l'echange
                partie = new PartieTennis(nomJoueur, nomAdversaire);
                GestionTennis.service(partie, sc);
                
                // envoie du Json contenant le premier service
                json = partie.toJSON().toString();
                
                //pour eviter un déréfrencement
                if (outputClient1 != null)
                    outputClient1.println(json);
            }
        }
        else  // Client 2
        {
            // Lecture du message du client1
            try {   
                obj = new JSONObject(inputClient2.readLine());
            } catch(IOException e) {
                System.err.println("Erreur lors de la lecture : " + e);
                System.exit(0);
            }
            // System.out.println("JSON recu: " + obj);


            // recuperation du nom du joueur 2 (chez le client 1)
            nomAdversaire = obj.getString("nom"); 

            // recuperation de l'information sur le joueur au service
            serviceAdvClient2 = obj.getBoolean("service");
            // System.out.println("Client 1 sert : "+ serviceAdvClient2);
    
            if(!serviceAdvClient2) {
                System.out.println("Vous allez affronter : "+nomAdversaire+"\n");
                // Création de l'echange
                partie = new PartieTennis(nomJoueur, nomAdversaire);
                GestionTennis.service(partie, sc);
                
                // envoie du Json contenant le premier service
                json = partie.toJSON().toString();
                //pour eviter un déréfrencement
                if (outputClient2 != null)
                    outputClient2.println(json);
            }
        }
        
        // Boucle de la partie de tennis
        do {
            // Lecture du message de l'adversaire
            if(numClient == 1){
                // Lecture du message du client1
                try { 
                    //pour eviter un déréfrencement
                    if (inputClient1 != null)  
                        json = inputClient1.readLine();
                } catch(IOException e) {
                    System.err.println("Erreur lors de la lecture : " + e);
                    System.exit(0);
                }
            } else {
                // Lecture du message du client1
                try { 
                    //pour eviter un déréfrencement
                    if (inputClient2 != null)
                        json = inputClient2.readLine();
                } catch(IOException e) {
                    System.err.println("Erreur lors de la lecture : " + e);
                    System.exit(0);
                }
            }
            obj = new JSONObject(json);
            System.out.println("\nJSON recu :\n"+ obj);

            
            //recuperation des données
            JSONObject tmp;           // 
            int positionAdversaire;   // récupère les données des joueurs
            int positionJoueur;       //
            JSONArray array = obj.getJSONArray("score");
            int[] score = {array.getInt(0), array.getInt(1)};
            int coup = obj.getInt("coup");
            String infos = obj.getString("infos");
            String commentaire = obj.getString("commentaire");

            if(numClient == 1) {
                tmp = obj.getJSONObject("joueur1");
                positionJoueur = tmp.getInt("position");
                tmp.remove("joueur1");

                tmp = obj.getJSONObject("joueur2");
                positionAdversaire = tmp.getInt("position");
                tmp.remove("joueur2");
            } else {
                tmp = obj.getJSONObject("joueur2");
                positionJoueur = tmp.getInt("position");
                tmp.remove("joueur2");

                tmp = obj.getJSONObject("joueur1");
                positionAdversaire = tmp.getInt("position");
                tmp.remove("joueur1");
            }
            if(numClient == 1) {
                partie = new PartieTennis(score, coup, infos, commentaire, nomJoueur, positionJoueur, nomAdversaire, positionAdversaire);
            } else {
                partie = new PartieTennis(score, coup, infos, commentaire, nomAdversaire, positionAdversaire, nomJoueur, positionJoueur);
            }

            if(numClient == 1) {
                if (score[0] >= 5) {
                    System.out.println("Bravo vous avez gagner");
                }
            } else {
                if (score[1] >= 5) {
                    System.out.println("Bravo vous avez gagner");
                }
            }
            
            // Traitement des donnes recu
            // le 0 represente le score du client1 et le 1 le score du client2
            if(numClient == 1)
                json = GestionTennis.gestionPartieTennis(partie, 0, 1);
            else
                json = GestionTennis.gestionPartieTennis(partie, 1, 0);  

            System.out.println("JSON envoie : "+json);



            // Création et envoi du segment TCP
            if(numClient == 1){
                //pour eviter un déréfrencement
                if (outputClient1 != null)
                    outputClient1.println(json);
            } else {
                //pour eviter un déréfrencement
                if (outputClient2 != null)
                   outputClient2.println(json);
            }

            if(numClient == 1) {
                System.out.println("\nScores :\n"+nomJoueur+" : "+score[1]+"\t\t"+nomAdversaire+" : "+score[0]+"\n");
                if (score[1] >= 5 && score[1] >= score[0]+2) {
                    System.out.println("Vous avez perdu ...");
                    break;
                }
            } else {
                System.out.println("\nScores :\n"+nomJoueur+" : "+score[0]+"\t\t"+nomAdversaire+" : "+score[1]+"\n");
                if (score[0] >= 5 && score[0] >= score[1]+2) {
                    System.out.println("Vous avez perdu ...");
                    break;
                }
            }
        } while(partie.getScore()[1] <= 5 || partie.getScore()[1] < partie.getScore()[0]+2);

        if(numClient == 1){
            // Fermeture des flux et de la socket
            try {
                inputClient1.close();
                outputClient1.close();
                socketClient1.close();
                socketServeur.close();
    
            } catch(IOException e) {
                System.err.println("Erreur lors de la fermeture des flux et de la socket : " + e);
                System.exit(0);
            }
        } else {
            // Fermeture des flux et de la socket
            try {
                //pour eviter un déréfrencement
                if (outputClient2 != null)
                    inputClient2.close();
                if (outputClient2 != null)
                    outputClient2.close();
                if(socketClient2 != null)
                    socketClient2.close();

            } catch(IOException e) {
                System.err.println("Erreur lors de la fermeture des flux et de la socket : " + e);
                System.exit(0);
            }
        }
    }
}