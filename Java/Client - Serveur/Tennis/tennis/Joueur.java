package tennis;

import java.io.Serializable;
import org.json.JSONObject;
import org.json.JSONException;

/**
 * Classe correspondant a une Joueur de tennis
 * @author Samuel Bertin / Kevin Charlier
 */
public class Joueur implements Serializable {
    private int position;
    private String nomJoueur;
    private static int numJoueur = 0;

    /**
     * Constructeur par défaut
     */
    public Joueur() {
        position = 3;
        nomJoueur = "J"+numJoueur++;
    }

    /**
     * Constructeur par initialisation pour service
     * @param joueur le nom du joueur
     */
    public Joueur(String nomJoueur) {
        this.position = 3;
        this.nomJoueur = nomJoueur;
    }

    /**
     * Constructeur par initialisation
     * @param position corresponds a la case sur laquelle se situe le joueur
     * @param joueur le nom du joueur
     */
    public Joueur(String nomJoueur, int position) {
        this.position = position;
        this.nomJoueur = nomJoueur;
    }

    /**
     * @return la position du joueur
     */
    public int getPosition() {
        return position;
    }

    /**
     * @return le nom du joueur
     */
    public String getJoueur() {
        return nomJoueur;
    }

    /**
     * Modifie la position du joueur
     * @param position
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * Modifie le nom du joueur
     * @param joueur
     */
    public void setJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }

    /**
     * Créer l'objet JSON correspondant à une personne
     * @return JSONObject
     */
    public JSONObject toJSON()
    {
        JSONObject object = new JSONObject();

        try {
            object.put("position", position);
            object.put("nomJoueur", nomJoueur);
        } catch(JSONException e) {
            System.err.println("Erreur lors de l'insertion de la partie.");
            System.err.println(e);
            System.exit(-1);
        }
        
        return object;
    }

    /**
     * Transforme le joueur sous forme de chaine de caractères
     * @return une chaine de caractères contenant la partie
     */
    public String toString() {
        return "Nom du joueur : "+ nomJoueur + " position : "+ position;
    }
}