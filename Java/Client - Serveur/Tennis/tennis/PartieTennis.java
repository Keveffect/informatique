package tennis;

import java.io.Serializable;
import org.json.JSONObject;
import org.json.JSONException;

/**
 * Classe correspondant a une Partie de tennis
 * @author Samuel Bertin / Kevin Charlier
 */
public class PartieTennis implements Serializable {
    private int score[];
    private int coup;
    private String infos;
    private String commentaire;
    private Joueur joueur1;
    private Joueur joueur2;

    /**
     * Constructeur par défaut
     */
    public PartieTennis() {
        score = new int[2];
        score[0] = 0;
        score[1] = 0;
        coup = 2;
        infos = "service";
        commentaire = "Let's party !";
        joueur1 = new Joueur();
        joueur2 = new Joueur();
    }

    /**
     * Constructeur par défaut
     */
    public PartieTennis(String nomJoueur1, String nomJoueur2) {
        score = new int[2];
        score[0] = 0;
        score[1] = 0;
        coup = 2;
        infos = "service";
        commentaire = "Let's party !";
        joueur1 = new Joueur(nomJoueur1);
        joueur2 = new Joueur(nomJoueur2);
    }

    /**
     * Constructeur par initialisation
     * @param prenom le prénom de la personne
     * @param nom le nom de la personne
     */
    public PartieTennis(int score[], int coup, String infos, String commentaire, String nomJoueur1, int postion1, String nomJoueur2, int position2) {
        this.score = score;
        this.coup = coup;
        this.infos = infos;
        this.commentaire = commentaire;
        joueur1 = new Joueur(nomJoueur1, postion1);
        joueur2 = new Joueur(nomJoueur2, position2);
    }

    /**
     * Retourne le numero du coup de la partie
     * @return le numero du coup de la partie
     */
    public int getCoup() {
        return coup;
    }

    /**
     * Retourne le score de la partie
     * @return le score de la partie
     */
    public int[] getScore() {
        return score;
    }

    /**
     * Retourne le infos de la partie
     * @return le infos de la partie
     */
    public String getInfos() {
        return infos;
    }

    /**
     * Retourne un commentaire de la partie
     * @return un commentaire de la partie
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Retourne le joueur1
     * @return le joueur1
     */
    public Joueur getJoueur1() {
        return joueur1;
    }

    /**
     * Retourne le joueur2
     * @return le joueur2
     */
    public Joueur getJoueur2() {
        return joueur2;
    }

    /**
     * Modifie le score de la partie
     * @param score le nouveau score
     */
    public void setScore(int[] score) {
        this.score = score;
    }

    /**
     * Modifie le coup de la partie
     * @param coup le nouveau coup
     */
    public void setCoup(int coup) {
        this.coup = coup;
    }
    
    /**
     * Modifie les infos de la partie
     * @param infos
     */
    public void setInfos(String infos) {
        this.infos = infos;
    } 

    /**
     * Modifie un commentaire de la partie
     * @param commentaire le nouveau commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }  

    /**
     * Modifie le joueur1
     * @param joueur1 le nouveau joueur1
     */
    public void setJoueur1(Joueur joueur1) {
        this.joueur1 = joueur1;
    }

    /**
     * Modifie le joueur2
     * @param joueur2 le nouveau joueur2
     */
    public void setJoueur2(Joueur joueur2) {
        this.joueur2 = joueur2;
    }

    /**
     * Créer l'objet JSON correspondant à la partie en cours
     * @return JSONObject
     */
    public JSONObject toJSON() {
        JSONObject object = new JSONObject();

        try {
            object.put("score", score);
            object.put("coup", coup);
            object.put("infos", infos);
            object.put("commentaire", commentaire);
            object.put("joueur1", joueur1.toJSON());
            object.put("joueur2", joueur2.toJSON());
        }catch(JSONException e) {
            System.err.println("Erreur lors de l'insertion de la partie.");
            System.err.println(e);
            System.exit(-1);
        }
        
        return object;
    }

    /**
    * affiche le terrain de revoie
    */
    public void afficherRenvoie(){
        System.out.println("_________________________");
        System.out.println("|                       |");
        System.out.println("|      4     3     5    |");
        System.out.println("|_______________________|");            
        System.out.println("|            |          |");
        System.out.println("|      1     |     2    |");
        System.out.println("|            |          |");
        System.out.println("-------------------------\n\n");
    }

    /**
    * affiche le terrain de recoit
    */
    public void afficherRecoit(){
        System.out.println("-------------------------");
        System.out.println("|            |          |");
        System.out.println("|      1     |     2    |");
        System.out.println("|____________|__________|");            
        System.out.println("|                       |");
        System.out.println("|     4      3     5    |");
        System.out.println("|_______________________|\n\n");
    }

    /**
     * Transforme la plartie sous forme de chaine de caractères
     * @return une chaine de caractères contenant la partie
     */
    public String toString() {
        return "Score joueur1 : "+ score[0] +"\nScore joueur2 : "+ score[1]+"\n Numero de coup joué : "+coup+"\n Un commentaire sympa (ou pas) : "+commentaire;
    }
}