package tennis;

import java.util.Scanner;
import java.io.IOException;

/**
 * Gestion UDP gere le traitement des données de la partie
 * @author Samuel Bertin / Kevin Charlier
 */
public class GestionTennis {

	/**
     * Genere le nouveau json de la partie
     * @param partie les données de la partie en cours
     * @param indiceScoreJoueur indice du jour qui fait le prochain coup
     * @param indiceScoreAdverse indice du joueur adverse
     * @return la chaine de caractere de la partie
     */
	public static String gestionPartieTennis(PartieTennis partie, int indiceScoreJoueur, int indiceScoreAdverse){
		int choix = 0;
		int nombreAleatoire;
		int tmp[] = {0, 0};
		Scanner sc = new Scanner(System.in);

        // nettoyer la console
        videConsole();

        if (!partie.getCommentaire().equals(" ")) {
            System.out.println("*adversaire* :   -"+partie.getCommentaire());                
        }
        // reinitialiser le commentaire a null si aucune reponse du joueur
        partie.setCommentaire(" ");


        Joueur joueur;
        if(indiceScoreJoueur == 1) {
            joueur = partie.getJoueur1();
        } else {
            joueur = partie.getJoueur2();
        }

        switch (joueur.getPosition()) {
            case 1:
                do {
                    partie.afficherRecoit();               
                    System.out.println("Vous vous trouvez actuelement au court gauche (1)");
                    System.out.println("Ou rattrapper la balle :\n\t1) court gauche\n\t2) court droit\n\t3) centre\n\t4) long gauche");
                    choix = Integer.parseInt(sc.nextLine());
                    if(choix >= 1 && choix <= 4) break;
                } while(choix < 1 || choix > 4);
                joueur.setPosition(choix);
                break;
            case 2:
                do {
                    partie.afficherRecoit();               
                    System.out.println("Vous vous trouvez actuelement au court droit (2)");                    
                    System.out.println("Ou arrivera la balle :\n\t1) court gauche\n\t2) court droit\n\t3) centre\n\t5) long droit");
                    choix = Integer.parseInt(sc.nextLine());
                    if(choix == 1 || choix == 2 || choix == 3 || choix == 5) break;
                } while(choix < 1 || choix > 3 || choix != 5);
                joueur.setPosition(choix);
                break;
            case 3:
                do {
                    partie.afficherRecoit();               
                    System.out.println("Vous vous trouvez actuelement au centre (3)");
                    System.out.println("Ou arrivera la balle :\n\t1) court gauche\n\t2) court droit\n\t3) centre\n\t4) long gauche\n\t5) long droit");
                    choix = Integer.parseInt(sc.nextLine());
                    if(choix >= 1 && choix <= 5) break;
                } while(choix < 1 || choix > 5);
                joueur.setPosition(choix);
                break;
            case 4:
                do {
                    partie.afficherRecoit();               
                    System.out.println("Vous vous trouvez actuelement en long gauche (4)");            
                    System.out.println("Ou arrivera la balle :\n\t1) court gauche\n\t3) centre\n\t4) long gauche");
                    choix = Integer.parseInt(sc.nextLine());
                    if(choix == 1 || choix == 3 || choix == 4) break;
                } while(choix != 1 || choix != 3 || choix != 4);
                joueur.setPosition(choix);
                break;
            case 5:
                do {
                    partie.afficherRecoit();  
                    System.out.println("Vous vous trouvez actuelement en long droit (5)");            
                    System.out.println("Ou arrivera la balle :\n\t2) court droit\n\t3) centre\n\t5) long droit");
                    choix = Integer.parseInt(sc.nextLine());
                    if(choix == 2 || choix == 3 || choix == 5) break;
                } while(choix != 2 || choix != 3 || choix != 5);
                joueur.setPosition(choix);
                break;
            default:
                break;
        }
        if(indiceScoreJoueur == 1) {
            partie.setJoueur1(joueur);
        } else {
            partie.setJoueur2(joueur);
        }

        // do {
        //     partie.afficherRecoit();               
        //     System.out.println("Ou arrivera la balle :\n\t1) court gauche\n\t2) court droit\n\t3) centre\n\t4) long gauche\n\t5) long droit");
        //     choix = Integer.parseInt(sc.nextLine());
        // } while(choix < 1 || choix > 5);
        
        switch (choix) {
            case 1:
                if(partie.getCoup() == 1){
                    renvoie(partie, sc);
                    choix = Integer.parseInt(sc.nextLine());

                } else if(partie.getCoup() == 2 || partie.getCoup() == 4) {
                	nombreAleatoire = (int)(Math.random() * 2);
                	if(nombreAleatoire == 1){
                		renvoieDeJustesse(partie, sc);
	                    choix = Integer.parseInt(sc.nextLine());
                	}  
                    else{
                        ratee(partie, sc, tmp, indiceScoreJoueur);
                        choix = Integer.parseInt(sc.nextLine());
                    }   
                } else {
                    ratee(partie, sc, tmp, indiceScoreJoueur);
                    choix = Integer.parseInt(sc.nextLine());
                }
                if (choix == 1) {
                    System.out.print("Votre message : ");
                    partie.setCommentaire(sc.nextLine());
                }
                break;

            case 2:
                if(partie.getCoup() == 2){
                    renvoie(partie, sc);
                    choix = Integer.parseInt(sc.nextLine());

                }else if(partie.getCoup() == 1 || partie.getCoup() == 5) {
                	nombreAleatoire = (int)(Math.random() * 2);
                	if(nombreAleatoire == 1){
                		renvoieDeJustesse(partie, sc);
	                    choix = Integer.parseInt(sc.nextLine());
                	} 
                    else{
                        ratee(partie, sc, tmp, indiceScoreJoueur);
                        choix = Integer.parseInt(sc.nextLine());
                    }    
                } else {
                    ratee(partie, sc, tmp, indiceScoreJoueur);
                    choix = Integer.parseInt(sc.nextLine());
                }
                if (choix == 1) {
                    System.out.print("Votre message : ");
                    partie.setCommentaire(sc.nextLine());
                }
                break;

            case 3:
                if(partie.getCoup() == 3){
                    renvoie(partie, sc);
                    choix = Integer.parseInt(sc.nextLine());

                } else if(partie.getCoup() == 1 || partie.getCoup() == 2 || partie.getCoup() == 4 || partie.getCoup() == 5) {
                	nombreAleatoire = (int)(Math.random() * 2);
                	if(nombreAleatoire == 1){
                		renvoieDeJustesse(partie, sc);
	                    choix = Integer.parseInt(sc.nextLine());
                	}
                    else{
                        ratee(partie, sc, tmp, indiceScoreJoueur);
                        choix = Integer.parseInt(sc.nextLine());
                    }    
                } else {
                    ratee(partie, sc, tmp, indiceScoreJoueur);
                    choix = Integer.parseInt(sc.nextLine());
                }
                if (choix == 1) {
                    System.out.print("Votre message : ");
                    partie.setCommentaire(sc.nextLine());
                }
                break;

            case 4:
                if(partie.getCoup() == 4){
                   renvoie(partie, sc);
                   choix = Integer.parseInt(sc.nextLine());

                } else if(partie.getCoup() == 1 || partie.getCoup() == 3) {
                	nombreAleatoire = (int)(Math.random() * 2);
                	if(nombreAleatoire == 1){
                		renvoieDeJustesse(partie, sc);
	                    choix = Integer.parseInt(sc.nextLine());
                	} 
                    else{
                        ratee(partie, sc, tmp, indiceScoreJoueur);
                        choix = Integer.parseInt(sc.nextLine());
                    }    
                } else {
                    ratee(partie, sc, tmp, indiceScoreJoueur);
                    choix = Integer.parseInt(sc.nextLine());
                }
                if (choix == 1) {
                    System.out.print("Votre message : ");
                    partie.setCommentaire(sc.nextLine());
                }
                break;

            case 5:
                if(partie.getCoup() == 5) {
                   renvoie(partie, sc);
                   choix = Integer.parseInt(sc.nextLine());

                } else if(partie.getCoup() == 3 || partie.getCoup() == 2) {
                	nombreAleatoire = (int)(Math.random() * 2);
                	if(nombreAleatoire == 1){
                		renvoieDeJustesse(partie, sc);
	                    choix = Integer.parseInt(sc.nextLine());
                	}  
                    else{
                        ratee(partie, sc, tmp, indiceScoreJoueur);
                        choix = Integer.parseInt(sc.nextLine());
                    }   
                } else {
                    ratee(partie, sc, tmp, indiceScoreJoueur);
                    choix = Integer.parseInt(sc.nextLine());
                }
                if (choix == 1) {
                    System.out.print("Votre message : ");
                    partie.setCommentaire(sc.nextLine());
                }
                break;

            default : System.err.println("Erreur d'entrée");
                break;
        }

        // renvoie du json
        return partie.toJSON().toString();
	}

	/**
     * Renvoie gere le choix de renvoie de la balle
     * @param partie les données de la partie en cours
     * @param sc le scanner qui lit les entrees clavier
     */
	public static void renvoie(PartieTennis partie, Scanner sc){
		videConsole();

        do {
            partie.afficherRenvoie();
            System.out.println("Ou renvoyer la balle :\n\t1) court gauche\n\t2) court droit\n\t3) centre\n\t4) long gauche\n\t5) long droit");
            partie.setCoup(Integer.parseInt(sc.nextLine()));
        } while(partie.getCoup() < 1 || partie.getCoup() > 5);
        partie.setInfos("renvoie");
        System.out.println("Un petit message ?\t1) OUIII\tautre) non ma balle suffira");
	}

	/**
     * RenvoieDeJustess gere le choix de renvoie de la balle
     * dans le cas ou le joueur a rattrapé la balle de justesse
     * @param partie les données de la partie en cours
     * @param sc le scanner qui lit les entrees clavier
     */
	public static void renvoieDeJustesse(PartieTennis partie, Scanner sc){
		videConsole();

        do {
            partie.afficherRenvoie();
            System.out.println("Il s'en est fallut de peu :\n\t1) court gauche\n\t2) court droit\n\t3) centre\n\t4) long gauche\n\t5) long droit");
            partie.setCoup(Integer.parseInt(sc.nextLine()));
        } while(partie.getCoup() < 1 || partie.getCoup() > 5);
        partie.setInfos("renvoie de justesse");
        System.out.println("Un petit message ?\t1) OUIII\tautre) non ma balle suffira");
	}

	/**
     * Rate gere le choix de renvoie de la balle quand
     * le joueur ne l'a pas rattrape
     * @param partie les données de la partie en cours
     * @param sc le scanner qui lit les entrees clavier
     * @param tmp[] un tableau temporaire
     * @param indiceScoreJoueur l'indice du tableau score
     */
	public static void ratee(PartieTennis partie, Scanner sc, int tmp[], int indiceScoreJoueur){
		videConsole();

        System.out.println("Dommage mauvais choix, rattrape toi au service");
        tmp = partie.getScore();
        tmp[indiceScoreJoueur]++;
        partie.setScore(tmp);
        do {
            partie.afficherRenvoie();
            System.out.println("Ou servir la balle :\n\t3) centre\n\t4) long gauche\n\t5) long droit");
            partie.setCoup(Integer.parseInt(sc.nextLine()));
        } while(partie.getCoup() < 3 || partie.getCoup() > 5);
        partie.setInfos("service"); 
        System.out.println("Un petit message ?\t1) OUIII\tautre) non ma balle suffira");
    }
    
    /**
	* gere le service
    * @param partie les données de la partie en cours
    * @param sc le scanner qui lit les entrees clavier
    */
    public static void service(PartieTennis partie, Scanner sc){
        int choix;

        videConsole();

    	 do {
            partie.afficherRenvoie();
            System.out.println("Ou servir la balle :\n\t3) centre\n\t4) long gauche\n\t5) long droit");
            partie.setCoup(Integer.parseInt(sc.nextLine()));
        } while(partie.getCoup() < 3 || partie.getCoup() > 5);
        partie.setInfos("service"); 
        System.out.println("Un petit message ?\t1) OUIII\tautre) non ma balle suffira");
        choix = Integer.parseInt(sc.nextLine());
        if (choix == 1) {
            partie.setCommentaire(sc.nextLine());
        } 
    }

    /**
     * Vide l'ecran de la console
     * en s'addaptant au terminaux qui n'interprete pas les caractere d'echapement ANSI comme le cmd.exe
     */
    public static void videConsole() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows"))
                // pour appeler la commande cls de windows
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                // ou la commande clear pour les systemes UNIX
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ex) {
            /* // Pour les terminaux UNIX et ceux interpretant les caracteres d'echapement 
            public void videConsole() {
                System.out.print("\033[H\033[2J");  
                System.out.flush();  
            }
            */
            System.out.println("\n");
        }
    }
}