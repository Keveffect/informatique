package JSON;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.json.JSONObject;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;


/**
 * Gestion JSON gere les données au format JSON
 * @author Samuel Bertin / Kevin Charlier
 */
public class GestionJSON {
    
    /**
     * Lit un fichier de configuration
     * @param fichier le nom du fichier
     * @return un objet JSON
     */
    public static JSONObject lectureAdresse(String fichier) {
        // Récupération de la chaîne JSON depuis le fichier
        String json = "";
        try {
          byte[] contenu = Files.readAllBytes(Paths.get("JSON/"+fichier));
          json = new String(contenu);        
        } catch(IOException e) {
            System.err.println("Erreur lors de la lecture du fichier '" +fichier+ "'");
            System.exit(0);
        } 
        // Création d'un objet JSON
        return new JSONObject(json);
    }

    /**
     * Création d'un fichier de configuration par défaut.
     * @param nomFichier le nom du fichier
     */
    public static void creerFichierConfiguation(String nomFichier) {
        int port = 3031;
        boolean nbPortTrouve = false;
        Config config = new Config(nomFichier, true);

        String monAdresse = null;
        try {
            monAdresse = InetAddress.getLocalHost().getHostAddress();  // garde que la partie IP de l'Inet adresse
        } catch(UnknownHostException e) {
            monAdresse = InetAddress.getLoopbackAddress().getHostAddress();
            System.err.println("Erreur lors de la création de l'adresse : " + e);
        }

        // continue de chercher tant que le port n'a pas ete trouve
        while(!nbPortTrouve){
            try {
                DatagramSocket soc = new DatagramSocket(port);
                config.ajouterValeur("adresse", monAdresse);
                config.ajouterValeur("port", port);
                nbPortTrouve = true;
                soc.close();
            }catch (IOException e) {
                System.out.println("port non autorise : " + port);
            }
            port++;
        }
        // Creation et sauvegarde du fichier de configuration
        config.sauvegarder();
    }
}