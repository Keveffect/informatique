public class Ville {
	
	private String nomVille;
	private	String nomPays;
	private	int nbreHabitants;
 
  //Constructeur par défaut
  public Ville(){
    System.out.println("Creation d'une ville !");          
    nomVille = "Inconnu";
    nomPays = "Inconnu";
    nbreHabitants = 0;
  }
 
 //Constructeur avec paramètres
  public Ville(String pNom, int pNbre, String pPays){
    System.out.println("Creation d'une ville avec des parametres !");
    nomVille = pNom;
    nomPays = pPays;
    nbreHabitants = pNbre;
  }        

//************* ACCESSEURS *************

  public String getNom()  
  {  return nomVille;}

  public String getNomPays()
  {return nomPays;}
  
  public int getNombreHabitants()
  {return nbreHabitants;} 
  
  //************* MUTATEURS *************

  public void setNom(String pNom)
  {nomVille = pNom;}
  
  public void setNomPays(String pPays)
  {nomPays = pPays;}

  public void setNombreHabitants(int nbre)
  {nbreHabitants = nbre;}
}