/**
 * Programme permettant de generer un editeur hexadecimal
 * @author Samuel Bertin | Kevin Charlier
 **/

#include <stdlib.h> 
#include <ncurses.h>
#include <string.h>
#include "gestion_fenetre.h"
#include "gestion_fichier.h"

#define K_ENTER 10

int main(int argc, char const *argv[]) 
{
  int ch, bloc=0, fd=-1, tailleActuel, sourisX, sourisY, numeroOctet, valOctet;
  fenetre_t win;

  /* Vérification de la présence d'un argument */
  if (argc != 2) {
    fprintf(stderr, "Il vous manque l'argument specifiant le fichier.");  
    exit(EXIT_FAILURE);
  }
  
  /* Initialisation de ncurses */
  ncurses_initialiser();
  ncurses_souris();
  ncurses_couleurs(); 
  palette();
  
  ouvertureFichier(argv[1], &fd);
  
  /* Vérification des dimensions du terminal */
  if((COLS < POSX1 + LARGEUR2 + LARGEUR3) || (LINES < POSY1 + HAUTEUR1 + HAUTEUR2)) {
    ncurses_stopper();
    fprintf(stderr,
        "Les dimensions du terminal sont insufisantes : l=%d,h=%d au lieu de l=%d,h=%d\n",
        COLS, LINES, POSX1 + LARGEUR2 + LARGEUR3, POSY1 + HAUTEUR1 + HAUTEUR2);
    exit(EXIT_FAILURE);
  }

  initialiserFenetres(&win);

  updateFenetres(&win, fd, bloc);

  tailleActuel = tailleFichier(fd);
  wprintw(win.sous_fenetre1, "Fichier charge : %s\n", argv[1]);
  wprintw(win.sous_fenetre1, "Taille du fichier : %d\n", tailleActuel);
  wrefresh(win.sous_fenetre1);

  refresh();
  while((ch = getch()) != KEY_F(2)) {
    if(ch == KEY_UP || ch ==KEY_DOWN) {
      /** Si la touche "fleche du bas" est pressée on regarde le bloc suivant
       *  Si la touche "fleche du haut" est pressée on revient au bloc précédent
       */
      ch == KEY_UP ? ((bloc != 0) ? --bloc : bloc) : ++bloc;
      updateFenetres(&win, fd, bloc);
    }
    tailleActuel = tailleFichier(fd);

    if((ch == KEY_MOUSE) && (souris_getpos(&sourisX, &sourisY, NULL) == OK)) {
      updateFenetres(&win, fd, bloc);

      /* Fenetre 2 */
      if(testClique(sourisX, sourisY) == 2) {

        numeroOctet = cliqueFenetre2(win, sourisX, sourisY, bloc);

        valOctet = colorFenetre2(win, sourisX, sourisY, bloc, fd, numeroOctet);

        if ((ch = getch()) == K_ENTER){
          entrerFenetre2(win, sourisX, sourisY, valOctet, numeroOctet, bloc, fd);
        }
        else if(ch  == KEY_BACKSPACE || ch == KEY_DC){
          supprimerOctet(&win, fd, bloc, numeroOctet, tailleActuel);
        }
      }
      /* Fenetre 3 */
      if(testClique(sourisX, sourisY) == 3) {

        numeroOctet = cliqueFenetre3(sourisX, sourisY, bloc);

        valOctet = colorFenetre3(win, sourisX, sourisY, bloc, fd, numeroOctet);

        if((ch = getch()) == K_ENTER){
          entrerFenetre3(win, sourisX, sourisY, valOctet, numeroOctet, bloc, fd);
        }
        else if(ch  == KEY_BACKSPACE || ch == KEY_DC) {
          supprimerOctet(&win, fd, bloc, numeroOctet, tailleActuel);
        }
      }
    }
  }

  /* Suppression des fenêtres */
  detruireFenetres(&win);
  
  /* Arrêt de ncurses */
  ncurses_stopper();

  return EXIT_SUCCESS;
}