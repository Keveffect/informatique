#ifndef GESTION_FENETRE
#define GESTION_FENETRE

#include <ncurses.h>   /* Pour toutes les fonctions/constantes ncurses */
#include <stdlib.h>    /* Pour exit, EXIT_FAILURE */
#include <string.h>    /* Pour strlen */
#include <fcntl.h>     /* Pour open */
#include <unistd.h>    /* Pour read */
#include <sys/stat.h>  /* Pour O_RDONLY */

#include "ncurses.h"
#include "gestion_fichier.h"

#define LARGEUR1 COLS /* Largeur de la 1ere fenêtre */
#define HAUTEUR1 5  +2 /* Hauteur de la 1ere fenêtre */
#define LARGEUR2 32 +2 /* Largeur de la 2eme fenêtre */
#define HAUTEUR2 16 +2 /* Hauteur de la 2eme fenêtre */
#define LARGEUR3 8  +2 /* Largeur de la 3eme fenêtre */
#define HAUTEUR3 16 +2 /* Hauteur de la 3eme fenêtre */

#define POSX1    0     /* Position horizontale de la 1ere fenêtre */
#define POSY1    0     /* Position verticale de la 1ere fenêtre */
#define POSX2    0     /* Position horizontale de la 2eme fenêtre */
#define POSY2    HAUTEUR1  /* Position verticale de la 2eme fenêtre */
#define POSX3    LARGEUR2  /* Position horizontale de la 3eme fenêtre */
#define POSY3    HAUTEUR1  /* Position verticale de la 3eme fenêtre */

/**
 * Créer et gère les différentes fenêtres
 * @param win prend en paramètre les fenêtres a créer 
 */
void initialiserFenetres(fenetre_t* win);

/**
 * Reset les fentres 2 et 3 et affiche les valeurs des blocs
 * @param win prend en paramètre les fenêtres a créer 
 * @param fd le descripteur de fichier
 * @param bloc l'indice de bloc
 */
void updateFenetres(fenetre_t *win, int fd, int bloc);

/**
 * Detruit l'ensemble des fenêtres
 * @param win structure contenant les différents pointeur de fenêtres
 */
void detruireFenetres(fenetre_t *win);

/**
* Permet de lire et d'afficher un bloc en hexadecimal
* @param win prend en paramètre les fenêtres a créer
* @param fd le descripteur de fichier
* @param bloc l'indice de bloc
* @param nbLigne le numero de ligne en hexa
*/
void lireBlocHexadecimal(WINDOW *sous_fenetre2, int fd, int bloc, int nbLigne);

/**
* Permet de lire et d'afficher un bloc en caractère
* @param sous_fenetre3 fenetre 3
* @param fd le descripteur de fichier
* @param bloc l'indice de bloc 
*/
void lireBlocCaractere(WINDOW* sous_fenetre3, int fd, int bloc);

/**
 * Demande une entrée clavier en Hexadécimal et verifie la validité
 * @param win prend en paramètre les fenêtres a créer
 * @param fd le descripteur de fichier 
 * @return renvoie la valeur decimal correspondantes
 */
int inputHexa(fenetre_t *win, int  fd);

/**
 * Demande une entrée clavier en caractère et verifie la validité
 * @param win prend en paramètre les fenêtres a créer 
 * @param le descripteur de fichier
 * @return renvoie la valeur decimal correspondantes
 */
int inputCaractere(fenetre_t *win, int  fd);

/**
 * Fait la gestion du clique dans la fenêtre 2
 * @param win prend en paramètre les fenêtres a créer 
 * @param sourisX
 * @param sourisY
 * @param bloc l'indice de bloc
 * @return l'indice de l'octet dans le fichier
 */
int cliqueFenetre2(fenetre_t win, int sourisX, int sourisY, int bloc);

/**
 * Fait la gestion du clique dans la fenêtre 2
 * @param sourisX
 * @param sourisY
 * @param bloc l'indice du bloc
 * @return l'indice de l'octet dans le fichier
 */
int cliqueFenetre3(int sourisX, int sourisY, int bloc);

/**
 * Met en evidence un caractère (noir sur fond blanc) lorsqu'il est sélectionné
 * @param win prend en paramètre les fenêtres a créer
 * @param sourisX
 * @param sourisY
 * @param bloc l'indice du bloc
 * @param fd le descripteur de fichier
 * @param numeroOctet indice de l'octet dans le fichier
 * @return la valeur de l'octet
 */
int colorFenetre2(fenetre_t win, int sourisX, int sourisY, int bloc, int fd, int numeroOctet);

/**
 * Met en evidence un caractère (noir sur fond blanc) lorsqu'il est sélectionné
 * @param win prend en paramètre les fenêtres a créer
 * @param sourisX
 * @param sourisY
 * @param bloc l'indice du bloc
 * @param fd le descripteur de fichier
 * @param numeroOctet indice de l'octet dans le fichier
 * @return la valeur de l'octet
 */
int colorFenetre3(fenetre_t win, int sourisX, int sourisY, int bloc, int fd, int numeroOctet);

/**
 * Donne la fenetre dans laquelle le clique est effectué
 * @param sourisX
 * @param sourisY
 * @return l'indice de la fenetre
 */
int testClique(int sourisX, int sourisY);

#endif