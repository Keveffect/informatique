/**
 * @author Samuel Bertin | Kevin Charlier
 **/

#include "gestion_fenetre.h"

/**
 * Créer et gère les différentes fenêtres
 * @param win prend en paramètre les fenêtres a créer 
 */
void initialiserFenetres(fenetre_t *win){
  /* Initialisation du fond */
  clear();
  refresh();
  /* création de la fenêtre et sous fenetre d'information */
  win->fenetre1 = newwin(HAUTEUR1, LARGEUR1, POSY1, POSX1);
  box(win->fenetre1, 0, 0);
  win->sous_fenetre1 = subwin(win->fenetre1, HAUTEUR1 - 2, LARGEUR1 - 2, POSY1 + 1, POSX1 + 1);
  wmove(win->fenetre1, 0, 1);
  wprintw(win->fenetre1, "Informations");
  scrollok(win->sous_fenetre1, TRUE);

  /* création de la fenêtre et sous fenetre d'affichage en hexadecimal */
  win->fenetre2 = newwin(HAUTEUR2, LARGEUR2, POSY2, POSX2);
  box(win->fenetre2, 0, 0);
  win->sous_fenetre2 = subwin(win->fenetre2, HAUTEUR2 - 2, LARGEUR2 - 2, POSY2 + 1, POSX2 + 1);
  wmove(win->fenetre2, 0, 1);
  wprintw(win->fenetre2, "Hexa");

  /* création de la fenêtre et sous fenetre d'affichage des caractère visible */
  win->fenetre3 = newwin(HAUTEUR3, LARGEUR3, POSY3, POSX3);
  box(win->fenetre3, 0, 0);
  win->sous_fenetre3 = subwin(win->fenetre3, HAUTEUR3 - 2, LARGEUR3 - 2, POSY3 + 1, POSX3 + 1);
  wmove(win->fenetre3, 0, 1);
  wprintw(win->fenetre3, "Car.");

  /* Création d'un cadre */
  wrefresh(win->fenetre1);
  wrefresh(win->sous_fenetre1);
  wrefresh(win->fenetre2);
  wrefresh(win->sous_fenetre2);
  wrefresh(win->fenetre3);
  wrefresh(win->sous_fenetre3);
}

/**
 * Reset les fentres 2 et 3 et affiche les valeurs des blocs
 */
void updateFenetres(fenetre_t *win, int fd, int bloc){
  int nbLigne = bloc*128-8; /* Pour arriver a 0 au passage de la premiere boucle */

  lireBlocHexadecimal(win->sous_fenetre2, fd, bloc, nbLigne);
  wrefresh(win->sous_fenetre2);
  wrefresh(win->fenetre2);

  lireBlocCaractere(win->sous_fenetre3, fd, bloc);
  wrefresh(win->sous_fenetre3);
  wrefresh(win->fenetre3);
}


/**
* Permet d'afficher un bloc choisie en hexadecimal
*/
void lireBlocHexadecimal(WINDOW *sous_fenetre2, int fd, int bloc, int nbLigne){
  unsigned char octetActuel = 0;
  int tailleBloc = 16*8;
  int position = tailleBloc * bloc;
  int i, j;
  
  /* wprintw(win->sous_fenetre1, "Valeur bloc : %d   (pos: %d\tnumLigne: %d)", bloc, position, nbLigne);
  wrefresh(win->sous_fenetre1); */

  if((i = lseek(fd, position, SEEK_SET)) == -1){
    ncurses_stopper();
    perror("Error lseek (lecture bloc Hexa)\n");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }

  wclear(sous_fenetre2);
  for(i = 0; i < 17; i++){
    nbLigne = nbLigne + 8;
    wprintw(sous_fenetre2, "%08x", nbLigne);
    j = 0;
    while(j++ < 8){
      if(read(fd, &octetActuel, sizeof(octetActuel)) == -1) {
        ncurses_stopper();
        perror("Erreur read ");
        fermetureFichier(fd);
        exit(EXIT_FAILURE);
      }
      wprintw(sous_fenetre2, " %02x",octetActuel);
    }
  }

  /* Cette deuxieme partie nous sert a eviter un bug d'ffichage en fin de bloc*/
  wmove(sous_fenetre2, 15, 29);
  if((i = lseek(fd, position + 127, SEEK_SET)) == -1){
    ncurses_stopper();
    perror("Error lseek (lecture bloc Hexa)\n");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }
  if(read(fd, &octetActuel, sizeof(octetActuel)) == -1) {
    ncurses_stopper();
    perror("Erreur read ");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }
  wprintw(sous_fenetre2, " %02x",octetActuel);
  
  wrefresh(sous_fenetre2);
}

/**
* Permet de lire un bloc choisie
*/
void lireBlocCaractere(WINDOW* sous_fenetre3, int fd, int bloc){
  char tmp;
  int tailleBloc = 16*8;
  int position = tailleBloc * bloc;
  int i, j;

  if((i = lseek(fd,position,SEEK_SET)) == -1){
    ncurses_stopper();
    perror("Error lseek (lecture bloc caractere)\n");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }

  wclear(sous_fenetre3);
  for(j = 0; j< 128; j++){
    if(read(fd, &tmp, sizeof(char)) == -1) {
      ncurses_stopper();
      perror("Erreur read ");
      fermetureFichier(fd);
      exit(EXIT_FAILURE);
    }
    if(tmp > 32 && tmp < 128){ /*gere les caracteres affichable*/
      wprintw(sous_fenetre3, "%c",tmp);
    }
    else{
      wprintw(sous_fenetre3, ".");
    }
  }
  printf("\n");
  wrefresh(sous_fenetre3);
}


/**
 * Detruit l'ensemble des fenêtres
 * @param win structure contenant les différents pointeur de fenêtres
 */
void detruireFenetres(fenetre_t *win){
  delwin(win->sous_fenetre1);
  delwin(win->fenetre1);
  delwin(win->sous_fenetre2);
  delwin(win->fenetre2);
  delwin(win->sous_fenetre3);  
  delwin(win->fenetre3);
}

/**
 * Demande une entrée clavier en caractère et verifie la validité
 * @return renvoie la valeur decimal correspondantes
 */
int inputCaractere(fenetre_t *win, int  fd){
  char val_char;
  wprintw(win->sous_fenetre1, "Entrez votre nouveau caractere Hexadecimal\n");
  wrefresh(win->sous_fenetre1);

  if(wscanw(win->sous_fenetre1, "%c", &val_char)  == EOF) {
    ncurses_stopper();
    perror("Erreur scanf ");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }
  wprintw(win->sous_fenetre1, "Valeur entree : char:%c int:%d\n", val_char, (int)val_char);
  wrefresh(win->sous_fenetre1); 

  return val_char;
}


/**
 * Demande une entrée clavier en Hexadécimal et verifie la validité
 * @return renvoie la valeur decimal correspondantes
 */
int inputHexa(fenetre_t *win, int  fd) {
  int hexa;
  wprintw(win->sous_fenetre1, "Entrez votre nouveau caractere Hexadecimal\n");
  wrefresh(win->sous_fenetre1);

  if(wscanw(win->sous_fenetre1, "%2x",&hexa)  == EOF) {
    ncurses_stopper();
    perror("Erreur scanf ");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }
  wprintw(win->sous_fenetre1, "Valeur entree : char:%c int:%d\n", (char)hexa, hexa);
  wrefresh(win->sous_fenetre1);

  return hexa;
}

/**
 * Fait la gestion du clique dans la fenêtre 2
 */
int cliqueFenetre2(fenetre_t win, int sourisX, int sourisY, int bloc){
  int numeroOctet;

  if (sourisX%3 == 1)
    wmove(win.sous_fenetre2, -(POSY2-sourisY)-1, -(POSX2-sourisX)-1);
  else 
    wmove(win.sous_fenetre2, -(POSY2-sourisY)-1, -(POSX2-sourisX)-2);

  numeroOctet = ((-(POSX2+10 - sourisX)/3) + (-(POSY2-sourisY)-1)*8) + bloc*128;
  return numeroOctet;
}

/**
 * Fait la gestion du clique dans la fenêtre 3
 */
int cliqueFenetre3(int sourisX, int sourisY, int bloc){
  int numeroOctet;

  numeroOctet = (-(POSX3+1 - sourisX)) + (-(POSY2-sourisY)-1)*8 + bloc*128;
  return numeroOctet;
}

/**
 * Met en evidence un caractère (noir sur fond blanc) lorsqu'il est sélectionné
 */
int colorFenetre2(fenetre_t win, int sourisX, int sourisY, int bloc, int fd, int numeroOctet){
  int i;
  char valOctet;

  if((i = lseek(fd, numeroOctet, SEEK_SET)) == -1){
      ncurses_stopper();
      perror("Error lseek\n");
      fermetureFichier(fd);
      exit(EXIT_FAILURE);
  }

  if(read(fd, &valOctet, sizeof(char)) == -1) {
    ncurses_stopper();
    perror("Erreur read ");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }

  /* Fenetre de gauche */
  wattron(win.sous_fenetre2, COLOR_PAIR(1));
  wprintw(win.sous_fenetre2, "%02x", valOctet);
  wattroff(win.sous_fenetre2, COLOR_PAIR(1));
  wrefresh(win.sous_fenetre2);

  /* Fenetre de droite */
  wmove(win.sous_fenetre3, -(POSY2-sourisY)-1, (POSX3-(POSX2+10-sourisX)-1)/3-11);
  wattron(win.sous_fenetre3, COLOR_PAIR(1));
  if(valOctet > 32 && valOctet < 126){ /*gere les caracteres affichable*/
    wprintw(win.sous_fenetre3, "%c", valOctet);
  }
  else{
    wprintw(win.sous_fenetre3, ".");
  }
  wattroff(win.sous_fenetre3, COLOR_PAIR(1));
  wrefresh(win.sous_fenetre3);

  wprintw(win.sous_fenetre1, "Vous avez selectionne l'octet %02x (valeur = %d)\n", numeroOctet, valOctet);
  wrefresh(win.sous_fenetre1);

  return valOctet;
}

/**
 * Met en evidence un caractère (noir sur fond blanc) lorsqu'il est sélectionné
 */
int colorFenetre3(fenetre_t win, int sourisX, int sourisY, int bloc, int fd, int numeroOctet){
  int i;
  char valOctet;

  if((i = lseek(fd, numeroOctet, SEEK_SET)) == -1){
    ncurses_stopper();
    perror("Error lseek\n");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }

  if(read(fd, &valOctet, sizeof(char)) == -1) {
    ncurses_stopper();
    perror("Erreur read ");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }
  /* Fenetre de gauche */
  wmove(win.sous_fenetre2, -(POSY2-sourisY)-1, (sourisX-POSX3-2)*3);
  wattron(win.sous_fenetre2, COLOR_PAIR(1));
  wprintw(win.sous_fenetre2, "%02x", valOctet);
  wattroff(win.sous_fenetre2, COLOR_PAIR(1));
  wrefresh(win.sous_fenetre2);

  /* Fenetre de droite */
  wmove(win.sous_fenetre3, -(POSY3-sourisY)-1, -(POSX3-sourisX)-1);
  wattron(win.sous_fenetre3, COLOR_PAIR(1));
  if(valOctet > 32 && valOctet < 126){ /*gere les caracteres affichable*/
    wprintw(win.sous_fenetre3, "%c", valOctet);
  }
  else{
    wprintw(win.sous_fenetre3, ".");
  }
  wattroff(win.sous_fenetre3, COLOR_PAIR(1));
  wrefresh(win.sous_fenetre3);

  wprintw(win.sous_fenetre1, "Vous avez selectionne l'octet %02x (valeur = %d)\n", numeroOctet, valOctet);
  wrefresh(win.sous_fenetre1);

  return valOctet;
}

/**
 * Donne la fenetre dans laquelle le clique est effectué
 */
int testClique(int sourisX, int sourisY){
  if((sourisX > POSX2+9) && (sourisX < POSX2 + LARGEUR2 - 1) && (sourisX%3 != 0) &&
        (sourisY > POSY2) && (sourisY < POSY2 + HAUTEUR2 - 1)){
    return 2;
  }

  if ((sourisX > POSX3) && (sourisX < POSX3 + LARGEUR3 - 1) &&
        (sourisY > POSY3) && (sourisY < POSY3 + HAUTEUR3 - 1)){
    return 3;
  }
  return 0;
}