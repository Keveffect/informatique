#ifndef _NCURSES_
#define _NCURSES_

#include <ncurses.h>

/** 
 * Structure des fenetres du projet
*/
typedef struct{
    WINDOW* fenetre1;
    WINDOW* sous_fenetre1;
    WINDOW* fenetre2;
    WINDOW* sous_fenetre2;
    WINDOW* fenetre3;
    WINDOW* sous_fenetre3;
}fenetre_t;

/**
 * Initialisation de ncurses.
 */
void ncurses_initialiser();

/**
 * Fin de ncurses.
 */
void ncurses_stopper();

/**
 * Initialisation des couleurs.
 */
void ncurses_couleurs();

/**
 * Initialise une paires de couleurs
 */
void palette();

/**
 * Initialisation de la souris.
 */
void ncurses_souris();

/**
 * Recupere la position x et y de la souris.
 * @param[out] x la position en x
 * @param[out] y la position en y
 * @param[out] bouton l'évenement associé au clic (ou NULL)
 * @return OK si reussite
 */
int souris_getpos(int *x, int *y, int *bouton);

#endif