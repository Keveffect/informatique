#ifndef GESTION_FICHIER
#define GESTION_FICHIER

#include <stdio.h>     /* Pour printf */
#include <string.h>    /* Pour strlen */
#include <fcntl.h>     /* Pour open */
#include <unistd.h>    /* Pour read */
#include <sys/stat.h>  /* Pour O_RDONLY */
#include <stdlib.h>    /* Pour exit, EXIT_SUCCESS, EXIT_FAILURE */
#include <ncurses.h>    /* Pour printw, attron, attroff, COLOR_PAIR, getch */
#include "gestion_fenetre.h"
#include "ncurses.h"

/**
* Ouvre le fichier si il existe, sinon le creer
 * @param tab argument specifiant le nom de fichier 
 * @param fd Descripteur de fichier 
*/
void ouvertureFichier(const char tab[], int *fd);

/**
* ferme le fichier
* @param fd Descripteur de fichier
*/
void fermetureFichier(int fd);

/**
 * Donne la taille actuel du fichier
 * @param fd Descripteur de fichier
 * @return la taille du fichier
 */
int tailleFichier(int fd);

/**
* ecrit une chaine de caractere saisie au clavier dans le fichier ouvert
* @param fd Descripteur de fichier
*/
void ecritureFichier(int fd);

/**
* Permet de lire un bloc choisie
* @param fd Descripteur de fichier
* @param bloc l'indice du bloc lu
*/
void lireBloc(int fd, int bloc);

/**
 * Permet de modifier un octet
 * @param fd Descripteur de fichier
 * @param indice Indice de l'octet à modifier
 * @param val la nouvelle valeur de l'octet 
 */
void modifierOctet(int fd, int indice, char val);

/**
 * Colore l'octet en bleu
 * @param win la structure qui contient les fenetres
 * @param souriX
 * @param sourisY
 * @param valOctet la valeur de l'octet en cour de lecture
 * @param numeroOctet l'indide de l'octet dans le fichier
 * @param bloc l'indice du bloc
 * @param fd Descripteur de fichier
 */
void entrerFenetre2(fenetre_t win, int sourisX, int sourisY, int valOctet, int numeroOctet, int bloc, int fd);

/**
 * Colore l'octet en bleu
 * @param win la structure qui contient les fenetres
 * @param souriX
 * @param sourisY
 * @param valOctet la valeur de l'octet en cour de lecture
 * @param numeroOctet l'indide de l'octet dans le fichier
 * @param bloc l'indice du bloc
 * @param fd Descripteur de fichier
 */
void entrerFenetre3(fenetre_t win, int sourisX, int sourisY, int valOctet, int numeroOctet, int bloc, int fd);

/**
 * Supprime l'octet selectionne puis decalle le reste des octets du fichier
 * @param win la structure qui contient les fenetres
 * @param fd Descripteur de fichier
 * @param bloc l'indice du bloc
 * @param numeroOctet l'indice de l'octet dans le fichier
 * @param tailleFichier la taille du fichier
 */
void supprimerOctet(fenetre_t* win, int fd, int bloc, int numeroOctet, int tailleFichier);

#endif