/**
 * Programme permettant de generer/ouvrir un fichier puis de le remplir
 * @author Samuel Bertin | Kevin Charlier
 **/

#include "gestion_fichier.h"

int main(int argc, char* argv[]) {
  int fd = -1;

   /* Vérification de la présence d'un argument */
  if (argc != 2) {
    fprintf(stderr, "Il vous manque l'argument specifiant le fichier.");  
    exit(EXIT_FAILURE);
  }
  
  ouvertureFichier(argv[1], &fd);

  ecritureFichier(fd);
  fermetureFichier(fd);
  return EXIT_SUCCESS;
}