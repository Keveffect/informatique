/**
 * Programme permettant de generer/ouvrir un fichier puis de le remplir
 * @author Samuel Bertin | Kevin Charlier
 **/

#include "gestion_fichier.h"

/**
* Ouvre le fichier si il existe, sinon le creer
*/
void ouvertureFichier(const char tab[], int *fd){
  if((*fd = open(tab, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR)) == -1) {
    ncurses_stopper();
    perror("Erreur lors de l'ouverture du fichier ");
    exit(EXIT_FAILURE);
  }
}

/**
* ferme le fichier
*/
void fermetureFichier(int fd){
  /* Fermeture du fichier */
  if(close(fd) == -1) {
    ncurses_stopper();
    perror("Erreur close ");
    exit(EXIT_FAILURE);
  } 
}

/**
 * Donne la taille actuel du fichier
 */
int tailleFichier(int fd){
  int tailleActuel;
  
  if((tailleActuel = lseek(fd,0,SEEK_END)) == -1){
    ncurses_stopper();
    perror("Error lseek (intro)\n");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }
  return tailleActuel;
}

/**
* ecrit une chaine de caractere saisie au clavier dans le fichier ouvert
*/
void ecritureFichier(int fd){
  int i, taille;
  char tab[256];
  
  /*on place le curseur en fin de fichier*/
  if((i = lseek(fd,0,SEEK_END)) == -1){
      ncurses_stopper();
      perror("Error lseek\n");
      fermetureFichier(fd);
      exit(EXIT_FAILURE);
  }

  printf("Saisissez une chaine de caractere : ");
  if(scanf("%[^\n]s",tab)  == EOF) {
    ncurses_stopper();
    perror("Erreur scanf ");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }

  taille = strlen(tab)+1;

  /* Ecriture de la chaine dans le fichier */
  if(write(fd, &tab, taille) == -1) {
    ncurses_stopper();
    perror("Erreur write ");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }
}

/**
* Permet de lire un bloc choisie
*/
void lireBloc(int fd, int bloc){
  unsigned char octetActuel = 0;
  char tmp;
  int tailleBloc = 16*8;
  int position = tailleBloc * (bloc-1);
  int i, j;

  position = tailleBloc * bloc;

  if((i = lseek(fd,position,SEEK_SET)) == -1){
    ncurses_stopper();
    perror("Error lseek\n");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }

  printf("\nbloc numero %d (hexa): ", bloc);

  for(i = 0; i < 17; i++){
    j = 0;
    while(j++ < 8){
      if(read(fd, &octetActuel, sizeof(octetActuel)) == -1) {
        ncurses_stopper();
        perror("Erreur read ");
        fermetureFichier(fd);
        exit(EXIT_FAILURE);
      }
      printf(" %02x",octetActuel);
    }
  }

  printf("\nbloc numero %d (caractere): ", bloc);

  if((i = lseek(fd,position,SEEK_SET)) == -1){
    ncurses_stopper();
    perror("Error lseek\n");
    fermetureFichier(fd);
    exit(EXIT_FAILURE);
  }

  for(j = 0; j< 128; j++){
    if(read(fd, &tmp, sizeof(char)) == -1) {
      ncurses_stopper();
      perror("Erreur read ");
      fermetureFichier(fd);
      exit(EXIT_FAILURE);
    }
    if(tmp > 32 && tmp < 126){ /*gere les caracteres affichable*/
      printf("%c ",tmp);
    }
    else{
      printf(".");
    }
  }
  printf("\n");
}

/**
 * Permet de modifier un octet
 * @param fd Descripteur de fichier
 * @param indice Indice de l'octet à modifier 
 */
void modifierOctet(int fd, int indice, char val){
  /* Déplacement dans le fichier */
  if(lseek(fd, sizeof(char) *indice, SEEK_SET) == (off_t)-1) {
    ncurses_stopper();
    perror("Erreur lors du deplacement dans le fichier ");
    exit(EXIT_FAILURE);
  }

  /* Ecriture de la valeur */
  if(write(fd, &val, sizeof(char)) == -1) {
    ncurses_stopper();
    perror("Erreur lors de l'ecriture de l'entier ");
    exit(EXIT_FAILURE);
  }
  
  /* Si ecriture apres la fin de fichier rajouter un caractère de fin */
  if( tailleFichier(fd) >= indice) {
    val = 0;
    if(write(fd, &val, sizeof(char)) == -1) {
      ncurses_stopper();
      perror("Erreur lors de l'ecriture de l'entier ");
      exit(EXIT_FAILURE);
    }
  }
}

void entrerFenetre2(fenetre_t win, int sourisX, int sourisY, int valOctet, int numeroOctet, int bloc, int fd){
  int val;

  if (sourisX%3 == 1)
    wmove(win.sous_fenetre2, -(POSY2-sourisY)-1, -(POSX2-sourisX)-1);
  else 
    wmove(win.sous_fenetre2, -(POSY2-sourisY)-1, -(POSX2-sourisX)-2);

  wattron(win.sous_fenetre2, COLOR_PAIR(2));
  wprintw(win.sous_fenetre2, "%02x", valOctet);
  wattroff(win.sous_fenetre2, COLOR_PAIR(2));
  wrefresh(win.sous_fenetre2);

  wmove(win.sous_fenetre3, -(POSY2-sourisY)-1, (POSX3-(POSX2+10-sourisX)-1)/3-11);
  wattron(win.sous_fenetre3, COLOR_PAIR(2));
  if(valOctet > 32 && valOctet <128){
    wprintw(win.sous_fenetre3, "%c", valOctet);
  }
  else{
    wprintw(win.sous_fenetre3, ".");
  }
  wattroff(win.sous_fenetre3, COLOR_PAIR(2));
  wrefresh(win.sous_fenetre3);

  val = inputHexa(&win, fd);
  if(val < 0 || val > 127) {
    wprintw(win.sous_fenetre1, "La valeur entrée n'est pas affichable\n");
    wprintw(win.sous_fenetre1, "Veuillez entrez une valeur entre '00' et '7F'\n");
    wrefresh(win.sous_fenetre1);
  }
  else
    modifierOctet(fd, numeroOctet, val);

  updateFenetres(&win, fd, bloc);
}

void entrerFenetre3(fenetre_t win, int sourisX, int sourisY, int valOctet, int numeroOctet, int bloc, int fd){
  int val;

  wmove(win.sous_fenetre2, -(POSY2-sourisY)-1, (sourisX-POSX3-2)*3);
  wattron(win.sous_fenetre2, COLOR_PAIR(2));
  wprintw(win.sous_fenetre2, "%02x", valOctet);
  wattroff(win.sous_fenetre2, COLOR_PAIR(2));
  wrefresh(win.sous_fenetre2);

  wmove(win.sous_fenetre3, -(POSY3-sourisY)-1, -(POSX3-sourisX)-1);
  wattron(win.sous_fenetre3, COLOR_PAIR(2));
  wprintw(win.sous_fenetre3, "%c", valOctet);
  wattroff(win.sous_fenetre3, COLOR_PAIR(2));
  wrefresh(win.sous_fenetre3);

  val = inputCaractere(&win, fd);
  modifierOctet(fd, numeroOctet, val);
  updateFenetres(&win, fd, bloc);
}

/**
 * supprime
 */
void supprimerOctet(fenetre_t* win, int fd, int bloc, int numeroOctet, int tailleFichier){
  int tailleTempo, w;
  char* tempo;
  char octetActuel;

  if (numeroOctet < tailleFichier-1){
    if((tailleTempo =  lseek(fd, numeroOctet+1, SEEK_SET)) == -1){
      ncurses_stopper();
      perror("Error lseek (intro)\n");
      fermetureFichier(fd);
      exit(EXIT_FAILURE);
    }

    tailleTempo = tailleFichier - tailleTempo;

    tempo = malloc(sizeof(char)*tailleTempo);

    if((w =  lseek(fd, numeroOctet+1, SEEK_SET)) == -1){
      ncurses_stopper();
      perror("Error lseek (intro)\n");
      fermetureFichier(fd);
      exit(EXIT_FAILURE);
    }

    for(w = 0; w < tailleTempo; w++){
      if(read(fd, &tempo[w], sizeof(char)) == -1) {
        ncurses_stopper();
        perror("Erreur read ");
        fermetureFichier(fd);
        exit(EXIT_FAILURE);
      }
    }
    refresh();

    if((w =  lseek(fd, numeroOctet, SEEK_SET)) == -1){
      ncurses_stopper();
      perror("Error lseek (intro)\n");
      fermetureFichier(fd);
      exit(EXIT_FAILURE);
    }

    for(w = 0; w < tailleTempo; w++){
      octetActuel = tempo[w];
      if(write(fd, &octetActuel, sizeof(char)) == -1) {
        ncurses_stopper();
        perror("Erreur lors de l'ecriture de l'entier ");
        exit(EXIT_FAILURE);
      }
    }

    free(tempo);

    updateFenetres(win, fd, bloc);
  }
  else if(numeroOctet == tailleFichier-1){ /*gestion suppresion caractere de fin de fichier*/
    wprintw(win->sous_fenetre1, "!! Impossible de supprimer le caractere de fin de fichier !!\n");
    wrefresh(win->sous_fenetre1);
  }
  else{/*gestion suppression apres la fin du fichier*/
    wprintw(win->sous_fenetre1, "Vous avez tenté de supprimer une valeur inexistante\nAction impossible\n");
    wrefresh(win->sous_fenetre1);
  }
}