%{
#include <stdio.h>
#include <stdlib.h>
void yyerror(const char *erreurMsg);
int yylex();
int erreur = 0;
%}

%token ENTIER
%left '+''-'
%left '*''/'

%%

programme: expression '\n' {
            if(erreur == 0)
                printf("=%d\n", $1);
            else
                erreur = 0; 
      }
      programme
      |
      ;

expression: ENTIER
      | expression '*' expression {
        $$ = $1 * $3;
      }
      | expression '/' expression {
        if($3 == 0){
            printf("divison par 0 impossible\n");
            erreur = 1;
        }
        else
          $$ = $1 / $3;
      }
      | expression '+' expression {
        $$ = $1 + $3;
      }
      | expression '-' expression {
        $$ = $1 - $3;
      };

%%

int main(void) {
  yyparse();
  return EXIT_SUCCESS;
}