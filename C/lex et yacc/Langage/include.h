#ifndef _INCLUDE_
#define _INCLUDE_

/* Valeurs possibles pour les opérandes */
typedef union {
    int entier;
    float reel;
} type_t;

/* Constantes et type pour les booléens */
#define TRUE 1
#define FALSE 0

typedef int bool;

#endif