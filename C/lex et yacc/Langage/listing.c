#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "include.h"
#include "machine.h"
#include "quadruplet.h"
#include "listing.h"

/**
 * Lit un listing depuis un fichier texte.
 * @param nomFichier le nom du fichier texte
 * @return le listing
 */
listing_t listing_lecture(char *nomFichier) {
    listing_t retour;
    FILE *f;
    char ligne[64];
    int i, numLigne = 1;
    
    memset(&retour, '\0', sizeof(retour));
    
    if((f = fopen(nomFichier, "r")) == NULL) {
        fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Erreur lors de l'ouverture du fichier '%s' ", nomFichier);
        perror("");
        exit(EXIT_FAILURE);
    }

    while(fgets(ligne, 64, f) != NULL) {
        /* Suppression des caractères inutiles en fin de chaîne */
        i = strlen(ligne) - 1;
        while((i > 0) && (isspace(ligne[i]))) {
            ligne[i] = '\0';
            i--;
        }

        /* Récupération du quadruplet */
        retour.ins[retour.nb] = quadruplet_lecture(ligne, numLigne);
        retour.nb++;
        numLigne++;
    }

    if(fclose(f) == -1) {
        perror("\033[0;31m[ERREUR]\033[0;0m Erreur lors de la fermeture du fichier ");
        exit(EXIT_FAILURE);
    }
    
    return retour;
}

/**
 * Affiche un listing.
 * @param listing le listing
 */
void listing_affiche(listing_t *listing) {
    int i;
    
    for(i = 0; i < listing->nb; i++) {
        printf("%5d : ", i);
        quadruplet_affiche(&listing->ins[i]);
    }
}

/**
 * Exécute un listing.
 * @param listing le listing
 * @param trace si 'TRUE' affiche la trace
 */
void listing_execute(listing_t *listing, bool trace) {
    machine_t machine = machine_creation();
    int i = 0;
    
    while((i < INS_MAX) && (machine.cpt < listing->nb)) {
        if(trace) {
            printf("%d : ", machine.cpt);
            quadruplet_affiche(&listing->ins[machine.cpt]);
        }
        quadruplet_execute(&machine, &listing->ins[machine.cpt]);
        if((machine.cpt < 0) || (machine.cpt > listing->nb)) {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m compteur ordinal incorrect.\n");
            exit(EXIT_FAILURE);
        }
        i++;
    }
    if(i >= INS_MAX) {
        fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m nombre d'instructions trop grand.\n");
        exit(EXIT_FAILURE);
    }
}