#ifndef CELLULE
#define CELLULE

#include "quadruplet.h"

typedef struct cellule cellule;
typedef struct cellule_quadruplet cellule_quadruplet;

struct cellule{
    cellule *pred;
    cellule *succ;
    int place;
    char variable[255];
    char type[7];
    char typededonnee[10];
    union valeur
    {
        int i;
        float f;
    } valeur;
};

struct cellule_quadruplet{
    cellule_quadruplet *pred;
    cellule_quadruplet *succ;
    quadruplet_t* quadruplet;
};

/**
 * declare une variable avec son nom
 * @param c une cellule
 * @param variable nom de la variable
 * @param type type de la variable
 */
void declarerCellule(cellule *c, char* variable, char* type, int place);

/**
 * declare une variable avec son nom
 * @param c une cellule
 * @param variable nom de la variable
 * @param type type de retour
 * @param typededonnee determine si il dagit d'une fonction
 */
void fonctionCellule(cellule *c, char* variable, char* type, char* typededonnee);

/**
 * initialise une cellule avec une valeur
 * @param c une cellule
 * @param variable le nom de la variable
 * @param valeur la valeur a inserer dans la variable
 */
void initialiserCellule(cellule *c, char* variable, union valeur valeur);


/**
 * alloue la memoire d'une cellule
 * @return emplacement memoire de la cellule
 */
cellule* creationCellule();

/**
 * alloue la memoire d'un quadruplet
 * @return emplacement memoire du quadruplet
 */
cellule_quadruplet* creationQuadruplet();

/**
 * initialise un quadruplet
 * @param cq une cellule de quadruplet
 * @param q un quadruplet
 */
void initialiserQuadruplet(cellule_quadruplet *cq, quadruplet_t* q);

#endif