%{
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table_hachage.h"
#include "gestion_fichier.h"

#define VRAI 1
#define FAUX 0

void fonctionTableSymbole(table_de_hachage* tabh, char* variable, char* type, char* typededonnee);
void declarationTableSymbole(table_de_hachage* tabh, char* variable, char* type, int place);
void insererTableSymbole(table_de_hachage* tabh, char* variable, union valeur valeur);
void* nouvTemp();
cellule* place(char* variable, table_de_hachage *tabh);
void ajoute(char* variable, char* type, table_de_hachage *tabh, int place);
void gen(liste_quadruplet* liste_quadruplet, unsigned char type, type_t ope1, int ope2, int ope3);
void yyerror(const char *erreurMsg);
int yylex();

extern FILE *yyin;
extern FILE *yyout;

int emplacement = 0;
int erreur = 0;
int affichage = 0;
int tmp = 0;
char type_var[7];
table_de_hachage* tabh, *tabh_fonction;
liste_quadruplet* liste_quad;
union valeur valeur;
type_t ope1;
%}

%union {
    int nombre; 
    char* caractere;
    float reel;
}

%token <nombre> ENTIER
%token <caractere> VAR
%token <caractere> CONST
%token <reel> REEL
%token <caractere> TYPE
%token <caractere> FONCT
%token <caractere> ASSIGN ET OU NON Si Sinon Alors EGALE INF_EGALE SUP_EGALE SUP INF 
DIFF DEFINIT PLUS MOINS MULTIPLI DIVISE MOD CAST_ENTIER CAST_REEL PARENTHESE_G PARENTHESE_D
ECRIRE LIRE VIRGULE COMMENTAIRE FONCTION PROCEDURE DEBUT FIN DECLARATION RETOURNER ALGORITHME
POUR ALLANT_DE A FAIRE FINPOUR FINSI TANTQUE FINTANTQUE

%left ASSIGN DEFINIT
%left ET OU NON
%left EGALE INF_EGALE SUP_EGALE SUP INF DIFF
%left PLUS MOINS
%left MULTIPLI DIVISE MOD
%right PUISSANCE
%left PARENTHESE_D PARENTHESE_G
%left CAST_ENTIER CAST_REEL
%left Sinon
%left Si Alors POUR ALLANT_DE A FAIRE
%left COMMENTAIRE

%type <nombre> expression_entier
%type <caractere> expression_variable
%type <reel> expression_reel
%type <caractere> expression_type
%type <caractere> expression_fonction

%%

programme : expression {
    if(!erreur){
            if(affichage){
                affichage = 0;
            }else{
                afficher_table_hachage(tabh);
                afficher_liste_quadruplet(liste_quad);
            }
        }
        else{
            printf(" \n\033[0;31m*** ERREUR ***\033[0;0mune erreur est survenu\n");
            erreur = 0; 
        } 
    }
    | 
    ;

expression:  expression_entier '\n' programme
           | expression_reel '\n' programme
           | expression_type '\n' programme
           | expression_variable '\n' programme
           | expression_fonction '\n' programme
      ;
      
/*************************************************     expression_entier 1     **************************************************************/
expression_entier: ENTIER
        /************************************ OPERATEURS RELATIONNELS *************************************/
        |ALGORITHME { affichage = 1;}
        |DEBUT {affichage = 1;}
        |FIN { 
            affichage = 1;
            YYACCEPT;
        }
        |DECLARATION {affichage = 1;}
/** == **/
        |expression_entier EGALE expression_entier { $$ = $1 == $3;}
        |expression_entier EGALE expression_reel { $$ = $1 == $3;}
        |expression_reel EGALE expression_reel {$$ = $1 == $3;}
        |expression_reel EGALE expression_entier {$$ = $1 == $3;}
        |expression_reel EGALE expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 == c->valeur.i;
                else
                    $$ = $1 == c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable EGALE expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $3 == c->valeur.i;
                else
                    $$ = $3 == c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier EGALE expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 == c->valeur.i;
                else
                    $$ = $1 == c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable EGALE expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $3 == c->valeur.i;
                else
                    $$ = $3 == c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable EGALE expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c2->valeur.i == c->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f == c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i == c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                            $$ = c->valeur.f == c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
/** <= **/
        |expression_entier INF_EGALE expression_entier { $$ = $1 <= $3;}
        |expression_entier INF_EGALE expression_reel { $$ = $1 <= $3;}
        |expression_reel INF_EGALE expression_reel {$$ = $1 <= $3;}
        |expression_reel INF_EGALE expression_entier {$$ = $1 <= $3;}
        |expression_reel INF_EGALE expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 <= c->valeur.i;
                else
                    $$ = $1 <= c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable INF_EGALE expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i <= $3;
                else
                    $$ = c->valeur.f <= $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier INF_EGALE expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 <= c->valeur.i;
                else
                    $$ = $1 <= c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable INF_EGALE expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i <= $3;
                else
                    $$ = c->valeur.f <= $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable INF_EGALE expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i <= c2->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f <= c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i <= c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.f <= c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        
/** >= **/
        |expression_entier SUP_EGALE expression_entier { $$ = $1 >= $3;}
        |expression_entier SUP_EGALE expression_reel { $$ = $1 >= $3;}
        |expression_reel SUP_EGALE expression_reel {$$ = $1 >= $3;}
        |expression_reel SUP_EGALE expression_entier {$$ = $1 >= $3;}
        |expression_reel SUP_EGALE expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 >= c->valeur.i;
                else
                    $$ = $1 >= c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable SUP_EGALE expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i >= $3;
                else
                    $$ = c->valeur.f >= $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier SUP_EGALE expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 >= c->valeur.i;
                else
                    $$ = $1 >= c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable SUP_EGALE expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i >= $3;
                else
                    $$ = c->valeur.f >= $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable SUP_EGALE expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i >= c2->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f >= c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i >= c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.f >= c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
/** < **/
        |expression_entier INF expression_entier { $$ = $1 < $3;}
        |expression_entier INF expression_reel { $$ = $1 < $3;}
        |expression_reel INF expression_reel {$$ = $1 < $3;}
        |expression_reel INF expression_entier {$$ = $1 < $3;}
        |expression_reel INF expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 < c->valeur.i;
                else
                    $$ = $1 < c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable INF expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i < $3;
                else
                    $$ = c->valeur.f < $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier INF expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 < c->valeur.i;
                else
                    $$ = $1 < c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable INF expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i < $3;
                else
                    $$ = c->valeur.f < $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable INF expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i < c2->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f < c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i < c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.f < c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
/** > **/
        |expression_entier SUP expression_entier { $$ = $1 > $3;}
        |expression_entier SUP expression_reel { $$ = $1 > $3;}
        |expression_reel SUP expression_reel {$$ = $1 > $3;}
        |expression_reel SUP expression_entier {$$ = $1 > $3;}
        |expression_reel SUP expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 > c->valeur.i;
                else
                    $$ = $1 > c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable SUP expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i > $3;
                else
                    $$ = c->valeur.f > $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier SUP expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 > c->valeur.i;
                else
                    $$ = $1 > c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable SUP expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i > $3;
                else
                    $$ = c->valeur.f > $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable SUP expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i > c2->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f > c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i > c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.f > c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
/** != **/
        |expression_entier DIFF expression_entier { $$ = $1 != $3;}
        |expression_entier DIFF expression_reel { $$ = $1 != $3;}
        |expression_reel DIFF expression_reel {$$ = $1 != $3;}
        |expression_reel DIFF expression_entier {$$ = $1 != $3;}
        |expression_reel DIFF expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 != c->valeur.i;
                else
                    $$ = $1 != c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable DIFF expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i != $3;
                else
                    $$ = c->valeur.f != $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier DIFF expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 != c->valeur.i;
                else
                    $$ = $1 != c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable DIFF expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i != $3;
                else
                    $$ = c->valeur.f != $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable DIFF expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i != c2->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f!= c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i != c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.f != c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
/** et **/
        /************************************ OPERATEURS LOGIQUES *************************************/
        |expression_entier ET expression_entier { $$ = $1 && $3;}
        |expression_entier ET expression_reel { $$ = $1 && $3;}
        |expression_reel ET expression_reel {$$ = $1 && $3;}
        |expression_reel ET expression_entier {$$ = $1 && $3;}
        |expression_reel ET expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 && c->valeur.i;
                else
                    $$ = $1 && c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable ET expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i && $3;
                else
                    $$ = c->valeur.f && $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier ET expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 && c->valeur.i;
                else
                    $$ = $1 && c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable ET expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i && $3;
                else
                    $$ = c->valeur.f && $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable ET expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i && c2->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f && c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i && c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.f && c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
/** ou **/
        |expression_entier OU expression_entier { $$ = $1 || $3;}
        |expression_entier OU expression_reel { $$ = $1 || $3;}
        |expression_reel OU expression_reel {$$ = $1 || $3;}
        |expression_reel OU expression_entier {$$ = $1 || $3;}
        |expression_reel OU expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 || c->valeur.i;
                else
                    $$ = $1 || c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable OU expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i || $3;
                else
                    $$ = c->valeur.f || $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_entier OU expression_variable{
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = $1 || c->valeur.i;
                else
                    $$ = $1 || c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        |expression_variable OU expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = c->valeur.i || $3;
                else
                    $$ = c->valeur.f || $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        |expression_variable OU expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i || c2->valeur.i;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.f || c2->valeur.i;
                    }
                    else if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.i || c2->valeur.f;
                    }
                    else if(strcmp(c->type, "reel") == 0 && strcmp(c2->type, "reel") == 0){
                        $$ = c->valeur.f || c2->valeur.f;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
/** non **/
        | NON expression_entier { $$ = !$2; }
        | NON expression_reel { $$ = !$2; }
        | NON expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$2)){
                if(strcmp(c->type, "entier") == 0)
                    $$ = !c->valeur.i;
                else
                    $$ = !c->valeur.f;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$2);
                erreur = 1;
            }
        }
        /**************************************** BLOC CONDITION ******************************************/
/** Si **/
        | Si expression_entier Alors expression_entier Sinon expression_entier FINSI {
            if($2){
                $$ = $4;
            }
            else{
                $$ = $6;
            }
        }
        | Si expression_entier Alors expression_entier FINSI{
            if($2){
                $$ = $4;
            }
        }
/** Pour **/
        | POUR expression_variable ALLANT_DE expression_entier A expression_entier FAIRE expression_entier FINPOUR{
            cellule *c ;
            union valeur valeur;
            int tmp = 0;
            if(rechercher_hachage(tabh,$2)){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                erreur = 1;
            }
            else{
                if($4 > $6){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Décrémentation \n\n");
                    erreur = 1;
                }
                else{
                    declarationTableSymbole(tabh, $2, "entier", emplacement);
                    emplacement++;
                    valeur.i = $4;
                    c = modifierCellule(tabh, $2, valeur);
                    for(c->valeur.i;c->valeur.i < $6; c->valeur.i++){
                        tmp = $8;
                    }
                    printf("%d\n",tmp);
                    $$ = tmp;
                }
            }
        }
        | POUR expression_variable ALLANT_DE expression_entier A expression_entier FAIRE expression_variable FINPOUR{
            cellule *c ;
            union valeur valeur;
            int tmp = 0;
            if(rechercher_hachage(tabh,$2)){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                erreur = 1;
            }
            else{
                if($4 > $6){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Décrémentation \n\n");
                    erreur = 1;
                }
                else{
                    declarationTableSymbole(tabh, $2, "entier", emplacement);
                    emplacement++;
                    valeur.i = $4;
                    c = modifierCellule(tabh, $2, valeur);
                    for(c->valeur.i;c->valeur.i < $6; c->valeur.i++){
                        
                    }
                    affichage = 1;
                }
            }
        }
        | POUR expression_variable ALLANT_DE expression_entier A expression_variable FAIRE expression_variable FINPOUR{
            cellule *c, *c2 ;
            union valeur valeur;
            int tmp;
            if(rechercher_hachage(tabh,$2)){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                erreur = 1;
            }
            else{
                if(c = rechercher_hachage(tabh,$6)){
                    if($4 > c->valeur.i){
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Décrémentation \n\n");
                        erreur = 1;
                    }
                    else{
                        declarationTableSymbole(tabh, $2, "entier", emplacement);
                        emplacement++;
                        valeur.i = $4;
                        c2 = modifierCellule(tabh, $2, valeur);
                        for(c->valeur.i;c->valeur.i < c2->valeur.i; c->valeur.i++){
                            
                        }
                    }
                }else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable non definit \n\n");
                    erreur = 1;
                }
            }
        }
/** TANTQUE **/
        | TANTQUE expression_entier FAIRE expression_entier FINTANTQUE{
            while($2){
                $4;
            }
        }
        /**************************************** CALCULES ******************************************/
        | expression_entier PUISSANCE expression_entier { $$ = pow($1, $3); }
        | expression_variable PUISSANCE expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = pow(c->valeur.i, $3);
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable indefinie\n\n");
                erreur = 1;
            }
        }
        | expression_entier PUISSANCE expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = pow($1, c->valeur.i);
                }else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Argument incorrect\n\n");
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable indefinie\n\n");
                erreur = 1;
            }
        }
        | expression_variable PUISSANCE expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$3)){
                if(c = rechercher_hachage(tabh,$1)){
                   if(strcmp(c->type, "entier") == 0 && strcmp(c2->type, "entier") == 0){
                        $$ = c->valeur.i && c2->valeur.i;
                    }
                    else {
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Argument incorrect\n\n");
                        erreur = 1;
                    }
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable indefinie\n\n");
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable indefinie\n\n");
                erreur = 1;
            }
        }
        | expression_entier MOD expression_entier { $$ = $1 % $3; }
        | expression_variable MOD expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i % $3;
                }else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : argument invalide\n");
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier MOD expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = $1 % c->valeur.i;
                }else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : argument invalide\n");
                    erreur = 1;
                }   
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        | expression_variable MOD expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, c2->type) == 0){
                        if(strcmp(c->type, "entier") == 0){
                            if(c2->valeur.i == 0){
                                printf("\n\033[0;31m*** ERREUR ***\033[0;0m divison par 0 impossible\n");
                                erreur = 1;
                            }
                            else{
                                $$ = c->valeur.i % c2->valeur.i ;
                            }
                        }
                    }
                    else{
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : les variables ne sont pas de meme type\n");
                        erreur = 1;
                    }
                }
                else{
                    printf("ERREUR : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier MULTIPLI expression_entier { $$ = $1 * $3; }
        | expression_variable MULTIPLI expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i * $3;
                }else
                    $$ = c->valeur.f * $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier MULTIPLI expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i * $1;
                }else
                    $$ = c->valeur.f * $1;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        | expression_variable MULTIPLI expression_variable {
            cellule *c;
            cellule *c2;
            int tmp, tmp2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, c2->type) == 0){
                        if(strcmp(c->type, "entier") == 0){
                            tmp = c->valeur.i;
                            tmp2 = c2->valeur.i;
                            $$ = tmp * tmp2 ;
                        }
                    }
                    else{
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : les variables ne sont pas de meme type\n");
                        erreur = 1;
                    }
                }
                else{
                    printf("ERREUR : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier DIVISE expression_entier {
            if($3 == 0){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m divison par 0 impossible\n");
                erreur = 1;
            }
            else{
                $$ = $1 / $3;
            }
        }
        | expression_variable DIVISE expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, c2->type) == 0){
                        if(strcmp(c->type, "entier") == 0){
                            if(c2->valeur.i == 0){
                                printf("\n\033[0;31m*** ERREUR ***\033[0;0m divison par 0 impossible\n");
                                erreur = 1;
                            }
                            else{
                                $$ = c->valeur.i / c2->valeur.i ;
                            }
                        }
                    }
                    else{
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : les variables ne sont pas de meme type\n");
                        erreur = 1;
                    }
                }
                else{
                    printf("ERREUR : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier PLUS expression_entier { $$ = $1 + $3; }
        | expression_variable PLUS expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i + $3;
                }else
                    $$ = c->valeur.f + $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier PLUS expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i + $1;
                }else
                    $$ = c->valeur.f + $1;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        | expression_variable PLUS expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, c2->type) == 0){
                        if(strcmp(c->type, "entier") == 0){
                            $$ = c->valeur.i + c2->valeur.i ;
                        }
                    }
                    else{
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : les variables ne sont pas de meme type\n");
                        erreur = 1;
                    }
                }
                else{
                    printf("ERREUR : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier MOINS expression_entier { $$ = $1 - $3; }
        | expression_variable MOINS expression_entier {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i - $3 ;
                }
                else{
                    $$ = c->valeur.f - $3 ;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_entier MOINS expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = $1 - c->valeur.i ;
                }
                else{
                    $$ = $1 - c->valeur.f ;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        | expression_variable MOINS expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, c2->type) == 0){
                        if(strcmp(c->type, "entier") == 0){
                            $$ = c->valeur.i - c2->valeur.i ;
                        }
                    }
                    else{
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : les variables ne sont pas de meme type\n");
                        erreur = 1;
                    }
                }
                else{
                    printf("ERREUR : variable '%s' non declaré\n",$3);
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | PARENTHESE_G expression_entier PARENTHESE_D { $$ = $2; }
        | MOINS PARENTHESE_G expression_entier PARENTHESE_D { $$ = -$3; }
        /**************************************** FONCTION ******************************************/
        | ECRIRE PARENTHESE_G expression_entier PARENTHESE_D {
            printf("%d\n",$3);
            affichage = 1;
        }
        | CAST_ENTIER expression_reel { $$ = (int) $2; }
        | CAST_ENTIER expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$2)){
                if(strcmp(c->type, "entier") == 0){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Cast de type identique au type du nombre\n");
                    erreur = 1;
                }
                else{
                    $$ = (int) c->valeur.f ;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$2);
                erreur = 1;
            }
        }
        | CAST_REEL expression_reel {
            printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Cast de type identique au type du nombre\n");
            erreur = 1;
        }
        | COMMENTAIRE expression_entier { affichage = 1; };
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
/*************************************************     expression_variable     *******************************************************/
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
    expression_variable: VAR
        /**************************************** VARIABLES ASSIGNATION ******************************************/
        | expression_variable ASSIGN expression_entier {
            cellule *c;
            valeur.i = $3;
            if(c = rechercher_hachage(tabh,$1)){
                memset(&ope1,0,sizeof(ope1));
                if(strcmp("reel", c->type)){
                    ope1.entier = valeur.i;
                   gen(liste_quad, QUAD_E_CONS, ope1, -1, c->place);
                   modifierCellule(tabh, $1, valeur);
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable de type reel mais assignation de type entiere\n");
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable non declaré\n");
            }
        }
        | expression_variable ASSIGN expression_reel {
            cellule *c;
            valeur.f = $3;
            if(c = rechercher_hachage(tabh,$1)){
                memset(&ope1,0,sizeof(ope1));
                if(strcmp("entier", c->type)){
                    ope1.reel = valeur.f;
                    gen(liste_quad, QUAD_R_CONS, ope1, -1, c->place);
                    modifierCellule(tabh, $1, valeur);
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable de type entiere mais assignation de type reel\n");
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable non declaré\n");
            }
        }
        | expression_variable ASSIGN expression_variable {
            cellule *c;
            cellule *c2;
            if(c = rechercher_hachage(tabh,$1)){
                if(c2 = rechercher_hachage(tabh,$3)){
                    if(strcmp(c->type, c2->type) == 0){
                        modifierCellule(tabh, $1, c2->valeur);
                    }
                    else{
                        printf("\n\033[0;31m*** ERREUR ***\033[0;0m : les variables ne sont pas de meme type\n");
                    }
                }
                else{
                    printf("ERREUR : variable '%s' non declaré\n",$3);
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
            }
        }
        /**************************************** BLOC CONDITION ******************************************/
/** Si **/
        | Si expression_entier Alors expression_variable Sinon expression_variable FINSI {
            if($2){
                $$ = $4;
            }
            else{
                $$ = $6;
            }
        }
        | Si expression_entier Alors expression_variable FINSI{
            if($2){
                $$ = $4;
            }
        }       
        /**************************************** FONCTION ******************************************/
        | ECRIRE PARENTHESE_G expression_variable PARENTHESE_D {
            cellule *c = rechercher_hachage(tabh, $3);
            memset(&ope1,0,sizeof(ope1));
            if (strcmp("entier", c->type) == 0){
                gen(liste_quad, QUAD_E_OUT, ope1, -1, c->place);
                //printf("%s= %d\n", c->variable, c->valeur.i);
            }  
            else{
                gen(liste_quad, QUAD_R_OUT, ope1, -1, c->place);
                //printf("%s= %f\n", c->variable, c->valeur.f);
            }
            affichage = 1;
        }
        | LIRE PARENTHESE_G expression_variable PARENTHESE_D {
            cellule *c;
            if(c = rechercher_hachage(tabh, $3)){
                memset(&ope1,0,sizeof(ope1));
                if (strcmp("entier", c->type) == 0){
                    gen(liste_quad, QUAD_E_IN, ope1, -1, c->place);
                    /*printf("Entrez la valeur de %s: ", $3);
                    scanf("%d", &c->valeur.i);
                    printf("\b");*/
                }
                else {
                    gen(liste_quad, QUAD_R_IN, ope1, -1, c->place);
                    /*printf("Entrez la valeur de %s: ", $3);  
                    scanf("%lf", &c->valeur.f);
                    printf("\b");*/
                }  
                modifierCellule(tabh, $3, c->valeur);
            }
        }
        | COMMENTAIRE expression_variable { affichage = 1; }
        ;
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
/*************************************************     expression_type     ***********************************************************/
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
    expression_type: TYPE
        /**************************************** VARIABLES DECLARATION ******************************************/
        | expression_variable DEFINIT expression_variable {
            strcpy(type_var, $3);
            if(strcmp("entier", $3) == 0 || strcmp("reel",$3) == 0){
                if(rechercher_hachage(tabh,$1)){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                }
                else{
                    declarationTableSymbole(tabh, $1, $3, emplacement);
                    emplacement++;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : type inconnue\n\n");
            }
        }
        | expression_variable VIRGULE expression_type {
            if(strcmp("entier", type_var) == 0 || strcmp("reel",type_var) == 0){
                if(rechercher_hachage(tabh,$1)){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                }
                else{
                    if(strcmp("entier", type_var) == 0){
                        declarationTableSymbole(tabh, $1, "entier", emplacement);
                        emplacement++;
                    }else if(strcmp("reel", type_var) == 0){
                        declarationTableSymbole(tabh, $1, "reel", emplacement);
                        emplacement++;
                    }
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : type inconnue\n\n");
            }
        }
        /**************************************** BLOC CONDITION ******************************************/
/** Si **/
         ;
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
/*************************************************     expression_reel     ***********************************************************/
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
    expression_reel: REEL
        /**************************************** CALCULES ******************************************/
        | expression_reel PUISSANCE expression_reel { $$ = powf($1, $3); }
        | expression_variable PUISSANCE expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "reel") == 0){
                    $$ = powf(c->valeur.f, $3);
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable indefinie\n\n");
                erreur = 1;
            }
        }
        | expression_reel PUISSANCE expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "reel") == 0){
                    $$ = powf($1, c->valeur.f);
                }else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Argument incorrect\n\n");
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable indefinie\n\n");
                erreur = 1;
            }
        }
        | expression_reel MULTIPLI expression_entier { $$ = $1 * $3; }
        | expression_entier MULTIPLI expression_reel { $$ = $1 * $3; }
        | expression_reel MULTIPLI expression_reel { $$ = $1 * $3; }
        | expression_reel DIVISE expression_entier {
            if($3 == 0){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m divison par 0 impossible\n");
                erreur = 1;
            }
            else{
                $$ = $1 / $3;
            }
        }
        | expression_entier DIVISE expression_reel {
            if($3 == 0){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m divison par 0 impossible\n");
                erreur = 1;
            }
            else{
                $$ = $1 / $3;
            }
        }
        | expression_reel DIVISE expression_reel {
            if($3 == 0){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m divison par 0 impossible\n");
                erreur = 1;
            }
            else{
                $$ = $1 / $3;
            }
        }
        | expression_reel PLUS expression_entier { $$ = $1 + $3; }
        | expression_entier PLUS expression_reel { $$ = $1 + $3; }
        | expression_reel PLUS expression_reel { $$ = $1 + $3; }
        | expression_variable PLUS expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i + $3;
                }else
                    $$ = c->valeur.f + $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_reel PLUS expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i + $1;
                }else
                    $$ = c->valeur.f + $1;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        | expression_reel MOINS expression_entier { $$ = $1 - $3; }
        | expression_entier MOINS expression_reel { $$ = $1 - $3; }
        | expression_reel MOINS expression_reel { $$ = $1 - $3; }
        | expression_variable MOINS expression_reel {
            cellule *c;
            if(c = rechercher_hachage(tabh,$1)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i - $3;
                }else
                    $$ = c->valeur.f - $3;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$1);
                erreur = 1;
            }
        }
        | expression_reel MOINS expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$3)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = c->valeur.i - $1;
                }else
                    $$ = c->valeur.f - $1;
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$3);
                erreur = 1;
            }
        }
        | expression_reel MOD expression_entier {
            erreur = 1;
            printf("\n\033[0;31m*** ERREUR ***\033[0;0m Cette opperation est impossible\n");
        }
        | expression_entier MOD expression_reel {
            erreur = 1;
            printf("\n\033[0;31m*** ERREUR ***\033[0;0m Cette opperation est impossible\n");
        }
        | expression_reel MOD expression_reel {
            erreur = 1;
            printf("\n\033[0;31m*** ERREUR ***\033[0;0m Cette opperation est impossible\n");
        }
        |PARENTHESE_G expression_reel PARENTHESE_D { $$ = $2; }
        |MOINS PARENTHESE_G expression_reel PARENTHESE_D { $$ = -$3; }
        /**************************************** BLOC CONDITION ******************************************/
/** Si **/
        | Si expression_entier Alors expression_reel Sinon expression_reel FINSI {
            if($2){
                $$ = $4;
            }
            else{
                $$ = $6;
            }
        }
        | Si expression_entier Alors expression_reel FINSI{
            if($2){
                $$ = $4;
            }
        }
        /**************************************** FONCTION ******************************************/
        | ECRIRE PARENTHESE_G expression_reel PARENTHESE_D {
            printf("%f\n",$3);
            affichage = 1;
        }
        | CAST_REEL expression_entier { $$ = (float) $2; }
        | CAST_REEL expression_variable {
            cellule *c;
            if(c = rechercher_hachage(tabh,$2)){
                if(strcmp(c->type, "entier") == 0){
                    $$ = (float) c->valeur.i;
                }
                else{
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Cast de type reel au type reel\n");
                    erreur = 1;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : variable '%s' non declaré\n",$2);
                erreur = 1;
            }
        }
        | CAST_ENTIER expression_entier {
            printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Cast de type identique au type du nombre\n");
            erreur = 1;
        }
        | COMMENTAIRE expression_reel { affichage = 1; }
        ;
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
/*************************************************     expression_entier 5     **************************************************************/
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
    expression_fonction: FONCT 
        | FONCTION expression_variable PARENTHESE_G expression_fonction PARENTHESE_D DEFINIT expression_variable {
            if(strcmp("entier", $7) == 0 || strcmp("reel",$7) == 0){
                if(rechercher_hachage(tabh,$2)){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de fonction \n\n");
                }
                else{
                    fonctionTableSymbole(tabh, $2, $7, "fonction");
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : type inconnue\n\n");
            }
        }
        | FONCTION expression_variable PARENTHESE_G PARENTHESE_D DEFINIT expression_variable {
            if(strcmp("entier", $6) == 0 || strcmp("reel",$6) == 0){
                if(rechercher_hachage(tabh,$2)){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de fonction \n\n");
                }
                else{
                    fonctionTableSymbole(tabh, $2, $6, "fonction");
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : type inconnue\n\n");
            }
        }
        | PROCEDURE expression_variable PARENTHESE_G expression_fonction PARENTHESE_D {
            if(rechercher_hachage(tabh,$2)){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de procedure \n\n");
            }
            else{
                fonctionTableSymbole(tabh, $2, "null", "procedure");
            }
        }
        | PROCEDURE expression_variable PARENTHESE_G PARENTHESE_D {
            if(rechercher_hachage(tabh,$2)){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de procedure \n\n");
            }
            else{
                fonctionTableSymbole(tabh, $2, "null", "procedure");
            }
        }
        | expression_variable expression_variable {
            strcpy(type_var, $1);
            if(strcmp("entier", $1) == 0 || strcmp("reel",$1) == 0){
                if(rechercher_hachage(tabh_fonction,$1)){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                }
                else{
                    declarationTableSymbole(tabh_fonction, $2, type_var, emplacement);
                    emplacement++;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : type inconnue\n\n");
            }
        }
        | expression_variable expression_variable VIRGULE expression_fonction {
            strcpy(type_var, $1);
            if(strcmp("entier", $1) == 0 || strcmp("reel",$1) == 0){
                if(rechercher_hachage(tabh_fonction,$2)){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                }
                else{
                    declarationTableSymbole(tabh_fonction, $2, $1, emplacement);
                    emplacement++;
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : type inconnue\n\n");
            }
        }
        | expression_variable expression_variable expression_fonction {
            strcpy(type_var, $1);
            if(strcmp("entier", $1) == 0 || strcmp("reel",$1) == 0){
                if(rechercher_hachage(tabh_fonction,$2)){
                    printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
                }
                else{
                    declarationTableSymbole(tabh_fonction, $2, $1, emplacement);
                    emplacement++;
                    if(tmp > 0){
                        donner_type_table_hachage(tabh_fonction, type_var);
                        tmp = 0;
                    }
                }
            }
            else{
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : type inconnue\n\n");
            }
        }
/********************************************************************************************************/
/********************************************************************************************************/
        | VIRGULE expression_variable expression_fonction {
            tmp++;
            if(rechercher_hachage(tabh_fonction,$2)){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
            }
            else{
                declarationTableSymbole(tabh_fonction, $2, "null", emplacement);
                emplacement++;
            }
        }
        |  VIRGULE expression_variable {
            tmp++;
            if(rechercher_hachage(tabh_fonction,$2)){
                printf("\n\033[0;31m*** ERREUR ***\033[0;0m : Redifinition de variable \n\n");
            }
            else{
                declarationTableSymbole(tabh_fonction, $2, "null", emplacement);
                emplacement++;
            }
        }
    ;
%%

int main(int argc, char* argv[]) {
    int taille;

     /* Vérification de la présence d'un argument */
    if (argc != 3) {
        fprintf(stderr, "\033[0;31m*** ERREUR ***\033[0;0m Nombre d'arguments incorrect.\n Exemple : ./algorithme algorithme.txt quadruplet.alg \n");  
        exit(EXIT_FAILURE);
    }

    tabh = malloc(sizeof(table_de_hachage));
    tabh_fonction = malloc(sizeof(table_de_hachage));
    liste_quad = malloc(sizeof(liste_quadruplet));

    taille = 11;
    
    initialiser_table_hachage(tabh_fonction, taille);
	initialiser_table_hachage(tabh, taille);
    initialiser_liste_quadruplet(liste_quad);
    if((yyin = fopen(argv[1], "r")) == NULL){
        perror("\n\033[0;31m*** ERREUR ***\033[0;0m Erreur lors de l'ouverture du fichier contenant l'algorithme\n");
        exit(EXIT_FAILURE);
    }
    printf("\033[0;32m*** Fichier Ouvert ***\033[0;0m\n");
    yyparse();
    afficher_liste_quadruplet(liste_quad);
    ecritureFichier(liste_quad, yyout, argv[2]);

    detruire_table_hachage(tabh);
    detruire_table_hachage(tabh_fonction);
    detruire_liste_quadruplet(liste_quad);
    fermetureFichier(yyin);
    fermetureFichier(yyout);
  
    return EXIT_SUCCESS;
}