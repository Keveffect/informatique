#ifndef TABLE_DE_HACHAGE
#define TABLE_DE_HACHAGE

#include "liste.h"

typedef struct table_de_hachage table_de_hachage;

struct table_de_hachage {
    liste** liste;
    int taille;
};

/**
 * initialise une table de hachage
 * @param tabh table de hachage
 * @param taille taille de la table
 */
void initialiser_table_hachage(table_de_hachage*, int);

/**
 * fonction utilisé pour le hachage
 * @param taille taille du mot
 * @return une valeur utilisé pour calculer le hachage
 */
unsigned long long puissance(int);

/**
 * calcule la taille du mot
 * @param tmp le mot
 * @return une valeur utilisé pour calculer le hachage
 */
unsigned long long convertir_ch_entier(char*);

/**
 * fonction de hachage
 * @param k le hachage
 * @param taille la taille de la table de hachage
 * @return emplacement dans la table de hachage
 */
int hachage(unsigned long long, int);

/**
 * affiche une table de hachage
 * @param tabh table de hachage
 */
void afficher_table_hachage(table_de_hachage*);

/**
 * detruit une table de hachage
 * @param tabh table de hachage
 */
void detruire_table_hachage(table_de_hachage*);

/**
 * insere un mot dans la table de hachage
 * @param tabh la table de hachage
 * @param c une cellule
 */
void inserer_hachage(table_de_hachage*, cellule*);

/**
 * cherche un mot dans la table de hachage
 * @param tabh la table de hachage
 * @param variable mot a rechercher
 * @return la cellule du mot
 */
cellule* rechercher_hachage(table_de_hachage *, char*);

/**
 * supprime un mot dans la table de hachage
 * @param tabh la table de hachage
 * @param mot mot a supprimer
 */
void supprimer_hachage(table_de_hachage*, char*);

/**
 * compte le nombre de mot dans la table
 * @param tabh la table de hachage
 * @return le nombre de mot
 */
int compter_table_hachage(table_de_hachage* );

/**
 * ajoute la valeur a une variable dans la table de hachage
 * @param tabh la table de hachage
 * @param variable mot a rechercher
 * @param valeur la valeur a inserer
 * @return la cellule du mot
 */
cellule* modifierCellule(table_de_hachage *tabh, char* variable, union valeur valeur);

/**
 * donne le type a une varibale
 * @param tabh table de hachage
 * @param type le nouveau type des variables
 */
void donner_type_table_hachage(table_de_hachage* tabh, char* type);

#endif