#include "include.h"
#include "machine.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Crée une nouvelle machine.
 * @return la machine
 */
machine_t machine_creation() {
    machine_t resultat;
    
    memset(resultat.mem, '\0', sizeof(resultat.mem));
    memset(resultat.pile, '\0', sizeof(resultat.pile));
    resultat.cpt = 0;
    resultat.sommet = 0;

    return resultat;
}

/**
 * Récupère une valeur de la mémoire ou de la pile.
 * @param machine la machine
 * @param index l'index (positif = mémoire, négatif = pile)
 * @return la valeur
 */
type_t machine_getvaleur(machine_t *machine, int index) {
    type_t valeur;

    if(index >= 0) {
        if(index >= MEM_MAX) {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Accès mémoire incorrect (indice = %d, taille de la mémoire = %d)\n", index, MEM_MAX);
            exit(EXIT_FAILURE);
        }
        valeur = machine->mem[index];
    }
    else {
        index = machine->sommet + index;
        if((index < 0) || (index >= machine->sommet)) {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Accès à une valeur en dehors de la pile (indice = %d, taille de la pile = %d)\n", index, machine->sommet);
            exit(EXIT_FAILURE);
        }
        valeur = machine->pile[index];
    }
        
    return valeur;
}

/**
 * Modifie une valeur de la mémoire ou de la pile.
 * @param machine la machine
 * @param index l'index (positif = mémoire, négatif = pile) 
 * @param valeur la valeur
 */
void machine_setvaleur(machine_t *machine, int index, type_t valeur) {
    if(index >= 0) {
        if(index >= MEM_MAX) {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Accès mémoire incorrect (indice = %d, taille de la mémoire = %d)\n", index, MEM_MAX);
            exit(EXIT_FAILURE);
        }
        machine->mem[index] = valeur;
    }
    else {
        index = machine->sommet + index;
        if((index < 0) || (index >= machine->sommet)) {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Accès à une valeur en dehors de la pile (indice = %d, taille de la pile = %d)\n", index, machine->sommet);
            exit(EXIT_FAILURE);
        }
        machine->pile[index] = valeur;
    }
}

/**
 * Empile une valeur.
 * @param machine la machine
 * @param valeur la valeur
 */
void machine_empile(machine_t *machine, type_t valeur) {
    if(machine->sommet == PILE_MAX) {
        fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Pile pleine\n");
        exit(EXIT_FAILURE);
    }
    machine->pile[machine->sommet] = valeur;
    machine->sommet++;
}

/**
 * Dépile des valeurs.
 * @param machine la machine
 * @param nb le nombre d'éléments à dépiler
 */
void machine_depile(machine_t *machine, int num) {
    if(machine->sommet > num)
        machine->sommet -= num;
    else
        machine->sommet = 0;
}
