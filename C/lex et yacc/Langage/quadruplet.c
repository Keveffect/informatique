#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "include.h"
#include "machine.h"
#include "quadruplet.h"

/**
 * Récupère le prochain item du buffer.
 * @param buf le buffer de recherche
 * @param position la position actuelle
 * @param str l'item trouvé
 * @param n la taille de str
 * @return -1 si l'item n'a pas été trouvé
 */
int recherche(char *buf, int *position, char *str, int n) {
    int i = *position, taille = strlen(buf);
    int resultat = 0;

    memset(str, 0, n);
    while((i < taille) && (buf[i] != ';')) i++;

    if((i - *position < n) && (i > *position)) {
        strncpy(str, &buf[*position], i - *position);
    }

    *position = i + 1;

    return resultat;
}

/**
 * Vérification d'un opérande.
 * @param buf la chaîne de caractères
 * @param numLigne le numéro de ligne actuel
 * @return le quadruplet
 */
void quadruplet_check(int valeur, int numLigne, int operande) {
    if((valeur < -PILE_MAX) || (valeur >= MEM_MAX)) {
        fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérande %d incorrect (doit être compris entre -%d et %d)\n", numLigne, operande, PILE_MAX, MEM_MAX);
        exit(EXIT_FAILURE);
    }
}

/**
 * Convertit une chaîne de caractères en quadruplet.
 * @param buf la chaîne de caractères
 * @param numLigne le numéro de ligne actuel
 * @return le quadruplet
 */
quadruplet_t quadruplet_lecture(char *buf, int numLigne) {
    int courant, i;
    quadruplet_t resultat;
    char tmp[3][TAILLE_MAX];

    /* Récupération de l'opérateur */
    courant = 0;
    if(recherche(buf, &courant, tmp[0], TAILLE_MAX) == -1) {
        fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d sur l'opérateur.\n", numLigne);
        exit(EXIT_FAILURE);
    }
    if((resultat.type = quadruplet_str2type(tmp[0])) == QUAD_UNDE) {
        fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérateur inconnu.\n", numLigne);
        exit(EXIT_FAILURE);
    }

    /* Récupération des opérandes */
    for(i = 1; i <= 3; i++) {
        if(courant >= strlen(buf)) {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : l'opérande %d n'a pas été spécifié.\n", numLigne, i);
            exit(EXIT_FAILURE);
        }
        if(recherche(buf, &courant, tmp[i - 1], TAILLE_MAX) == -1) {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d sur l'opérande %d.\n", numLigne, i);
            exit(EXIT_FAILURE);
        }
    }
    
    /* Vérification de la cohérence */
    switch(resultat.type) {
        /* Deux arguments : [entier;;adresse] */
        case QUAD_E_CONS:
            resultat.ope1.entier = atoi(tmp[0]);
            if(strlen(tmp[1]) != 0) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérande 2 incorrect\n", numLigne);
                exit(EXIT_FAILURE);
            }
            resultat.ope2 = 0;
            resultat.ope3 = atoi(tmp[2]);
            quadruplet_check(resultat.ope3, numLigne, 3);
            break;
        /* Deux arguments : [adresse;;adresse] */
        case QUAD_AFFE:
        case QUAD_E_CONV:
        case QUAD_R_CONV:
            resultat.ope1.entier = atoi(tmp[0]);
            quadruplet_check(resultat.ope1.entier, numLigne, 1);
            if(strlen(tmp[1]) != 0) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérande 2 incorrect\n", numLigne);
                exit(EXIT_FAILURE);
            }
            resultat.ope2 = 0;
            resultat.ope3 = atoi(tmp[2]);
            quadruplet_check(resultat.ope3, numLigne, 3);
            break;
        /* Deux arguments : [réel;;adresse] */
        case QUAD_R_CONS:
            resultat.ope1.reel = atof(tmp[0]);
            if(strlen(tmp[1]) != 0) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérande 2 incorrect\n", numLigne);
                exit(EXIT_FAILURE);
            }
            resultat.ope2 = 0;
            resultat.ope3 = atoi(tmp[2]);
            quadruplet_check(resultat.ope3, numLigne, 3);
            break;
        /* Un argument : [;;adresse] */
        case QUAD_E_IN:
        case QUAD_E_OUT:
        case QUAD_R_IN:
        case QUAD_R_OUT:
        case QUAD_E_MOINU:
        case QUAD_R_MOINU:
        case QUAD_GOTI:
        case QUAD_EMPI:
            if((strlen(tmp[0]) != 0) || (strlen(tmp[1]) != 0)) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérande 0 et/ou 1 incorrect(s)\n", numLigne);
                exit(EXIT_FAILURE);
            }
            resultat.ope1.entier = 0;
            resultat.ope2 = 0;
            resultat.ope3 = atoi(tmp[2]);
            quadruplet_check(resultat.ope3, numLigne, 3);
            break;
        /* Un argument : [;;entier positif] */
        case QUAD_DEPI:
            if((strlen(tmp[0]) != 0) || (strlen(tmp[1]) != 0)) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérande 0 et/ou 1 incorrect(s)\n", numLigne);
                exit(EXIT_FAILURE);
            }
            resultat.ope1.entier = 0;
            resultat.ope2 = 0;
            resultat.ope3 = atoi(tmp[2]);
            if((resultat.ope3 <= 0) || (resultat.ope3 > PILE_MAX)) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : valeur incorrecte pour l'opérande 3\n", numLigne);
                exit(EXIT_FAILURE);
            }
            break;
        /* Un argument : [;;entier] */
        case QUAD_GOTO:
            if((strlen(tmp[0]) != 0) || (strlen(tmp[1]) != 0)) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m ligne %d : opérande 0 et/ou 1 incorrect(s)\n", numLigne);
                exit(EXIT_FAILURE);
            }
            resultat.ope1.entier = 0;
            resultat.ope2 = 0;
            resultat.ope3 = atoi(tmp[2]);
            break;
            
        /* Trois arguments : [adresse;adresse;adresse] */
        case QUAD_E_PLUS:
        case QUAD_E_MOIN:
        case QUAD_E_MULT:
        case QUAD_E_DIVI:
        case QUAD_E_MODU:
        case QUAD_R_PLUS:
        case QUAD_R_MOIN:
        case QUAD_R_MULT:
        case QUAD_R_DIVI:
        case QUAD_E_EGAL:
        case QUAD_E_DIFF:
        case QUAD_E_INFE:
        case QUAD_E_INEG:
        case QUAD_E_SUPE:
        case QUAD_E_SUEG:
        case QUAD_R_EGAL:
        case QUAD_R_DIFF:
        case QUAD_R_INFE:
        case QUAD_R_INEG:
        case QUAD_R_SUPE:
        case QUAD_R_SUEG:
            resultat.ope1.entier = atoi(tmp[0]);
            quadruplet_check(resultat.ope1.entier, numLigne, 1);
            resultat.ope2 = atoi(tmp[1]);
            quadruplet_check(resultat.ope2, numLigne, 2);
            resultat.ope3 = atoi(tmp[2]);
            quadruplet_check(resultat.ope3, numLigne, 3);
            break;
    }

    return resultat;
}

/**
 * Affiche un quadruplet sous forme d'une instruction à 3 adresses.
 * @param quad le quadruplet
 */
void quadruplet_affiche(quadruplet_t *quad) {
    switch(quad->type) {
        case QUAD_E_CONS:
            printf("MEM[%d] = %d\n", quad->ope3, quad->ope1.entier);
            break;
        case QUAD_R_CONS:
            printf("MEM[%d] = %f\n", quad->ope3, quad->ope1.reel);
            break;
        case QUAD_AFFE:
            printf("MEM[%d] = MEM[%d]\n", quad->ope3, quad->ope1.entier);
            break;
        case QUAD_E_IN:
            printf("lireEntier(MEM[%d])\n", quad->ope3);
            break;
        case QUAD_R_IN:
            printf("lireReel(MEM[%d])\n", quad->ope3);
            break;
        case QUAD_E_OUT:
            printf("afficherEntier(MEM[%d])\n", quad->ope3);
            break;
        case QUAD_R_OUT:
            printf("afficherReel(MEM[%d])\n", quad->ope3);
            break;
        case QUAD_GOTO:
            printf("goto %d\n", quad->ope3);
            break;
        case QUAD_GOTI:
            printf("goto MEM[%d]\n", quad->ope3);
            break;
        case QUAD_EMPI:
            printf("empiler MEM[%d]\n", quad->ope3);
            break;
        case QUAD_DEPI:
            printf("depiler %d\n", quad->ope3);
            break;
        case QUAD_E_CONV:
            printf("MEM[%d] = (entier)MEM[%d]\n", quad->ope3, quad->ope1.entier);
            break;
        case QUAD_R_CONV:
            printf("MEM[%d] = (réel)MEM[%d]\n", quad->ope3, quad->ope1.entier);
            break;
        case QUAD_E_PLUS:
            printf("MEM[%d] = MEM[%d] +(entier) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_E_MOIN:
            printf("MEM[%d] = MEM[%d] -(entier) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_E_MULT:
            printf("MEM[%d] = MEM[%d] *(entier) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_E_DIVI:
            printf("MEM[%d] = MEM[%d] /(entier) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_E_MODU:
            printf("MEM[%d] = MEM[%d] %%(entier) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_E_MOINU:
            printf("MEM[%d] = -(entier) MEM[%d]\n", quad->ope3, quad->ope1.entier);
            break;
            
        case QUAD_R_PLUS:
            printf("MEM[%d] = MEM[%d] +(réel) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_R_MOIN:
            printf("MEM[%d] = MEM[%d] -(réel) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_R_MULT:
            printf("MEM[%d] = MEM[%d] *(réel) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_R_DIVI:
            printf("MEM[%d] = MEM[%d] /(réel) MEM[%d]\n", quad->ope3, quad->ope1.entier, quad->ope2);
            break;
        case QUAD_R_MOINU:
            printf("MEM[%d] = -(reel) MEM[%d]\n", quad->ope3, quad->ope1.entier);
            break;
        case QUAD_E_EGAL:
            printf("Si MEM[%d] ==E MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_E_DIFF:
            printf("Si MEM[%d] !=E MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_E_INFE:
            printf("Si MEM[%d] <E MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_E_INEG:
            printf("Si MEM[%d] <=E MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_E_SUPE:
            printf("Si MEM[%d] >E MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_E_SUEG:
            printf("Si MEM[%d] >=E MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_R_EGAL:
            printf("Si MEM[%d] ==R MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_R_DIFF:
            printf("Si MEM[%d] !=R MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_R_INFE:
            printf("Si MEM[%d] <R MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_R_INEG:
            printf("Si MEM[%d] <=R MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_R_SUPE:
            printf("Si MEM[%d] >R MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
        case QUAD_R_SUEG:
            printf("Si MEM[%d] >=R MEM[%d] goto %d\n", quad->ope1.entier, quad->ope2, quad->ope3);
            break;
    }
}

/**
 * Convertit une chaîne de caractères en type de quadruplet.
 * @param str la chaîne de caractères
 * @return le type de quadruplet
 */
int quadruplet_str2type(char *str) {
    int resultat = QUAD_UNDE;
    
    if(strcmp(str, "consE") == 0)
        resultat = QUAD_E_CONS;
    else if(strcmp(str, "consR") == 0)
        resultat = QUAD_R_CONS;
    else if(strcmp(str, ":=") == 0)
        resultat = QUAD_AFFE;
    else if(strcmp(str, "inE") == 0)
        resultat = QUAD_E_IN;
    else if(strcmp(str, "inR") == 0)
        resultat = QUAD_R_IN;
    else if(strcmp(str, "outE") == 0)
        resultat = QUAD_E_OUT;
    else if(strcmp(str, "outR") == 0)
        resultat = QUAD_R_OUT;
    else if(strcmp(str, "goto") == 0)
        resultat = QUAD_GOTO;
    else if(strcmp(str, "gotoI") == 0)
        resultat = QUAD_GOTI;
    else if(strcmp(str, "empiler") == 0)
        resultat = QUAD_EMPI;
    else if(strcmp(str, "depiler") == 0)
        resultat = QUAD_DEPI;
    else if(strcmp(str, "entier") == 0)
        resultat = QUAD_E_CONV;
    else if(strcmp(str, "réel") == 0)
        resultat = QUAD_R_CONV;
    else if(strcmp(str, "+E") == 0)
        resultat = QUAD_E_PLUS;
    else if(strcmp(str, "-E") == 0)
        resultat = QUAD_E_MOIN;
    else if(strcmp(str, "*E") == 0)
        resultat = QUAD_E_MULT;
    else if(strcmp(str, "/E") == 0)
        resultat = QUAD_E_DIVI;
    else if(strcmp(str, "%E") == 0)
        resultat = QUAD_E_MODU;
    else if(strcmp(str, "+R") == 0)
        resultat = QUAD_R_PLUS;
    else if(strcmp(str, "-R") == 0)
        resultat = QUAD_R_MOIN;
    else if(strcmp(str, "*R") == 0)
        resultat = QUAD_R_MULT;
    else if(strcmp(str, "/R") == 0)
        resultat = QUAD_R_DIVI;
    else if(strcmp(str, "==E") == 0)
        resultat = QUAD_E_EGAL;
    else if(strcmp(str, "!=E") == 0)
        resultat = QUAD_E_DIFF;
    else if(strcmp(str, "<E") == 0)
        resultat = QUAD_E_INFE;
    else if(strcmp(str, "<=E") == 0)
        resultat = QUAD_E_INEG;
    else if(strcmp(str, ">E") == 0)
        resultat = QUAD_E_SUPE;
    else if(strcmp(str, ">=E") == 0)
        resultat = QUAD_E_SUEG;
    else if(strcmp(str, "==R") == 0)
        resultat = QUAD_R_EGAL;
    else if(strcmp(str, "!=R") == 0)
        resultat = QUAD_R_DIFF;
    else if(strcmp(str, "<R") == 0)
        resultat = QUAD_R_INFE;
    else if(strcmp(str, "<=R") == 0)
        resultat = QUAD_R_INEG;
    else if(strcmp(str, ">R") == 0)
        resultat = QUAD_R_SUPE;
    else if(strcmp(str, ">=R") == 0)
        resultat = QUAD_R_SUEG;

    return resultat;
}

/**
 * Convertit une chaîne de caractères en type de quadruplet.
 */
char* quadruplet_type2strstr(unsigned char type) {
    char* resultat = malloc(sizeof(char)*8);

    if(type == QUAD_E_CONS)
        strcpy(resultat, "consE");
    else if(type == QUAD_R_CONS) 
        strcpy(resultat, "consR");
    else if(type == QUAD_AFFE)
        strcpy(resultat, ":=");
    else if(type == QUAD_E_IN)
        strcpy(resultat, "inE");
    else if(type == QUAD_R_IN)
        strcpy(resultat, "inR");
    else if(type == QUAD_E_OUT)
        strcpy(resultat, "outE");
    else if(type == QUAD_R_OUT)
        strcpy(resultat, "outR");
    else if(type == QUAD_GOTO)
        strcpy(resultat, "goto");
    else if(type == QUAD_GOTI)
        strcpy(resultat, "gotoI");
    else if(type == QUAD_EMPI)
       strcpy(resultat, "empiler") ;
    else if(type == QUAD_DEPI)
        strcpy(resultat, "depiler");
    else if(type == QUAD_E_CONV)
        strcpy(resultat, "entier");
    else if(type == QUAD_R_CONV)
        strcpy(resultat, "réel");
    else if(type == QUAD_E_PLUS)
        strcpy(resultat, "+E");
    else if(type == QUAD_E_MOIN)
        strcpy(resultat, "-E");
    else if(type == QUAD_E_MULT)
        strcpy(resultat, "*E");
    else if(type == QUAD_E_DIVI)
        strcpy(resultat, "/E");
    else if(type == QUAD_E_MODU)
        strcpy(resultat, "%E");
    else if(type == QUAD_R_PLUS)
        strcpy(resultat, "+R");
    else if(type == QUAD_R_MOIN)
        strcpy(resultat, "-R");
    else if(type == QUAD_R_MULT)
        strcpy(resultat, "*R");
    else if(type == QUAD_R_DIVI)
        strcpy(resultat, "/R");
    else if(type == QUAD_E_EGAL)
        strcpy(resultat, "==E");
    else if(type == QUAD_E_DIFF)
        strcpy(resultat, "!=E");
    else if(type == QUAD_E_INFE)
        strcpy(resultat, "<E");
    else if(type == QUAD_E_INEG)
        strcpy(resultat, "<=E");
    else if(type == QUAD_E_SUPE)
        strcpy(resultat, ">E");
    else if(type == QUAD_E_SUEG)
        strcpy(resultat, ">=E");
    else if(type == QUAD_R_EGAL)
        strcpy(resultat, "==R");
    else if(type ==QUAD_R_DIFF)
         strcpy(resultat, "!=R");
    else if(type ==QUAD_R_INFE)
         strcpy(resultat, "<R");
    else if(type == QUAD_R_INEG)
        strcpy(resultat, "<=R");
    else if(type == QUAD_R_SUPE)
        strcpy(resultat, ">R");
    else if(type == QUAD_R_SUEG)
        strcpy(resultat, ">=R");

    return resultat;
}

/**
 * Exécute un quadruplet.
 * @param machine la machine
 * @param quad le quadruplet
 */
void quadruplet_execute(machine_t *machine, quadruplet_t *quad) {
    type_t valeur;
    
    machine->cpt++;
    switch(quad->type) {
        case QUAD_E_CONS:
        case QUAD_R_CONS:
            machine_setvaleur(machine, quad->ope3, quad->ope1);
            break;
        case QUAD_AFFE:
            machine_setvaleur(machine, quad->ope3, machine_getvaleur(machine, quad->ope1.entier));
            break;
        case QUAD_E_IN:
            printf("Saisir un entier : ");
            if(scanf("%d", &valeur.entier) != 1) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Erreur d'exécution : lecture d'un entier.\n");
                exit(EXIT_FAILURE);
            }
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_R_IN:
            printf("Saisir un réel : ");
            if(scanf("%f", &valeur.reel) != 1) {
                fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Erreur d'exécution : lecture d'un réel.\n");
                exit(EXIT_FAILURE);
            }
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_E_OUT:
            printf("Affichage : %d\n", machine_getvaleur(machine, quad->ope3).entier);
            break;
        case QUAD_R_OUT:
            printf("Affichage : %f\n", machine_getvaleur(machine, quad->ope3).reel);
            break;
        case QUAD_GOTO:
            machine->cpt = quad->ope3;
            break;
        case QUAD_GOTI:
            machine->cpt = machine_getvaleur(machine, quad->ope3).entier;
            break;
        case QUAD_EMPI:
            machine_empile(machine, machine_getvaleur(machine, quad->ope3));
            break;
        case QUAD_DEPI:
            machine_depile(machine, quad->ope3);
            break;
        case QUAD_E_CONV:
            valeur.entier = (int)machine_getvaleur(machine, quad->ope1.entier).reel;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_R_CONV:
            valeur.reel = (float)machine_getvaleur(machine, quad->ope1.entier).entier;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_E_PLUS:
            valeur.entier = machine_getvaleur(machine, quad->ope1.entier).entier + machine_getvaleur(machine, quad->ope2).entier;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_E_MOIN:
            valeur.entier = machine_getvaleur(machine, quad->ope1.entier).entier - machine_getvaleur(machine, quad->ope2).entier;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_E_MULT:
            valeur.entier = machine_getvaleur(machine, quad->ope1.entier).entier * machine_getvaleur(machine, quad->ope2).entier;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_E_DIVI:
            valeur.entier = machine_getvaleur(machine, quad->ope1.entier).entier / machine_getvaleur(machine, quad->ope2).entier;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_E_MODU:
            valeur.entier = machine_getvaleur(machine, quad->ope1.entier).entier % machine_getvaleur(machine, quad->ope2).entier;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_E_MOINU:
            valeur.entier = -machine_getvaleur(machine, quad->ope1.entier).entier;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_R_PLUS:
            valeur.reel = machine_getvaleur(machine, quad->ope1.entier).reel + machine_getvaleur(machine, quad->ope2).reel;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_R_MOIN:
            valeur.reel = machine_getvaleur(machine, quad->ope1.entier).reel - machine_getvaleur(machine, quad->ope2).reel;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_R_MULT:
            valeur.reel = machine_getvaleur(machine, quad->ope1.entier).reel * machine_getvaleur(machine, quad->ope2).reel;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_R_DIVI:
            valeur.reel = machine_getvaleur(machine, quad->ope1.entier).reel / machine_getvaleur(machine, quad->ope2).reel;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;
        case QUAD_R_MOINU:
            valeur.reel = -machine_getvaleur(machine, quad->ope1.entier).reel;
            machine_setvaleur(machine, quad->ope3, valeur);
            break;

        case QUAD_E_EGAL:
            if(machine_getvaleur(machine, quad->ope1.entier).entier == machine_getvaleur(machine, quad->ope2).entier)
                machine->cpt = quad->ope3;
            break;
        case QUAD_E_DIFF:
            if(machine_getvaleur(machine, quad->ope1.entier).entier != machine_getvaleur(machine, quad->ope2).entier)
                machine->cpt = quad->ope3;
            break;
        case QUAD_E_INFE:
            if(machine_getvaleur(machine, quad->ope1.entier).entier < machine_getvaleur(machine, quad->ope2).entier)
                machine->cpt = quad->ope3;
            break;
        case QUAD_E_INEG:
            if(machine_getvaleur(machine, quad->ope1.entier).entier <= machine_getvaleur(machine, quad->ope2).entier)
                machine->cpt = quad->ope3;
            break;
        case QUAD_E_SUPE:
            if(machine_getvaleur(machine, quad->ope1.entier).entier > machine_getvaleur(machine, quad->ope2).entier)
                machine->cpt = quad->ope3;
            break;
        case QUAD_E_SUEG:
            if(machine_getvaleur(machine, quad->ope1.entier).entier >= machine_getvaleur(machine, quad->ope2).entier)
                machine->cpt = quad->ope3;
            break;
            
        case QUAD_R_EGAL:
            if(machine_getvaleur(machine, quad->ope1.entier).reel == machine_getvaleur(machine, quad->ope2).reel)
                machine->cpt = quad->ope3;
            break;
        case QUAD_R_DIFF:
            if(machine_getvaleur(machine, quad->ope1.entier).reel != machine_getvaleur(machine, quad->ope2).reel)
                machine->cpt = quad->ope3;
            break;
        case QUAD_R_INFE:
            if(machine_getvaleur(machine, quad->ope1.entier).reel < machine_getvaleur(machine, quad->ope2).reel)
                machine->cpt = quad->ope3;
            break;
        case QUAD_R_INEG:
            if(machine_getvaleur(machine, quad->ope1.entier).reel <= machine_getvaleur(machine, quad->ope2).reel)
                machine->cpt = quad->ope3;
            break;
        case QUAD_R_SUPE:
            if(machine_getvaleur(machine, quad->ope1.entier).reel > machine_getvaleur(machine, quad->ope2).reel)
                machine->cpt = quad->ope3;
            break;
        case QUAD_R_SUEG:
            if(machine_getvaleur(machine, quad->ope1.entier).reel >= machine_getvaleur(machine, quad->ope2).reel)
                machine->cpt = quad->ope3;
            break;
    }
}