#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cellule.h"
#include "liste.h"
#include "table_hachage.h"

/**
 * initialise une table de hachage
 * @param tabh table de hachage
 * @param taille taille de la table
 */
void initialiser_table_hachage(table_de_hachage* tabh, int taille) {
	int i;
	
	tabh->taille = taille;

	tabh->liste = malloc(sizeof(liste) * tabh->taille);
	for (i = 0; i < tabh->taille; i++) {
		tabh->liste[i] = malloc(sizeof(liste));
	}
	printf("\033[0;32m*** Table de symbole initialisé ***\033[0;0m\n");
}

/**
 * affiche une table de hachage
 * @param tabh table de hachage
 */
void afficher_table_hachage(table_de_hachage* tabh) {
	int i;

	printf("\nTable de hachage :\n");
	for (i = 0; i < tabh->taille; i++) {
		printf("[%d]-> ", i);
		afficher_liste(tabh->liste[i]);
	}
	printf("\n");
}

/**
 * detruit une table de hachage
 * @param tabh table de hachage
 */
void detruire_table_hachage(table_de_hachage* tabh) {
	int i;

	for (i = 0; i < tabh->taille; i++) {
		detruire_liste(tabh->liste[i]);
	}
	free(tabh);
	printf("\033[0;32m*** Table de symbole detruite ***\033[0;0m\n");
}

/**
 * fonction utilisé pour le hachage
 * @param taille taille du mot
 * @return une valeur utilisé pour calculer le hachage
 */
unsigned long long puissance(int taille) {
	unsigned long long resultat = 1;
	int i;
	
	for (i = 0; i < taille; i++) {
		resultat *= 128;
	}
	
	return resultat;
}

/**
 * calcule la taille du mot
 * @param tmp le mot
 * @return une valeur utilisé pour calculer le hachage
 */
unsigned long long convertir_ch_entier(char* tmp) {
	unsigned long long k = 0;
	int i = 0, taille;

	while (tmp[i] != '\0') {
		i++;
	}
	
	taille = i - 1;
	i = 0;
	
	while (taille >= 0) {
		k += tmp[i++] * puissance(taille--);
	}

	return k;
}

/**
 * fonction de hachage
 * @param k le hachage
 * @param taille la taille de la table de hachage
 * @return emplacement dans la table de hachage
 */
int hachage(unsigned long long k, int taille) {
	return k % taille;
}

/**
 * insere un mot dans la table de hachage
 * @param tabh la table de hachage
 * @param c une cellule
 */
void inserer_hachage(table_de_hachage *tabh, cellule *c) {
	int emplacement;
	emplacement = hachage(convertir_ch_entier(c->variable), tabh->taille);
	inserer(tabh->liste[emplacement], c);
}

/**
 * cherche un mot dans la table de hachage
 * @param tabh la table de hachage
 * @param variable mot a rechercher
 * @return la cellule du mot
 */
cellule* rechercher_hachage(table_de_hachage *tabh, char* variable) {
	int emplacement;

	emplacement = hachage(convertir_ch_entier(variable), tabh->taille);
	
	return rechercher(tabh->liste[emplacement], variable);
}

/**
 * supprime un mot dans la table de hachage
 * @param tabh la table de hachage
 * @param mot mot a supprimer
 */
void supprimer_hachage(table_de_hachage *tabh, char* mot) {
	cellule* c = malloc(sizeof(cellule));
	int emplacement;

	emplacement = hachage(convertir_ch_entier(c->variable), tabh->taille);
	c = rechercher(tabh->liste[emplacement], mot);
	if (c != NULL)
		free(c);
}

/**
 * compte le nombre de mot dans la table
 * @param tabh la table de hachage
 * @return le nombre de mot
 */
int compter_table_hachage(table_de_hachage* tabh) {
	int i, compteur = 0;

	for (i = 0; i < tabh->taille; i++) {
		compteur += compter_liste(tabh->liste[i]);
	}
	
	return compteur;
}

/**
 * ajoute la valeur a une variable dans la table de hachage
 * @param tabh la table de hachage
 * @param variable mot a rechercher
 * @param valeur la valeur a inserer
 * @return la cellule du mot
 */
cellule* modifierCellule(table_de_hachage *tabh, char* variable, union valeur valeur){
	cellule* c;

	c = rechercher_hachage(tabh,variable);

	if (strcmp("entier", c->type) == 0){
		c->valeur.i = valeur.i;
	}
	else{
		c->valeur.f = valeur.f;
	}
	
	return c;
}

/**
 * donne le type a une varibale
 * @param tabh table de hachage
 * @param type le nouveau type des variables
 */
void donner_type_table_hachage(table_de_hachage* tabh, char* type) {
	int i;

	for (i = 0; i < tabh->taille; i++) {
		cellule *cellule = tabh->liste[i]->tete;
		while (cellule){
			if (strcmp(cellule->type, "null") == 0 && strcmp(cellule->typededonnee, "fonction") != 0 && strcmp(cellule->typededonnee, "procedure") != 0)
			{
				strcpy(cellule->type, type);
			}
			cellule = cellule->succ;
		}
	}
}