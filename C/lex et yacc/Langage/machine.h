#ifndef _MACHINE_
#define _MACHINE_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MEM_MAX 20
#define PILE_MAX 20

/* Définition d'une machine */
typedef struct {
    type_t mem[MEM_MAX];                /* Le contenu de la mémoire */
    unsigned int cpt;                   /* Le compteur ordinal */
    type_t pile[PILE_MAX];              /* Le contenu de la pile */
    unsigned int sommet;                /* Le sommet de la pile */
} machine_t;

/**
 * Crée une nouvelle machine.
 * @return la machine
 */
machine_t machine_creation();

/**
 * Récupère une valeur de la mémoire ou de la pile.
 * @param machine la machine
 * @param index l'index (positif = mémoire, négatif = pile)
 * @return la valeur
 */
type_t machine_getvaleur(machine_t *machine, int index);

/**
 * Modifie une valeur de la mémoire ou de la pile.
 * @param machine la machine
 * @param index l'index (positif = mémoire, négatif = pile) 
 * @param valeur la valeur
 */
void machine_setvaleur(machine_t *machine, int index, type_t valeur);

/**
 * Empile une valeur.
 * @param machine la machine
 * @param valeur la valeur
 */
void machine_empile(machine_t *machine, type_t valeur);

/**
 * Dépile des valeurs.
 * @param machine la machine
 * @param nb le nombre d'éléments à dépiler
 */
void machine_depile(machine_t *machine, int num);

#endif