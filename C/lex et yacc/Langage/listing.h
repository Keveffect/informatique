#ifndef _LISTING_
#define _LISTING_

typedef struct {
    quadruplet_t ins[LISTING_MAX];      /* Les quadruplets */
    int nb;                             /* Le nombre de quadruplets */
} listing_t;

/**
 * Lit un listing depuis un fichier texte.
 * @param nomFichier le nom du fichier texte
 * @return le listing
 */
listing_t listing_lecture(char *nomFichier);

/**
 * Affiche un listing.
 * @param listing le listing
 */
void listing_affiche(listing_t *listing);

/**
 * Exécute un listing.
 * @param listing le listing
 * @param trace si 'TRUE' affiche la trace
 */
void listing_execute(listing_t *listing, bool trace);

#endif