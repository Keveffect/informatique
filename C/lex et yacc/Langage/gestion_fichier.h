#ifndef GESTION_FICHIER
#define GESTION_FICHIER

#include <stdio.h>     /* Pour printf */
#include <string.h>    /* Pour strlen */
#include <fcntl.h>     /* Pour open */
#include <unistd.h>    /* Pour read */
#include <sys/stat.h>  /* Pour O_RDONLY */
#include <stdlib.h>    /* Pour exit, EXIT_SUCCESS, EXIT_FAILURE */
#include "liste.h"

/**
* Ouvre le fichier si il existe, sinon le creer
*/
void ouvertureFichierEntrant(FILE *yyin, char * fichier);

/**
* Ouvre le fichier si il existe, sinon le creer
*/
void ouvertureFichierSortant(FILE *yyout, char * fichier);

/**
* ferme le fichier
*/
void fermetureFichier(FILE * fichier);

/**
* ecrit une chaine de caractere saisie au clavier dans le fichier ouvert
*/
void ecritureFichier(liste_quadruplet* liste_quad, FILE *yyout, char * fichier);

#endif