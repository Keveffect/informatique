#ifndef _QUADRUPLET_
#define _QUADRUPLET_

#include "include.h"
#include "machine.h"

/* Taille maximale du texte d'un opérande */
#define TAILLE_MAX          16

/* Constantes pour les types des quadruplets */

/* Opérateurs génériques */
#define QUAD_UNDE             0     /* Erreur ou non défini */
#define QUAD_AFFE             1     /* Affectation */
#define QUAD_GOTO             2     /* Saut inconditionnel */
#define QUAD_GOTI             3     /* Saut inconditionnel indirect */
#define QUAD_EMPI             4     /* Empile un élément */
#define QUAD_DEPI             5     /* Dépile un élément */
#define QUAD_E_CONV           6     /* Conversion en entier */
#define QUAD_R_CONV           7     /* Conversion en réel */

/* Opérateurs sur les entiers */
#define QUAD_E_CONS         100     /* Affectation d'une constante entière */
#define QUAD_E_IN           101     /* Lecture d'un entier */
#define QUAD_E_OUT          102     /* Affichage d'un entier */
#define QUAD_E_PLUS         103     /* + entier */
#define QUAD_E_MOIN         104     /* - entier */
#define QUAD_E_MULT         105     /* * entier */
#define QUAD_E_DIVI         106     /* / entier */
#define QUAD_E_MODU         107     /* % entier */
#define QUAD_E_MOINU        108     /* - unaire entier */

/* Opérateurs sur les réels */
#define QUAD_R_CONS         120     /* Affectation d'une constante réelle */
#define QUAD_R_IN           121     /* Lecture d'un réel */
#define QUAD_R_OUT          122     /* Affichage d'un réel */
#define QUAD_R_PLUS         123     /* + réel */
#define QUAD_R_MOIN         124     /* - réel */
#define QUAD_R_MULT         125     /* * réel */
#define QUAD_R_DIVI         126     /* / réel */
#define QUAD_R_MOINU        127     /* - unaire réel */

/* Opérateurs relationels */
#define QUAD_E_EGAL         130     /* == entier */
#define QUAD_E_DIFF         131     /* != entier */
#define QUAD_E_INFE         132     /* < entier */
#define QUAD_E_INEG         133     /* <= entier */
#define QUAD_E_SUPE         134     /* > entier */
#define QUAD_E_SUEG         135     /* >= entier */
#define QUAD_R_EGAL         140     /* == réel */
#define QUAD_R_DIFF         141     /* != réel */
#define QUAD_R_INFE         142     /* < réel */
#define QUAD_R_INEG         143     /* <= réel */
#define QUAD_R_SUPE         144     /* > réel */
#define QUAD_R_SUEG         145     /* >= réel */

/* Structure représentant un quadruplet */
typedef struct {
    unsigned char type;     /* Le type */
    type_t ope1;            /* Opérande 1 */
    int ope2;               /* Opérande 2 */
    int ope3;               /* Opérande 3 */
} quadruplet_t;

/**
 * Convertit une chaîne de caractères en quadruplet.
 * @param buf la chaîne de caractères
 * @param numLigne le numéro de ligne actuel
 * @return le quadruplet
 */
quadruplet_t quadruplet_lecture(char *buf, int numLigne);

/**
 * Vérification d'un opérande.
 * @param buf la chaîne de caractères
 * @param numLigne le numéro de ligne actuel
 * @return le quadruplet
 */
void quadruplet_check(int valeur, int numLigne, int operande);

/**
 * Affiche un quadruplet sous forme d'une instruction à 3 adresses.
 * @param quad le quadruplet
 */
void quadruplet_affiche(quadruplet_t *quad);

/**
 * Convertit une chaîne de caractères en type de quadruplet.
 * @param str la chaîne de caractères
 * @return le type de quadruplet
 */
int quadruplet_str2type(char *str);

char* quadruplet_type2strstr(unsigned char type);

/**
 * Exécute un quadruplet.
 * @param machine la machine
 * @param quad le quadruplet
 */
void quadruplet_execute(machine_t *machine, quadruplet_t *quad);

#endif