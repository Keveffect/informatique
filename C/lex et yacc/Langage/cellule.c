#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "cellule.h"

/**
 * declare une variable avec son nom
 * @param c une cellule
 * @param variable nom de la variable
 * @param type type de la variable
 */
void declarerCellule(cellule *c, char* variable, char* type, int place){
    c->pred = NULL;
    c->succ = NULL;
    strcpy(c->type, type);
    strcpy(c->variable, variable);
    c->place = place;
}

/**
 * initialise une cellule avec une valeur
 * @param c une cellule
 * @param variable le nom de la variable
 * @param type le type de retour
 * @param typededonnee definit si il sagit d'une fonction
 */
void fonctionCellule(cellule *c, char* variable, char* type, char* typededonnee){
    c->pred = NULL;
    c->succ = NULL;
    strcpy(c->variable, variable);
    strcpy(c->type, type);
    strcpy(c->typededonnee, typededonnee);
}

/**
 * initialise une cellule avec une valeur
 * @param c une cellule
 * @param variable le nom de la variable
 * @param valeur la valeur a inserer dans la variable
 */
void initialiserCellule(cellule *c, char* variable, union valeur valeur){
    c->pred = NULL;
    c->succ = NULL;
    strcpy(c->variable, variable);
    c->valeur = valeur;
}

/**
 * alloue la memoire d'une cellule
 * @return emplacement memoire de la cellule
 */
cellule* creationCellule(){
    cellule *c = malloc(sizeof(cellule));
    return c;
}

/**
 * alloue la memoire d'un quadruplet
 * @return emplacement memoire du quadruplet
 */
cellule_quadruplet* creationQuadruplet(){
    cellule_quadruplet *q = malloc(sizeof(cellule_quadruplet));
    return q;
}

/**
 * initialise un quadruplet
 * @param cq une cellule de quadruplet
 * @param q un quadruplet
 */
void initialiserQuadruplet(cellule_quadruplet *cq, quadruplet_t* q){
    cq->pred = NULL;
    cq->succ = NULL;
    cq->quadruplet = q;
}