#ifndef LISTE
#define LISTE

#include "cellule.h"

typedef struct liste liste;
typedef struct liste_quadruplet liste_quadruplet;

struct liste{
    struct cellule *tete;
};

struct liste_quadruplet{
    struct cellule_quadruplet *tete;
};

/**
 * initialise la tete de liste a NULL
 * @param l la liste
 */
void initialiser_liste(liste*);

/**
 * initialise la tete de liste a NULL
 * @param l la liste de quadruplet
 */
void initialiser_liste_quadruplet(liste_quadruplet *l);

/**
 * vide la memoire de la liste
 * @param liste la liste
 */
void detruire_liste(liste*);

/**
 * vide la memoire de la liste
 * @param listye la liste
 */
void detruire_liste_quadruplet(liste_quadruplet *liste);

/**
 * insere une cellule dans la file
 * @param l la liste
 * @param c une cellule
 */
void inserer(liste*, cellule*);

/**
 * insere une cellule dans la file
 * @param l la liste
 * @param c une cellule de quadruplet
 */
void inserer_quadruplet(liste_quadruplet *l, cellule_quadruplet *c);

/**
 * affiche la file en commencant par la tete
 * @param liste une liste
 */
void afficher_liste(liste*);

/**
 * affiche la file en commencant par la tete
 * @param liste une liste de quadruplet
 */
void afficher_liste_quadruplet(liste_quadruplet *liste);

/**
 * affiche la file en commencant par la tete
 * @param liste une liste
 * @return le nombre de cellule
 */
int compter_liste(liste*);

/**
 * affiche la file en commencant par la tete
 * @param liste une liste
 * @return le nombre de quadruplet
 */
int compter_liste_quadruplet(liste_quadruplet* liste);

/**
 * cherche si une cellule fait partie de la lile
 * si oui on la retourne
 * sinon on retourne NULL puisqu'on a parcouru la liste entiere
 * @param l la liste
 * @param mot le mot qu'on recherche
 * @return la cellule recherché ou NULL
 */
cellule* rechercher(liste*,char*);

/**
 * cherche si une cellule fait partie de la lile
 * si oui on la retourne
 * sinon on retourne NULL puisqu'on a parcouru la liste entiere
 * @param l la liste
 * @param mot le mot qu'on recherche
 * @return la cellule recherché ou NULL
 */
//quadruplet* rechercher_quadruplet(liste_quadruplet *l,char* mot);

/**
 * supprime une cellule de la liste
 * si la liste ne contient pas de cellule on ne fait rien
 * @param l la liste
 * @param mot le mot a supprimer
 */
void supprimer(liste*, char*);

/**
 * supprime une cellule de la liste
 * si la liste ne contient pas de cellule on ne fait rien
 * @param l la liste
 * @param mot le mot a supprimer
 */
//void supprimer(liste_quadruplet *l, char* mot);

#endif