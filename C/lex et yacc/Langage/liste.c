#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "cellule.h"
#include "liste.h"
#include "quadruplet.h"

/**
 * initialise la tete de liste a NULL
 * @param l la liste
 */
void initialiser_liste(liste *l){
    l->tete = NULL;
}

/**
 * initialise la tete de liste a NULL
 * @param l la liste de quadruplet
 */
void initialiser_liste_quadruplet(liste_quadruplet *l){
    l->tete = NULL;
    printf("\033[0;32m*** Liste de quadruplet initialisé ***\033[0;0m\n");
}

/**
 * vide la memoire de la liste
 * @param liste la liste
 */
void detruire_liste(liste *liste){
    if (liste != NULL){
        while (liste->tete != NULL){
            cellule *aSupprimer = liste->tete;
            liste->tete = liste->tete->succ;
            free(aSupprimer);
        }
    }
    free(liste);
}

/**
 * vide la memoire de la liste
 * @param listye la liste
 */
void detruire_liste_quadruplet(liste_quadruplet *liste){
    if (liste != NULL){
        while (liste->tete != NULL){
            cellule_quadruplet *aSupprimer = liste->tete;
            liste->tete = liste->tete->succ;
            free(aSupprimer);
        }
    }
    free(liste);
	printf("\033[0;32m*** Liste de quadruplet detruite ***\033[0;0m\n");
}

/**
 * insere une cellule dans la file
 * @param l la liste
 * @param c une cellule
 */
void inserer(liste *l, cellule *c){
    c->succ = l->tete;
    if (l->tete != NULL){
        l->tete->pred = c;
    }
    l->tete = c;
    c->pred = NULL;
}

/**
 * insere une cellule dans la file
 * @param l la liste
 * @param c une cellule de quadruplet
 */
void inserer_quadruplet(liste_quadruplet *l, cellule_quadruplet *c){
    c->succ = l->tete;
    if (l->tete != NULL){
        l->tete->pred = c;
    }
    l->tete = c;
    c->pred = NULL;
}

/**
 * affiche la file en commencant par la tete
 * @param liste une liste
 */
void afficher_liste(liste *liste)
{
    cellule *cellule = liste->tete;
    printf("Debut->");
    while (cellule){
        if (strcmp("fonction", cellule->typededonnee)==0){
            printf("[%s, %s, %s]->", cellule->variable, cellule->typededonnee, cellule->type);
        }
        else if (strcmp("procedure", cellule->typededonnee)==0){
            printf("[%s, %s]->", cellule->variable, cellule->typededonnee);
        }
        else{
            if (strcmp("entier", cellule->type) == 0)
                printf("[%s, %d]->", cellule->variable, cellule->valeur.i);
            else 
                printf("[%s, %f]->", cellule->variable, cellule->valeur.f);
        }
        cellule = cellule->succ;
    }
    printf("Fin\n");
}

/**
 * affiche la file en commencant par la tete
 * @param liste une liste de quadruplet
 */
void afficher_liste_quadruplet(liste_quadruplet *liste)
{
    cellule_quadruplet *cellule = liste->tete;
    printf("liste de quadruplets : ");
    printf("Debut->");
    while (cellule){
        quadruplet_affiche(cellule->quadruplet);
        cellule = cellule->succ;
    }
    printf("Fin\n\n");
}

/**
 * affiche la file en commencant par la tete
 * @param liste une liste
 * @return le nombre de cellule
 */
int compter_liste(liste* liste)
{
    int compteur = 0;
    cellule* cellule = liste->tete;

    while (cellule) {
        compteur++;
        cellule = cellule->succ;
    }

    return compteur;
}

/**
 * affiche la file en commencant par la tete
 * @param liste une liste
 * @return le nombre de quadruplet
 */
int compter_liste_quadruplet(liste_quadruplet* liste)
{
    int compteur = 0;
    cellule_quadruplet* cellule = liste->tete;

    while (cellule) {
        compteur++;
        cellule = cellule->succ;
    }

    return compteur;
}

/**
 * cherche si une cellule fait partie de la lile
 * si oui on la retourne
 * sinon on retourne NULL puisqu'on a parcouru la liste entiere
 * @param l la liste
 * @param mot le mot qu'on recherche
 * @return la cellule recherché ou NULL
 */
cellule* rechercher(liste *l,char* mot){
    cellule *c = l->tete;
    while(c != NULL && strcmp(c->variable, mot)){
        c = c->succ;
    }
    return c;
}

/**
 * cherche si une cellule fait partie de la lile
 * si oui on la retourne
 * sinon on retourne NULL puisqu'on a parcouru la liste entiere
 * @param l la liste
 * @param mot le mot qu'on recherche
 * @return la cellule recherché ou NULL
 */
/*
cellule_quadruplet* rechercher_quadruplet(liste_quadruplet *l,char* mot){
    cellule_quadruplet *c = l->tete;
    while(c != NULL && strcmp(c->variable, mot)){
        c = c->succ;
    }
    return c;
}*/

/**
 * supprime une cellule de la liste
 * si la liste ne contient pas de cellule on ne fait rien
 * @param l la liste
 * @param mot le mot a supprimer
 */
void supprimer(liste *l, char* mot){
    cellule* c = l->tete;
    while (c != NULL && strcmp(c->variable, mot)) {
        c = c->succ;
    }
    if (c) {
        free(c);
        printf("Vous avez supprime %s\n", mot);
    }
}

/**
 * supprime une cellule de la liste
 * si la liste ne contient pas de cellule on ne fait rien
 * @param l la liste
 * @param mot le mot a supprimer
 */
/*
void supprimer(liste_quadruplet *l, char* mot){
    quadruplet* c = l->tete;
    while (c != NULL && strcmp(c->variable, mot)) {
        c = c->succ;
    }
    if (c) {
        free(c);
        printf("Vous avez supprime %s\n", mot);
    }
}*/