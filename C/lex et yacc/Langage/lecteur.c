#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "include.h"
#include "machine.h"
#include "quadruplet.h"
#include "listing.h"

int main(int argc, char *argv[]) {
    listing_t listing;
    bool affichelisting = FALSE, affichetrace = FALSE;
    int i;
    
    if(argc < 2) {
        fprintf(stderr, "\033[0;31mNombre d'arguments incorrect\033[0;0m\n");
        fprintf(stderr, "Usage: %s fichier [options]\n", argv[0]);
        fprintf(stderr, "Où : \n");
        fprintf(stderr, "\tfichier : le nom du fichier à ouvrir\n");
        fprintf(stderr, "\t[options] : des options parmis\n");
        fprintf(stderr, "\t\t-l : affiche le listing\n");
        fprintf(stderr, "\t\t-t : affiche la trace\n");
        exit(EXIT_FAILURE);
    }
    
    for(i = 2; i < argc; i++) {
        if(strcmp(argv[i], "-l") == 0)
            affichelisting = TRUE;
        else if(strcmp(argv[i], "-t") == 0)
            affichetrace = TRUE;
        else {
            fprintf(stderr, "\033[0;31m[ERREUR]\033[0;0m Option %d inconnue (%s).\n", i - 1, argv[i]);
            exit(EXIT_FAILURE);
        }
    }

    listing = listing_lecture(argv[1]);
    
    if(affichelisting) {
        printf("\033[0;32m*** Listing ***\033[0;0m\n");
        listing_affiche(&listing);
        printf("\n");
    }

    printf("\033[0;32m*** Execution ***\033[0;0m\n");
    listing_execute(&listing, affichetrace);
    printf("\n");
    
    printf("\033[0;32m*** Execution terminée ***\033[0;0m\n");

    return EXIT_SUCCESS;
}
