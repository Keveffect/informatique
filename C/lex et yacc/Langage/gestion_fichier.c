/**
 * Programme permettant de generer/ouvrir un fichier puis de le remplir
 * @author Samuel Bertin | Kevin Charlier
 **/

#include "gestion_fichier.h"

/**
* Ouvre le fichier si il existe, sinon le creer
*/
void ouvertureFichierSortant(FILE *yyout, char * fichier){
  if((yyout = fopen(fichier, "w")) == NULL){
        perror("\n\033[0;31m*** ERREUR ***\033[0;0m Erreur lors de l'ouverture du fichier contenant l'algorithme\n");
        exit(EXIT_FAILURE);
    }
    printf("\033[0;32m*** Fichier Ouvert ***\033[0;0m\n");
}

/**
* Ouvre le fichier si il existe, sinon le creer
*/
void ouvertureFichierEntrant(FILE *yyin, char * fichier){
  if((yyin = fopen(fichier, "r")) == NULL){
        perror("\n\033[0;31m*** ERREUR ***\033[0;0m Erreur lors de l'ouverture du fichier contenant l'algorithme\n");
        exit(EXIT_FAILURE);
    }
  printf("\033[0;32m*** Fichier Ouvert ***\033[0;0m\n");
}

/**
* ferme le fichier
*/
void fermetureFichier(FILE * fichier){
  if(fclose(fichier) == -1){
        perror("\n\033[0;31m*** ERREUR ***\033[0;0m Erreur lors de la fermeture du fichier\n");
        exit(EXIT_FAILURE);
    }
  printf("\033[0;32m*** Fichier Fermé ***\033[0;0m\n");
}

/**
* ecrit une chaine de caractere saisie au clavier dans le fichier ouvert
*/
void ecritureFichier(liste_quadruplet* liste_quad, FILE *yyout, char * fichier){
    int i, taille;
    char quad_out[32];
    
    if((yyout = fopen(fichier, "w")) == NULL){
        perror("\n\033[0;31m*** ERREUR ***\033[0;0m Erreur lors de l'ouverture du fichier contenant l'algorithme\n");
        exit(EXIT_FAILURE);
    }
    printf("\033[0;32m*** Fichier Ouvert ***\033[0;0m\n");
    
    cellule_quadruplet *c_quad = liste_quad->tete;

    while (c_quad->succ != NULL)
        c_quad = c_quad->succ;
    

    while(c_quad != NULL) {

        taille = sprintf(quad_out, "%s;", quadruplet_type2strstr(c_quad->quadruplet->type));
        if(c_quad->quadruplet->ope1.entier) 
            taille += sprintf(quad_out + taille, "%d;", c_quad->quadruplet->ope1.entier);
        else if(c_quad->quadruplet->ope1.reel)
            taille += sprintf(quad_out + taille, "%f;", c_quad->quadruplet->ope1.reel);
        else
           taille += sprintf(quad_out + taille, ";");
        if(c_quad->quadruplet->ope2 != -1)
            taille += sprintf(quad_out + taille, "%d", c_quad->quadruplet->ope2);
        taille += sprintf(quad_out     + taille, ";%d\n", c_quad->quadruplet->ope3);

        /* Ecriture de la chaine dans le fichier */
        if(fwrite(&quad_out, sizeof(char), taille, yyout) == -1) {
            perror("\n\033[0;31m*** ERREUR ***\033[0;0m Erreur write ");
            exit(EXIT_FAILURE);
        }
        c_quad = c_quad->pred;
    }
}