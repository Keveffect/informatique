#include "arrete.h"
#include "liste.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**
\ initialise une arrete avec ses valeurs
\param un int
\param deux int
\param poids int
\return arrete
*/
arrete* initialiser_arrete(int un, int deux, int poids){
    arrete *a = malloc(sizeof(arrete));
    a->origine = un;
    a->extremite = deux;
    a->poids = poids;

    return a;
}

/**
\ affiche le contenue de la table
\param tab arrete
\param taille int
*/
void afficher_tab(arrete** tab, int taille){
    int i;

    for(i=0; i<taille; i++){
        printf("[%d]- %d ->[%d]\n",tab[i]->origine, tab[i]->poids, tab[i]->extremite);
    }
}

/**
\ creer le tableau d'arrete
\param a arrete
\param g graphe
*/
void creer_tab_arrete(arrete** a, graphe* g) {
    int i, j, k;

    k = 0;
    for (i = 0; i < g->nbSommet; i++) {
        for (j = 0; j < g->nbSommet; j++) {
            if (g->m_adj[i][j] != 0) {
                a[k++] = initialiser_arrete(i, j, g->m_adj[i][j]);
            }
        }
    }
}

/**
\ creer un tableau d'arrete aleatoire
\param a arrete
\param nb int
*/
void creer_tab_arrete_aleatoire(arrete** a, int nb) {
    int i, un, deux, poids;
    
    srand(time(NULL));

    for (i = 0; i < nb; i++) {
        un=rand()%10000;
        do{
            deux=rand()%10000;
        } while (un == deux);
        poids=rand()%10000; 

        a[i] = initialiser_arrete(un,deux,poids);
    }
}
