#ifndef ARRETE
#define ARRETE

#include "graphe.h"

typedef struct arrete arrete;

struct arrete{
    int origine;
    int extremite;
    int poids;
};

arrete* initialiser_arrete(int, int, int);

void creer_tab_arrete(arrete**, graphe*);

void afficher_tab(arrete**, int);

#endif