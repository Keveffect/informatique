#include <stdio.h>

int main(){
    int nbr1, nbr2, pgcd, i;
  
    printf("Entrez un entier: ");
    scanf("%d", &nbr1);
    printf("Entrez un deuxieme entier: ");
    scanf("%d", &nbr2);
  
    for(i=1; i <= nbr1 && i <= nbr2; ++i){
        if(nbr1%i==0 && nbr2%i==0){
            pgcd = i;
        }
            
    }
  
    printf("PGCD de %d et %d = %d", nbr1, nbr2, pgcd);
    
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}