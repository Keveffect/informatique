#include <stdlib.h> 
#include <stdio.h>
#include "cellule.h"
#include "liste.h"


/**
\initialise la tete de liste a NULL
\param l liste
*/
void initialiser_liste(liste *l){
    l->tete = NULL;
}


/**
\ vide la memoire de la liste
\param liste liste
*/
void detruire_liste(liste *liste){
    if (liste != NULL){
        while (liste->tete != NULL){
            cellule *aSupprimer = liste->tete;
            liste->tete = liste->tete->succ;
            free(aSupprimer);
        }
    }
    free(liste);
}


/**
\ insere une cellule dans la liste
\param l liste
\param c cellule
*/
void inserer(liste *l, cellule *c){
    c->succ = l->tete;
    if (l->tete != NULL){
        l->tete->pred = c;
    }
    l->tete = c;
    c->pred = NULL;
}


/**
\ affiche la file en commencant par la tete
\param liste liste
*/
void afficher_liste(liste *liste)
{
    cellule *cellule = liste->tete;
    printf("Debut->");
    while (cellule){
        printf("[%d]->", cellule->id);
        cellule = cellule->succ;
    }
    printf("Fin\n");
}


/**
\ on affiche la liste avec les poids (pour le graphe)
\param liste liste
*/
void afficher_liste_poids(liste *liste)
{
    cellule *cellule = liste->tete;
    printf("Debut->");
    while (cellule){
        printf("%d->", cellule->id);
        cellule = cellule->succ;
        if (cellule) {
            printf("[%d]->", cellule->id);
            cellule = cellule->succ;
        }
    }
    printf("Fin\n");
}


/**
\ cherche si une cellule fait partie de la pile
\       si oui on la retourne
\       sinon on retourne NULL puisqu'on a parcouru la liste entiere
\param l liste
\param k int
\return cellule
*/
cellule rechercher(liste *l,int k){
    cellule *c = l->tete;
    while(c != NULL && c->id != k){
        c = c->succ;
    }
    return *c;
}


/**
\ supprime une cellule de la liste
\       si la liste ne contient pas de cellule on ne fait rien
\param l liste
\param i int
*/
void supprimer(liste *l, int i){
    cellule* aSupp = l->tete;
    cellule* courant = l->tete;

    if (l != NULL){
        if (l->tete->id == i) {
            l->tete = l->tete->succ;
            l->tete->pred = NULL;
        }
        else{
            while (courant != NULL || courant->id != i)
                courant = courant->succ;
            aSupp = courant;
            courant->pred->succ = courant->succ;
            courant->succ->pred = courant->pred;
        }
        free(aSupp);
    }
}

/**
\ compte le nombre de cellule dans la liste
\param liste liste
\return int
*/
int compter_liste(liste* liste)
{
    int compteur = 0;
    cellule* cellule = liste->tete;

    while (cellule) {
        compteur++;
        cellule = cellule->succ;
    }

    return compteur;
}