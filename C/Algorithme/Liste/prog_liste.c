#include <stdio.h>
#include <stdlib.h> 
#include "cellule.h"
#include "liste.h"

int main(){
    liste *l = malloc(sizeof(liste)); 
    cellule *c = malloc(sizeof(cellule));
    int nbSommet;
    int i;
    int sommet;
    
    i = 0;

    initialiser_liste(l);

    printf("Combien de cellules voulez vous ? ");
    scanf("%d",&nbSommet);

    if(nbSommet<0){
        nbSommet = -nbSommet;
    }

    printf("Vous avez choisi %d cellules(s)\n",nbSommet);
    for(i=0;i<nbSommet;i++){
        c = creationCellule();
        printf("Entrez votre id de cellule [%d] : ",i+1);
        scanf("%d",&sommet);
        initialiserCellule(c, sommet);
        inserer(l,c);
    }

    afficher_liste(l);

    printf("\nQuelle cellule voulez vous chercher ? \n");
    scanf("%d", &i);
    *c = rechercher(l, i);
    if (c)
        printf("Trouve !\n");
    else
        printf("Element introuvable\n");

    printf("\nQuelle cellule voulez vous supprimer ? \n");
    scanf("%d", &i);
    supprimer(l, i);
    afficher_liste(l);

    detruire_liste(l);

    return 0;
}