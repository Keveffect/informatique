#ifndef LISTE
#define LISTE

#include "cellule.h"

typedef struct liste liste;

struct liste{
    struct cellule *tete;
};

void initialiser_liste();

void detruire_liste(liste*);

void inserer(liste*, cellule*);

void afficher_liste(liste*);

void afficher_liste_poids(liste *);

cellule rechercher(liste*,int);

void supprimer(liste*,int);

int compter_liste(liste*);

#endif