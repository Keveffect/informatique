#include <stdio.h>
#include <stdlib.h>

typedef struct Cellule{
	int val ;
	struct Cellule *suiv;
}Cellule ;
	
typedef struct Liste{
	struct Cellule *tete ;
}Liste ;

Liste *initialiser(){
    Liste *l = malloc(sizeof(Liste*));
    Cellule *c = malloc(sizeof(Cellule*));

    if(l == NULL || c == NULL){
        printf("Erreur a l'initialisation\n");
        exit(0);
    }
    printf("Creation d'une liste avec succes\n");
    c->val = 0 ;
    c ->suiv = NULL;
    l->tete = c;

    return l;
}

void insertion(Liste *liste, int nb){
    /* Création d'une cellule */
    Cellule *nouveau = malloc(sizeof(Cellule*));
    
    if (liste == NULL || nouveau == NULL){
        printf("La liste est vide monsieur\n");
        exit(0);
    }
    
    printf("insertion d'un element\n");
    nouveau->val = nb;
    nouveau->suiv = liste->tete;
    liste->tete = nouveau;
    printf("insertion reussie\n\n");
}

void ajouterAuRang(Liste *l, int position, int nb){
    int taille, curseur;
    taille = curseur = 0;
    Cellule *precedente = malloc(sizeof(Cellule*));
    Cellule *actuel = malloc(sizeof(Cellule*));
    Cellule *suivante = malloc(sizeof(Cellule*));
    Cellule *size = malloc(sizeof(Cellule*));

    if(position == 0 || position == 1){
        insertion(l,nb);
    }
    else{
        suivante = l->tete;
        precedente = l->tete;
        actuel->suiv = NULL ;
        actuel->val = nb;

        size= l->tete;
        while (size != NULL){
            size = size->suiv; //on cherche la taille de la liste
            taille++;
        }

        printf("La liste contient %d element(s)\n",taille); 

        while(position > taille || position < 0){
            printf("Votre saisie est invalide, veuillez recommencer: ");
            scanf("%d",&position);
        }
        printf("Saisie correct, insertion de l'element en court...\n");

        for ( curseur ; curseur < position-1; curseur++){
            if(curseur){
                precedente =precedente->suiv;
            }
            suivante =suivante->suiv;
        }
        precedente->suiv = actuel;
        actuel->suiv = suivante;
        printf("Votre nombre a été inseré avec succes\n");
    }
}

void concatener(Liste *l1, Liste *l2){
    Cellule *c1 = malloc(sizeof(Cellule*));
    Cellule *cursor = malloc(sizeof(Cellule*));
    Cellule *c2 = malloc(sizeof(Cellule*));
    int compteur = 0;

    if (l1 == NULL || l2 == NULL){
        printf("Une liste est vide\n");
        exit(0);
    }

    c1 = l1->tete;
    cursor = l1->tete->suiv;
    c2 = l2->tete;
    
    while(c1->val < c2->val){ //si la premiere valeur de l1 est inferieur on place notre nombre en premier
        ajouterAuRang(l1,++compteur,c2->val); 
        c2 = c2->suiv;
    }   

    while(c2 != NULL){
        ++compteur;
        
        if(c1->suiv->val == 0 && c2 != NULL){
            while(c1->suiv->val == 0){
                ajouterAuRang(l1,compteur,c2->val);
                c2 = c2->suiv;
            }
            return ;
        }

        if(c1->val > c2->val && cursor->val <= c2->val){
            ajouterAuRang(l1,++compteur,c2->val); 
            c1 = c1->suiv;
            c2 = c2->suiv;
            cursor = cursor->suiv;
        }
        else{
            c1 = c1->suiv;
            cursor = cursor->suiv;
        }
        
    }
}

void afficherListe(Liste *liste){
    if (liste == NULL){
        printf("La liste est vide");
        exit(0);
    }

    Cellule *actuel = liste->tete;
    printf("Debut->");
    while (actuel != NULL){
        printf("[%d]->", actuel->val);
        actuel = actuel->suiv;
    }
    printf("Fin\n\n");
}

void suppression(Liste *liste){
    if (liste == NULL){
        exit(0);
    }

    if (liste->tete != NULL){
        Cellule *aSupprimer = liste->tete;
        liste->tete = liste->tete->suiv;
        free(aSupprimer);
    }
}

int main(){
    Liste *l1 = initialiser();
    Liste *l2 = initialiser();
    int choix,taille,i,val, position;


    printf("nombre de val dans la liste : ");
    scanf("%d",&taille);

    for ( i = 0; i < taille; i++){
        printf("entrez la valeur : ");
        scanf("%d",&val);
        insertion(l1,val);
    }

    printf("\nSelectionnez dans le menu :\n\t1. Afficher la liste\n\t2. Concatener listes triées\n\t3. Ajouter val au rand\n");
    scanf("%d",&choix);


    switch (choix)
    {
    case 1: afficherListe(l1);
        break;

    case 2: 
        printf("nombre de val dans la liste : ");
        scanf("%d",&taille);

        for ( i = 0; i < taille; i++){
            printf("entrez la valeur : ");
            scanf("%d",&val);
            insertion(l2,val);
        }
    
        afficherListe(l2);

        concatener(l1,l2);
        
        afficherListe(l1);
        
        suppression(l2);
        break;
    
    default:
        printf("Saisissez la valeur a inserer: ");
        scanf("%d",&val);
        printf("Saisissez la position a laquelle inserer [%d]: ",val);
        scanf("%d",&position);
        
        ajouterAuRang(l1,position,val);

        afficherListe(l1);
        break;
    }
    
    suppression(l1);
 
 	printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}