#include <stdio.h>

void echange(int *t1, int *t2) {
    int tmp;
    tmp = *t1;
    *t1 = *t2;
    *t2 = tmp;
}
void TriRapide(int tab[], int inf, int sup) {
    int pivot, i, j;
    if(inf < sup) {
        pivot = inf;
        i = inf;
        j = sup;
        while (i < j) {
            while(tab[i] <= tab[pivot] && i < sup)
                i++;
            while(tab[j] > tab[pivot])
                j--;
            if(i < j) 
                echange(&tab[i], &tab[j]);
        }
        echange(&tab[pivot], &tab[j]);
        TriRapide(tab, inf, j - 1);
        TriRapide(tab, j + 1, sup);
    }
}
int main(){
    int tab[100], nb, i;
  
    printf("\n Entrez le nombre de nombres :  ");
    scanf("%d", &nb);
    
    for(i = 0; i < nb; i++){
        printf("\n Entrer l'element n°%d: ",i+1);
        scanf("%d", &tab[i]);
    }

    TriRapide(tab, 0, nb - 1);
    
    printf("\n Tableau trié : ");
    for(i = 0; i < nb; i++)  {
        printf(" %d", tab[i]);
    }
    printf("\n");
    
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}