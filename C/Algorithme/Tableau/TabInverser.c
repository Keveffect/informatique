#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int i, tmp, n;
    int *p1, *p2;
    int *A;

    printf("Taille du tableau : ");
    scanf("%d",&n);

    A = malloc(sizeof(int)*n);

    for ( i = 0; i < n; i++){
        printf("Entrez une valeur : ");
        scanf("%d",&A[i]);
    }
    
    p1=A;
    p2=A+n-1;

    printf("\ntableau A : \n");
    for(p1=A; p1 < A+n; p1++){
        printf("%d ",*p1);
    }
    printf("\n\n");
    for(p1=A; p1 < p2; p1++){
        tmp=*p1;
        *p1=*p2;
        *p2=tmp;
        p2--;
    }
    printf("tableau A (inverser) :\n");
    for(p1=A; p1 < A+n; p1++){
        printf("%d ", *p1);
    }

    free(A); A = NULL;

	printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}
