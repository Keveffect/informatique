#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    int i, n, boolean;
    char tmp[30], *p1, *p2, *T;
    boolean = 1;
    i=0;
    printf("Saisir votre mot : ");
    scanf("%s", &tmp[0]);
    p1 = tmp;
    p2 = tmp + strlen(tmp)-1;
    for(p1; p1 < p2; p1++ , p2--){
        if(*p1 != *p2){
            boolean = 0;   
        }
    }
    if (boolean){
        printf("%s est un palindrome", tmp);
    }
    else{
        printf("%s n est pas un palindrome", tmp);
    }
  
  	printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}