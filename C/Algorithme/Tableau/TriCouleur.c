#include <stdio.h>

void swap(int *x, int *y){
    int w = *x;
    *x = *y;
    *y = w;
}

int main(){
    int i,b,w,r;    
    int tab[100], nb;
  
    printf("\n Entrez le nombre de nombres :  ");
    scanf("%d", &nb);
    
    for(i = 0; i < nb; i++){
        printf("\n Entrer l'element n°%d: ",i+1);
        scanf("%d", &tab[i]);
    }


    printf("Tableau non trié :\n");
    for(i = 0; i < nb; i++){
        printf("%d ", tab[i]);
    }   
    b=0;w=1,r=nb-1;
    while(w <= r){
        if (tab[w] == 2) {//blanc
            w++;
        }
        else if (tab[w] == 1){//bleu
            swap(&tab[b],&tab[w]);
            b++;
            w++;
        }
        else{// rouge
            swap(&tab[w],&tab[r]);
            r--;
        }
    }
    printf("\nTableau trié :\n");
    for(i = 0; i <nb; i++){
        printf("%d ", tab[i]);
    } 
  
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}