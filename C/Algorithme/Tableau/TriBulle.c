#include <stdio.h>

void swap(int *x, int *y){
    int w = *x;
    *x = *y;
    *y = w;
}

int main(int argc, char const *argv[])
{
    int x, i, j, taille=9, n=taille;
    int tab[100];
  
    printf("\n Entrez le nombre de nombres :  ");
    scanf("%d", &taille);
    
    for(i = 0; i < taille; i++){
        printf("\n Entrer l'element n°%d: ",i+1);
        scanf("%d", &tab[i]);
    }

    printf("Tableau non trié :\n");
    for(i = 0; i < taille; i++)
    {
        printf("%d ", tab[i]);
    }   
    while(taille>0){
        for(i=0; i<n; i++){
            if(tab[i]>tab[i+1]){
                swap(&tab[i], &tab[i+1]);
            }
        }
        --taille;
    }
    printf("Tableau trié :\n");
    for(i = 0; i < n; i++)
    {
        printf("%d ", tab[i]);
    }   
    
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}