#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int i, x, n;
    int *p1, *p2;
    int *A;

    printf("taille du tableau : ");
    scanf("%d",&n);

    A = malloc(sizeof(int)*n);

    for (i = 0; i < n; i++){
        printf("Entrez une valeur: ");
        scanf("%d",&A[i]);
    }

    p1=A;
    p2=A+n-1;
    
    printf("Quelle valeur chercher vous : ");
    scanf("%d", &x);
    
    // Affiche le tableau
    for(p1=A; p1<A+n; p1++){
        printf("%d",*p1);
    }
    printf("\n");
    // Tassement
    for(p1=p2=A; p1<A+n; p1++){
        *p2 = *p1;
        if(*p2 != x){
            p2++;
        }
    }
    // Affiche le tableau tasse
    n=p2-A;
    for(p1=A; p1<A+n; p1++){
        printf("%d",*p1);
    }

    free(A); A  = NULL;
	
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}