#include <stdio.h>
#include <stdlib.h>


int main() {
    int i, j, n, k; 
    int **A, **B, **C;

    printf("entrez la taille de vos matrice : ");
    scanf("%d",&n);

    A = (int**)malloc(sizeof(int*)*n);
    B = (int**)malloc(sizeof(int*)*n);
    C = (int**)malloc(sizeof(int*)*n);
    for(i=0; i<n; i++){
        A[i] = (int*)malloc(sizeof(int)*n);
        B[i] = (int*)malloc(sizeof(int)*n);
        C[i] = (int*)malloc(sizeof(int)*n);
    }
    
    //remplir des matrices avec des valeur binaires
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
            printf("A[%d][%d] : ", i, j);
            scanf("%d", &A[i][j]);     
        } 
    }
    printf("\n");
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           printf("B[%d][%d] : ", i, j);
           scanf("%d", &B[i][j]);
        } 
    }
    printf("\n");
    // affichage de A et B 
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           printf("A[%d][%d] = %d, ", i, j,A[i][j]);        
        } 
    }
    printf("\n");
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           printf("B[%d][%d] = %d, ", i, j,B[i][j]);
        } 
    }
    printf("\n");
    //res
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           C[i][j] = 0;
           for(k=0; k<n; k++){
                C[i][j] = C[i][j] + A[i][k] * B[k][j];
           }
       }
    }
   for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           printf("C[%d][%d] = %d, ", i, j,C[i][j]);        
        } 
    } 

    free(A); A = NULL;
    free(B); B = NULL;
    free(C); C = NULL;

    printf("\nAppuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}