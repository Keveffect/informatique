#include <stdio.h>
#include <stdlib.h>

int main(){
    int x,i,j,n;
    int **v;
    int b;//le boolean

    printf("selectionnez la taille de la matrice : ");
    scanf("%d",&n);//taille du tableau

    v = (int **)malloc(sizeof(int*)*n);//allocatin dynamique de la matrice
    
    for(i=0;i<n;i++){
        v[i] = (int *)malloc(sizeof(int)*n);//allocatin dynamique de la matrice
    }

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            printf("selectionnez la valeur a inserer en v[%d][%d]: ",i,j);
            scanf("%d",&v[i][j]);
        }
    }

    b=0;

    printf("selectionnez la valeur a chercher dans la matrice : ");
    scanf("%d",&x);//taille du tableau

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
           printf("v[%d][%d] = %d\n",i , j ,v[i][j]); //on affiche
        }
    }

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            if(x==v[i][j]){//on cherche la valeur
                b=1;
            }
        }
    }

    if(b){
        printf("trouvé !\n");
    }
    else{
        printf("%d inconnuu au bataillon",x);
    }
    
    free(v);
    v=NULL;

	printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}