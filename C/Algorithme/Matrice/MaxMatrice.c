#include <stdio.h>
#include <stdlib.h>


int main() {
    int i, j, n, tmp; 
    int **A, **B, **C;

    printf("entrez la taille de vos matrice: ");
    scanf("%d",&n);

    A = (int**)malloc(sizeof(int*)*n);
    B = (int**)malloc(sizeof(int*)*n);    //on cree 3 tableaux a 3 cases 
    C = (int**)malloc(sizeof(int*)*n);
    for(i=0; i<n; i++){
        A[i] = (int*)malloc(sizeof(int)*n);
        B[i] = (int*)malloc(sizeof(int)*n);  //chaque case de chaque tableau pointe vers 3 cases
        C[i] = (int*)malloc(sizeof(int)*n);
    }

    //remplir la matrice A avec es valeurs binaires
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           do{ 
               printf("A[%d][%d] : ", i, j);
               scanf("%d", &tmp);
           }while(tmp<0 || tmp>1);
           A[i][j] = tmp;
        } 
    }
    printf("\n");
//remplir la matrice B avec es valeurs binaires
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           do{
                printf("B[%d][%d] : ", i, j);
               scanf("%d", &tmp);
           }while(tmp<0 || tmp>1);
           B[i][j] = tmp;
        } 
    }
    printf("\n");
    // affichage de A
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           printf("A[%d][%d] = %d, ", i, j,A[i][j]);        
        } 
    }
    printf("\n");
    // affichage de B
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           printf("B[%d][%d] = %d, ", i, j,B[i][j]);
        } 
    }
    printf("\n");
    //on cree C
    for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
            if(A[i][j] == 0  && B[i][j] == 0)
                C[i][j] = 0;
            else
                C[i][j] = 1;
        } 
    }
   /*  Autre resolution possible 
   for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
            if(A[i][j] > B[i][j])
                C[i][j] = A[i][j];
            else
                C[i][j] = B[i][j];
        } 
    }
    */
    // affichage de C
   for(i=0 ; i < n ; i++){
       for(j=0 ; j < n; j++){
           printf("C[%d][%d] = %d, ", i, j,C[i][j]);        
        } 
    } 

    free(A); A = NULL;
    free(B); B = NULL;
    free(C); C = NULL;
    
	printf("\nAppuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}