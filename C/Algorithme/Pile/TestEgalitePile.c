#include<stdio.h>
#include<stdlib.h>
/****************************************/
/*              Structures              */
/****************************************/
typedef struct Element Element;
struct Element
{
    int nombre;
    Element *suivant;
};

typedef struct Pile Pile;
struct Pile
{
    Element *premier;
};

/****************************************/
/*              Methodes                */
/****************************************/
Pile *initialiser(){
    Pile *pile = malloc(sizeof(*pile));
    pile->premier = NULL;
}

// -------------EMPILER--------
void empiler(Pile *pile,int nvNombre)
{
    Element *nouveau = malloc(sizeof(*nouveau));
    if (pile == NULL || nouveau == NULL){
        printf("je suis videe...");
        exit(0);
    }
    nouveau->nombre = nvNombre;
    nouveau->suivant = pile->premier;
    pile->premier = nouveau;
}

// -------------ESTVIDE--------
int estVide(Pile *pile){
    if(pile != NULL && pile->premier != NULL)
        return 0;
    else
        return 1;
}

// -------------DEPILER--------
int depiler(Pile *pile){
    if (estVide(pile)){
        exit(0);
    }
    int nombreDepile = 0;
    Element *elementDepile = pile->premier;
    if (!estVide(pile)){
        nombreDepile = elementDepile->nombre;
        pile->premier = elementDepile->suivant;
        free(elementDepile);
    }
    return nombreDepile;
}

// -------------SOMMET--------
int sommetPile(Pile *p){
    for( p ; estVide(p) ; p++);
    return p->premier->nombre;
}

void afficherPile(Pile *pile){
    if (pile == NULL){
        printf("la pile est vide\n");
        exit(0);
    }
    printf("\n");
    Element *actuel = pile->premier;
    while (actuel != NULL){
        printf("%d\n", actuel->nombre);
        actuel = actuel->suivant;
    }
    printf("\n");
}

/****************************************/
/*                 Main                 */
/****************************************/
int main(){
    int val,taille,bool=1, i; //vrai
    Pile *p1 = initialiser();
    Pile *p2 = initialiser();
    Pile *p3 = initialiser();
    Pile *p4 = initialiser();

    printf("Nombre de valeur :");
    scanf("%d",&taille);

    printf("Pile 1 :\n");
    for (i = 0; i < taille; i++){
        printf("Entrez la valeur : ");
        scanf("%d",&val);
        empiler(p1,val);
    }
    
    afficherPile(p1);
    
    printf("Pile 2 :\n");
     for (i = 0; i < taille; i++){
        printf("Entrez la valeur : ");
        scanf("%d",&val);
        empiler(p2,val);
    }

    afficherPile(p2);
    
    while(!estVide(p1)){
        empiler(p3, sommetPile(p1));
        depiler(p1);
    }
    while(!estVide(p2) && !estVide(p3)){
        if(sommetPile(p2) != sommetPile(p3)){
            bool = 0;
        }
        empiler(p4, sommetPile(p2));
        depiler(p3);
        depiler(p2);
    }
    printf("inverse de Pile 1 :\n");
    afficherPile(p3);
    if(bool){
        printf("c'est la meme");
    }
    else{
        printf("c'est pas la meme");
    }
    free(p1);
    free(p2);
    free(p3);
    free(p4);
    
    
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}