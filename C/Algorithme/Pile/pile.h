#ifndef PILE
#define PILE

#include "file_t.h"

typedef struct pile pile;

struct pile{
    int sommet;
    sommet** tab;
    int taille;
};

void initialiser_pile(pile*,int);

void detruire_pile(pile*);

int pile_vide(pile*);

int pile_pleine(pile*);

void empiler(pile*,sommet*);

sommet* depiler(pile*);

void afficherPile(pile*);

#endif