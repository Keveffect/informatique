#include<stdio.h>
#include<stdlib.h>
/****************************************/
/*              Structures              */
/****************************************/
typedef struct Element Element;
struct Element
{
    int nombre;
    Element *suivant;
};

typedef struct Pile Pile;
struct Pile
{
    Element *premier;
};

/****************************************/
/*              Methodes                */
/****************************************/
Pile *initialiser(){
    Pile *pile = malloc(sizeof(*pile));
    pile->premier = NULL;
}

// -------------EMPILER--------
void empiler(Pile *pile,int nvNombre)
{
    Element *nouveau = malloc(sizeof(*nouveau));
    if (pile == NULL || nouveau == NULL){
        printf("je suis videe...");
        exit(0);
    }
    nouveau->nombre = nvNombre;
    nouveau->suivant = pile->premier;
    pile->premier = nouveau;
}

// -------------ESTVIDE--------
int estVide(Pile *pile){
    if(pile != NULL && pile->premier != NULL)
        return 0;
    else
        return 1;
}

// -------------DEPILER--------
int depiler(Pile *pile){
    if (estVide(pile)){
        exit(0);
    }
    int nombreDepile = 0;
    Element *elementDepile = pile->premier;
    if (!estVide(pile)){
        nombreDepile = elementDepile->nombre;
        pile->premier = elementDepile->suivant;
        free(elementDepile);
    }
    return nombreDepile;
}

// -------------SOMMET--------
int sommetPile(Pile *p){
    for( p ; estVide(p) ; p++);
    return p->premier->nombre;
}

void afficherPile(Pile *pile){
    if (pile == NULL){
        printf("la pile est vide\n");
        exit(0);
    }
    printf("\n");
    Element *actuel = pile->premier;
    while (actuel != NULL){
        printf("%d\n", actuel->nombre);
        actuel = actuel->suivant;
    }
    printf("\n");
}

/****************************************/
/*                 Main                 */
/****************************************/
int main(){
    Pile *p1 = initialiser();
    Pile *p2 = initialiser();
    int x,trouve,taille,i;

    trouve=0;

    printf("Entrez le nombre de valeur a inserer : ");
    scanf("%d",&taille);

    for (i = 0; i < taille; i++){
        printf("Entrez une valeur : ");
        scanf("%d",&x);
        empiler(p1,x);
    } 
    
    afficherPile(p1);
    
    printf("entrez la valeur recherché : ");
    scanf("%d",&x);

    while (!estVide(p1)){
        empiler(p2,sommetPile(p1));
        depiler(p1);
        if (sommetPile(p2)==x){printf("%d trouvé dans la pile\n",x);}
        else {printf("en cours de recherche...\n",x);}
    }
    while (!estVide(p2)){
        empiler(p1,sommetPile(p2));
        depiler(p2);
    }
    
    free(p1);
    free(p2);
  
  	printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}