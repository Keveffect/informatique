#include <stdio.h>
#include <stdlib.h> 
#include <limits.h>
#include "pile.h"
#include "file_t.h"

/**
\ initialise une pile par un tableau de taille donnée
\param p pile
\param taille int
*/
void initialiser_pile(pile *p, int taille){
    int i;
    p->taille = taille;
    p->tab = malloc(sizeof(sommet)*taille);
    for(i=0; i<taille; i++){
        p->tab[i] = malloc(sizeof(sommet));
    }
    p->sommet = -1;
}

/**
\ vide l'espace memoire de la pile
\param p pile
*/
void detruire_pile(pile* p){
    if(!pile_vide(p)){
        free(p->tab);
        free(p);
    }
}

/**
\ teste si la pile ne contient pas d'element
\param p pile
\return int
*/
int pile_vide(pile* p){
    return p->sommet == -1;
}

/**
\ teste si la pile est rempli
\param p pile
\return int
*/
int pile_pleine(pile *p){
    return p->sommet == p->taille;
}
 
/**
\ place un sommet au rang sommet de la pile
\param p pile
\param s sommet
*/
void empiler(pile* p, sommet*s){
    if(!pile_pleine(p)){
        p->sommet++; 
        p->tab[p->sommet] = s;
    }
}
 
/**
\ depile un element et le retourne
\param p pile
\return sommet
*/
sommet* depiler(pile* p){
    sommet *s = malloc(sizeof(sommet));

    if(!pile_vide(p)){
        s = p->tab[p->sommet];
        p->sommet--;    
    }
    return s;
}

/**
\ taffiche la pile du rang sommet au rang 0
\param p pile
*/
void afficherPile(pile* p){
    int i;
    printf("<-sommet");
    if(!pile_vide(p)){
        for(i = p->sommet; i>-1; i--){
            printf("<-[%d]",p->tab[i]->val);
        }
        printf("\n");
    }
}