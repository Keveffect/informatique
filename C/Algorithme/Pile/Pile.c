#include <stdio.h>
#include <stdlib.h>

/****************************************/
/*              Structures              */
/****************************************/
typedef struct Element Element;
struct Element {
    int nombre;
    Element *suivant;
};

typedef struct Pile{
    Element *premier;
}Pile;

/****************************************/
/*              Methodes                */
/****************************************/
// -------------INNITIALISER--------
Pile *initialiser(){
    Pile *pile = malloc(sizeof(*pile));
    pile->premier = NULL;
}

// -------------ESTVIDE--------
int estVide(Pile *pile){
    if(pile != NULL && pile->premier != NULL)
        return 0;
    else
        return 1;
}

// -------------EMPILER--------
void empiler(int nvNombre, Pile *pile)
{
    Element *nouveau = malloc(sizeof(*nouveau));
    if (pile == NULL || nouveau == NULL)
    {
        printf("je suis videe...");
        exit(EXIT_FAILURE);
    }

    nouveau->nombre = nvNombre;
    nouveau->suivant = pile->premier;
    pile->premier = nouveau;
}
// -------------DEPILER--------
int depiler(Pile *pile){
    if (estVide(pile)){

        exit(EXIT_FAILURE);
    }

    int nombreDepile = 0;
    Element *elementDepile = pile->premier;

    if (!estVide(pile)){
        nombreDepile = elementDepile->nombre;
        pile->premier = elementDepile->suivant;
        free(elementDepile);
    }

    return nombreDepile;
}

// -------------SOMMET--------
int sommetPile(Pile *p){
    for( p ; estVide(p) ; p++);
    return p->premier->nombre;
}
// -------------AFFICHERPILE--------
void afficherPile(Pile *pile)
{
    if (pile == NULL)
    {
        exit(EXIT_FAILURE);
    }
    Element *actuel = pile->premier;

    while (actuel != NULL)
    {
        printf("%d\n", actuel->nombre);
        actuel = actuel->suivant;
    }

    printf("\n");
}

/****************************************/
/*                 Main                 */
/****************************************/

int main(int argc, char const *argv[]){
    int i, val, n;
    Pile *p = initialiser();
   
    do{
        printf("entrez une valeur a inserer dans la pile: ");
        scanf("%d",&val);

        if(val==0){
            printf("Voulez vous :\n\t1-Inserer 0 dans la Pile\n\t2-Quitter la programme\n");
            scanf("%d",&n);
            switch(n){
                case 1: empiler(0,p);
                        val = 1; //pour ne pas quitter la boucle
                        break;
                default: printf("Fin des insertions\n");
            }
        }
        else{
            empiler(val,p);
        }
        
    }while (val != 0);
    
    afficherPile(p);

    free(p);
    
    
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}