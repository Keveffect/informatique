#include <stdio.h>
#include <stdlib.h>

/****************************************/
/*              		Structures             	        */
/****************************************/
typedef struct Element Element;
struct Element{
    int nombre;
    Element *suivant;
};

typedef struct Pile{
    Element *premier;
}Pile;

/****************************************/
/*              		Methodes                	        */
/****************************************/
// -------------INNITIALISER--------
Pile *initialiser(){
    Pile *pile = malloc(sizeof(*pile));
    pile->premier = NULL;
}

// -------------ESTVIDE--------
int estVide(Pile *pile){
    if(pile != NULL && pile->premier != NULL)
        return 0;
    else
        return 1;
}

// -------------EMPILER--------
void empiler(Pile *pile, int nvNombre)
{
    Element *nouveau = malloc(sizeof(*nouveau));
    if (pile == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }

    nouveau->nombre = nvNombre;
    nouveau->suivant = pile->premier;
    pile->premier = nouveau;
}

// -------------DEPILER--------
int depiler(Pile *pile)
{
    if (pile == NULL)
    {
        exit(EXIT_FAILURE);
    }

    int nombreDepile = 0;
    Element *elementDepile = pile->premier;

    if (pile != NULL && pile->premier != NULL)
    {
        nombreDepile = elementDepile->nombre;
        pile->premier = elementDepile->suivant;
        free(elementDepile);
    }

    return nombreDepile;
}
// -------------SOMMET--------
int sommetPile(Pile *p){
    for( p ; estVide(p) ; p++);
    return p->premier->nombre;
}
// -------------AFFICHERPILE--------
void afficherPile(Pile *pile)
{
    if (pile == NULL)
    {
        exit(EXIT_FAILURE);
    }
    Element *actuel = pile->premier;

    while (actuel != NULL)
    {
        printf("%d\n", actuel->nombre);
        actuel = actuel->suivant;
    }

    printf("\n");
}

/****************************************/
/*                		 Main                 	        */
/****************************************/
int main(){
    int min;
    Pile *p1 = initialiser();
    Pile *p2 = initialiser();
    Pile *p3 = initialiser();
    
    empiler(p1,3);
    empiler(p1,2);
    empiler(p1,5);
    empiler(p1,4);
    empiler(p1,1);   

    while(estVide(p1) || estVide(p2)){
        min = sommetPile(p1);
        
        while(estVide(p1)){
            if(sommetPile(p1) < min){
                min = sommetPile(p1);
            }
            empiler(p2, sommetPile(p1));
            depiler(p1);
        }
        while(estVide(p2)){
            if(sommetPile(p2) == min){
                empiler(p3, sommetPile(p2));
                if(estVide(p2)){
                    depiler(p2);
                }
            }
            if(estVide(p2)){
                empiler(p1, sommetPile(p2));
                depiler(p2);
            }    
        }
    }

    printf("\np1 : ");
    afficherPile(p1);
    printf("\np2 : ");
    afficherPile(p2);
    printf("\np3 : ");
    afficherPile(p3);
    free(p1) ;
    free(p2) ;
    free(p3) ;
    
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}