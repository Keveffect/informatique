#include <stdlib.h>
#include <stdio.h>
#include "file_t.h"
#include "pile.h"

int main(){
    int nbSommet;
    int i;
    int sommets;
    pile* p = malloc(sizeof(pile));
    sommet* s = NULL;

    i = 0;

    printf("Combien de sommets voulez vous dans votre pile ? ");
    scanf("%d", &nbSommet);

    if (nbSommet < 0) {
        nbSommet = -nbSommet;
    }

    initialiser_pile(p, nbSommet);

    printf("Vous avez choisi %d sommet(s)\n", nbSommet);
    for (i = 0;i < nbSommet;i++) {
        s = malloc(sizeof(sommet));
        initialiser_sommet(s);
        printf("Entrez votre sommet [%d] : ", i+1);
        scanf("%d", &sommets);
        s->val = sommets;
        empiler(p, s);
    }

    afficherPile(p);

    depiler(p);

    afficherPile(p);

    detruire_pile(p);

    free(s);

    return 0;
}