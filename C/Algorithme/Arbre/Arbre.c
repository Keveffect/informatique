#include <stdio.h>
#include <stdlib.h>

typedef struct noeud arbre;
////////////////////////STRUCT ARBRE/////////////////////:
struct noeud{
    int val ;
	arbre *fg;
	arbre *fd ;
	arbre *p ;
};

/********************************************/
/*************CREER ARBRE********************/
/********************************************/
arbre* CreerArbre(int val,arbre *ls, arbre *rs){
	arbre* X = NULL;

	X = malloc(sizeof(arbre*)*4);

	if( X == NULL ){
		printf("Impossible d'allouer le noeud");
		exit(0);
	}

	X->val = val;
	X->fg = ls;
	X->fd = rs;

	return X;
}

/********************************************/
/*******************EST VIDE*****************/
/********************************************/
int estVide( arbre *T){
	return T == NULL;
}

/********************************************/
/****************Ajouter Noeud***************/
/********************************************/
void ajouterNoeud(arbre *src, int nouveau){
	if ( src == NULL ){
		src = CreerArbre(nouveau,NULL,NULL);
	}
	else if ( estVide(src->fg) ){
		src->fg = CreerArbre(nouveau,NULL,NULL);
	}
	else if ( estVide(src->fd) ){
		src->fd = CreerArbre(nouveau,NULL,NULL);
	}
	else{
		ajouterNoeud(src->fg,nouveau);
	}
}

/********************************************/
/********************POSTFIXE****************/
/********************************************/
void Parcourspostfixe(arbre *racine){
    if( !estVide(racine)){
        Parcourspostfixe(racine->fg) ; 
		Parcourspostfixe(racine -> fd) ;
		printf("%d ",racine -> val) ;
	} 
}

/********************************************/
/********************INFIXE******************/
/********************************************/
void Parcoursinfixe(arbre *racine){
    if( !estVide(racine)){
        Parcoursinfixe(racine -> fg) ; 
        printf("%d ",racine -> val) ;
        Parcoursinfixe(racine -> fd) ;
	} 
}

/********************************************/
/********************PREFIXE******************/
/********************************************/
void Parcoursprefixe(arbre *racine){
if( !estVide(racine)){
printf("%d",racine->val) ;
Parcoursprefixe(racine -> fg) ; 
		Parcoursprefixe(racine -> fd) ;
	} 
}

/********************************************/
/********************TAILLE******************/
/********************************************/
int taille(arbre *a) {
	if( a == NULL) 
		return 0 ; 
	else
		return ( 1 + taille( a->fd ) + taille ( a->fg ) ) ; 
}

/********************************************/
/********************HAUTEUR******************/
/********************************************/
int max(int a, int b){
    return (a>b) ? a : b;
}

int hauteurArbre(arbre *a){
    if(estVide(a))    
        return 0;
    else
        return 1 + max(hauteurArbre(a->fg),hauteurArbre(a->fd));    
}

/********************************************/
/**************EST UNE FEUILLE***************/
/********************************************/
int estUneFeuille (arbre *T){
	if (estVide(T))
		return 0;
	else if (estVide(T->fg) && estVide(T->fd)) 
		return 1;
	else
		return 0;
}

/********************************************/
/*********EST UN NOEUD INTERNE***************/
/********************************************/
int estNoeudInterne (arbre *T){
	return ! estUneFeuille(T) ; // si ce n’est pas une feuille on retourne vrai
}

/********************************************/
/********************RECHERCHE***************/
/********************************************/
int recherche(arbre *src , int element){
	if ( estVide(src) )
		return 0;
	else if ( src->val == element )
		return 1;
	else if ( src->val > element )
		return recherche(src->fg, element);
	else
		return recherche(src->fd, element);
}

/********************************************/
/********************SUPPRIMER***************/
/********************************************/
void supprime(arbre *src){
	if( ! estVide(src) ){
		supprime( src -> fg );
		supprime(src ->fd);

		free( src );
		src = NULL;
	}
}

void afficherArbre(arbre **arbre, unsigned int nb){
        unsigned int i;
        if(*arbre == NULL){
            for(i=0; i< nb; i++) printf("\t");
            printf("*\n");
            return;
        }
        afficherArbre(&(*arbre)->fd, nb+1);
        for(i=0 ; i < nb; i++)
        {   printf("\t");   }
        printf("[%d]\n", (*arbre)->val);
        afficherArbre(&(*arbre)->fg, nb+1);
    }

/********************************************/
/*                  MAIN                    */
/********************************************/
int main(){
	int i,taille,val,choix;
    arbre *droite = NULL;
    arbre *gauche = NULL;

	printf("combien de val voulez vous ?\n");
	scanf("%d",&taille);
    
	printf("entrez une val: ");
	scanf("%d",&val);

    arbre *A = CreerArbre(val, gauche, droite);

	for ( i = 0; i < taille-1; i++)
	{
		printf("entrez une val: ");
		scanf("%d",&val);
		ajouterNoeud(A, val);
	}
	
	printf("1) Recherche\n\t2)Parcours prefixe\n\t3)Parcour infixe\n\t4) Parcours postfixe\n");
	scanf("%d",&choix);

	switch (choix)
	{
	case 1:
		printf("quelle val voulez vous chercher ?\n");
		scanf("%d",&val);
		recherche(A,val);
		break;
	case 2:	Parcoursprefixe(A);
			printf("\n");
		break;
	case 3:	Parcoursinfixe(A);
			printf("\n");
		break;
	case 4:	Parcourspostfixe(A);
    		printf("\n");
		break;
	default:	
		break;
	}

    supprime(A);

    printf("\nAppuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}