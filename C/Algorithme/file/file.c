#include <stdio.h>
#include <stdlib.h> 
#include <limits.h>
#include "file_t.h"
 
/**
\ cree un sommet en initialisant tous ses attributs a 0
\param s sommet
*/
void initialiser_sommet(sommet *s){
    s->couleur = 0;
    s->dateDebut=0;
    s->dateFin=0;
    s->val=0;
    s->pere = NULL;
}

/**
\ créé une file par tableau avec sa taille passée en parametre
\param f file
\param taille int
*/
void initialiser_file(file_t *f, int taille){
    int i;
    f->taille = taille;
    f->tab = malloc(sizeof(sommet)*taille);
    for(i =0; i<taille; i++){
        f->tab[i] = malloc(sizeof(sommet));
    }
    f->tete = 0;
    f->queue = 0; 
}

/**
\ vide la file de l'espace memoire
\param f file
*/
void detruire_file(file_t* f){
    if(!file_vide(f)){
        free(f->tab);
        free(f);
    }    
}


/**
\ teste si la file ne contient pas d'element
\param f file
\return int
*/
int file_vide(file_t* f){
    return f->tete == f->queue;
}
 
/**
\ teste si la file est complette
\param f file
*/
int file_pleine(file_t *f){
    int etat_file;
    if(f->tete == 0)
        etat_file = f->queue == f->taille-1;
    else 
        etat_file = f->queue == f->tete-1;
    
    return etat_file;
}


/**
\ enfile un sommet dans la file
\param f file
\param s sommet
*/
void enfiler(file_t* f, sommet *s){
    if(!file_pleine(f)){
       f->tab[f->queue] = s;
        if(f->queue == f->taille-1)
            f->queue = 0;
        else
            f->queue++; 
    }
}


/**
\ defile un sommet de la file
\param f file
\return sommet
*/
sommet* defiler(file_t* f){
    sommet* s = malloc(sizeof(sommet));

    if(!file_vide(f)){
        s = f->tab[f->tete];
        if(f->tete == f->taille-1)
            f->tete = 0;
        else
            f->tete++;    
    }
    return s;
}
 
/**
\ affiche la file en commencant par la tete
\param f file
*/
void afficherFile(file_t* f){
    int i = f->tete;
    printf("<-tete");
    while(i != f->queue){
        if(i == f->taille){
            printf("<-[%d]",f->tab[i]->val);
            i = 0;
        }else{
            printf("<-[%d]",f->tab[i]->val);
            i++;  
        }
    } 
    printf("<-queue\n");
}