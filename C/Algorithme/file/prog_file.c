#include <stdio.h>
#include <stdlib.h>
#include "file_t.h"

int main(){
    int nbSommet;
    int i;
    int sommets;
    file_t* f = malloc(sizeof(file_t));
    sommet *s = NULL;

    i = 0;

    printf("Combien de sommets voulez vous dans votre file ? ");
    scanf("%d", &nbSommet);

    if (nbSommet < 0) {
        nbSommet = -nbSommet;
    }

    initialiser_file(f,nbSommet);

    printf("Vous avez choisi %d sommet(s)\n", nbSommet);
    for (i = 0;i < nbSommet;i++) {
        s = malloc(sizeof(sommet));
        initialiser_sommet(s);
        printf("Entrez votre sommet [%d] : ", i+1);
        scanf("%d", &sommets);
        s->val = sommets;
        enfiler(f,s);
    }

    afficherFile(f);

    defiler(f);

    afficherFile(f);

    detruire_file(f);

    afficherFile(f);

    free(s);

    return 0;
}