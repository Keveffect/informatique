#ifndef FILE_T
#define FILE_T

typedef struct sommet sommet;
typedef struct file_t file_t;

struct sommet{
    int couleur;
    int dateDebut;
    int dateFin;
    int val;
    sommet *pere;
};

struct file_t{
    int tete;
    int queue;
    sommet** tab;
    int taille;
};

void initialiser_sommet(sommet*);

void initialiser_file(file_t*,int);

void detruire_file(file_t*);

int file_vide(file_t*);

int file_pleine(file_t*);

void enfiler(file_t*,sommet*);

sommet* defiler(file_t*);

void afficherFile(file_t*);

#endif