#include <stdio.h>
#include <stdlib.h> 
#include "cellule.h"

/**
\initialise une cellule avec une valeur
\param nb int
\param c cellule
*/
void initialiserCellule(cellule *c, int nb){
    c->pred = NULL;
    c->succ = NULL;
    c->id = nb;
}

/**
\alloue l'alocation memoire d'une cellule
\return cellule
*/
cellule* creationCellule(){
    cellule *c = malloc(sizeof(cellule));
    return c;
}