#ifndef CELLULE
#define CELLULE

typedef struct cellule cellule;

struct cellule{
    cellule *pred;
    cellule *succ;
    int id;
};

void initialiserCellule(cellule *c, int nb);

cellule* creationCellule();

#endif




