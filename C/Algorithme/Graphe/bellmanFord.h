#ifndef BELLMANFORD
#define BELLMANFORD

#include "graphe.h"
#include "arrete.h"

typedef struct donnees donnees;

struct donnees {
    int* distance;
    int* pere;
};

void source_unique_initialisation(graphe*, int, donnees*);

void relacher(int, int, int, donnees*);

int bellman_ford(graphe*, int, arrete**, donnees*);

#endif