#include "cellule.h"
#include "liste.h"
#include "graphe.h"
#include "file_t.h"
#include "arrete.h"
#include "tas.h"
#include "tri.h"
#include "kruskal.h"
#include "ensemble.h"
#include "prim.h"
#include "bellmanFord.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    int choix;
    graphe* g = malloc(sizeof(graphe));
    arrete** tabArrete = NULL;
    donnees* donnees = malloc(sizeof(donnees));
    FILE* fichier;


    //////////////////////////     BELLMAN FORD      ////////////////////////////////

    printf("Choisissez le graphe a etudier :\n\t1.graphe6.txt\n\t2.graphe_3353.txt\n\t2.graphe_10000.txt\n\t2.graphe_264346.txt\n");
	scanf("%d", &choix);

	switch (choix)
	{
		case 1 : fichier = fopen("../TP3/graphe5.txt", "r");
			break;
        case 2 : fichier = fopen("graphe_3353.txt", "r");
			break;
        case 3 : fichier = fopen("graphe_10000.txt", "r");
			break;
        default : fichier = fopen("graphe_264346.txt", "r");
			break;
	}

    initialiser_graphe(fichier, g);

    tabArrete = malloc(sizeof(arrete*)*g->nbArrete);

    afficher_graphe(g);

    printf("Nombre d'arc dans le graphe : %d\n\n", g->nbArrete);

    do{
        printf("Choisissez le sommet par lequel demarrer l'algorithme de bellman ford :");
        scanf("%d",&choix);
    }while(choix < 0 || choix > g->nbSommet-1);
    

    if (g->value == 1) {

        creer_tab_arrete(tabArrete, g);

        afficher_tab(tabArrete, g->nbArrete);
        printf("\n");

        bellman_ford(g, choix, tabArrete, donnees); 
    }

    detruire_graphe(g);

    return 0;
}