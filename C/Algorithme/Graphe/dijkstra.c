#include "graphe.h"
#include "dijkstra.h"
#include <stdlib.h>
#include <limits.h>

/**
\ initialise les sommet a l'infini
\param g graphe
\param sommet int
\param donneesdij donneesdij
*/
void source_unique_initialisation_dij(graphe*g, int sommet, donneesdij* donneesDij) {
	int i, max;
	
	donneesDij->distance = malloc(sizeof(int)*g->nbSommet);
	donneesDij->pere = malloc(sizeof(int) * g->nbSommet);
	donneesDij->visite = malloc(sizeof(int) * g->nbSommet);

	max = INT_MAX / 2; // pour eviter le depassement lors du relachement

	for ( i = 0; i < g->nbSommet; i++)
	{
		donneesDij->distance[i] = max;
	}
	donneesDij->distance[sommet] = 0;

	for ( i = 0; i < g->nbSommet; i++)
	{
		donneesDij->pere[i] = -1;
	}

	for ( i = 0; i < g->nbSommet; i++)
	{
		donneesDij->visite[i] = 0;
	}
}

int min_dij(int *tabDistance, int *tabCouvert, int nbSommet) {
	int i = 0, min = 0;

	while (tabCouvert[i++]) {
		min++;
	}
	for (i = 0; i < nbSommet; i++) {
		if (tabDistance[i] < tabDistance[min] && tabCouvert[i] == 0) {
			min = i;
		}
	}
	return min;
}

/**
\ relache un sommet
\param u int
\param v int
\param poids int
\param donneesdij donneesdij
*/
void relacher_dij(int u, int v, int poids, donneesdij* donneesdij) {
	if (donneesdij->distance[v] > donneesdij->distance[u] + poids) {
		donneesdij->distance[v] = donneesdij->distance[u] + poids;
		donneesdij->pere[v] = u;
		donneesdij->visite[v] = 1;
	}
}

/**
\ dijkstra
\param g graphe
\param sommet int
\param tabArrete arrete
\param donneesdij donneesdij
*/
void dijkstra(graphe*g, int sommet, arrete** tabArrete, donneesdij* donneesdij){
	int u,i;
	cellule* c = malloc(sizeof(cellule));
	cellule* c2 = malloc(sizeof(cellule));

	source_unique_initialisation_dij(g, sommet, donneesdij);

	for (i = 0; i < g->nbSommet; i++) {

		u = min_dij(donneesdij->distance, donneesdij->visite, g->nbSommet);
		c = g->l_adj[u].tete->succ;
		c2 = g->l_adj[u].tete; //poids de c
		donneesdij->visite[u] = 1;

		while (c !=NULL) {
			if (!donneesdij->visite[c->id] && c2->id < donneesdij->distance[c->id]) {
				donneesdij->pere[c->id] = u;
				donneesdij->distance[c->id] = c2->id;
			}
			c = c->succ;
			relacher_dij(u, c->id, donneesdij->distance[u], donneesdij);
		}
	}

	for(i=0; i<g->nbSommet; i++)
        printf("poids du sommet %d -> %d\n", i, donneesdij->distance[i]);
}