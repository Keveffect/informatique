#ifndef PRIM
#define PRIM

#include "arrete.h"
#include "liste.h"

int min(int*, int*, int);

void genere_acpm_prim_tableau(liste*, int, int);

void afficher_acpm(int*, int*, int*, int);

#endif