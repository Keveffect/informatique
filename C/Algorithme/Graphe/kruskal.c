#include "../TP3/arrete.h"
#include "../TP1/liste.h"
#include "ensemble.h"
#include <stdio.h>
#include <stdlib.h>

/**
\ initialise une arrete avec ses valeurs
\param a arrete
\param taile int
*/
void afficher_acmp(arrete**a, int taille) {
	int i, poids;

	printf("\nKRUSKAL :\n");
	afficher_tab(a, taille);

	poids = 0;

	for (i = 0; i < taille; i++) {
		poids += a[i]->poids;
	}

	printf("\nPoids de l'arbre couvrant minimal : %d\n", poids);
}

/**
\ realise kruskal en tableau
\param nbSommet int
\param nbArrete int
\param a arrete
*/
void genere_acpm_kruskal_tableau(int nbSommet, int nbArrete,arrete** a) {
	int i, j, k, icc;
	int* cc = malloc(sizeof(int)*nbSommet);
	arrete **ensemble = malloc(sizeof(arrete*)*nbSommet-1);

	icc = 0;
	k = 0;

	for (i = 0; i <nbSommet; i++) {
		cc[i] = i;
	}

	for (i = 0; i <nbSommet-1 ; i++) {
		ensemble[i] = malloc(sizeof(arrete));
	}

	for (i = 0; i < nbArrete; i++) {
		if (cc[a[i]->origine] != cc[a[i]->extremite]) {

			ensemble[k++] = a[i];
			icc = cc[a[i]->extremite];
			for (j = 0; j < nbSommet; j++) {
				if(cc[j] == icc) {
					cc[j] = cc[a[i]->origine];
				}
			}
		}
	}
	
	afficher_acmp(ensemble, nbSommet-1);
}

/**
\ affiche le resultat de kruskal
\param nbSommet int
\param nbArrete int
\param a arrete
*/
void generer_acpm_kruskal_ensembles(int nbSommet, int nbArrete, arrete** a){
	int i;
	ensemble *ensemble = malloc(sizeof(ensemble));
	ensemble->liste = malloc(sizeof(liste) *nbSommet);

	for (i = 0; i < nbSommet; i++) {
		initialiser_liste(&ensemble->liste[i]);
	}

	for (i = 0; i < nbSommet; i++) {
		ensemble->liste[i] = creer_ensemble(i);
	}

	for (i = 0; i < nbSommet; i++) {
		afficher_liste(&ensemble->liste[i]);
	}
}