#ifndef GRAPHE
#define GRAPHE

#include "cellule.h"
#include "liste.h"
#include <stdio.h>

typedef struct graphe graphe;

struct graphe {
    int nbSommet;
    int oriente;
    int value;
    liste* l_adj;
    int** m_adj;
    int* m_stockage;
    int nbArrete;
};

void initialiser_graphe(FILE*, graphe*);

void afficher_graphe(graphe*);

void detruire_graphe(graphe*);

#endif