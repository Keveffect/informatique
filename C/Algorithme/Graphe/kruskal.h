#ifndef KRUSKAL
#define KRUSKAL

#include "../TP3/arrete.h"

void genere_acpm_kruskal_tableau(int, int, arrete**);

void afficher_acmp(arrete**, int);

void generer_acpm_kruskal_ensembles(int, int, arrete**);

#endif