#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "cellule.h"
#include "liste.h"
#include "graphe.h"
#include "../TP3/arrete.h"

/**
\ initialise un graphe
\param f file
\param g graphe
*/
void initialiser_graphe(FILE* fichier, graphe* g) {
    int i, j, p;
    char tmp[30];
    cellule* c = NULL;
    cellule* c2 = NULL;
    cellule* cp = NULL;
    cellule* cp2 = NULL;

    g->nbArrete = 0;

    fscanf(fichier, "%s", tmp);
    fscanf(fichier, "%d", &g->nbSommet);

    fscanf(fichier, "%s", tmp);
    fscanf(fichier, "%d", &g->oriente);

    fscanf(fichier, "%s", tmp);
    fscanf(fichier, "%d", &g->value);

    //on alloue la matrice d'adjacence
    g->m_stockage = (int *)malloc(sizeof (int) * g->nbSommet* g->nbSommet);
    g->m_adj = (int **)malloc(sizeof(int*) * g->nbSommet);
    for (i = 0; i < g->nbSommet; i++){
        g->m_adj[i] = &g->m_stockage[i* g->nbSommet];
    }

    //on alloue le tableau de liste adjacente
    g->l_adj = malloc(sizeof(liste) * g->nbSommet);
    for (i = 0; i < g->nbSommet; i++) {
        initialiser_liste(&g->l_adj[i]);
    }

    fscanf(fichier, "%s", tmp);       //DEBUT_DEF_ARRETES

    //graphe non oriente sans poids
    if (g->value == 0 && g->oriente == 0) {
        fgets(tmp, 30, fichier);
        sscanf(tmp, "%d %d", &i, &j);
        
        while (strcmp(tmp, "FIN_DEF_ARETES")) {
            fgets(tmp, 30, fichier);
            if (strcmp(tmp, "FIN_DEF_ARETES")) {
                sscanf(tmp, "%d %d ", &i, &j);

                c = creationCellule();
                initialiserCellule(c, j);
                inserer(&g->l_adj[i], c);

                c2 = creationCellule();
                initialiserCellule(c2, i);
                inserer(&g->l_adj[j], c2); //on cree la liste d'adjacence

                g->m_adj[i][j] = 1;
                g->m_adj[j][i] = 1; // on complete la matrice d'adjacence

            }
        }
    }

    //graphe oriente sans poids
    if (g->value == 0 && g->oriente == 1) {
        fgets(tmp, 30, fichier);
        sscanf(tmp, "%d %d", &i, &j);

        while (strcmp(tmp, "FIN_DEF_ARCS")) {
            fgets(tmp, 30, fichier);
            if (strcmp(tmp, "FIN_DEF_ARCS")) {
                sscanf(tmp, "%d %d ", &i, &j);

                c = creationCellule();
                initialiserCellule(c, j);
                inserer(&g->l_adj[i], c);

                g->m_adj[i][j] = 1;
            }
        }
    }

    //graphe non oriente avec poids
    if (g->value == 1 && g->oriente == 0) {
        fgets(tmp, 30, fichier);
        sscanf(tmp, "%d %d %d", &i, &j, &p);

        while (strcmp(tmp, "FIN_DEF_ARETES")) {
            fgets(tmp, 30, fichier);
            if (strcmp(tmp, "FIN_DEF_ARETES")) {
                sscanf(tmp, "%d %d %d", &i, &j, &p);

                c = creationCellule();
                initialiserCellule(c, j);
                inserer(&g->l_adj[i], c);

                cp = creationCellule();
                initialiserCellule(cp, p);
                inserer(&g->l_adj[i], cp);

                c2 = creationCellule();
                initialiserCellule(c2, i);
                inserer(&g->l_adj[j], c2); //on cree la liste d'adjacence

                cp2 = creationCellule();
                initialiserCellule(cp2, p);
                inserer(&g->l_adj[j], cp2);

                g->m_adj[i][j] = p;
                g->m_adj[j][i] = p; // on complete la matrice d'adjacence

                g->nbArrete++;
            }
        }
    }

    //graphe oriente avec poids
    if (g->value == 1 && g->oriente == 1) {
        
        fgets(tmp, 30, fichier);
        sscanf(tmp, "%d %d %d", &i, &j, &p);

        while (strcmp(tmp, "FIN_DEF_ARETES")) {
            fgets(tmp, 30, fichier);
            if (strcmp(tmp, "FIN_DEF_ARETES")) {
                sscanf(tmp, "%d %d %d", &i, &j, &p);

                cp = creationCellule();
                initialiserCellule(cp, j);
                inserer(&g->l_adj[i], cp);

                c = creationCellule();
                initialiserCellule(c, j);
                inserer(&g->l_adj[i], c);

                g->m_adj[i][j] = p;

                g->nbArrete++;
            }
        }
    }

    fclose(fichier);

    if(!g->oriente)
        g->nbArrete *= 2;

    free(c);
    free(c2);
    free(cp);
}

/**
\ afficher graphe
\param g graphe
*/
void afficher_graphe(graphe* g) {
    int i, j;

    printf("\n Matrice d'adjacence \n\n");
    
    for (i =0; i < g->nbSommet; i++){
        printf("\t");
        for (j =0 ; j < g->nbSommet; j++){
            printf("%d ",g->m_adj[i][j]);
        }
        printf("\n");
    }
    
    printf("\n Liste d'adjacence \n\n");

    for (i = 0; i < g->nbSommet; i++) {
        printf("\t[%d]->", i);
        if (g->value)
            afficher_liste_poids(&g->l_adj[i]);
        else
            afficher_liste(&g->l_adj[i]);
    }

    printf("\n");
}

/**
\ detruit graphe
\param g graphe
*/
void detruire_graphe(graphe* g) {
    detruire_liste(g->l_adj);
    free(g->m_adj);
    free(g->m_stockage);
    free(g);
}