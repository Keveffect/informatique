#include "arrete.h"
#include "prim.h"
#include "cellule.h"
#include "liste.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void afficher_acpm(int* tabDistance, int* tabCouvert, int* tabPere, int nbSommet) {
	int i, poids = 0;

	printf("\nPRIM :\n");
	
	printf("	      |");
	for (i = 0; i < nbSommet; i++) {
		printf("%d|", i);
	}
	printf("\n");

	printf("tab distance : ");
	for (i = 0; i < nbSommet; i++) {
		printf("%d ", tabDistance[i]);
	}
	printf("\ntab couvert  : ");
	for (i = 0; i < nbSommet; i++) {
		printf("%d ", tabCouvert[i]);
	}
	printf("\ntab pere    : ");
	for (i = 0; i < nbSommet; i++) {
		printf("%d ", tabPere[i]);
	}
	printf("\n");
	
	for (i = 0; i < nbSommet; i++) {
		poids += tabDistance[i];
	}

	printf("\nPoids de l'arbre couvrant minimal : %d\n", poids);

	free(tabDistance);
	free(tabCouvert);
	free(tabPere);
}

int min(int *tabDistance, int *tabCouvert, int nbSommet) {
	int i = 0, min = 0;

	while (tabCouvert[i++]) {
		min++;
	}
	for (i = 0; i < nbSommet; i++) {
		if (tabDistance[i] < tabDistance[min] && tabCouvert[i] == 0) {
			min = i;
		}
	}
	return min;
}

void genere_acpm_prim_tableau(liste* listeAdj, int nbSommet, int nbArrete) {
	int i, u;
	
	int* tabDistance = malloc(sizeof(int)*nbSommet);
	int* tabCouvert = malloc(sizeof(int) * nbSommet);
	int* tabPere = malloc(sizeof(int) * nbSommet);
	cellule* c = malloc(sizeof(cellule));
	cellule* c2 = malloc(sizeof(cellule));

	for (i = 0; i < nbSommet; i++) {
		tabDistance[i] = INT_MAX;
		tabPere[i] = -1;
		tabCouvert[i] = 0;
	}
	
	tabDistance[0] = 0;

	for (i = 0; i < nbSommet; i++) {

		u = min(tabDistance, tabCouvert, nbSommet);
		c = listeAdj[u].tete->succ;
		c2 = listeAdj[u].tete; //poids de c
		tabCouvert[u] = 1;

		while (c !=NULL) {
			if (!tabCouvert[c->id] && c2->id < tabDistance[c->id]) {
				tabPere[c->id] = u;
				tabDistance[c->id] = c2->id;
			}
			c = c->succ;
			if (c != NULL) {
				c = c->succ;
				c2 = c->pred;
			}
		}
	}

	free(c);
	free(c2);
	afficher_acpm(tabDistance, tabCouvert, tabPere, nbSommet);
}