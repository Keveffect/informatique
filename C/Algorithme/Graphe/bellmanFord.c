#include "graphe.h"
#include "bellmanFord.h"
#include "liste.h"
#include "arrete.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/**
\ initialise les sommet a l'infini
\param g graphe
\param sommet int
\param donnees donnees
*/
void source_unique_initialisation(graphe*g, int sommet, donnees* donnees) {
	int i, max;
	
	donnees->distance = malloc(sizeof(int)*g->nbSommet);
	donnees->pere = malloc(sizeof(int) * g->nbSommet);

	max = __INT_MAX__ / 2; // pour eviter le depassement lors du relachement

	for ( i = 0; i < g->nbSommet; i++)
	{
		donnees->distance[i] = max;
	}
	donnees->distance[sommet] = 0;

	for ( i = 0; i < g->nbSommet; i++)
	{
		donnees->pere[i] = -1;
	}
}

/**
\ relacher un sommet
\param u int
\param v int
\param poids int
\param donnees donnes
*/
void relacher(int u, int v, int poids, donnees* donnees) {
	if (donnees->distance[v] > donnees->distance[u] + poids) {
		donnees->distance[v] = donnees->distance[u] + poids;
		donnees->pere[v] = u;
	}
}

/**
\ bellman ford
\param g graphe
\param sommet int
\param tabArrete arrete
\param donnees donnees
*/
int bellman_ford(graphe*g, int sommet, arrete** tabArrete, donnees* donnees) {
	int i, j;
	int boolean;

	boolean = 0;

	source_unique_initialisation(g, sommet, donnees);

	for ( i = 0; i < g->nbSommet; i++){
		for (j = 0; j < g->nbArrete; j++){
			relacher(tabArrete[j]->origine, tabArrete[j]->extremite, tabArrete[j]->poids, donnees);
		}
	}

	for(i = 0; i<g->nbArrete; i++){
			printf("%d %d %d\n",tabArrete[i]->origine, tabArrete[i]->extremite, tabArrete[i]->poids);
		if (donnees->distance[tabArrete[i]->extremite] > donnees->distance[tabArrete[i]->origine] + tabArrete[i]->poids){
			boolean = 1;
		}
	}

	if (boolean){
		printf("PRESENCE DE CYCLE ABSORBANT\n");
	}
	else{
		printf("AUCCUN CYCLE ABSORBANT DETECTE\n");
	}

	return boolean;
}