#ifndef DIJKSTRA
#define DIJKSTRA

#include "graphe.h"
#include "arrete.h"

typedef struct donneesdij donneesdij;

struct donneesdij {
    int* distance;
    int* pere;
    int* visite;
};

void source_unique_initialisation_dij(graphe*, int, donneesdij*);

void relacher_dij(int, int, int, donneesdij*);

void dijkstra(graphe*, int, arrete**, donneesdij*);

#endif