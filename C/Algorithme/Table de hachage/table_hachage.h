#ifndef TABLE_DE_HACHAGE
#define TABLE_DE_HACHAGE

#include "liste.h"

typedef struct table_de_hachage table_de_hachage;

struct table_de_hachage {
    liste** liste;
    int taille;
};

void initialiser_table_hachage(table_de_hachage*, int);

unsigned long long puissance(int);

unsigned long long convertir_ch_entier(char*);

int hachage(unsigned long long, int);

void afficher_table_hachage(table_de_hachage*);

void detruire_table_hachage(table_de_hachage*);

void inserer_hachage(table_de_hachage*, cellule*);

cellule* rechercher_hachage(table_de_hachage*, char*);

void supprimer_hachage(table_de_hachage*, char*);

int compter_table_hachage(table_de_hachage* );

#endif