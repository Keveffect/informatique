#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "cellule.h"
#include "liste.h"
#include "table_hachage.h"

int main() {
	int compteur, choix, taille;
	char tmp[26];
	char* mot = NULL;
	float temps;
	clock_t t1, t2;
	FILE* fichier;
	table_de_hachage* tabh = malloc(sizeof(table_de_hachage));
	cellule* c = malloc(sizeof(cellule));

	taille = 11;

	initialiser_table_hachage(tabh, taille);

	t1 = clock();

	printf("Choisissez le fichier a lire :\n\t1.texte1.txt\n\t2.texte2.txt\n\t3.histoire_quebec.txt\n\t4.dico_france-quebec.txt\n\t5.dico_ae.txt\n\t6.dico.txt\n");
	scanf("%d", &choix);

	switch (choix)
	{
	case 1: fichier = fopen("texte1.txt", "r");
		break;
	case 2: fichier = fopen("texte2.txt", "r");
		break;
	case 3: fichier = fopen("histoire_quebec.txt", "r");
		break;
	case 4: fichier = fopen("dico_france-quebec.txt", "r");
		break;
	case 5: fichier = fopen("dico_ae.txt", "r");
		break;
	default: fichier = fopen("dico.txt", "r");
		break;
	}

	printf("\n");

	while (!feof(fichier)) {
		fscanf(fichier, "%s", tmp);
		mot = malloc(sizeof(char) * strlen(tmp) + 1);
		strcpy(mot, tmp);
		c = rechercher_hachage(tabh, mot); // on teste si le mot fait deja partie de la table
		if (c == NULL && strlen(tmp) < 26) {
			c = creationCellule();
			initialiserCellule(c, mot);
			inserer_hachage(tabh, c);
		}
	}

	fclose(fichier);

	//afficher_table_hachage(tabh);

	compteur = compter_table_hachage(tabh);

	printf("\nNombre de mots différents : %d\n\n", compteur);

	detruire_table_hachage(tabh);

	t2 = clock();
	temps = (float)(t2 - t1) / CLOCKS_PER_SEC;
	printf("Temps d'execution = %f secondes\n", temps);

	return 0;
}