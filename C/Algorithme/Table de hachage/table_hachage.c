#include <stdio.h>
#include <stdlib.h>
#include "cellule.h"
#include "liste.h"
#include "table_hachage.h"

/**
\ creer une table de hachage
\param tabh table_de_hachage
\param taille int
*/
void initialiser_table_hachage(table_de_hachage* tabh, int taille) {
	int i;
	
	tabh->taille = taille;

	tabh->liste = malloc(sizeof(liste) * tabh->taille);
	for (i = 0; i < tabh->taille; i++) {
		tabh->liste[i] = malloc(sizeof(liste));
	}
}

/**
\ affiche le contenue de la table
\param tabh table_de_hachage
*/
void afficher_table_hachage(table_de_hachage* tabh) {
	int i;

	printf("Table de hachage :\n\n");
	for (i = 0; i < tabh->taille; i++) {
		printf("[%d]-> ", i);
		afficher_liste(tabh->liste[i]);
	}
	printf("\n");
}

/**
\ detruit une table de hachage
\param tabh table_de_hachage
*/
void detruire_table_hachage(table_de_hachage* tabh) {
	int i;

	for (i = 0; i < tabh->taille; i++) {
		detruire_liste(tabh->liste[i]);
	}
	free(tabh);
}

/**
\ puissance
\param taille int
\return unsigned long long
*/
unsigned long long puissance(int taille) {
	unsigned long long resultat = 1;
	int i;
	
	for (i = 0; i < taille; i++) {
		resultat *= 128;
	}
	
	return resultat;
}

/**
\ convertie
\param tabh table_de_hachage
\return unsigned long long
*/
unsigned long long convertir_ch_entier(char* tmp) {
	unsigned long long k = 0;
	int i = 0, taille;

	while (tmp[i] != '\0') {
		i++;
	}
	
	taille = i - 1;
	i = 0;
	
	while (taille >= 0) {
		k += tmp[i++] * puissance(taille--);
	}

	return k;
}

/**
\ hachage
\param k unsigned long long
\param taille int
\return int
*/
int hachage(unsigned long long k, int taille) {
	return k % taille;
}

/**
\ insere dans la table
\param tabh table_de_hachage
\param c cellule
*/
void inserer_hachage(table_de_hachage *tabh, cellule *c) {
	int emplacement;

	emplacement = hachage(convertir_ch_entier(c->mots), tabh->taille);
	inserer(tabh->liste[emplacement], c);
}

/**
\ recherche table de hachage
\param tabh table_de_hachage
\param mot char
\return cellule
*/
cellule* rechercher_hachage(table_de_hachage *tabh, char* mot) {
	int emplacement;
	cellule* c = malloc(sizeof(cellule));
	
	initialiserCellule(c, mot);
	emplacement = hachage(convertir_ch_entier(c->mots), tabh->taille);
	
	return rechercher(tabh->liste[emplacement], mot);
}

/**
\ supprime une table de hachage
\param tabh table_de_hachage
\param mot char
*/
void supprimer_hachage(table_de_hachage *tabh, char* mot) {
	cellule* c = malloc(sizeof(cellule));
	int emplacement;

	emplacement = hachage(convertir_ch_entier(c->mots), tabh->taille);
	c = rechercher(tabh->liste[emplacement], mot);
	if (c != NULL)
		free(c);
}

/**
\ compte dans la table
\param tabh table_de_hachage
\return int
*/
int compter_table_hachage(table_de_hachage* tabh) {
	int i, compteur = 0;

	for (i = 0; i < tabh->taille; i++) {
		compteur += compter_liste(tabh->liste[i]);
	}
	
	return compteur;
}