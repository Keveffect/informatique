#include<stdio.h>

int Ackermann(int m,int n){
    if(m == 0){
        return n+1;
    }
    else if(n == 0){
        return Ackermann(m-1,1);
    } else {
        return Ackermann(m-1,Ackermann(m, n-1));
    }    
}


int main(){
    int m, n, res;
    printf("Entrer m: ");
    scanf("%d",&m);
    printf("Entrer n: ");
    scanf("%d",&n);
    res = Ackermann(m,n);
    printf("le resultat est : %d\n", res);
 
 	printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}