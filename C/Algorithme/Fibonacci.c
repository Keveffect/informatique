#include <stdio.h>
#include <stdlib.h>

int main(){
    int m,i;
    int *v;

    printf("selectionnez la taille du tableau : ");
    scanf("%d",&m);//taille du tableau

    v = (int *)malloc(sizeof(int)*m);//allocatin dynamique deu tableau

    if(m>=0){
        v[0]=0;
        v[1]=1;
    }

    for(i=2;i<m;i++){
        v[i]= v[i-1] + v[i-2]; //on rempli le tableau
    }

    for(i=0;i<m;i++){
        printf("%d ",v[i]); //on affiche
    }
    
    free(v);
    v=NULL;

  
  	printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}