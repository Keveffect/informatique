#include <stdio.h>
#include <stdlib.h>

/****************************************/
/*              		Structures             	        */
/****************************************/
typedef struct Element Element;
struct Element{
    int nombre;
    Element *suivant;
};

typedef struct Pile{
    Element *premier;
}Pile;

/****************************************/
/*              		Methodes                	        */
/****************************************/
// -------------INNITIALISER--------
Pile *initialiser(){
    Pile *pile = malloc(sizeof(*pile));
    pile->premier = NULL;
}

// -------------ESTVIDE--------
int estVide(Pile *pile){
    if(pile != NULL && pile->premier != NULL)
        return 0;
    else
        return 1;
}

// -------------EMPILER--------
void empiler(int nvNombre, Pile *pile)
{
    Element *nouveau = malloc(sizeof(*nouveau));
    if (pile == NULL || nouveau == NULL)
    {
        printf("je suis videe...");		//si l’élément n’a pas été créé on termine le programme
        exit(EXIT_FAILURE);
    }
    nouveau->nombre = nvNombre;
    nouveau->suivant = pile->premier;	//on ajoute l’élément en haut de la pile
    pile->premier = nouveau;
}

// -------------DEPILER--------
int depiler(Pile *pile){
    if (estVide(pile)){
        exit(EXIT_FAILURE);			//si la pile est déjà vide on quitte le programme
    }
    int nombreDepile = 0;
    Element *elementDepile = pile->premier;
    if (!estVide(pile)){
        nombreDepile = elementDepile->nombre;
        pile->premier = elementDepile->suivant;
        free(elementDepile);
    }
    return nombreDepile;
}
// -------------SOMMET--------
int sommetPile(Pile *p){
    for( p ; estVide(p) ; p++);
    return p->premier->nombre;
}
// -------------AFFICHERPILE--------
void afficherPile(Pile *pile){
    if (pile == NULL){
        exit(EXIT_FAILURE);		//si la pile est déjà vide on quitte le programme
    }
    Element *actuel = pile->premier;
    while (actuel != NULL)    {	//tant qu’elle n’est pas vide on peut afficher
        printf("%d\n", actuel->nombre);
        actuel = actuel->suivant;
    }
    printf("\n");
}

/****************************************/
/*                 		Main                 	        */
/****************************************/

int main(int argc, char const *argv[]){
    int m, n, res;
    Pile *p = initialiser();

    printf("Entrer m: ");
    scanf("%d",&m);
    printf("Entrer n: ");
    scanf("%d",&n);
 
    empiler(m,p);
    empiler(n,p);

    while(!estVide(p)){
        n = sommetPile(p);
        printf("n = %d\n", n);
        afficherPile(p);
        depiler(p);
        if(!estVide(p)){		//la pile n’est pas vide, on continue
            m = sommetPile(p);
            depiler(p);
        }
        else {
            break;			//fin de la récursive
        }
        if (m==0){
            empiler(n+1, p);
        } else {  
            if (n==0){
                empiler(m-1,p);
                empiler(1,p);
            } else {
                empiler(m-1,p);
                empiler(m,p);	//on effectue les calcules
                empiler(n-1,p);
            }
        }
    }
    afficherPile(p);
    printf("Le resultat est %d\n", n);
    free(p);
   	
    printf("Appuyer sur une touche pour continuer...");
	scanf("\n");
	return 0;
}