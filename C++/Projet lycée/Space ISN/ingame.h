#include <formes.h>
#include <cstdlib>
#include <ctime>

     int compteurScore = 0;
     int score;   
     int oldn1, oldn2, oldn3, oldn4, oldn5, oldnb;
        int vie = 5;  // Le nombre de coeur en jeu (Les coeurs affich�s s'adaptent) 
        int const viec = vie;

        // Fonction qui permet l'affichage d'un carr�
    void Carre1(int taille, int positionX, int positionY)
    {
         for(int i = 0; i < taille; i++)
         {
                 MetPoint(20,positionX+i,positionY,255,255,255);
                 MetPoint(20,positionX+i,positionY+taille-1,255,255,255);
                 MetPoint(20,positionX,positionY+i,255,255,255);
                 MetPoint(20,positionX+taille-1,positionY+i,255,255,255);
         }
                 
    }
    void Carre2(int taille, int positionX, int positionY)
    {
         for(int i = 0; i < taille; i++)
         {
                 MetPoint(20,positionX+i,positionY,255, 0, 0);
                 MetPoint(20,positionX+i,positionY+taille-1,255, 0, 0);
                 MetPoint(20,positionX,positionY+i,255,0,0);
                 MetPoint(20,positionX+taille-1,positionY+i,255, 0, 0);
         }
                 
    }

int aleatoire(int a, int b){
    return rand()%(b-a) + a;   
}
    
      // couleur s�paration informations
      int xBarSeparatorR = 20;
      int xBarSeparatorV = 20;
      int xBarSeparatorB = 20;    


// Fonction qui permet d'afficher une ligne (Fonctionne a moiti�)
void Ligne(int taille,int positionX, int positionY)
{

     bar(positionX, positionY, 800, positionY+taille);

}

     // Fonction qui permet au programme entier d'attendre X milliseconds
      void Attendre(int ms){
           Sleep(ms);
      }

      // Les positions de bases des coeurs
      int vieX,vieY;
      int vieXbase = 1;
      int vieYbase = 28;
      
      int n1,n2,n3,n4,n5;

      void AffichageScore(int nb){
         
         
        // On sauvegarde le score actuel
        oldnb = nb;   
        
        // On s�pare le nombre du score en 5 chiffres
        n1 = nb%10; 
        nb /= 10;
        n2 = nb%10;
        nb /= 10;
        n3 = nb%10;
        nb /= 10;
        n4 = nb%10;
        nb /=10;
        n5 = nb%10;
        nb /= 10;
        
        if (vie==0)
        {
                     // Pour �viter de raffraichir tout le temps, on le fait d�s que le nombre est diff�rent que le pr�c�dent
        
         // On affiche un carr� noir avant d'afficher le nombre pour "effacer" l'ancien
           number(8, n5, (15*10)/5, 40,243,214,23); // On affiche le nombre
            // On sauvegarde le nombre actuel
       
           number(8, n4, (20*10)/5, 40,243,214,23);
         
           number(8, n3, (25*10)/5, 40,243,214,23);
            
           number(8, n2, (30*10)/5, 40,243,214,23);
          
           number(8, n1, (35*10)/5, 40,243,214,23);
              
                   }else {
        
  

        // Chaque chiffres sont affich�s ind�pendament
        if (oldn5 != n5) // Pour �viter de raffraichir tout le temps, on le fait d�s que le nombre est diff�rent que le pr�c�dent
        {
         setfillstyle(SOLID_FILL,BLACK);
           bar(((vieXbase + 10 + viec*2)*20),vieYbase*20,((vieXbase + 11 + viec*2)*20),(vieYbase+2)*20); // On affiche un carr� noir avant d'afficher le nombre pour "effacer" l'ancien
           number(5, n5, ((vieXbase + 10 + viec*2)*20)/5, 112,200,200,200); // On affiche le nombre
           oldn5 = n5; // On sauvegarde le nombre actuel
        }
        if (oldn4 != n4) // M�me chose qu'au dessus mais pour le chiffre d'apr�s
        {
         setfillstyle(SOLID_FILL,BLACK);
           bar((vieXbase+12 + viec*2)*20,vieYbase*20,((vieXbase + 13 + viec*2)*20),(vieYbase+2)*20);
           number(5, n4, ((vieXbase + 12 + viec*2)*20)/5, 112,200,200,200);
           oldn4 = n4;
        }
        if (oldn3 != n3)// M�me chose qu'au dessus mais pour le chiffre d'apr�s
        {
         setfillstyle(SOLID_FILL,BLACK);
           bar(((vieXbase + 14 + viec*2)*20),vieYbase*20,((vieXbase + 15 + viec*2)*20),(vieYbase+2)*20);
           number(5, n3, ((vieXbase + 14 + viec*2)*20)/5, 112,200,200,200);
           oldn3 = n3;
        }
        if (oldn2 != n2)// M�me chose qu'au dessus mais pour le chiffre d'apr�s
        {
            setfillstyle(SOLID_FILL,BLACK);
            bar(((vieXbase + 16 + viec*2)*20),vieYbase*20,((vieXbase + 17 + viec*2)*20),(vieYbase+2)*20);
            
           number(5, n2, ((vieXbase + 16 + viec*2)*20)/5, 112,200,200,200);
           oldn2 = n2;
        }
        if (oldn1 != n1)// M�me chose qu'au dessus mais pour le chiffre d'apr�s
        {
            setfillstyle(SOLID_FILL,BLACK);
            
            bar(((vieXbase + 18 + viec*2)*20),vieYbase*20,((vieXbase + 20 + viec*2)*20),(vieYbase+2)*20);
           number(5, n1, ((vieXbase + 18 + viec*2)*20)/5, 112,200,200,200);
           oldn1 = n1;
        }
                   
        
        }
        
        
        
      }



int gestionScore(){
     //Gestion du score
  
       if (compteurScore > 10)
       {
          score += 1; // On incr�mente le score de 1 � chaque tour de boucle
          compteurScore = 0;
          }
       compteurScore++;
       AffichageScore(score); // On envoit la valeur du score dans la fonction pour l'afficher
       
       return score;
}

    void AffichageVie(int vie, int viec){

   
        vieX = vieXbase;
        vieY = vieYbase;


       for(int i = 0; i < viec; i++){
           
           forme(5,'m',vieX*4,vieY*4,20,20,20); // On supprimes les coeurs
           vieX += 2; // On d�place le prochain coeur de deux
           
       }
       
       setfillstyle(SOLID_FILL,COLOR(xBarSeparatorR,xBarSeparatorV,xBarSeparatorB));
       bar((vieX*20)+15,545,(vieX*20)+20,600);
       
       ScoreText(5,((vieX*20)+15)/5,112,200,200,200);
       
        vieX = vieXbase;
        vieY = vieYbase;
        for(int i = 0; i < vie; i++){
           
           forme(5,'m',vieX*4,vieY*4,255,0,0); // On affiche les coeurs
           vieX += 2; // On d�place le prochain coeur de deux
        }
      
       

    }


    void gameover(){
         
         for(int i = 0; i <= 3; i++){ 
             setbkcolor(RED);
             cleardevice();
             Attendre(100);
             setbkcolor(BLACK);
             cleardevice();
             Attendre(100);
         }
         
             setbkcolor(BLACK);
             cleardevice();         

             
             GameOverText(10,3,255,255,255); // On affiche le texte du GameOver
             Carre1(6,9,20);
             Carre2(6,25,20);
             AffichageScore(gestionScore());
             while(1){
            
                          }      
            
         
    
    }
        int positionX = 20;
        int positionY = 25;
        
void StartParty(){
     
        


// Initialisation des variables utiles pour le jeu
        bool mort = false;
        bool quitter = false;
        int score = 0;
        int compteur = 0;
        int ennemiPositionX = 20;
        int ennemiPositionY = 5;
        int compteurScore = 0;
        int acceleration = 0;
        int compteurVaisseau = 0;
        int BalleTirerY;
        int BalleTirerX;
        int compteurTir = 0;
        int compteurTirBalle = 0;
        bool tirer = false;  
        bool toucher;  
        bool game = false;
        bool reverse = false;

        setfillstyle(SOLID_FILL,COLOR(xBarSeparatorR,xBarSeparatorV,xBarSeparatorB));
        bar(0,545,800,550);
        
        
        // On affiche le score � 0
       number(5, 0, ((vieXbase + 10 + viec*2)*20)/5, 112,200,200,200);
       number(5, 0, ((vieXbase + 12 + viec*2)*20)/5, 112,200,200,200);
       number(5, 0, ((vieXbase + 14 + viec*2)*20)/5, 112,200,200,200);
       number(5, 0, ((vieXbase + 16 + viec*2)*20)/5, 112,200,200,200);
       number(5, 0, ((vieXbase + 18 + viec*2)*20)/5, 112,200,200,200);
        
     
        AffichageVie(vie, viec);
        

        
        while(mort == 0) // Tant que le joueur n'est pas mort, la boucle continue d'�tre execut�
        {

                   Attendre(10);
                   gestionScore();
                   
                   // D�placement de l'�nnemie
                    if (compteur > 10){ // L'�nnemi se d�place toutes les 10 boucles
                       MetPoint(20,ennemiPositionX,ennemiPositionY,0,0,0);

                       if ((ennemiPositionX >= 38  && reverse == false) || ennemiPositionY >= 23 || (ennemiPositionX <= 1  && reverse == true)) // Une fois arriv� au bout de l'�cran, on le renvoit au d�but
                       {
                           if (ennemiPositionX >= 38){
                              ennemiPositionY += 1;
                              reverse = true;
                           }else if(ennemiPositionX <= 1){
                                 reverse = false;
                                 ennemiPositionY += 1;
                           }else{
                                 ennemiPositionY = 5;
                                 ennemiPositionX = 1;   
                           }
                       }else{ // Sinon on le d�place vers la gauche
                              if (reverse == false){
                                          ennemiPositionX += 1;
                                 }else{ 
                                        ennemiPositionX -= 1;
                                 }
                                       
                             }
                       if (ennemiPositionY >= 25) // Une fois arriv� en bas de l'�cran, on le renvoit au d�but
                       {
                          ennemiPositionY = 5;
                       }

                       MetPoint(20,ennemiPositionX,ennemiPositionY,70,70,70);
                       compteur = 0;
                    }
                    compteur++;
                   
                   if (tirer == 0) { // Si "Tirer" = false, (qu'il n'y ai pas de balle sur l'�cran)
                    if (aleatoire(0,50) == 7 ){ // Au bout de 100 boucles, la balle est tir�
                       
                      MetPoint(20,ennemiPositionX,ennemiPositionY+1,255,255,0);
                      BalleTirerY = ennemiPositionY;
                      BalleTirerX = ennemiPositionX;
                      tirer = true;
                      compteurTir = 0;
                    }
                    compteurTir++;
                }else{ // S'il y a une balle, on la d�place sur l'�cran
                       if (compteurTirBalle > (8-acceleration)){ // D�placement de la balle toutes les 10 boucles
                           if(BalleTirerY <= 25){
                                             
                               MetPoint(20,BalleTirerX,BalleTirerY,0,0,0);
                               BalleTirerY += 1;
                               if (acceleration < 8) acceleration += 1;
                               MetPoint(20,BalleTirerX,BalleTirerY,255,255,0);
                           }else{
                                 acceleration = 0;
                                 MetPoint(20,BalleTirerX,BalleTirerY,0,0,0);
                                 tirer = false;
                                 
                           }
                           compteurTirBalle = 0;
                       }
                       compteurTirBalle++;
                    // Dans le cas o� le vaisseau serait touch�
                    if(BalleTirerX == positionX && BalleTirerY == positionY)
                           if(toucher == 0){
                            toucher = true;
                            
                            MetPoint(20,BalleTirerX,BalleTirerY,0,0,0);
                            BalleTirerX = 0;
                            BalleTirerY = 0;
                            tirer = false;    
                            vie--;
                               
                            cout << "Vous �tes touch� !" << endl;
                            if(vie < 1){
                               cout << "Vous �tes mort !" << endl;
                               mort = true;
                               gameover();
                            }else{
                                cout << "Il vous reste " << vie << " coeurs" << endl;
                                AffichageVie(vie, viec);
                                toucher = false;
                            }
                            
    
                          }
                     }
                   if(mort == 0){
                if (compteurVaisseau > 4){
                    // D�placement de l'alli�
                    if(GetKeyState('Q') & 0x8000) // Si la touche Q est press�
                    {
                        if (positionX >= 2)
                        {
                           MetPoint(20,positionX,positionY,0,0,0);
                           positionX -= 1;
                           MetPoint(20,positionX,positionY,255,255,255);
                           }
                           
                    } else if(GetKeyState('D') & 0x8000) // Si la touche D est press�
                    {
                           if (positionX <= 37)
                                        {
        
                           MetPoint(20,positionX,positionY,0,0,0);
                           positionX += 1;
                           MetPoint(20,positionX,positionY,255,255,255); 
                           }
                    }else if(GetKeyState(VK_SPACE) & 0x8000){ // Au cas ou..
                          cout << "vous avez appuyez sur ESPACE !" << endl;
                          }
                    MetPoint(20,positionX,positionY,255,255,255);
                    compteurVaisseau = 0;
                    }
                    compteurVaisseau++;
                    
                }

                
                
    
                
    
    

                
                        
                        
        }
}
