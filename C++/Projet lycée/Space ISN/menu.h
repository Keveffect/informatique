//-------------------------------------
//         Menu Principale
//-------------------------------------

int posLevel = 0;
int compteurSelector = 0;


int etape = 0;
bool menuPrincipal = true;

void selector(int posX,int posY){ // Permet d'afficher/d�placer le selecteur de menu
     setfillstyle(SOLID_FILL,BLACK);
     bar(18*10, 32*10, 27*10, 58*10);
     bar(55*10, 32*10, 62*10, 58*10);
     forme(10,'m',posX-4,posY,255,255,255);
     forme(10,'m',posX+33,posY,255,255,255); 
    
}

//Couleur Texte Accueil
int CouleurMenuStartTextR = 255;
int CouleurMenuStartTextV = 0;
int CouleurMenuStartTextB = 0;

void InitializeMenu() // Affiche les principaux texts + Lignes blanche
{
    Ligne(18,0,280);
    
    MenuJouerText(8,30,44,255,255,255);
    MenuJouerDeuxText(8,30,54,255,255,255);
    MenuQuitterText(8,30,64,255,63,148);
    MenuStartText(2,1,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
    selector(22,35);
    posLevel = 0;



}

/*
posLevel = 0 >> Selecteur tout en haut
posLevel = 1 >> Selecteur au milieu
posLevel = 2 >> Selecteur tout en bas

*/

void menuUp(){ // Fait monter le selecteur
     if(posLevel == 1){
        selector(22,35);
        posLevel = 0;
     }else if(posLevel == 2){
        selector(22,43);
        posLevel = 1;
     }
     
     
}

void menuDown(){ // Fait descendre le selecteur
     if(posLevel == 0){
        selector(22,43);
        posLevel = 1;
     }else if(posLevel == 1){
        selector(22,51);
        posLevel = 2;
     }
}

/*---------------------------------------*/
/* Animation du Texte du menu principale */
/*---------------------------------------*/

int compteurMenu = 0;
int anim = 1;

void AnimeText(){ // Permet de faire bouger le texte "Space Invader"
        Attendre(10);

        /*----------------------------*/

        if(etape == 0){
            CouleurMenuStartTextR--;
            CouleurMenuStartTextV++;
            if(CouleurMenuStartTextV == 255) etape = 1;
        }else if(etape == 1){
            CouleurMenuStartTextV--;
            CouleurMenuStartTextB++;
            if (CouleurMenuStartTextB == 255) etape = 2;
        }else if(etape == 2){
            CouleurMenuStartTextB--;
            CouleurMenuStartTextR++;
            if (CouleurMenuStartTextR == 255) etape = 0;
        }
     
        /*----------------------------*/
        
        if (anim == 1){  // Position initial
            MenuStartText(2,1,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
        }else if (anim == 2){ // Position vers le bas
            MenuStartText(2,2,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
        }else if(anim == 3){ // Position � droite
            MenuStartText(3,2,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
        }else if(anim == 0){ // Position � haut
            MenuStartText(3,1,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
        }
        
        /*----------------------------*/

     
        if(compteurMenu > 40){
            compteurMenu = 0;
            setfillstyle(SOLID_FILL,BLACK);
            bar(0, 0, 40*20, 14*20); // Avec un gros bloc noir, on efface le text pr�sent avant d'afficher le prochain
            
            if (anim == 0){  // Position initial
                MenuStartText(2,1,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
                anim++;
            }else if (anim == 1){ // Position vers le bas
                MenuStartText(2,2,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
                anim++;
            }else if(anim == 2){ // Position � droite
                MenuStartText(3,2,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
                anim++;
            }else if(anim == 3){ // Position � haut
                anim = 0; // On remet la valeur � la position initial pour qu'a la prochaine execution, ce sois la position initial qui sois affich�
                MenuStartText(3,1,CouleurMenuStartTextR,CouleurMenuStartTextV,CouleurMenuStartTextB);
            }
        
        }else{
              compteurMenu++;
        }


     
}

/*-------------------------------------------*/
/* Fin Animation du Texte du menu principale */
/*-------------------------------------------*/


/*------------------------------*/
/* Affichage du menu principale */
/*------------------------------*/

void Menu()
{
    while(menuPrincipal){
        
        AnimeText(); // Fonction pour animer le text
        if (compteurSelector > 10){ // On attend 10 boucles avant de rev�rifier
            if(GetKeyState('Z') & 0x8000)  // On v�rifie si la touche Z est press�
            {
                menuUp();
                Attendre(25);
            } else if (GetKeyState('S') & 0x8000) // On v�rifie si la touche S est press�
            {
                menuDown();
                Attendre(25);
            } else if (GetKeyState(VK_SPACE) & 0x8000) // On v�rifie si la touche Espace est press� >> Valide le choix du menu
            {
                if(posLevel == 0){ // Si le selecteur �tait en position 0 > Jeu solo
                    cleardevice(); 
                    StartParty();
                    break;
                }else if(posLevel == 1){ // Si le selecteur �tait en position 1 > Jeu a 2
                    gameover();
                    break;
                    cout << "Fonction pas encore disponible" << endl;
                }else if(posLevel == 2){ // Si le selecteur �tait en position 2 > Quitter
                    break;
                }
            }
            compteurSelector = 0;
        }
        compteurSelector++;
        
    }
}

/*----------------------------------*/
/* Fin affichage du menu principale */
/*----------------------------------*/
