void GameOverText(int positionX, int positionY, int R, int V, int B){
    letter(20,'G',positionX,positionY,R,V,B);
    letter(20,'A',positionX+5,positionY,R,V,B);
    letter(20,'M',positionX+10,positionY,R,V,B);
    letter(20,'E',positionX+16,positionY,R,V,B);
    letter(20,'O',positionX,positionY+6,R,V,B);
    letter(20,'V',positionX+5,positionY+6,R,V,B);
    letter(20,'E',positionX+11,positionY+6,R,V,B);
    letter(20,'R',positionX+16,positionY+6,R,V,B);
}

void MenuStartText(int positionX, int positionY, int R, int V, int B)
    {
    letter(20,'S',positionX+5,positionY,R,V,B);
    letter(20,'P',positionX+10,positionY,R,V,B);
    letter(20,'A',positionX+15,positionY,R,V,B);
    letter(20,'C',positionX+20,positionY,R,V,B);
    letter(20,'E',positionX+25,positionY,R,V,B); 
    letter(20,'I',positionX+9,positionY+6,R,V,B);
    letter(20,'S',positionX+14,positionY+6,R,V,B);
    letter(20,'N',positionX+20,positionY+6,R,V,B);
   
    
}

void ScoreRandom(int taille, int positionX, int positionY, int R, int V, int B)
{
     
     number(taille, 1, positionX, positionY,R,V,B);
     number(taille, 2, positionX+5, positionY,R,V,B);
     number(taille, 3, positionX+10, positionY,R,V,B);
     number(taille, 4, positionX+15, positionY,R,V,B);
     number(taille, 5, positionX+20, positionY,R,V,B);
     number(taille, 6, positionX+25, positionY,R,V,B);
     number(taille, 7, positionX+30, positionY,R,V,B);
}

void MenuJouerText(int taille, int positionX, int positionY, int R, int V, int B)
    {
    letter(taille,'J',positionX+9,positionY,R,V,B);
    letter(taille,'O',positionX+13,positionY,R,V,B);
    letter(taille,'U',positionX+18,positionY,R,V,B);
    letter(taille,'E',positionX+23,positionY,R,V,B);
    letter(taille,'R',positionX+28,positionY,R,V,B);
    
}

void MenuJouerDeuxText(int taille, int positionX, int positionY, int R, int V, int B)
    {
    letter(taille,'A',positionX+7,positionY,R,V,B);
    letter(taille,'D',positionX+14,positionY,R,V,B);
    letter(taille,'E',positionX+19,positionY,R,V,B);
    letter(taille,'U',positionX+24,positionY,R,V,B);
    letter(taille,'X',positionX+29,positionY,R,V,B);
    
}

void MenuQuitterText(int taille, int positionX, int positionY, int R, int V, int B)
    {
    letter(taille,'Q',positionX+5,positionY,R,V,B);
    letter(taille,'U',positionX+10,positionY,R,V,B);
    letter(taille,'I',positionX+15,positionY,R,V,B);
    letter(taille,'T',positionX+19,positionY,R,V,B);
    letter(taille,'T',positionX+23,positionY,R,V,B);
    letter(taille,'E',positionX+27,positionY,R,V,B);
    letter(taille,'R',positionX+32,positionY,R,V,B);
    
}

void ScoreText(int taille, int positionX, int positionY, int R, int V, int B)
    {
    letter(taille,'S',positionX+5,positionY,R,V,B);
    letter(taille,'C',positionX+10,positionY,R,V,B);
    letter(taille,'O',positionX+15,positionY,R,V,B);
    letter(taille,'R',positionX+20,positionY,R,V,B);
    letter(taille,'E',positionX+25,positionY,R,V,B);
    
}
