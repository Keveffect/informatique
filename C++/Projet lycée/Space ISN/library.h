#include <iostream>   // librairie g�rant les flux d'entree et de sortie
#include <fstream>   // librairie g�rant les fichiers
#include <math.h>
#include <graphics.h>
using namespace std;  // espace de noms contenant les flux  cin/cout

// Variable globale
int F=1 ;// Cette variable doit �tre accessible partout
float Pi=3.14159265;

//************************************
//      Zones des Fonctions          *
//************************************
void InitialiseFenetreGraphique(int f)
// F est le format, divise l'�cran en ligne
//  colonnes et points de largeur-longueur F
// F ne peut prendre que les valeurs 1, 2, 4, 8, 16, 32, 64.
{
     initwindow(800,600,"Space Invader",0,0);
     setbkcolor(BLACK);
     cleardevice();
     setcolor(BLACK);
     F=f;    
}//initialisation mode graphique et l'�cran     

void FermeFenetreGraphique()
{
   closegraph();
   cout<< "A bientot..."<<endl;       
}//Fin fonction FermeFenetreGraphique   


void MetPoint(int taille, int x, int y, int R = 255, int V = 255, int B = 255)
{
     int X,Y;
     int i,j;
     X=x*taille; Y=y*taille;// initialisation des variables �cran r�elles   
     setcolor(COLOR(R,V,B));
     setfillstyle(SOLID_FILL,COLOR(R,V,B));
     bar(X, Y, X+taille, Y+taille);


}//Fin fonction MetPoint



void Grille()
// Dessine � l'�cran le quadrillage du rep�re li� � F
{
     
     int i,j;
     int MaxX,MaxY;
     MaxX =1280/F;
     MaxY=768/F;
     setcolor(COLOR(200,200,200));
     for(i=0;i<=MaxX;i++)
            line(i*F,0,i*F,768);
     for(i=0;i<=MaxX;i++)
            line(0,i*F,1280,i*F);                   
}//Fin proc�dure grille
 




