#!/usr/bin/python3

import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        client.subscribe("certificateRequest")
    else:
        print("Connection failed")

# The callback for when a PUBLISH message is received from the server
def on_message(client, userdata, msg):
    print(str(msg.payload.decode("utf-8")))
    client.publish("Message", "Bonjour je suis le message autentifie")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("100.100.100.50",1883,60)

# Blocking call that processes network traffic, dispatches callbacks and handles re
client.loop_forever()