#!/usr/bin/python3

import paho.mqtt.client as mqtt
from OpenSSL import crypto
import subprocess as sp

# The callback for when the client receives a CONNACK response from th>
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        client.subscribe("certificateRequest")
    else:
        print("Connection failed")

# The callback for when a PUBLISH message is received from the server
def on_message(client, userdata, msg):
    print(str(msg.payload.decode("utf-8")))
    with open('server.csr','wb') as fichiercsr :
      fichiercsr.write(msg.payload)
      fichiercsr.close()
    # input expiration days
    validDate = input("Nombre de jour avant expiration : ")

    sp.call(['openssl', 'x509', '-req', '-in', 'server.csr', '-CA', 'ca.crt', '-CAkey', 'ca.key',
         '-CAcreateserial', '-out', 'server.crt', '-days', validDate, '-sha256'])


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("100.100.100.50",1883,60)

client.loop_forever()