#!/usr/bin/python3

import paho.mqtt.client as mqtt
import subprocess
import datetime
from cryptography.hazmat.backends import default_backend
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa

# The callback for when the client receives a CONNACK response from the server
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        client.subscribe("certificateRequest")
    else:
        print("Connection failed")

def write_in_file(name,msg):
    servercrt = open(name,'w')
    servercrt.write(str(msg.payload.decode("utf-8")))
    servercrt.close()

# The callback for when a PUBLISH message is received from the server
def on_message(client, userdata, msg):
    print(str(msg.payload.decode("utf-8")))

    write_in_file("server.rcrt",msg)

    serverrcrt = "server.rcrt"
    cacrt ="ca.crt"
    cakey = "ca.key"
    servercrt = "server.crt"

    retour = subprocess.run("openssl x509 -req -in " + serverrcrt + " -CA "+ cacrt +
           " -CAkey " + cakey +" -CAcreateserial -out " + servercrt + " -days 365",
           shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

    if retour.returncode == 0:
        with open("server.crt","r") as servercrtfile:
           servercrt = servercrtfile.read()
           servercrtfile.close()
           print(servercrt)
    else:
        print("Une erreur a eu lieu. Certificat non cree")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("100.100.100.50",1883,60)

# Blocking call that processes network traffic, dispatches callbacks and handles re
client.loop_forever()    