N = 100
M = 100

I = rep(0,times=M)
e = rep(0,times=M)

Lambda = 1

for(j in 0:M){ 
  N = N + 50*j
  term = rep(0, times=N)
  A = rexp(N,Lambda)
  
  for(i in 1:N){
    term[i] = (exp(-((A[i])^2)/2))/(dexp(A[i],Lambda))
  }
  
  I[j] = mean(term)
  e[j] = abs(I[j] - sqrt(pi/2))
}

mI = mean(I)
me = mean(e)

b=sqrt(pi/2)
x = 1:length(I)

y = rep(b,times=length(I))
plot(x,I,type="l",col="red",main = "Convergence num?rique vers des valeurs th?orique")
lines(x,y,type="l",col="blue")

print(I)
print(e)

print(mI)
print(me)