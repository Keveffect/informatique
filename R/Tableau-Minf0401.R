matable=matrix(c(9,12,1,3,0,3,16,12,10,0,0,1,30,13,1,0,0,3,24,8,0,0,0,1,3),byrow=TRUE,ncol=5)
colnames(matable)=c("[5,8[","[8,10[","[10,12[","[12,15[","[15,19[")
rownames(matable)=c("[5,8[","[8,10[","[10,12[","[12,15[","[15,19[")
print(matable)

frtable=prop.table(matable)
print(frtable)
sum(frtable)

MargeX=rep(0,times=5)
for(i in 1:5){MargeX[i]=sum(matable[i,])}
print(MargeX)

MargeY=rep(0,times=5)
for(i in 1:5){MargeY[i]=sum(matable[,i])}
print(MargeY)

frMargeX=prop.table(MargeX)
print(MargeX)
frMargeY=prop.table(MargeY)
print(MargeY)

X=c(6.5,9,11,13.5,17)
Y=c(6.5,9,11,13.5,17)
#les moyennes
Xbar=t(MargeX)%*%X/sum(MargeX)
print(Xbar)
Ybar=t(MargeY)%*%Y/sum(MargeY)
print(Ybar)


VarX=(t(MargeX)%*%(X*X))/sum(MargeX)-Xbar^2
print(VarX)
VarY=(t(MargeY)%*%(X*X))/sum(MargeX)-Ybar^2
print(VarY)

# moyenne conditionnelle de X sachant Y
Xcond=rep(0,times=5)
for(i in 1:5){
	Xcond[i]=t(X)%*%matable[,i]/MargeY[i]
	print(Xcond[i])
}

Ycond=rep(0,times=5)
for(i in 1:5){
	Ycond[i]=t(Y)%*%matable[i,]/MargeX[i]
	print(Ycond[i])
}

plot(X,Ycond, type="l",main="Courbes de régression",col="red",lwd=2.5)
lines(Xcond,Y,type="l",col="blue",lwd=2.5)
legend(8,15,legend=c("X en Y", "Y en X"),col=c("red", "blue"), lty=1:2, cex=0.8)